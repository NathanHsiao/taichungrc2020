//
//  Thumbnail.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/15/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import PDFKit
import AVFoundation
struct Thumbnail {
    func pdfThumbnail(url: URL, width: CGFloat = 240) -> UIImage? {
        if #available(iOS 11.0, *) {
            guard let data = try? Data(contentsOf: url),
                  let page = PDFDocument(data: data)?.page(at: 0) else {
                return nil
            }
            let pageSize = page.bounds(for: .mediaBox)
            let pdfScale = width / pageSize.width

            // Apply if you're displaying the thumbnail on screen
            let scale = UIScreen.main.scale * pdfScale
            let screenSize = CGSize(width: pageSize.width * scale,
                                    height: pageSize.height * scale)

            return page.thumbnail(of: screenSize, for: .mediaBox)
        } else {
            // Fallback on earlier versions
        }

        return nil
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }

        return nil
    }
}
