//
//  Extension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/31/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import Foundation
import UIKit
struct Logout: Codable{
    var data:Bool
    var message: String
}


extension UIViewController{
    func registerLanguageChange(){
        NotificationCenter.default.addObserver(self, selector: #selector(changeLanguage), name: Notification.Name("changeLanguage"), object: nil)
    }
    func postLangeChange(){
        NotificationCenter.default.post(name: Notification.Name("changeLanguage"), object: nil)
    }
    @objc func changeLanguage(){
        //
    }
}

extension Array where Element: Equatable {

    public func uniq() -> [Element] {
        var arrayCopy = self
        arrayCopy.uniqInPlace()
        return arrayCopy
    }

    mutating public func uniqInPlace() {
        var seen = [Element]()
        var index = 0
        for element in self {
            if seen.contains(element) {
                remove(at: index)

            } else {
                seen.append(element)
                index += 1
            }
        }
    }
}

struct Account : Codable {
    
    var user: User
    var access_token:String
}

struct User : Codable {
    var id:String
    var attributes : UserAttributes
}

struct UserAttributes:Codable {
    var username:String
    var name:String
}

struct UserData : Codable {
    var data : UserD?
}

struct UserD : Codable {
    var attributes : UserDAttribute?
}
struct UserAccount: Codable {
    var code : String?
    var name: String?
    var bussiness_number : String?
    var tel: String?
    var contact : String?
    var address : String?
}
struct UserDAttribute : Codable{
    var name : String?
    var username : String?
    var mainrole_name : String?
    var currencies : [String]
    var currency_groups : [String]
    var account : UserAccount?
    var discount_percent : Int?
}
extension String{
    
    var localized: String{
        
        let lang = UserDefaults.standard.getLanguage()
       
        let bundle = Bundle(path: Bundle.main.path(forResource: lang, ofType: "lproj")!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        
        
    }
}


enum StockMode {
    case Choices
    
    case Fields
    
    case Stocks
}
class StockApi {
    static let shared = StockApi()
    var cjson : [ApplicationData]?
    var stocks : [Stock]?
    var fields : [S_Fields]?
    func tryCall<T:Codable>( group: DispatchGroup,data:Data?,decodable:T.Type,mode: StockMode) {
        var token = ""
        var update_parameter = ""
        var parameter = ""
        var dataAny : Any?
        if mode == .Choices {
            update_parameter = "api-or/v1/stock_categories/check"
            parameter = "api-or/v1/stock_categories"
        }
        else if mode == .Fields {
            update_parameter = "api-or/v1/stock_fields/check"
            parameter = "api-or/v1/stock_fields"
        }
        else{
            update_parameter = "api-or/v1/stocks/check"
            parameter = "api-or/v1/stocks"
        }
        if let choice = data{
            
            if let choiceJson = try? JSONDecoder().decode(decodable, from: choice) {

                if let at = choiceJson as? Application {
                    token = at.meta.update_token
                    
                    dataAny = at.data
                    
                }
                if let at = choiceJson as? StockFields {
                    token = at.meta.update_token
                    dataAny = at.data
                }
                
                if let at = choiceJson as? Stocks {
                    token = at.meta.update_token
                    dataAny = at.data
                }
                
                NetworkCall.shared.postCall(parameter: update_parameter, dict: ["check_token":token], decoderType: UpdateClass.self) { (json) in
                    if let json = json{
                        if json.data {
                            if let tt = dataAny as? [ApplicationData] {
                                self.cjson = tt
                            }
                            if let tt = dataAny as? [Stock] {
                                self.stocks = tt
                            }
                            if let tt = dataAny as? [S_Fields] {
                                self.fields = tt
                            }
                            
                            group.leave()
                        }
                        else{
                            NetworkCall.shared.getCall(parameter: parameter, decoderType: decodable) { (json) in
                                

                                if let json = json {
                                    if let dd = json as? Application {
                                        self.cjson = dd.data
                                        if let encode = try? JSONEncoder().encode(json) {
                                            
                                            UserDefaults.standard.saveStockChoice(data: encode)
                                            
                                        }

                                    }
  
                                    
                                    if let dd = json as? Stocks {
                                        self.stocks = dd.data
                                        if let encode = try? JSONEncoder().encode(json) {
                                            
                                            UserDefaults.standard.saveStocks(data: encode)
                                            
                                        }

                                    }
 
                                    
                                    if let dd = json as? StockFields {
                                        self.fields = dd.data
                                        if let encode = try? JSONEncoder().encode(json) {
                                            
                                            UserDefaults.standard.saveStockFields(data: encode)
                                            
                                        }

                                    }
                                    group.leave()


                                }

                                    
                                
                                

                                
                            }
                        }
                    }
                    else{
                        group.leave()
                    }
                }
            }
            else{
                group.leave()
            }

   
        }
        else{
            NetworkCall.shared.getCall(parameter: parameter, decoderType: decodable) { (json) in
                

                if let json = json{
                    if let dd = json as? Application {
                        self.cjson = dd.data
                        if let encode = try? JSONEncoder().encode(json) {
                            
                            UserDefaults.standard.saveStockChoice(data: encode)
                            
                        }

                    }
                    
                    if let dd = json as? Stocks {
                        self.stocks = dd.data
                        if let encode = try? JSONEncoder().encode(json) {
                            
                            UserDefaults.standard.saveStocks(data: encode)
                            
                        }

                    }

                    
                    if let dd = json as? StockFields {
                        self.fields = dd.data
                        if let encode = try? JSONEncoder().encode(json) {
                            
                            UserDefaults.standard.saveStockFields(data: encode)
                            
                        }

                    }
                    group.leave()

                }
                else{
                    group.leave()
                }
                    
                
            }
        }
    }
}
