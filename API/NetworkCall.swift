//
//  NetworkCall.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/19/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import FirebaseMessaging
class NetworkCall {

    
    static let shared = NetworkCall()
    func getError(data:Data) {
        if let error = try? JSONDecoder().decode(ErrorApi.self, from: data) {
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "提示 : 帳號已被登出", message: nil, preferredStyle: .alert)
                let action = UIAlertAction(title: "確定", style: .default, handler: {
                    _ in
                    Messaging.messaging().deleteToken { (_) in
                        //
                        NetworkCall.shared.postCall(parameter: "api-victor/v1/auth/logout", dict: [:], decoderType: Logout.self) { (logout) in
                            if let log = logout,log.data {
                                    UserDefaults.standard.saveToken(token: "")
                                    
                                
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                                    let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                                    keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
                                }
                            }
                        }
                    }
                    
                })
                alertController.addAction(action)
                General().getTopVc()?.present(alertController, animated: true, completion: nil)
            }

        }
    }
    func getCall<T : Decodable>(parameter:String?,decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if !Reachability.isConnectedToNetwork() {
            completion(nil)
            return
        }
        if let parameter = parameter {
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(992,url.absoluteString)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let data = data{
                    print(data)
                    do{
                        let json = try JSONDecoder().decode(decoderType, from: data)
                        completion(json)
                    }
                    catch let error{
                        print(error)
                        self.getError(data: data)
                        
                        
                        completion(nil)
                    }
                }
            }.resume()
        }
        else{
            completion(nil)
        }
    }
    
    func postCall<T : Decodable>(parameter:String?,dict:[String:String],decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(321,url,dict)
            do {
                let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)

                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = data
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if let data = data{
                        
                        do{
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func postCall<T : Decodable>(parameter:String?,param:[String:Any],decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(667,url.absoluteString)
            do {
                let data = try JSONSerialization.data(withJSONObject: param, options: .prettyPrinted)
                if let JSONString = String(data: data, encoding: String.Encoding.utf8) {
                   print(JSONString)
                }
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = data
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    if let data = data{
                        
                        do{
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    func getPostString(params:[String:String]) -> String
        {
            var data = [String]()
            for(key, value) in params
            {
                data.append(key + "=\(value)")
            }
            return data.map { String($0) }.joined(separator: "&")
        }
     func postCallform<T : Codable>(parameter:String?,dict:[String:String],decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
         if let parameter = parameter {
             if !Reachability.isConnectedToNetwork() {
                 completion(nil)
                 return
             }
             guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                 return}
             print(111,url)
             do {
                 //let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)

                 var request = URLRequest(url: url)
                 request.httpMethod = "POST"
                 request.httpBody = getPostString(params: dict).data(using: .utf8)
                 //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                 request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                 URLSession.shared.dataTask(with: request) { (data, response, error) in
                     if let data = data{
                         
                         do{

                             let json = try JSONDecoder().decode(decoderType, from: data)
                             completion(json)
                         }
                         catch let error{
                             print(error)
                            self.getError(data: data)
                             completion(nil)
                         }
                     }
                 }.resume()

                 
             }
             catch {
                 completion(nil)
             }


         }
         else{
             completion(nil)
         }
     }
    
    
    func postCallZip<T : Decodable>(parameter:String?,data:Data,decoderType:T.Type,completion : @escaping ((_ json:T?) -> Void)){
        if let parameter = parameter {
            if !Reachability.isConnectedToNetwork() {
                completion(nil)
                return
            }
            guard let url = URL(string: "\(AppURL().baseURL)\(parameter)") else { completion(nil)
                return}
            print(3312,url.absoluteString)
            do {
                
                
             let boundary = "Boundary-\(UUID().uuidString)"
//                var body = ""
//                body += "--\(boundary)\r\n"
//                body += "Content-Disposition:form-data; name=\"zip_file\""
//                body += "; filename=\"upload.zip\"\r\n"
//                        + "Content-Type: \"content-type header\"\r\n\r\n\(String(data:data,encoding: .utf8) ?? "")\r\n"
//
//                body += "--\(boundary)--\r\n";
                let httpBody = NSMutableData()
                
                httpBody.append(convertFileData(fieldName: "zip_file", fileName: "upload.zip", mimeType: "file", fileData: data, using: boundary))
                httpBody.appendString("--\(boundary)--")
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.httpBody = httpBody as Data
                
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                request.setValue("Bearer \(UserDefaults.standard.getToken())", forHTTPHeaderField: "Authorization")
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    if let data = data{
                       
                        do{
                            print(77,try! JSONSerialization.jsonObject(with: data, options: []))
                            let json = try JSONDecoder().decode(decoderType, from: data)
                            completion(json)
                        }
                        catch let error{
                            print(331,error)
                            self.getError(data: data)
                            completion(nil)
                        }
                    }
                }.resume()

                
            }
            catch {
                completion(nil)
            }


        }
        else{
            completion(nil)
        }
    }
    
    func convertFileData(fieldName: String, fileName: String, mimeType: String, fileData: Data, using boundary: String) -> Data {
      let data = NSMutableData()

      data.appendString("--\(boundary)\r\n")
      data.appendString("Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(fileName)\"\r\n")
        data.appendString("Content-Type: \(mimeType)\r\n\r\n")
        data.append(fileData)
        data.appendString("\r\n")


      return data as Data
    }
}
extension NSMutableData {
  func appendString(_ string: String) {
    if let data = string.data(using: .utf8) {
      self.append(data)
    }
  }
}
