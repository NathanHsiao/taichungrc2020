//
//  ErrorApi.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/25/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct ErrorApi : Codable {
    let errors : [Token_Error]
}

struct Token_Error : Codable{
    let status : String
}
