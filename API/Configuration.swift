//
//  Configuration.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/19/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import SwiftMessages
extension UserDefaults {
    func saveHistory(data:Data) {
        self.setValue(data, forKey: "History")
    }
    func loadHistory() -> Data?{
        return self.data(forKey: "History")
    }
    func saveChosenCurrency(text:String) {
        self.setValue(text, forKey: "Currency")
    }
    func getChosenCurrency() -> String {
        self.string(forKey: "Currency") ?? ""
    }
    func saveUpdateToken(token:String,id:String) {
        self.setValue(token, forKey: "\(id)-token")
    }
    func getUpdateToken(id:String) -> String {
        return self.string(forKey: "\(id)-token") ?? ""
    }
    func saveToken(token:String) {
        self.setValue(token, forKey: "Token")
    }
    func saveUserId(id:String) {
        self.setValue(id, forKey: "UserId")
    }
    func getUserId() -> String{
        self.string(forKey: "UserId") ?? ""
    }
    func getToken() -> String {
        return self.string(forKey: "Token") ?? ""
    }
    func saveCategoryData(data:Data) {
        self.setValue(data, forKey: "Category")
    }
    func getCategoryData() -> Data? {
        return self.data(forKey: "Category")
    }
    func saveCatValueData(data:Data,id:String) {
        self.setValue(data, forKey: "\(id)-data")
    }
    func setCustomersData(data:Data) {
        return self.setValue(data, forKey: "Customers")
    }
    func getCustomersData() -> Data?{
        return self.data(forKey: "Customers")
    }
    func saveUserData(data:Data ,id:String) {
        self.setValue(data, forKey: "\(id)-user")
    }
    func getUserData(id:String) -> Data?{
        return self.data(forKey: "\(id)-user")
    }
    func getCatValueData(id:String) -> Data? {
        return self.data(forKey: "\(id)-data")
    }
    
    func saveAppLanguage(lang:String) {
        self.setValue(lang, forKey: "AppLanguage")
    }
    func getLanguage() -> String{
        return self.string(forKey: "AppLanguage") ?? ""
    }
    func getStockChoice() -> Data?{
        return self.data(forKey: "StockChoice")
    }
    func saveStockChoice(data:Data){
        self.setValue(data, forKey: "StockChoice")
    }
    
    func getStocks() -> Data?{
        return self.data(forKey: "Stocks")
    }
    func saveStocks(data:Data){
        self.setValue(data, forKey: "Stocks")
    }
    
    func getStockFields() -> Data?{
        return self.data(forKey: "StockFields")
    }
    func saveStockFields(data:Data){
        self.setValue(data, forKey: "StockFields")
    }
}
struct UpdateClass : Codable {
    var data: Bool
}
struct MajorColor {
    var mainColor = #colorLiteral(red: 0.9294170737, green: 0.4222778082, blue: 0.01915801316, alpha: 1)
    var subColor = #colorLiteral(red: 0.9427522421, green: 0.5120122433, blue: 0.02646821924, alpha: 1)
}
struct AppURL {
    var baseURL:String = {
       //http://61.66.123.79:8080
        //http://192.168.1.51:8000
        //http://victor.public.192.168.1.51.xip.io/
        //http://0c1e27690ae7.jp.ngrok.io/
        return Bundle.main.infoDictionary?["API_URL"] as? String ?? ""
      // return "http://33df9835f583.jp.ngrok.io/"
    }()
    
    lazy var imageURL:String = {
        return "\(baseURL)uploads/"
    }()
}

struct ErrorChecking {
    static func ErrorView(message: String?) {
        let success = MessageView.viewFromNib(layout: .messageView)
        success.configureTheme(.warning)
        success.configureDropShadow()

        success.configureContent(title: "如何比較", body:message ?? "")

        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top


        successConfig.duration = .seconds(seconds: 2.5)
        
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        successConfig.preferredStatusBarStyle = UIStatusBarStyle.lightContent
        
        SwiftMessages.show(config: successConfig, view: success)

    }
}
