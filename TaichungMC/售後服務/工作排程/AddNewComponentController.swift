//
//  AddNewComponentController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/16.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class AddNewComponentController: UIViewController {
    let container = UIView()
    var containeranchor:AnchoredConstraints!
    let header = UILabel()
    let codelabel = NormalFormView()
    let componentlabel = NormalFormView()
    let componentprice = CalcView()
    let multiply = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    let quantitylabel = CalcView()
    let equal = UIImageView(image: #imageLiteral(resourceName: "ic_equal"))
    let totallabel = CalcView()
    let userlabel = NormalFormView()
    let reasonlabel = NormalFormView()
    let paymentstatus = NumberOfSelectionView(title: "收費狀況", texts: ["收費","不收費"], widths: [64,82], number: 2)
    let fixstatus = NumberOfSelectionView(title: "維修狀況", texts: ["維修","退回維修"], widths: [64,100], number: 2)
    let addbutton = UIButton(type: .system)
    let dismissbutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    override func viewDidLoad() {
        super.viewDidLoad()
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            container.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        view.addSubview(container)
        containeranchor = container.anchor(top: view.bottomAnchor, leading: nil, bottom: nil, trailing: nil,size: .init(width: self.view.frame.width, height: 647.calcvaluey()))
        container.centerXInSuperview()
        
        header.text = "新增換件項目"
        header.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        container.addSubview(header)
        header.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0))
        header.centerXInSuperview()
        
        container.addSubview(codelabel)
        codelabel.tlabel.text = "故障代碼"
        codelabel.anchor(top: header.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 46.calcvaluey(), left: 165.calcvaluex(), bottom: 0, right: 0),size: .init(width: 341.calcvaluex(), height: 80.calcvaluey()))
        
        container.addSubview(componentlabel)
        componentlabel.tlabel.text = "零件編號"
        componentlabel.anchor(top: codelabel.topAnchor, leading: codelabel.trailingAnchor, bottom: nil, trailing: nil,padding:.init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 341.calcvaluex(), height: 80.calcvaluey()))
        
        
            view.addSubview(componentprice)
            componentprice.labelview.text = "零件售價"
            componentprice.anchor(top: codelabel.bottomAnchor, leading: codelabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 166.calcvaluex(), height: 108.calcvaluey()))
        

        container.addSubview(multiply)
//        multiply.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
//        multiply.text = "x"
//        multiply.textAlignment = .center
//        multiply.font = UIFont(name: "Roboto-Regular", size: 24.calcvaluex())
        multiply.anchor(top: nil, leading: componentprice.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        multiply.centerYAnchor.constraint(equalTo: componentprice.centerYAnchor).isActive = true
        
        container.addSubview(quantitylabel)
        quantitylabel.labelview.text = "數量"
        quantitylabel.anchor(top: nil, leading: multiply.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 0),size: .init(width: 166.calcvaluex(), height: 108.calcvaluey()))
        quantitylabel.centerYAnchor.constraint(equalTo: multiply.centerYAnchor).isActive = true
        container.addSubview(equal)
//        equal.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
//        equal.text = "="
//        equal.textAlignment = .center
//        equal.font = UIFont(name: "Roboto-Regular", size: 24.calcvaluex())
        equal.anchor(top: nil, leading: quantitylabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        equal.centerYAnchor.constraint(equalTo: quantitylabel.centerYAnchor).isActive = true

        container.addSubview(totallabel)
        totallabel.labelview.text = "總計"
        totallabel.anchor(top: nil, leading: equal.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 0),size: .init(width: 166.calcvaluex(), height: 108.calcvaluey()))
        totallabel.centerYAnchor.constraint(equalTo: equal.centerYAnchor).isActive = true
        
        container.addSubview(userlabel)
        userlabel.tlabel.text = "持有零件者"
        userlabel.anchor(top: componentprice.bottomAnchor, leading: componentprice.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 341.calcvaluex(), height: 80.calcvaluey()))
        
        container.addSubview(reasonlabel)
        reasonlabel.tlabel.text = "更換原因"
        reasonlabel.anchor(top: nil, leading: userlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 341.calcvaluex(), height: 80.calcvaluey()))
        reasonlabel.centerYAnchor.constraint(equalTo: userlabel.centerYAnchor).isActive = true
        
        container.addSubview(paymentstatus)
        paymentstatus.anchor(top: userlabel.bottomAnchor, leading: userlabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 186.calcvaluex(), height: 53.calcvaluey()))
        
        container.addSubview(fixstatus)
        fixstatus.anchor(top: reasonlabel.bottomAnchor, leading: reasonlabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 204.calcvaluex(), height: 53.calcvaluey()))
        
        
            self.addbutton.setTitle("新增", for: .normal)
            self.addbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
            self.addbutton.setTitleColor(.white, for: .normal)
            self.addbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
            container.addSubview(self.addbutton)
            self.addbutton.anchor(top: nil, leading: nil, bottom: self.container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
            self.addbutton.centerXInSuperview()
            self.addbutton.layer.cornerRadius = 48.calcvaluey()/2
        
        self.addbutton.addTarget(self, action: #selector(addnewlist), for: .touchUpInside)
        
        container.addSubview(dismissbutton)
        dismissbutton.isUserInteractionEnabled = true
        dismissbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addnewlist)))
        dismissbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 25.28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        
    }
    @objc func addnewlist(){
        self.view.backgroundColor = .clear
        self.containeranchor.top?.isActive = false
        self.container.topAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        UIView.animate(withDuration: 0.4, animations: {
            
            self.view.layoutIfNeeded()
        }, completion: {
            _ in
            self.dismiss(animated: false, completion: nil)
        })
        
//        self.view.backgroundColor = .clear
//        UIView.animate(withDuration: 0.7, animations: {
//            self.view.layoutIfNeeded()
//        }) { (_) in
//            self.dismiss(animated: true, completion: nil)
//        }
            }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        CATransaction.begin()
//        CATransaction.setAnimationDuration(2)
//
//        self.view.layoutIfNeeded()
//        CATransaction.commit()
        self.view.layoutIfNeeded()
        containeranchor.top?.constant = -647.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            
            self.view.layoutIfNeeded()
        }
    }
}
