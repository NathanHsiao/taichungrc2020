//
//  ScheduleWorkController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class SelectGroupTextField:UITextField{
    var rightimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        addSubview(rightimage)
        rightimage.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        rightimage.centerYInSuperview()
        
        let picker = UIPickerView()
        inputView = picker
        
        self.tintColor = .clear
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 34.calcvaluex(), dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 34.calcvaluex(), dy: 0)
    }
}
class ScheduleSelectTextField:UITextField {
     var rightimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    var padding : CGFloat = 0
    init(padding:CGFloat) {
        super.init(frame: .zero)
        self.padding = padding
        self.backgroundColor = .white
        self.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        addSubview(rightimage)
        rightimage.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        rightimage.centerYInSuperview()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: self.padding, bottom: 0, right: 0))
    }
}
class ScheduleWorkController: SampleController {
    let container = UIView()
    var containeranchor:AnchoredConstraints!
    let selectgroup = SelectGroupTextField()
    let selectzone = SelectGroupTextField()
    let statuscollectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let tableview = UITableView(frame: .zero, style: .grouped)
    let addNewScheduleButton = AddButton()
    var rightview = UIView()
    var rightviewanchor:AnchoredConstraints!
    weak var maincontroller:ViewController!
    var shrink = false
    let rightviewtoplabel = UILabel()
    let bottomtableview = UITableView(frame: .zero, style: .plain)
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "工作排程"
        titleviewanchor.leading?.constant = 333.calcvaluex()
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        view.addSubview(container)
        container.backgroundColor = .clear
        containeranchor = container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 333.calcvaluex(), bottom: 0, right: 0),size: .init(width: 665.calcvaluex(), height: 0))
        
        selectgroup.text = "全部組別"
        selectgroup.addshadowColor(color: #colorLiteral(red: 0.8776529431, green: 0.8799210191, blue: 0.885224998, alpha: 1))
        container.addSubview(selectgroup)
        selectgroup.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 1, bottom: 0, right: 1),size: .init(width: 0, height: 46.calcvaluey()))
        
        selectgroup.layer.cornerRadius = 46.calcvaluey()/2
        
        selectzone.text = "全部已派地區"
        selectzone.addshadowColor(color: #colorLiteral(red: 0.8776529431, green: 0.8799210191, blue: 0.885224998, alpha: 1))
        container.addSubview(selectzone)
        selectzone.anchor(top: selectgroup.bottomAnchor, leading: selectgroup.leadingAnchor, bottom: nil, trailing: selectgroup.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 46.calcvaluey()))
        selectzone.layer.cornerRadius = 46.calcvaluey()/2
        
        (statuscollectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        
        container.addSubview(statuscollectionview)
        statuscollectionview.backgroundColor = .clear
        statuscollectionview.delegate = self
        statuscollectionview.dataSource = self
        statuscollectionview.anchor(top: selectzone.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,size: .init(width: 0, height: 50.calcvaluey()))
        statuscollectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        container.addSubview(tableview)
        tableview.separatorStyle = .none
        tableview.anchor(top: statuscollectionview.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor)
        tableview.backgroundColor = .clear
        
        tableview.delegate = self
        tableview.dataSource = self
        
        
        

        
        
        view.addSubview(rightview)
        rightview.backgroundColor = .clear
        rightviewanchor = rightview.anchor(top: topview.bottomAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 595.calcvaluex(), height: 0))
        rightview.isHidden = true
        
        
        rightview.addSubview(rightviewtoplabel)
        
        rightviewtoplabel.text = "派工列表"
        rightviewtoplabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        rightviewtoplabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        rightviewtoplabel.anchor(top: topview.bottomAnchor, leading: rightview.leadingAnchor, bottom: nil, trailing: rightview.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        rightview.addSubview(bottomtableview)
        bottomtableview.separatorStyle = .none
        bottomtableview.backgroundColor = .clear
        bottomtableview.anchor(top: rightviewtoplabel.bottomAnchor, leading: rightview.leadingAnchor, bottom: rightview.bottomAnchor, trailing: rightview.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 0))
        bottomtableview.delegate = self
        bottomtableview.dataSource = self
        
        
        view.addSubview(addNewScheduleButton)
        addNewScheduleButton.titles.text = "建立派工"
        addNewScheduleButton.anchor(top: nil, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 178.calcvaluex(), height: 48.calcvaluey()))
        addNewScheduleButton.layer.cornerRadius = 48.calcvaluey()/2
        
        addNewScheduleButton.addTarget(self, action: #selector(showNewSchedule), for: .touchUpInside)
    }
    @objc func showNewSchedule(){
        let vd = NewScheduleController()
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    override func popview() {
        

       
        maincontroller.showcircle()
        titleviewanchor.leading?.constant = 333.calcvaluex()

        
        containeranchor.leading?.constant = 333.calcvaluex()
        containeranchor.width?.constant = 665.calcvaluex()
        statuscollectionview.collectionViewLayout.invalidateLayout()
        rightviewanchor.leading?.constant = 0
        rightview.isHidden = true
        shrink = false
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ScheduleWorkController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        cell.backgroundColor = .clear
        let button = UIButton(type: .system)
        cell.addSubview(button)

        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        button.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: nil, trailing: cell.trailingAnchor,padding: .init(top: 12.calcvaluey(), left:0, bottom: 0, right: 0),size: .init(width: 0, height: 38.calcvaluey()))
        button.layer.cornerRadius = 38.calcvaluey()/2
        
        if indexPath.item == 0{
            button.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
            button.setTitle("全部", for: .normal)
        }
        else{
            button.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            button.setTitle("出勤", for: .normal)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: shrink == true ? 102.calcvaluex():166.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if shrink {
            return 26.calcvaluex()
        }
        return 66.calcvaluex()
    }
}

extension ScheduleWorkController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tableview {
        let vd = UIView()
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        label.text = "交機組"
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(label)
            label.anchor(top: nil, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 2, bottom: 12.calcvaluey(), right: 0))
        
        return vd
        }
        return UIView()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableview {
        return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableview {
        return 92.calcvaluey()
        }
        return 166.calcvaluey()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableview {
        let cell = ScheduleWorkControllerCell(style: .default, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        if indexPath.item == 0{
            cell.rightimage.isHidden = false
            
        }
        else if indexPath.item == 3{
            cell.restlabel.isHidden = false
        }
        else{
            cell.rightimage.isHidden = true
            cell.restlabel.isHidden = true
        }
        return cell
        }
        else{
            let cell = WorkListCell(style: .default, reuseIdentifier: "cellid")
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tableview{
        if section == 0{
        return 63.calcvaluey()
        }
        else{
            return 40.calcvaluey()
        }
        }
        
        return 0
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if tableView == tableview {
            
        maincontroller.hidecircle()
            
            
            titleviewanchor.leading?.constant = 80.calcvaluex()
        containeranchor.leading?.constant = 26.calcvaluex()
        containeranchor.width?.constant = 359.calcvaluex()
        
        
        rightviewanchor.leading?.constant = -621.calcvaluex()
        rightview.isHidden = false
        shrink = true
        self.statuscollectionview.collectionViewLayout.invalidateLayout()
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 3, options: .curveEaseInOut, animations: {
//
//
//                self.view.layoutIfNeeded()
//
//            }, completion: nil)
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            }
            
        }
        else{
            let vd = WorkDetailController()
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
    }
}

class ScheduleWorkControllerCell:UITableViewCell {
    let container = UIView()
    let numberlabel = UILabel()
    let seperator = UIView()
    let titlelabel = UILabel()
    let zonelabel = UILabel()
    let rightimage = UIImageView(image: #imageLiteral(resourceName: "ic_error"))
    let restlabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        addSubview(container)
        container.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 2, left: 2, bottom: 0, right: 2),size: .init(width: 0, height: 80.calcvaluey()))
        
        numberlabel.text = "3"
        numberlabel.font = UIFont(name: "Roboto-Bold", size: 24.calcvaluex())
        numberlabel.textColor = #colorLiteral(red: 0.7599468827, green: 0.2864739597, blue: 0.325997889, alpha: 1)
        
        container.addSubview(numberlabel)
        numberlabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 34.calcvaluex(), bottom: 0, right: 0))
        numberlabel.centerYInSuperview()
        
        container.addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        seperator.anchor(top: nil, leading: numberlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0.7.calcvaluex(), height: 52.calcvaluey()))
        seperator.centerYInSuperview()
        
        titlelabel.text = "廖漢祥 - "
        titlelabel.textColor = #colorLiteral(red: 0.7599468827, green: 0.2864739597, blue: 0.325997889, alpha: 1)
        titlelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        container.addSubview(titlelabel)
        titlelabel.anchor(top: nil, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0))
        titlelabel.centerYInSuperview()
        
        zonelabel.text = "西屯區"
        zonelabel.textColor = #colorLiteral(red: 0.7599468827, green: 0.2864739597, blue: 0.325997889, alpha: 1)
        zonelabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        container.addSubview(zonelabel)
        zonelabel.anchor(top: nil, leading: titlelabel.trailingAnchor, bottom: nil, trailing: nil)
        zonelabel.centerYInSuperview()
        
        container.addSubview(rightimage)
        rightimage.isHidden = true
        rightimage.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 40.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        rightimage.centerYInSuperview()
        
        container.addSubview(restlabel)
        restlabel.isHidden = true
        restlabel.textAlignment = .center
        restlabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        restlabel.text = "休假"
        restlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        restlabel.layer.cornerRadius = 5.calcvaluex()
        restlabel.layer.borderWidth = 1.calcvaluex()
        restlabel.layer.borderColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        restlabel.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 56.calcvaluex(), height: 30.calcvaluey()))
        restlabel.centerYInSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
