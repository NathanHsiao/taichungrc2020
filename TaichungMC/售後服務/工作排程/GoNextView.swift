//
//  GoNextView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/16.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class GoNextView: UIView {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_product_normal2").withRenderingMode(.alwaysTemplate))
    let hintlabel = UILabel()
    let seperator = UIView()
    let hinttext = UILabel()
    let nextbutton = UIButton(type: .system)
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageview.tintColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        addSubview(imageview)
        imageview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 20.calcvaluex(), height: 20.calcvaluey()))
        imageview.centerYInSuperview()
        
        hintlabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        hintlabel.text = "小提示"
        hintlabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(hintlabel)
        hintlabel.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        hintlabel.centerYInSuperview()
        
        addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        seperator.anchor(top: nil, leading: hintlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 58.calcvaluey()))
        seperator.centerYInSuperview()
        
        addSubview(hinttext)
        hinttext.text = "請選擇一筆報修服務"
        hinttext.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        hinttext.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        hinttext.anchor(top: nil, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        hinttext.centerYInSuperview()
        
        addSubview(nextbutton)
        nextbutton.setTitle("下一步", for: .normal)
        nextbutton.setTitleColor(.white, for: .normal)
        nextbutton.backgroundColor = .black
        nextbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        nextbutton.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 200.calcvaluex(), height: 0))
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
