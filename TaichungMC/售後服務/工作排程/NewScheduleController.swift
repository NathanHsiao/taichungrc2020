//
//  NewScheduleController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/16.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class ScheduleFirstStepCell : UITableViewCell {
    let containercell = UIView()
    let statuslabel = UILabel()
    let toplabel = UILabel()
    let sublabel = UILabel()
    let timelabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        contentView.addSubview(containercell)
        containercell.layer.cornerRadius = 15
        containercell.backgroundColor = .white
        containercell.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        containercell.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 2, left: 2, bottom: 0, right: 2),size: .init(width: 0, height: 104.calcvaluey()))
        
        containercell.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: containercell.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        statuslabel.centerYInSuperview()
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.text = "處理中"
        statuslabel.textAlignment = .center
        statuslabel.layer.cornerRadius = 5
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderWidth = 1.calcvaluex()
        
        containercell.addSubview(toplabel)
        toplabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        toplabel.text = "機台運作問題"
        toplabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        toplabel.anchor(top: containercell.topAnchor, leading: statuslabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0))
        
        
        containercell.addSubview(sublabel)
        sublabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        sublabel.text = "久大行銷股份有限公司"
        sublabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        sublabel.anchor(top: toplabel.bottomAnchor, leading: toplabel.leadingAnchor, bottom:nil , trailing: nil,padding: .init(top: 3, left: 0, bottom: 0, right: 0))
        
        containercell.addSubview(timelabel)
        timelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        timelabel.text = "10分鐘前"
        timelabel.anchor(top: sublabel.bottomAnchor, leading: sublabel.leadingAnchor, bottom:nil , trailing: nil,padding: .init(top: 3, left: 0, bottom: 0, right: 0))
        timelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleFirstStep : UIView,UITableViewDelegate,UITableViewDataSource {
    let searchtext = SearchTextField()
    let selectlocation = ScheduleSelectTextField(padding: 59.calcvaluex())
    let tableview = UITableView(frame: .zero, style: .grouped)
    let righttableview = CustomTableView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(searchtext)
        
        searchtext.attributedPlaceholder = NSAttributedString(string: "搜尋標題或客戶名稱", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchtext.backgroundColor = .white
        searchtext.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 2, left: 2, bottom: 0, right: 0),size: .init(width: 411.calcvaluex(), height: 46.calcvaluey()))
        searchtext.layer.cornerRadius = 46.calcvaluey()/2
        searchtext.addshadowColor(color: #colorLiteral(red: 0.8803046346, green: 0.8825793862, blue: 0.8878995776, alpha: 1))
        
        
        
        
        selectlocation.addshadowColor(color: #colorLiteral(red: 0.8803046346, green: 0.8825793862, blue: 0.8878995776, alpha: 1))
        addSubview(selectlocation)
        
        selectlocation.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 58.calcvaluey(), left: 2, bottom: 0, right: 0),size: .init(width: 411.calcvaluex(), height: 46.calcvaluey()))
        selectlocation.layer.cornerRadius = 46.calcvaluey()/2
        selectlocation.text = "全部地區"
        
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.anchor(top: selectlocation.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing:nil,padding: .init(top: 2, left: 0, bottom: 0, right: 0),size: .init(width: 412.calcvaluex(), height: 0))
        tableview.delegate = self
        tableview.contentInset.bottom = 46.calcvaluey()
        tableview.dataSource = self
        let rightview = UIView()
        addSubview(rightview)
        rightview.addshadowColor(color: #colorLiteral(red: 0.8442915082, green: 0.843693316, blue: 0.8547492623, alpha: 1))
        rightview.backgroundColor = .white
        rightview.layer.cornerRadius = 15
        if #available(iOS 11.0, *) {
            rightview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        rightview.anchor(top: topAnchor, leading: tableview.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 2, left: 18.calcvaluex()-2, bottom: 0, right: 2))
        rightview.addSubview(righttableview)
        
        righttableview.backgroundColor = .clear
        righttableview.bounces = true
        righttableview.contentInset.bottom = 70.calcvaluey()
        righttableview.fillSuperview()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 66.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = UIView()
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        label.text = "交機組"
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(label)
            label.anchor(top: nil, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 2, bottom: 12.calcvaluey(), right: 0))
        
        return vd
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ScheduleFirstStepCell(style: .default, reuseIdentifier: "ab")
        cell.selectionStyle = .none
        return cell
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleSecondStepCell:UITableViewCell {
    let container = UIView()
    let datelabel = UILabel()
    let timelabel = UILabel()
    let deletebutton = UIView()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_delete"))
    //let imageview = UIImageView(image: #imageLiteral(resourceName: <#T##String#>))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        container.addshadowColor(color: #colorLiteral(red: 0.8722851872, green: 0.8745393753, blue: 0.8798108697, alpha: 1))
        
        addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 26.calcvaluex(), bottom: 2, right: 26.calcvaluex()))
        
        container.addSubview(datelabel)
        datelabel.text = "2019年12月15日"
        datelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        datelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        datelabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 0))
        datelabel.centerYInSuperview()
        
        container.addSubview(timelabel)
        timelabel.text = "下午9:00"
        timelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        timelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        timelabel.anchor(top: nil, leading: datelabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 66.calcvaluex(), bottom: 0, right: 0))
        timelabel.centerYInSuperview()
        
        container.addSubview(deletebutton)
        deletebutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        deletebutton.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            deletebutton.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        deletebutton.anchor(top: container.topAnchor, leading: nil, bottom: container.bottomAnchor, trailing: container.trailingAnchor,size: .init(width: 104.calcvaluex(), height: 0))
        
        deletebutton.addSubview(imageview)
        imageview.centerInSuperview(size: .init(width: 48.calcvaluex(), height: 48.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleSecondStepAddTimeCell:UITableViewCell {
    let addButton = AddButton()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(addButton)
        selectionStyle = .none
        addButton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        addButton.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        addButton.titles.text = "新增時間"
        addButton.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: nil,padding: .init(top: 2, left: 0, bottom: 2, right: 0),size: .init(width: 178.calcvaluex(), height: 0))
        addButton.centerXInSuperview()
        addButton.layer.cornerRadius = (48.calcvaluey()-4)/2
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleSecondStep : UIView,UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 3{
            let cell = ScheduleSecondStepAddTimeCell(style: .default, reuseIdentifier: "time")
            cell.backgroundColor = .clear
            
            return cell
        }
        let cell = ScheduleSecondStepCell(style: .default, reuseIdentifier: "cc")
        cell.backgroundColor = .clear
        return cell
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 26.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3{
            return 48.calcvaluey()
        }
        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vd = UIView()
        
        return vd
    }
    
    let tableview = UITableView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 15.calcvaluex()
        addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.fillSuperview(padding: .init(top: 29.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleThirdStepCell:UICollectionViewCell {
    let container = UIView()
    let datelabel = UILabel()
    let timelabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8618018627, green: 0.8640291095, blue: 0.8692371845, alpha: 1))
        container.layer.cornerRadius = 15.calcvaluex()
        addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        
        datelabel.text = "2019年12月15日"
        datelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        datelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        timelabel.text = "上午9:00"
        timelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        timelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        container.addSubview(datelabel)
        datelabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        datelabel.centerYInSuperview()
        container.addSubview(timelabel)
        timelabel.anchor(top: nil, leading: datelabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0))
        timelabel.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleThirdStepSelectionCell:UITableViewCell {
    let container = UIView()
    let checkimage = UIImageView(image: #imageLiteral(resourceName: "btn_check_box_normal"))
    let seperator = UIView()
    let numlabel = UILabel()
    let vsep = UIView()
    let namelabel = UILabel()
    let zonelabel = UILabel()
    let statuslabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        container.backgroundColor = .clear
        container.layer.cornerRadius = 15.calcvaluex()
        addSubview(container)
        container.fillSuperview()
        container.addSubview(checkimage)
        checkimage.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 51.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        checkimage.centerYInSuperview()
        checkimage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(Selecting)))
        checkimage.isUserInteractionEnabled = true
        
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        numlabel.font = UIFont(name: "Roboto-Bold", size: 24.calcvaluex())
        numlabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        numlabel.text = "3"
        addSubview(numlabel)
        numlabel.anchor(top: nil, leading: checkimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        numlabel.centerYInSuperview()
        
        vsep.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        addSubview(vsep)
        vsep.anchor(top: nil, leading: numlabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 52.calcvaluey()))
        vsep.centerYInSuperview()
        
        namelabel.text = "廖漢祥 - "
        namelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        namelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        addSubview(namelabel)
        namelabel.anchor(top: nil, leading: vsep.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0))
        namelabel.centerYInSuperview()
        
        zonelabel.text = "西屯區"
        zonelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        zonelabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        addSubview(zonelabel)
        zonelabel.anchor(top: nil, leading: namelabel.trailingAnchor, bottom: nil, trailing: nil)
        zonelabel.centerYInSuperview()
        
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        statuslabel.textColor = #colorLiteral(red: 0.7599468827, green: 0.2864739597, blue: 0.325997889, alpha: 1)
        statuslabel.text = "延後"
        statuslabel.layer.cornerRadius = 5.calcvaluex()
        statuslabel.layer.borderWidth = 1.calcvaluex()
        statuslabel.layer.borderColor = #colorLiteral(red: 0.7599468827, green: 0.2864739597, blue: 0.325997889, alpha: 1)
        statuslabel.textAlignment = .center
        
        addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: zonelabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 56.calcvaluex(), height: 30.calcvaluey()))
        statuslabel.centerYInSuperview()
    }
    @objc func Selecting(){
        checkimage.image = #imageLiteral(resourceName: "btn_check_box_pressed")
        container.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        seperator.isHidden = true
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleThirdStepBottomView: UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ScheduleThirdStepSelectionCell(style: .default, reuseIdentifier: "ct")
        cell.backgroundColor = .clear
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 63.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = UIView()
        
        let label = UILabel()
        label.text = "交機組"
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        vd.addSubview(label)
        label.anchor(top: nil, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 0))
        return vd
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.calcvaluey()
    }
    
    let allbutton = UIButton(type: .system)
    let historybutton = UIButton(type: .system)
    let zonebutton = UIButton(type: .system)
    let searchfield = SearchTextField()
    let selectallbutton = UIButton(type: .system)
    let tableview = UITableView(frame: .zero, style: .grouped)
    let stackview = UIStackView()
    var stackviewanchor:AnchoredConstraints!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        layer.cornerRadius = 15.calcvaluex()
        addshadowColor(color: #colorLiteral(red: 0.8581020236, green: 0.8603197336, blue: 0.8655055165, alpha: 1))
        
        allbutton.setTitle("全部", for: .normal)
        allbutton.setTitleColor(.white, for: .normal)
        allbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        allbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        allbutton.constrainWidth(constant: 102.calcvaluex())
        allbutton.layer.cornerRadius = 38.calcvaluey()/2
        
        historybutton.setTitle("歷史人員", for: .normal)
        historybutton.setTitleColor(.white, for: .normal)
        historybutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        historybutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        historybutton.constrainWidth(constant: 138.calcvaluex())
        historybutton.layer.cornerRadius = 38.calcvaluey()/2
        
        zonebutton.setTitle("地區人員", for: .normal)
        zonebutton.setTitleColor(.white, for: .normal)
        zonebutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        zonebutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        zonebutton.constrainWidth(constant: 138.calcvaluex())
        zonebutton.layer.cornerRadius = 38.calcvaluey()/2
        
        searchfield.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        searchfield.constrainWidth(constant: 264.calcvaluex())
        searchfield.attributedPlaceholder = NSAttributedString(string: "搜尋派工人員", attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
        searchfield.layer.cornerRadius = 38.calcvaluey()/2
        
        selectallbutton.setTitle("套用全部日期", for: .normal)
        selectallbutton.setTitleColor(.white, for: .normal)
        selectallbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        selectallbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        selectallbutton.layer.cornerRadius = 38.calcvaluey()/2
        
        for i in [allbutton,historybutton,zonebutton,searchfield,selectallbutton] {
            stackview.addArrangedSubview(i)
        }

        
        stackview.axis = .horizontal
        stackview.spacing = 26.calcvaluex()
        
        addSubview(stackview)
        stackviewanchor = stackview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 27.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        
        tableview.backgroundColor = .clear
        addSubview(tableview)
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.anchor(top: stackview.bottomAnchor, leading: stackview.leadingAnchor, bottom: bottomAnchor, trailing: stackview.trailingAnchor)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleThirdStep:UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        cell.backgroundColor = .clear
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 298.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ScheduleThirdStepCell
        cell.datelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        cell.timelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
    }
    let topview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let bottomview = ScheduleThirdStepBottomView()
    override init(frame: CGRect) {
        super.init(frame: frame)

        topview.backgroundColor = .clear
        (topview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        topview.delegate = self
        topview.dataSource = self
        topview.register(ScheduleThirdStepCell.self, forCellWithReuseIdentifier: "cell")

        
        addSubview(topview)
        topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 52.calcvaluey()))
        addSubview(bottomview)
        bottomview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol NewScheduleCompleteViewDelegate {
    func closeCompleteView()
    func closeConfirmView()
}
class NewScheduleCompleteView:UIView {
    let title = UILabel()
    let doneview = ScheduleThirdStep()
    let closebutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    var delegate:NewScheduleCompleteViewDelegate!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        layer.cornerRadius = 15.calcvaluex()
        title.text = "已選維修人員列表"
        title.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        title.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        addSubview(title)
        title.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0))
        title.centerXInSuperview()
        
        addSubview(closebutton)
        closebutton.isUserInteractionEnabled = true
        closebutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        closebutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closingview)))
        doneview.bottomview.stackviewanchor.height?.constant = 0
        doneview.bottomview.stackviewanchor.top?.constant = 0
        doneview.bottomview.stackview.isHidden = true
        addSubview(doneview)
        doneview.anchor(top: title.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    @objc func closingview(){
        delegate.closeCompleteView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ScheduleConfirmView : UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ScheduleConfirmViewCell(style: .default, reuseIdentifier: "cell")
        cell.backgroundColor = .clear
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 311.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    let title = UILabel()
    let sendbutton = UIButton(type: .system)
    let tableview = UITableView(frame: .zero, style: .grouped)
    let closebutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    var delegate:NewScheduleCompleteViewDelegate!
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        layer.cornerRadius = 15.calcvaluex()
        title.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        title.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(title)
        title.text = "確認派工資訊"
        title.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0))
        title.centerXInSuperview()
        
        addSubview(closebutton)
        closebutton.isUserInteractionEnabled = true
        closebutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        closebutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeView)))
        
        sendbutton.setTitle("派工", for: .normal)
        sendbutton.setTitleColor(.white, for: .normal)
        sendbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        sendbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        sendbutton.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        
        addSubview(sendbutton)
        sendbutton.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        sendbutton.layer.cornerRadius = 48.calcvaluey()/2
        sendbutton.centerXInSuperview()
        
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.anchor(top: title.bottomAnchor, leading: leadingAnchor, bottom: sendbutton.topAnchor, trailing: trailingAnchor,padding: .init(top: 30.calcvaluey(), left: 64.calcvaluex(), bottom: 25.calcvaluey(), right: 0))
        
    }
    @objc func closeView(){
        delegate.closeConfirmView()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ScheduleConfirmViewCell:UITableViewCell {
    //久大行銷股份有限公司 機台運作問題 台中市西屯區文心路三段241號17樓 葉德雄 2019年9月22日 上午9:00 交機組 - 廖漢祥 機械組 - 黃書賢
    let labelarray = ["報修公司:","報修問題:","地點:","派工人員:","維修時間:","維修人員:"]
    let contentarray = ["久大行銷股份有限公司","機台運作問題","台中市西屯區文心路三段241號17樓","葉德雄","2019年9月22日 上午9:00","交機組 - 廖漢祥\n\n機械組 - 黃書賢"]
    let seperator = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let stackview = UIStackView()
        stackview.axis = .vertical
        stackview.spacing = 20.calcvaluey()
        
        for i in labelarray {
            let label = UILabel()
            label.text = i
            label.textAlignment = .right
            label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
            label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
//
//            let content = UILabel()
//            content.text = contentarray[i]
//            content.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
//            content.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
//
//            let hstack = UIStackView(arrangedSubviews: [label,content])
//            hstack.axis = .horizontal
//            hstack.spacing = 10.calcvaluex()
            
            
            stackview.addArrangedSubview(label)
        }
        stackview.addArrangedSubview(UIView())
        addSubview(stackview)
        stackview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 140.calcvaluex(), height: 0))
        
        seperator.backgroundColor = #colorLiteral(red: 0.8704903722, green: 0.8706433177, blue: 0.8704920411, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 64.calcvaluex()),size: .init(width: 426.calcvaluex(), height: 1.calcvaluey()))
        let stackview2 = UIStackView()
        stackview2.axis = .vertical
        stackview2.spacing = 20.calcvaluey()
               for i in contentarray {
                    let label = UILabel()
                    label.text = i
                    
                    label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
                    label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
                label.numberOfLines = 0
                    
                    
                    stackview2.addArrangedSubview(label)
                }
        addSubview(stackview2)
        stackview2.addArrangedSubview(UIView())
        stackview2.anchor(top: stackview.topAnchor, leading: stackview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NewScheduleController: SampleController,NewScheduleCompleteViewDelegate {
    func closeConfirmView() {
        self.view.insertSubview(self.completeview, aboveSubview: self.fullview)
        self.view.bringSubviewToFront(nextview)
        confirmview.alpha = 0
        fullview.alpha = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()

        })
        currentcomplete = false
    }
    
    func closeCompleteView() {
        fullview.alpha = 0
        completeviewanchor.top?.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        currentcomplete = false
    }
    
    let selectedview = SelectedView()
    var selectedviewanchor:AnchoredConstraints!
    let nextview = GoNextView()
    var current = 0
    let stackview = UIStackView()
    let step1 = ScheduleFirstStep()
    var step1anchor:AnchoredConstraints!
    let step2 = ScheduleSecondStep()
    var step2anchor:AnchoredConstraints!
    let step3 = ScheduleThirdStep()
    var step3anchor:AnchoredConstraints!
    var fullview = UIView()
    var completeview = NewScheduleCompleteView()
    var completeviewanchor:AnchoredConstraints!
    var currentcomplete = false
    var confirmview = ScheduleConfirmView()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        stackview.axis = .horizontal
        stackview.distribution = .fillEqually
        
        for i in ["STEP1 選擇報修問題","STEP2 選擇維修時間","STEP3 選擇維修人員"] {
            let step = StepView()
            step.bottomlabel.text = i
            stackview.addArrangedSubview(step)
        }
        
        view.addSubview(stackview)
        stackview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 34.calcvaluey()))
        
        
        view.addSubview(selectedview)
        let st = stackview.arrangedSubviews[0] as! StepView
        st.bottomlabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        selectedviewanchor = selectedview.anchor(top: nil, leading: st.leadingAnchor, bottom: nil, trailing: st.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 8.calcvaluey()))
        selectedview.centerYAnchor.constraint(equalTo: st.topview.centerYAnchor).isActive = true
        
        step1.backgroundColor = .clear
        view.addSubview(step1)
        step1anchor = step1.anchor(top: stackview.bottomAnchor, leading:view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 27.calcvaluex(), bottom: 0, right: 0),size: .init(width: 971.calcvaluex(), height: 588.calcvaluey()))
        
        view.addSubview(step2)

        step2anchor = step2.anchor(top: step1.topAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 972.calcvaluex(), height: 0))
        
       
        view.addSubview(step3)
        step3.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        step3anchor = step3.anchor(top: step2.topAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 972.calcvaluex(), height: 0))
        fullview.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.addSubview(fullview)
        fullview.fillSuperview()
        fullview.alpha = 0
        
        view.addSubview(completeview)
        completeview.delegate = self
        completeviewanchor = completeview.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 567.calcvaluey()))
        view.addSubview(nextview)
        nextview.backgroundColor = .white
        nextview.addtopshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        nextview.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 92.calcvaluey()))
        nextview.nextbutton.addTarget(self, action: #selector(gonext), for: .touchUpInside)
        nextview.nextbutton.isUserInteractionEnabled = true
        

        view.addSubview(confirmview)
        confirmview.centerInSuperview(size: .init(width: 704.calcvaluex(), height: 600.calcvaluey()))
        confirmview.alpha = 0
        confirmview.delegate = self
        
    }
    @objc func gonext(){
       
        if !currentcomplete{
        if current < stackview.arrangedSubviews.count - 1{

            if current == 0 {
                step2anchor.leading?.isActive = false
                step2.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 26.calcvaluex()).isActive = true
                
            }
            else if current == 1{
                
                step3anchor.leading?.isActive = false
                step3.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 26.calcvaluex()).isActive = true
                
            }
        
        let st = stackview.arrangedSubviews[current] as! StepView
        let st2 = stackview.arrangedSubviews[current+1] as! StepView
        selectedview.centerxconstraint.constant = (st.frame.width * CGFloat((current + 1)))/2
        selectedviewanchor.trailing?.constant = st2.frame.width * CGFloat((current + 1))
        self.backbutton.tintColor = .white
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
                self.backbutton.image = #imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate)
//                self.backbutton.setImage(#imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate), for: .normal)
            }) { (_) in
                st.bottomlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
                st2.bottomlabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
                
                if self.current == 0{
                    self.nextview.hinttext.text = "請選擇一筆報修服務"
                    
                }
                else if self.current == 1{
                    self.nextview.hinttext.text = "可新增多個維修時間"
                    
                    self.step1.isHidden = true
                    
                }
                else{
                    self.nextview.hinttext.text = "請選擇維修人員"
                    self.nextview.nextbutton.setTitle("確認派工", for: .normal)
                    self.step2.isHidden = true
                }
            }

        current += 1
        }
        else{
            fullview.alpha = 1
            completeviewanchor.top?.constant = -567.calcvaluey()
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            currentcomplete = true
        }
        }
        else{
            completeviewanchor.top?.constant = 0
            self.view.bringSubviewToFront(fullview)
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()

            }) { (_) in
                self.confirmview.alpha = 1
                self.view.insertSubview(self.confirmview, aboveSubview: self.fullview)

            }
            
        }
    }
}
