//
//  WorkListCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/11.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class WorkListCell: UITableViewCell {
    let container = UIView()
    let statuslabel = UILabel()
    let topheader = UILabel()
    let subheader = UILabel()
    let location = UILabel()
    let maintaintime = UILabel()
    let donetime = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        container.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        container.layer.cornerRadius = 15.calcvaluex()
        container.backgroundColor = .white
        addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 12.calcvaluey(), right: 2))
        
        container.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 38.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        statuslabel.centerYInSuperview()
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        statuslabel.textColor = #colorLiteral(red: 0.1806353629, green: 0.5779913068, blue: 0.4244887233, alpha: 1)
        statuslabel.text = "已完成"
        statuslabel.textAlignment = .center
        statuslabel.layer.cornerRadius = 5.calcvaluex()
        statuslabel.layer.borderColor = #colorLiteral(red: 0.1806353629, green: 0.5779913068, blue: 0.4244887233, alpha: 1)
        statuslabel.layer.borderWidth = 1.calcvaluex()
        
        container.addSubview(topheader)
        topheader.anchor(top: container.topAnchor, leading: statuslabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 38.calcvaluex(), bottom: 0, right: 0))
        topheader.text = "久大寰宇科技股份有限公司"
        topheader.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        topheader.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        subheader.text = "工具機問題"
        subheader.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        subheader.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        container.addSubview(subheader)
        subheader.anchor(top: topheader.bottomAnchor, leading: topheader.leadingAnchor, bottom: nil, trailing: nil)
        
        [location,maintaintime,donetime].forEach { (lb) in
            lb.font = UIFont(name: "Roboto-Light", size: 16.calcvaluey())
            lb.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
            
            container.addSubview(lb)
        }
        location.text = "報修地點: 西屯區"
        maintaintime.text = "維修時間: 下午3:00"
        donetime.text = "完成時間: 2019年9月20日 下午4:26"
        
        location.anchor(top: subheader.bottomAnchor, leading: subheader.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 0, right: 0))
        maintaintime.anchor(top: location.bottomAnchor, leading: subheader.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 0, right: 0))
        donetime.anchor(top: maintaintime.bottomAnchor, leading: subheader.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
