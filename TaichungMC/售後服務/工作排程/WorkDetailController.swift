//
//  WorkDetailController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/11.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class WorkDetailView : UIView {
    let topview = UIView()
    let titlelabel = UILabel()
    var titlelabelAnchor:NSLayoutConstraint?
    override init(frame: CGRect) {
        super.init(frame: frame)
        addshadowColor(color: #colorLiteral(red: 0.8419348598, green: 0.8461790681, blue: 0.8565961123, alpha: 1))
        layer.cornerRadius = 15.calcvaluex()
        backgroundColor = .white
        addSubview(topview)
        topview.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        
        topview.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            topview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 48.calcvaluey()))
        topview.addSubview(titlelabel)
        titlelabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        titlelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        titlelabel.centerYInSuperview()
        titlelabelAnchor = titlelabel.centerXAnchor.constraint(equalTo: topview.centerXAnchor,constant: 0)
        titlelabelAnchor?.isActive = true
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class BottomRightView:WorkDetailView {
    let commentbar = CommentBar()
    var rightviewtableview = CustomTableView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(commentbar)
        commentbar.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 68.calcvaluey()))

        
        addSubview(rightviewtableview)
        rightviewtableview.anchor(top:topview.bottomAnchor, leading: leadingAnchor, bottom: commentbar.topAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 2, right: 0))


    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TopLeftView:WorkDetailView {
    let topimage = UIImageView(image: #imageLiteral(resourceName: "ic_drawer_user").withRenderingMode(.alwaysTemplate))
    let namelabel = UILabel()
    let posttimelabel = UILabel()
    let servicecrew = TopLeftDetail()
    let maintaintime = TopLeftDetail()
    let maintaindetail = TopLeftDetail()
    override init(frame: CGRect) {
        super.init(frame: frame)
        topimage.tintColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        
        addSubview(topimage)
        topimage.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 22.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 50.calcvaluex(), height: 50.calcvaluey()))
        namelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        namelabel.text = "葉德雄"
        namelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        
        addSubview(namelabel)
        namelabel.anchor(top: topimage.topAnchor, leading: topimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0))
        
        posttimelabel.text = "5分鐘前"
        posttimelabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        posttimelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        
        addSubview(posttimelabel)
        posttimelabel.anchor(top: namelabel.bottomAnchor, leading: namelabel.leadingAnchor, bottom: topimage.bottomAnchor, trailing: nil)
        
        addSubview(servicecrew)
        
        servicecrew.titlelabel.text = "服務人員"
        servicecrew.sublabel.text = "交機組 - 廖漢祥 / 機械組 - 黃偉利"
        servicecrew.anchor(top: topimage.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        
        maintaintime.tinyimage.image = #imageLiteral(resourceName: "ic_content_time")
        maintaintime.titlelabel.text = "維修時間"
        maintaintime.sublabel.text = "9月20日 下午4:30"
        addSubview(maintaintime)
        maintaintime.anchor(top: servicecrew.bottomAnchor, leading: servicecrew.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 15.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        
        maintaindetail.tinyimage.image = #imageLiteral(resourceName: "ic_content_work")
        maintaindetail.titlelabel.text = "工作內容"
        maintaindetail.sublabel.text = "請記得攜帶工具包"
        addSubview(maintaindetail)
        maintaindetail.anchor(top: maintaintime.bottomAnchor, leading: maintaintime.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 15.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class TopRightView:WorkDetailView {
    let headerlabel = UILabel()
    let location = TopRightDetail()
    let phone = TopRightDetail()
    let member = TopRightDetail()
    let rightarraow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next").withRenderingMode(.alwaysTemplate))
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(headerlabel)
        headerlabel.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0))
        headerlabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        headerlabel.text = "久大行銷股份有限公司"
        headerlabel.textColor = #colorLiteral(red: 0.123444654, green: 0.08487261087, blue: 0.07682070881, alpha: 1)
        
        addSubview(location)
        location.titlelabel.text = "地點："
        location.sublabel.text = "台中市西屯區文心路三段241號17樓"
        location.anchor(top: headerlabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 329.calcvaluex(), height: 24.calcvaluey()))
        addSubview(phone)
        phone.tinyimage.image = #imageLiteral(resourceName: "ic_content_call")
        phone.titlelabel.text = "電話："
        phone.sublabel.text = "04-1234567#21"
        phone.anchor(top: location.bottomAnchor, leading: location.leadingAnchor, bottom: nil, trailing: location.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
        addSubview(member)
        member.titlelabel.text = "承辦人員："
        member.sublabel.text = "葉德雄"
        member.tinyimage.image = #imageLiteral(resourceName: "ic_content_user")
                member.anchor(top: phone.bottomAnchor, leading: phone.leadingAnchor, bottom: nil, trailing: phone.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
        
        rightarraow.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(rightarraow)
        rightarraow.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 79.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class TopRightDetail:UIView {
    let tinyimage = UIImageView(image: #imageLiteral(resourceName: "ic_content_address"))
    let titlelabel = UILabel()
    let sublabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(tinyimage)
        tinyimage.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        
        addSubview(titlelabel)
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        titlelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        titlelabel.anchor(top: nil, leading: tinyimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        titlelabel.centerYAnchor.constraint(equalTo: tinyimage.centerYAnchor).isActive = true
        addSubview(sublabel)
        
        sublabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        sublabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        sublabel.anchor(top: nil, leading: titlelabel.trailingAnchor, bottom: nil, trailing: nil)
        sublabel.centerYAnchor.constraint(equalTo: titlelabel.centerYAnchor).isActive = true
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class TopLeftDetail:UIView {
    let tinyimage = UIImageView(image: #imageLiteral(resourceName: "ic_content_user"))
    let titlelabel = UILabel()
    let sublabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(tinyimage)
        tinyimage.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        titlelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(titlelabel)
        titlelabel.anchor(top: nil, leading: tinyimage.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        titlelabel.centerYAnchor.constraint(equalTo: tinyimage.centerYAnchor).isActive = true
        addSubview(sublabel)
        sublabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        sublabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        sublabel.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BottomLeftView:WorkDetailView {
    let addservicebutton = AddButton3(type: .system)
    let noserviceview = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(addservicebutton)
        addservicebutton.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        addservicebutton.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        
        addservicebutton.titles.text = "新增服務單"
        
        addservicebutton.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 14.calcvaluex(), bottom: 14.calcvaluey(), right: 14.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        addservicebutton.layer.cornerRadius = 38.calcvaluey()/2
        
        addSubview(noserviceview)
        noserviceview.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        noserviceview.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        noserviceview.text = "尚無服務單"
        noserviceview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 25.calcvaluey(), left: 56.calcvaluex(), bottom: 0, right: 0))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class WorkDetailController: SampleController {
    let topleftview = TopLeftView()
    let bottomleftivew = BottomLeftView()
    let toprightview = TopRightView()
    let bottomrightview = BottomRightView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.9763614535, green: 0.9765318036, blue: 0.9763634801, alpha: 1)
        topleftview.titlelabel.text = "派工單"
        view.addSubview(topleftview)
        topleftview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 412.calcvaluex(), height: 351.calcvaluey()))
        
        bottomleftivew.titlelabel.text = "服務單"
        view.addSubview(bottomleftivew)
        
        bottomleftivew.anchor(top: topleftview.bottomAnchor, leading: topleftview.leadingAnchor, bottom: view.bottomAnchor, trailing: topleftview.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 26.calcvaluey(), right: 0))
        
        toprightview.titlelabel.text = "客戶資訊"
        view.addSubview(toprightview)
        toprightview.anchor(top: topleftview.topAnchor, leading: topleftview.trailingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 235.calcvaluey()))
        
        if #available(iOS 11.0, *) {
            bottomrightview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        bottomrightview.titlelabel.text = "報修內容"
        view.addSubview(bottomrightview)
        bottomrightview.anchor(top: toprightview.bottomAnchor, leading: toprightview.leadingAnchor, bottom: view.bottomAnchor, trailing: toprightview.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        
        
        bottomleftivew.addservicebutton.addTarget(self, action: #selector(newserviceform), for: .touchUpInside)
        
    }
    @objc func newserviceform(){
        let vd = NewServiceFormController()
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
}
