//
//  CommentCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class CommentCell: UITableViewCell {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_drawer_user").withRenderingMode(.alwaysTemplate))
    let titlelabel = UILabel()
    let sublabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imageview.tintColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        contentView.addSubview(imageview)
        imageview.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 50.calcvaluex(), height: 50.calcvaluey()))
        imageview.centerYInSuperview()
        
        titlelabel.text = "葉德雄"
        titlelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        contentView.addSubview(titlelabel)
        titlelabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        titlelabel.anchor(top: imageview.topAnchor, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4, left: 12.calcvaluex(), bottom: 0, right: 0))
        contentView.addSubview(sublabel)
        sublabel.text = "機台會在一開始時閃爍"
        sublabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        sublabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        sublabel.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: nil, trailing: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
