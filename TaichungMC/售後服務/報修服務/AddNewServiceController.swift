//
//  AddNewServiceController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class PaymentSelectionView:UIView {
    let selectionbutton = UIImageView(image: #imageLiteral(resourceName: "btn_radio_normal").withRenderingMode(.alwaysTemplate))
    let selectlabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(selectionbutton)

        
        selectionbutton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        selectionbutton.centerYInSuperview()
        selectionbutton.tintColor = .black
        addSubview(selectlabel)
        selectlabel.text = "選擇機台"
        selectlabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        selectlabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        selectlabel.anchor(top: nil, leading: selectionbutton.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        selectlabel.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CircleSelectionView:UIView {
    let selectionbutton = UIImageView(image: #imageLiteral(resourceName: "btn_radio_normal"))
    let selectlabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(selectionbutton)

        
        selectionbutton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        selectionbutton.centerYInSuperview()
        
        addSubview(selectlabel)
        selectlabel.text = "選擇機台"
        selectlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        selectlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        selectlabel.anchor(top: nil, leading: selectionbutton.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        selectlabel.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddNewServiceController: UIViewController {
    let selectMachine = CircleSelectionView()
    let container = UIView()
    let titlelabel = UILabel()
    let timelabel = EditView()
    let sublabel = EditView()
    let addfilebutton = AddFileButton()
    let donebutton = UIButton(type: .system)
    var containeranchor:AnchoredConstraints!
    var extravstack = UIStackView()
    var extravstackanhor:AnchoredConstraints!
    
    let xbutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    
    let enterMachine = CircleSelectionView()
    override func viewDidLoad() {
        super.viewDidLoad()
        container.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        
        container.layer.cornerRadius = 15
        
        view.addSubview(container)
        container.centerInSuperview()
        containeranchor = container.anchor(top: nil, leading: nil, bottom: nil, trailing: nil,size: .init(width: 573.calcvaluex(), height: 538.calcvaluey()))


        titlelabel.text = "新增報修"
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        titlelabel.textAlignment = .center
        container.addSubview(titlelabel)
        titlelabel.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        titlelabel.centerXInSuperview()
        
        
        container.addSubview(selectMachine)
        selectMachine.anchor(top: titlelabel.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluex(), left: 44.calcvaluex(), bottom: 0, right: 0),size: .init(width: 100.calcvaluex(), height: 25.calcvaluey()))
        
        container.addSubview(enterMachine)
        enterMachine.selectlabel.text = "自行輸入機台"
        enterMachine.anchor(top: nil, leading: selectMachine.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 56.calcvaluex(), bottom: 0, right: 0),size: .init(width: 136.calcvaluex(), height: 25.calcvaluey()))
        enterMachine.centerYAnchor.constraint(equalTo: selectMachine.centerYAnchor).isActive = true
        timelabel.header.text = "報修機台"
        
        
        
        timelabel.newtextfield.text = "選擇機台"
        container.addSubview(timelabel)
        timelabel.anchor(top: selectMachine.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 486.calcvaluex(), height: 90.calcvaluey()))
        timelabel.centerXInSuperview()
        
        

        

        
        sublabel.header.text = "報修內容"
        
        
        sublabel.newtextfield.attributedPlaceholder = NSAttributedString(string: "輸入內容", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)])
        container.addSubview(sublabel)
        sublabel.anchor(top: timelabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 486.calcvaluex(), height: 90.calcvaluey()))
        sublabel.centerXInSuperview()
        
        
        addfilebutton.image.image = #imageLiteral(resourceName: "ic_btn_attach_file")
        addfilebutton.titles.text = "附加檔案(最多上傳六個檔案)"
        addfilebutton.titles.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        addfilebutton.titles.textColor = .white
        addfilebutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        container.addSubview(addfilebutton)
        addfilebutton.anchor(top: sublabel.bottomAnchor, leading: sublabel.leadingAnchor, bottom: nil, trailing: sublabel.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 48.calcvaluey()))
        addfilebutton.layer.cornerRadius = 48.calcvaluey()/2
        addfilebutton.addTarget(self, action: #selector(addingfile), for: .touchUpInside)
        addfilebutton.centerXInSuperview()
        
        donebutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        donebutton.setTitle("新增", for: .normal)
        donebutton.setTitleColor(.white, for: .normal)
        donebutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        container.addSubview(donebutton)
        donebutton.addshadowColor(color: #colorLiteral(red: 0.8537854552, green: 0.8559919596, blue: 0.861151576, alpha: 1))
        donebutton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        donebutton.centerXInSuperview()
        donebutton.layer.cornerRadius = 48.calcvaluey()/2
        
        
        extravstack.spacing = 9.calcvaluey()
        extravstack.axis = .vertical
        extravstack.distribution = .fillEqually
        extravstack.alignment = .center
        for _ in 0...1 {
            let hstack = UIStackView()
            hstack.axis = .horizontal
            hstack.spacing = 9.calcvaluex()
            hstack.distribution = .fillEqually
            hstack.alignment = .center
            for _ in 0...2 {
                let image = UIImageView(image: #imageLiteral(resourceName: "image_rec_lib_banner"))
                image.contentMode = .scaleAspectFill
                
                image.clipsToBounds = true
                //image.layer.masksToBounds = true
                
                hstack.addArrangedSubview(image)
            }
            extravstack.addArrangedSubview(hstack)
            
        }
        
        container.addSubview(extravstack)
        extravstackanhor = extravstack.anchor(top: addfilebutton.bottomAnchor, leading: addfilebutton.leadingAnchor, bottom: nil, trailing: addfilebutton.trailingAnchor,padding: .init(top: 9.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height:  197.calcvaluey()))
        extravstack.isHidden = true
        
        container.addSubview(xbutton)
        xbutton.isUserInteractionEnabled = true
        xbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popview)))
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.centerYAnchor.constraint(equalTo: titlelabel.centerYAnchor).isActive = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
    @objc func addingfile(){
        containeranchor.height?.constant = 744.calcvaluey()
        
        extravstack.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}
