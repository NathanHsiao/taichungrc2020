//
//  CommentHeader.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class CommentHeader: UIView {
    let topview = UIView()
    let seperator = UIView()
    let seperator2 = UIView()
    let commentview = UIView()
    let pagecontrol = UIPageControl()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    let imageviewscroll = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let machinesublabel = UILabel()
    let machinetitle = UILabel()
    let statuslabel = UILabel()
    let titlelabel = UILabel()
    let datelabel = UILabel()
    let numberofcomment = UILabel()
    let threedotoption = UIImageView(image: #imageLiteral(resourceName: "ic_more").withRenderingMode(.alwaysOriginal))
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        addSubview(topview)
//        topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 0, height: <#T##CGFloat#>))
        
        addSubview(seperator2)
        seperator2.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        seperator2.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 1.calcvaluey()))
        
        addSubview(commentview)
        commentview.anchor(top: nil, leading: leadingAnchor, bottom: seperator2.topAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 50.calcvaluey()))
        
        addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: commentview.topAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        addSubview(pagecontrol)
        pagecontrol.pageIndicatorTintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        pagecontrol.currentPageIndicatorTintColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        pagecontrol.numberOfPages = 3
        pagecontrol.anchor(top: nil, leading: nil, bottom: seperator.topAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 0),size: .init(width: 36.calcvaluex(), height: 8.calcvaluey()))
        pagecontrol.centerXInSuperview()
        
        addSubview(imageviewscroll)
        imageviewscroll.backgroundColor = .clear
        //imageview.contentMode = .scaleAspectFit
        (imageviewscroll.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        imageviewscroll.isPagingEnabled = true
        imageviewscroll.register(CommentHeaderImage.self, forCellWithReuseIdentifier: "cellid")
        imageviewscroll.showsHorizontalScrollIndicator = false
        imageviewscroll.anchor(top: nil, leading: leadingAnchor, bottom: pagecontrol.topAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 228.48.calcvaluey()))
        //imageview.centerXInSuperview()
        imageviewscroll.delegate = self
        imageviewscroll.dataSource = self
        addSubview(machinesublabel)
        machinesublabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        machinesublabel.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        machinesublabel.anchor(top: nil, leading: leadingAnchor, bottom: imageviewscroll.topAnchor, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 41.23.calcvaluey(), right: 0))
        machinesublabel.text = "機台運作問題"
        
        addSubview(machinetitle)
        machinetitle.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        machinetitle.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        machinetitle.text = "VMT-X400"
        machinetitle.anchor(top: nil, leading: machinesublabel.leadingAnchor, bottom: machinesublabel.topAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 4.calcvaluey(), right: 0))
        addSubview(statuslabel)
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        statuslabel.textAlignment = .center
        statuslabel.text = "處理中"
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderWidth = 1
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.cornerRadius = 5
        statuslabel.anchor(top: nil, leading: machinetitle.leadingAnchor, bottom: machinetitle.topAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 40.calcvaluey(), right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        titlelabel.text = "久大行銷股份有限公司"
        titlelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        addSubview(titlelabel)
        titlelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        titlelabel.anchor(top: topAnchor, leading: statuslabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0))
        
        addSubview(datelabel)
        datelabel.text = "10分鐘前"
        datelabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        datelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        datelabel.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: nil, trailing: nil)
        
        commentview.addSubview(numberofcomment)
        numberofcomment.centerInSuperview()
        numberofcomment.text = "7則留言"
        numberofcomment.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        numberofcomment.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
        
        addSubview(threedotoption)
        threedotoption.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 32.calcvaluey(), left: 0, bottom: 0, right: 26.04.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension CommentHeader : UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath)
        cell.backgroundColor = .white
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pagecontrol.currentPage = indexPath.item
    }
}
