//
//  CommentHeaderImage.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/10.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class CommentHeaderImage: UICollectionViewCell {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(imageview)
        imageview.fillSuperview(padding: .init(top: 0, left: 88.65.calcvaluex(), bottom: 0, right: 89.52.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
