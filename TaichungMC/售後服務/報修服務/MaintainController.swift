//
//  MaintainController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/5.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class CommentBar: UIView{
    let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_attach_file_message"))
    let ctextfield = CustomTextField()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addtopshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        addSubview(imageview)
        imageview.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        imageview.centerYInSuperview()
        
        
        ctextfield.attributedPlaceholder = NSAttributedString(string: "留言", attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
        ctextfield.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        
        addSubview(ctextfield)
        ctextfield.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 442.calcvaluex(), height: 49.calcvaluey()))
        ctextfield.centerYInSuperview()
        ctextfield.layer.cornerRadius = 49.calcvaluey()/2
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class MaintainController: SampleController {
    let searchController = SearchTextField()
    let tableview = UITableView(frame: .zero, style: .plain)
    let filterbutton = UIImageView(image: #imageLiteral(resourceName: "ic_filter").withRenderingMode(.alwaysTemplate))
    let beginningview = UIView()
    var beginningviewanchor:AnchoredConstraints!
    var rightview = UIView()
    var rightviewanchor : AnchoredConstraints!
    var rightviewtableview = CustomTableView(frame: .zero, style: .grouped)
    weak var mainview:ViewController?
    
    let commentbar = CommentBar()
    var addfix = AddButton2()
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "報修服務"
        titleviewanchor.leading?.constant = 333.calcvaluex()
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        view.addSubview(beginningview)
        
        beginningviewanchor = beginningview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 333.calcvaluex(), bottom: 0, right: 0),size: .init(width: 665.calcvaluex(), height: 0))
//        searchController.placeholder = "搜尋標題或客戶名稱"
        
        searchController.attributedPlaceholder = NSAttributedString(string: "搜尋標題或客戶名稱", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchController.addshadowColor(color: #colorLiteral(red: 0.8442915082, green: 0.843693316, blue: 0.8547492623, alpha: 1))
        
        beginningview.addSubview(searchController)
        searchController.anchor(top: beginningview.topAnchor, leading: beginningview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 46.calcvaluey()))
        searchController.layer.cornerRadius = 46.calcvaluey()/2
        
        searchController.backgroundColor = .white
        
        
        beginningview.addSubview(tableview)
        tableview.dataSource = self
        tableview.delegate = self
        tableview.separatorStyle = .none
        //tableview.backgroundColor = .red
        tableview.backgroundColor = .clear
        tableview.anchor(top: searchController.bottomAnchor, leading: beginningview.leadingAnchor, bottom: beginningview.bottomAnchor, trailing: beginningview.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        filterbutton.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        filterbutton.isUserInteractionEnabled = true
        filterbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filteraction)))
        beginningview.addSubview(filterbutton)
        filterbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: beginningview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        filterbutton.centerYAnchor.constraint(equalTo: searchController.centerYAnchor).isActive = true
        
        searchController.trailingAnchor.constraint(equalTo: filterbutton.leadingAnchor,constant: -16.calcvaluex()).isActive = true
        
        view.addSubview(rightview)
        rightview.isHidden = true
        rightview.addshadowColor(color: #colorLiteral(red: 0.8442915082, green: 0.843693316, blue: 0.8547492623, alpha: 1))
        rightview.backgroundColor = .white
        rightview.layer.cornerRadius = 15
        if #available(iOS 11.0, *) {
            rightview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        rightviewanchor = rightview.anchor(top: topview.bottomAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 542.calcvaluex(), height: 0))
        commentbar.backgroundColor = .white
        commentbar.addtopshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        rightview.addSubview(commentbar)
        commentbar.anchor(top: nil, leading: rightview.leadingAnchor, bottom: rightview.bottomAnchor, trailing: rightview.trailingAnchor,size: .init(width: 0, height: 68.calcvaluey()))
        
        rightview.addSubview(rightviewtableview)

        rightviewtableview.anchor(top: rightview.topAnchor, leading: rightview.leadingAnchor, bottom: commentbar.topAnchor, trailing: rightview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 2, right: 0))
        
        topview.addSubview(addfix)
        addfix.backgroundColor = .black
        addfix.titles.text = "新增報修"
        addfix.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 80.calcvaluex()),size: .init(width: 142.calcvaluex(), height: 38.calcvaluey()))
        addfix.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.height/2).isActive = true
        addfix.layer.cornerRadius = 38.calcvaluey()/2
        addfix.addTarget(self, action: #selector(newserviceaction), for: .touchUpInside)
        
    }
    @objc func newserviceaction(){
        let vd = AddNewServiceController()
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    @objc func filteraction(){
        let vd = FilterActionController()
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    override func popview() {
        mainview?.showcircle()
        titleviewanchor.leading?.constant = 333.calcvaluex()
                beginningviewanchor.leading?.constant = 333.calcvaluex()
        beginningviewanchor.width?.constant = 665.calcvaluex()

                rightview.isHidden = true
                rightviewanchor.leading?.constant = 0
                UIView.animate(withDuration: 0.4) {
                    self.view.layoutIfNeeded()
                }
    }
}
extension MaintainController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == rightviewtableview {
            return 3
        }
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == rightviewtableview {
            return 1
        }
        return 4
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == rightviewtableview {
            return 0
        }
        return 12.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == rightviewtableview {
            return 89.calcvaluey()
        }
        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let vd = UIView()
        vd.backgroundColor = .clear
        
        return vd
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == rightviewtableview {
            return 499.calcvaluey()
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = CommentHeader()
        vd.layer.cornerRadius = 15
        vd.backgroundColor = .white
        if #available(iOS 11.0, *) {
            vd.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        return vd
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == rightviewtableview {
            let cell = CommentCell(style: .default, reuseIdentifier: "cellidd")
            cell.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
            return cell
        }
        let cell = MaintainCell(style: .default, reuseIdentifier: "ab")
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        mainview!.hidecircle()
        
        beginningviewanchor.leading?.constant = 26.calcvaluex()
        beginningviewanchor.width?.constant = 412.calcvaluex()
        titleviewanchor.leading?.constant = 80.calcvaluex()
//        tableview.reloadData()
        rightview.isHidden = false
        rightviewanchor.leading?.constant = -568.calcvaluex()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }

    
}
