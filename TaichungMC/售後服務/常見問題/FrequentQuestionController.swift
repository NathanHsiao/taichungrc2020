//
//  FrequentQuestionController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/18.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class FrequentQuestionController: SampleController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cc", for: indexPath)
        cell.backgroundColor = .white
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 274.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 34.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         if !shrink{
        maincontroller.hidecircle()
            titleviewanchor.leading?.constant = 80.calcvaluex()
        searchfieldanchor.trailing?.constant = -48.calcvaluex()
        searchfieldanchor.width?.constant = 928.calcvaluex()
        

                UIView.animate(withDuration: 0.4) {
                self.collectionview.collectionViewLayout.invalidateLayout()
                self.view.layoutIfNeeded()
            }
            shrink = true
        }
         else{
            let vd = FrequentQuestionDetailController()
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
        }
    }
    override func popview() {
       shrink = false
        maincontroller.showcircle()
            titleviewanchor.leading?.constant = 333.calcvaluex()
        searchfieldanchor.trailing?.constant = -26.calcvaluex()
        searchfieldanchor.width?.constant = 665.calcvaluex()
        
        UIView.animate(withDuration: 0.4) {
            self.collectionview.collectionViewLayout.invalidateLayout()
            self.view.layoutIfNeeded()
        }
            


    }
    let searchfield = SearchTextField()
    var searchfieldanchor:AnchoredConstraints!
    let bottomview = UIView()
    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    weak var maincontroller:ViewController!
    var shrink = false
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "常見問題"
        titleviewanchor.leading?.constant = 333.calcvaluex()
        searchfield.attributedPlaceholder = NSAttributedString(string: "搜尋機台", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchfield.backgroundColor = .white
        searchfield.addshadowColor(color: #colorLiteral(red: 0.8889377713, green: 0.8912348151, blue: 0.8966072798, alpha: 1))
        view.addSubview(searchfield)
        searchfieldanchor = searchfield.anchor(top: topview.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluey()),size: .init(width: 665.calcvaluex(), height: 46.calcvaluey()))
        searchfield.layer.cornerRadius = 46.calcvaluey()/2
        
        bottomview.backgroundColor = .white
        view.addSubview(bottomview)
        bottomview.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 584.calcvaluey()))
        
        (collectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        view.addSubview(collectionview)
        collectionview.backgroundColor = .clear
        collectionview.anchor(top: bottomview.topAnchor, leading: searchfield.leadingAnchor, bottom: view.bottomAnchor, trailing: bottomview.trailingAnchor,padding: .init(top: 84.calcvaluey(), left: 0, bottom: 0, right: 0))
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.register(FrequentQuestionControllerCell.self, forCellWithReuseIdentifier: "cc")
    }
}
class FrequentQuestionControllerCell: UICollectionViewCell {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    let shadowbackground = UIView()
    
    let sublabel = UILabel()
    let titlelabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        shadowbackground.backgroundColor = .white
        shadowbackground.layer.shadowColor = #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1)
        shadowbackground.layer.shadowOffset = .init(width: 0, height: 5)
        shadowbackground.layer.shadowRadius = 2
        shadowbackground.layer.shadowOpacity = 1
        shadowbackground.layer.cornerRadius = 12
        shadowbackground.layer.shouldRasterize = true
        shadowbackground.layer.rasterizationScale = UIScreen.main.scale
        addSubview(shadowbackground)
        shadowbackground.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 2, bottom: 160.calcvaluey(), right: 0),size: .init(width: 0, height: 294.calcvaluey()))
        
        addSubview(imageview)
        imageview.contentMode = .scaleAspectFit
        imageview.centerXInSuperview()
        imageview.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -296.calcvaluey()).isActive = true
        imageview.constrainWidth(constant: 201.calcvaluex())
        imageview.constrainHeight(constant: 204.calcvaluey())
        

        titlelabel.text = "立式加工中心機"
        titlelabel.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        sublabel.text = "Vcenter-85B/102B"
        sublabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        let stackview = UIStackView(arrangedSubviews: [titlelabel,sublabel])
        stackview.axis = .vertical
        stackview.alignment = .leading
        shadowbackground.addSubview(stackview)
        stackview.centerXInSuperview()
        stackview.anchor(top: nil, leading: nil, bottom: shadowbackground.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 42.calcvaluey(), right: 0),size: .init(width: 0, height: 49.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
