//
//  FrequentQuestionDetailController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/18.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class CustomTitle:UIView {
    let lspacer = UIView()
    let header = UILabel()
    let subtitle = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        lspacer.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        addSubview(lspacer)
        lspacer.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 6.calcvaluex(), height: 56.calcvaluey()))
        lspacer.layer.cornerRadius = 6.calcvaluex()/2
        addSubview(header)
        header.anchor(top: topAnchor, leading: lspacer.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 0))
        header.text = "立式五軸加工中心機"
        header.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        header.font = UIFont(name: "Roboto-Bold", size: 42.calcvaluex())
        
        addSubview(subtitle)
        subtitle.font = UIFont(name: "Roboto-Light", size: 20.calcvaluey())
        subtitle.text = "VCENTER-AX380"
        subtitle.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        subtitle.anchor(top: header.bottomAnchor, leading: header.leadingAnchor, bottom: nil, trailing: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FrequentLeftViewCell:UICollectionViewCell {
    let title = CustomTitle()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(title)
        title.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 83.calcvaluex()))
        
        addSubview(imageview)
        imageview.contentMode = .scaleAspectFit
        imageview.anchor(top: title.bottomAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 353.calcvaluex(), height: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FrequentLeftView:UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemcount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    let leftarrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_before"))
    let rightarrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next"))
    var current = 0
    let itemcount = 5
    override init(frame: CGRect) {
        super.init(frame:frame)
        backgroundColor = .white
        collectionview.backgroundColor = .clear
        (collectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        addSubview(collectionview)
        collectionview.fillSuperview(padding: .init(top: 74.calcvaluey(), left: 48.calcvaluex(), bottom: 89.calcvaluey(), right: 95.calcvaluex()))
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.isPagingEnabled = true
        collectionview.register(FrequentLeftViewCell.self, forCellWithReuseIdentifier: "cell")
        
        addSubview(leftarrow)
        leftarrow.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 255.calcvaluey(), right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        leftarrow.isUserInteractionEnabled = true
        leftarrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(scrolltoprevious)))
        leftarrow.isHidden = true
        addSubview(rightarrow)
        rightarrow.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 255.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        rightarrow.isUserInteractionEnabled = true
        rightarrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(scrolltonext)))
    }
    @objc func scrolltoprevious(){
        if current >= 0 {
        current -= 1
        
        collectionview.scrollToItem(at: IndexPath(item: current, section: 0), at: .centeredHorizontally, animated: true)
            rightarrow.isHidden = false
            if current == 0{
                leftarrow.isHidden = true
            }
        }
    }
    @objc func scrolltonext(){
        
        if current < itemcount {
        current += 1
        
        collectionview.scrollToItem(at: IndexPath(item: current, section: 0), at: .centeredHorizontally, animated: true)
            leftarrow.isHidden = false
            if current == itemcount - 1{
                rightarrow.isHidden = true
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        current = indexPath.item
        
        if current == 0 {
            leftarrow.isHidden = true
            rightarrow.isHidden = false
        }
        else if current == itemcount - 1{
            rightarrow.isHidden = true
            leftarrow.isHidden = false
        }
        else{
            rightarrow.isHidden = false
            leftarrow.isHidden = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FrequentQuestionDetailController: SampleController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ct", for: indexPath) as! SelectionViewCell
        if indexPath.item == 0{
        cell.vd.isHidden = false
            cell.label.text = "客戶常見文題"
            cell.label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        }
        else{
            cell.label.text = "內部技術文件"
            cell.vd.isHidden = true
            cell.label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 120.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 55.calcvaluex()
    }
    let whitespace = UIView()
    let selectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let leftview = FrequentLeftView()
    let rightview = FrequentRightView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(whitespace)
        whitespace.backgroundColor = .white
        whitespace.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 65.calcvaluey()))
        whitespace.layer.shadowColor = #colorLiteral(red: 0.9489133954, green: 0.9490793347, blue: 0.948915422, alpha: 1)
        whitespace.layer.shadowRadius = 5.calcvaluex()
        whitespace.layer.shadowOffset = .init(width: 0, height: 5.calcvaluex())
        whitespace.layer.shadowOpacity = 1
        whitespace.layer.rasterizationScale = UIScreen.main.scale
        whitespace.layer.shouldRasterize = true
        (selectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        view.addSubview(selectionview)
        selectionview.delegate = self
        selectionview.dataSource = self
        selectionview.anchor(top: whitespace.topAnchor, leading: whitespace.leadingAnchor, bottom: whitespace.bottomAnchor, trailing: whitespace.trailingAnchor,padding: .init(top: 0, left: 80.calcvaluex(), bottom: -3, right: 0))
        selectionview.backgroundColor = .clear
        selectionview.register(SelectionViewCell.self, forCellWithReuseIdentifier: "ct")
        view.addSubview(leftview)
        
        leftview.anchor(top: whitespace.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 542.calcvaluex(), height: 0))
        
        view.addSubview(rightview)
        
        rightview.anchor(top: whitespace.bottomAnchor, leading: leftview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        view.bringSubviewToFront(whitespace)
        view.bringSubviewToFront(selectionview)
        
        
    }

}

class FrequentRightView:UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return starray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if expanding[indexPath.row] {
            return 218.calcvaluey()
        }
        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FrequentRightViewCell(style: .default, reuseIdentifier: "ce")
        cell.backgroundColor = .clear
        cell.label.text = starray[indexPath.row]
        if expanding[indexPath.row] {
            cell.ctanchor.height?.constant = 198.calcvaluey()
            //cell.extraviewanchor.height?.constant = 114.calcvaluey()
            cell.extraview.isHidden = false
            cell.addView = true
        }
        else{
            cell.ctanchor.height?.constant = 84.calcvaluey()
            //cell.extraviewanchor.height?.constant = 0
            cell.extraview.isHidden = true
            cell.addView = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if !expanding[indexPath.row] {
            expanding[indexPath.row] = true
        }
        else{
            expanding[indexPath.row] = false
        }
        tableView.beginUpdates()
        tableView.reloadData()
        tableView.endUpdates()
    }
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    let subtitle = UILabel()
    let tableview = UITableView()
    let starray = ["漏氣","零件鬆脫處理辦法","調壓閥","零件有聲音"]
    var expanding = [false,false,false,false]
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        addSubview(imageview)
        imageview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 56.calcvaluey(), left: 184.calcvaluex(), bottom: 0, right: 0),size: .init(width: 114.calcvaluex(), height: 110.calcvaluey()))
        
        addSubview(subtitle)
        subtitle.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        subtitle.text = "油壓零件"
        subtitle.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        subtitle.anchor(top: imageview.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0))
        subtitle.centerXAnchor.constraint(equalTo: imageview.centerXAnchor).isActive = true
        
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.anchor(top: subtitle.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 43.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FrequentRightViewCell:UITableViewCell {
    let ct = UIView()
    var ctanchor:AnchoredConstraints!
    let label = UILabel()
    let dropdownimage = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_down"))
    let extraview = UIView()
    var extraviewanchor:AnchoredConstraints!
    let textview = UITextView()
    var addView = false{
        didSet{
            if self.addView {
            textview.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
            textview.text = "(1) 檢查壓力錶是否正常\n\n(2) 更換新品"
                textview.backgroundColor = .clear
            textview.textColor = #colorLiteral(red: 0.3097652793, green: 0.3098255694, blue: 0.3097659945, alpha: 1)
            extraview.addSubview(textview)
                textview.fillSuperview(padding: .init(top: 27.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0))
            }
            else{
                textview.removeFromSuperview()
            }
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        ct.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        ct.backgroundColor = .white
        ct.layer.cornerRadius = 15.calcvaluex()
        addSubview(ct)
        ctanchor = ct.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 2, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 84.calcvaluey()))
        
        ct.addSubview(label)
        label.text = "漏氣"
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        label.anchor(top: ct.topAnchor, leading: ct.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 28.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0))
        
        
        ct.addSubview(dropdownimage)
        dropdownimage.anchor(top: ct.topAnchor, leading: nil, bottom: nil, trailing: ct.trailingAnchor,padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 24.26.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        ct.addSubview(extraview)
        extraview.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            extraview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        extraview.isHidden = true
        extraview.backgroundColor = #colorLiteral(red: 0.9842038751, green: 0.9843754172, blue: 0.9842056632, alpha: 1)
        extraviewanchor = extraview.anchor(top: label.bottomAnchor, leading: ct.leadingAnchor, bottom: ct.bottomAnchor, trailing: ct.trailingAnchor,padding: .init(top: 28.calcvaluey(), left: 0, bottom: 0, right: 0))
       

        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
