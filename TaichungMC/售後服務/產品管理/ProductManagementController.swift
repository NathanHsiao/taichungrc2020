//
//  ProductManagementController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension UIView {
    func addShadowColor(opacity:Float) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = opacity
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    func addshadowColor(color:UIColor = .white,offset:CGSize = .zero,opacity : Float = 0.1) {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 3
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func addtopshadowColor(color:UIColor) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOffset = .init(width: 0, height: -2)
        self.layer.shadowOpacity = 1
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
}
class SearchTextField : UITextField{
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clearButtonMode = .always
        font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.leftViewMode = .unlessEditing
        let imageview = UIImageView(image: #imageLiteral(resourceName: "ic_search").withRenderingMode(.alwaysTemplate))
        imageview.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        imageview.frame = .init(x: 26.calcvaluex(), y: 0, width: 24.calcvaluex(), height: 24.calcvaluex())
        imageview.contentMode = .scaleAspectFit
        let test = UIView(frame: .init(x: 26.calcvaluex(), y: 0, width: 24.calcvaluex(), height: 24.calcvaluex()))
        test.addSubview(imageview)
        self.leftView = test
        self.returnKeyType = .search
        self.autocorrectionType = .no
        self.autocapitalizationType = .none
        
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 0))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 0))
    }
    override func clearButtonRect(forBounds bounds: CGRect) -> CGRect {
        var originalrect = super.clearButtonRect(forBounds: bounds)
        return originalrect.offsetBy(dx: -10.calcvaluex(), dy: 0)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension CGFloat {
    func calcvaluey() -> CGFloat {
        return UIScreen.main.bounds.height * self/768
    }
    func calcvaluex() -> CGFloat {
        return UIScreen.main.bounds.width * self/1024
    }
}
extension Int {
    func calcvaluey() -> CGFloat {
        return UIScreen.main.bounds.height * CGFloat(self)/768
    }
    func calcvaluex() -> CGFloat {
        return UIScreen.main.bounds.width * CGFloat(self)/1024
    }
}
extension Double {
    func calcvaluey() -> CGFloat {
        return UIScreen.main.bounds.height * CGFloat(self)/768
    }
    func calcvaluex() -> CGFloat {
        return UIScreen.main.bounds.width * CGFloat(self)/1024
    }
}
protocol ProductManagmentDelegate {
    func expand()
    func collapse()
}
enum ProductOrientation {
    case Vertical
    case Horizontal
}
class ProductManagementController: SampleController {
    let container = UIView()
    var containeranchor: AnchoredConstraints!
    var searchbar = SearchTextField()
    var topcolorview = UIView()
    var bottomcolorview = UIView()
    var selectionview : UICollectionView!
    var dataview : UICollectionView!
    let registerbutton = AddButton()
    let changeorientation = UIImageView(image: #imageLiteral(resourceName: "ic_product_list"))
    var delegate:ProductManagmentDelegate!
    var isExpand = false
    var orientation:ProductOrientation = .Horizontal
    var dataviewanchor:AnchoredConstraints!
    let settingbutton = UIImageView(image: #imageLiteral(resourceName: "ic_set"))
    var settingbuttonanchor:AnchoredConstraints!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "產品管理"
        titleviewanchor.leading?.constant = 333.calcvaluex()
        topview.addSubview(settingbutton)
        
//        settingbuttonanchor = settingbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: -28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
//        settingbutton.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.height/2).isActive = true
//        settingbutton.isHidden = true
//        settingbutton.isUserInteractionEnabled = true
//        settingbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showSetting)))
        view.addSubview(topcolorview)
        topcolorview.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        view.addSubview(bottomcolorview)
        bottomcolorview.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        topcolorview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: UIScreen.main.bounds.height * 166/768))
        
        bottomcolorview.anchor(top: topcolorview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        backbutton.image = #imageLiteral(resourceName: "ic_menu")
//        backbutton.setImage(#imageLiteral(resourceName: "ic_menu").withRenderingMode(.alwaysOriginal), for: .normal)
        
        view.backgroundColor = .white
        view.addSubview(container)
        containeranchor = container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 333.calcvaluex(), bottom: 0, right: 0))
        //container.backgroundColor = .red
        container.addSubview(searchbar)
        searchbar.backgroundColor = .white
        searchbar.addshadowColor(color: #colorLiteral(red: 0.9489133954, green: 0.9490793347, blue: 0.948915422, alpha: 1))

        searchbar.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 91.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        searchbar.layer.cornerRadius = 46.calcvaluey()/2
        searchbar.attributedPlaceholder = NSAttributedString(string: "搜尋公司或機台", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        view.addSubview(changeorientation)
        
        changeorientation.anchor(top: nil, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 38.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        changeorientation.centerYAnchor.constraint(equalTo: searchbar.centerYAnchor).isActive = true
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        
        selectionview = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        selectionview.delegate = self
        selectionview.dataSource = self
        
        container.addSubview(selectionview)
        selectionview.backgroundColor = .clear
        selectionview.contentInset.left = 2
        selectionview.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        selectionview.anchor(top: searchbar.bottomAnchor, leading: searchbar.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 46.calcvaluey()))
        
        let newflowlayout = UICollectionViewFlowLayout()
        newflowlayout.scrollDirection = .horizontal
        
        dataview = UICollectionView(frame: .zero, collectionViewLayout: newflowlayout)
        dataview.delegate = self
        dataview.dataSource = self

        dataview.backgroundColor = .clear
        dataview.register(ProductManagerCell.self, forCellWithReuseIdentifier: "dcell")
        dataview.register(ProductManagerVerticalCell.self, forCellWithReuseIdentifier: "cell2")
        container.addSubview(dataview)
        dataviewanchor = dataview.anchor(top: selectionview.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 78.calcvaluey(), left: 0, bottom: 128.calcvaluey(), right: 0))
        
        registerbutton.backgroundColor = .black
        registerbutton.image.backgroundColor = .black
        registerbutton.titles.text = "機台註冊"
        registerbutton.titles.textColor = .white
        registerbutton.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        container.addSubview(registerbutton)
        registerbutton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 178.calcvaluex(), height: 48.calcvaluey()))
        registerbutton.layer.cornerRadius = 48.calcvaluey()/2
        registerbutton.addTarget(self, action: #selector(registermachine), for: .touchUpInside)
        
        changeorientation.isUserInteractionEnabled = true
        changeorientation.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changeorien)))
    }
//    @objc func showSetting(){
//
//        let vd = SettingController()
//
//        vd.modalPresentationStyle = .overFullScreen
//        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        vd.modalTransitionStyle = .crossDissolve
//        present(vd, animated: true, completion: nil)
//    }
    func didscrollin(){
      //  settingbuttonanchor.trailing?.constant = -26.calcvaluex()
       // settingbutton.isHidden = false
        titleviewanchor.leading?.constant = 80.calcvaluex()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    @objc func registermachine(){
        let vd = RegisterMachineController()
        
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        
        self.present(vd, animated: true, completion: nil)
    }
    @objc func changeorien(){
        
        
        if orientation == .Horizontal {
            orientation = .Vertical
            changeorientation.image = #imageLiteral(resourceName: "ic_product_ic")
            if let flow = dataview.collectionViewLayout as? UICollectionViewFlowLayout {
                flow.scrollDirection = .vertical
            }
            dataviewanchor.top?.constant = 26.calcvaluey()
            dataviewanchor.bottom?.constant = 0
            bottomcolorview.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        }
        else{
            orientation = .Horizontal
            changeorientation.image = #imageLiteral(resourceName: "ic_product_list")
            if let flow = dataview.collectionViewLayout as? UICollectionViewFlowLayout {
                flow.scrollDirection = .horizontal
                
            }
            
            dataviewanchor.top?.constant = 78.calcvaluey()
            dataviewanchor.bottom?.constant = -128.calcvaluey()
            bottomcolorview.backgroundColor = .white
            self.view.layoutIfNeeded()
        }



        dataview.reloadData()
    }
    override func popview() {
        isExpand = false
        delegate.collapse()
    }
}
extension ProductManagementController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == dataview {
            if orientation == .Horizontal {
            return .init(width: 274.calcvaluex(), height: collectionView.frame.height-8.calcvaluey())
            }
            else{
                return .init(width: collectionView.frame.width, height: 108.calcvaluey())
            }
        }
        else{
         
        return .init(width: 204.calcvaluex(), height: 42.calcvaluey())
            
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == dataview {
            if orientation == .Horizontal {
            return 34.calcvaluex()
            }
            else{
                return 22.calcvaluey()
            }
            

        }
        return 18.calcvaluex()
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        if collectionView == dataview {
//            if orientation == .Vertical {
//                return 0
//            }
//        }
//        return 0
//    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == dataview {
            if orientation == .Horizontal {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dcell", for: indexPath)

            
            return cell
            }
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath)
                
                return cell
            }
        }
        else{
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)

        cell.addshadowColor(color: #colorLiteral(red: 0.8900358081, green: 0.8945220113, blue: 0.9055349231, alpha: 1))
        
        cell.layer.cornerRadius = 42.calcvaluey()/2
        let label = UILabel()
            label.text = "久大行銷股份有限公司"
            label.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
            cell.addSubview(label)
            label.centerInSuperview()
            
            
            if indexPath.item == 0 {
                cell.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
                label.textColor = .white
            }
            else {
                cell.backgroundColor = .white
                label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            }
        
        return cell
            

        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == dataview {
            if !isExpand {
            delegate.expand()
            self.didscrollin()
            self.isExpand = true
            }
            else{
                
                let vd = ProductInfoController()
                vd.modalPresentationStyle = .fullScreen
                self.present(vd, animated: true, completion: nil)
            }
        }
    }

}
