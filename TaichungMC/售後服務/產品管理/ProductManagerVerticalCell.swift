//
//  ProductManagerVerticalCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductManagerVerticalCell: UICollectionViewCell {
    let shadowcontainer = UIView()
    let statuslabel = UILabel()
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    let sublabel = UILabel()
    let titlelabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        shadowcontainer.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        shadowcontainer.backgroundColor = .white
        addSubview(shadowcontainer)
        shadowcontainer.centerYInSuperview()
        shadowcontainer.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 2, bottom: 0, right: 48.calcvaluex()),size: .init(width: 0, height: 104.calcvaluey()))
        shadowcontainer.layer.cornerRadius = 15
        
        shadowcontainer.addSubview(statuslabel)
        statuslabel.text = "保固中"
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.cornerRadius = 5
        statuslabel.layer.borderWidth = 1
        statuslabel.textAlignment = .center
        
        statuslabel.centerYInSuperview()
        statuslabel.anchor(top: nil, leading: shadowcontainer.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        
        shadowcontainer.addSubview(imageview)
        imageview.centerYInSuperview()
        imageview.anchor(top: nil, leading: statuslabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 48.43.calcvaluex(), bottom: 0, right: 0),size: .init(width: 78.23.calcvaluex(), height: 70.56.calcvaluey()))
        
        titlelabel.text = "立式加工中心機"
        titlelabel.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        sublabel.text = "Vcenter-85B/102B"
        sublabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        let stackview = UIStackView(arrangedSubviews: [titlelabel,sublabel])
        stackview.axis = .vertical
        stackview.alignment = .leading
        shadowcontainer.addSubview(stackview)
        stackview.centerYInSuperview()
        stackview.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 29.33.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 49.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
