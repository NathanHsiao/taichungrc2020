//
//  RegisterMachineController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class RegisterMachineController: UIViewController {
    let shadowcontainer = UIView()
    let titlelabel = UILabel()
    //let ximageview = UIImageView(image: #imageLiteral(resourceName: <#T##String#>))
    let enterfield = AccountField()
    
    let qrbutton = UIButton(type: .system)
    let confirmbutotn = UIButton(type: .system)
    let xbutton = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication"))
    override func viewDidLoad() {
        super.viewDidLoad()
        shadowcontainer.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        shadowcontainer.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        
        view.addSubview(shadowcontainer)
        shadowcontainer.layer.cornerRadius = 15
        shadowcontainer.centerInSuperview()
        shadowcontainer.constrainWidth(constant: 573.calcvaluex())
        shadowcontainer.constrainHeight(constant: 361.calcvaluey())
        
        shadowcontainer.addSubview(titlelabel)
        titlelabel.text = "註冊機台"
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        titlelabel.textAlignment = .center
        titlelabel.centerXInSuperview()
        titlelabel.anchor(top: shadowcontainer.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        shadowcontainer.addSubview(enterfield)
        enterfield.header.text = "機台序號"
        enterfield.header.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        enterfield.header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        //enterfield.textfield.placeholder.text = "輸入機台序號"
        enterfield.textfield.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        enterfield.textfield.layer.cornerRadius = 15
        enterfield.textfield.attributedPlaceholder = NSAttributedString(string: "輸入機台序號", attributes: [NSAttributedString.Key.foregroundColor:UIColor.black])
        
        
        enterfield.anchor(top: titlelabel.bottomAnchor, leading: shadowcontainer.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 44.calcvaluex(), bottom: 0, right: 0),size: .init(width: 486.calcvaluex(), height: 90.calcvaluey()))
        
        qrbutton.setTitle("掃描QRcode", for: .normal)
        qrbutton.setTitleColor(.white, for: .normal)
        qrbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        qrbutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        shadowcontainer.addSubview(qrbutton)
        qrbutton.centerXInSuperview()
        qrbutton.anchor(top: enterfield.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 174.calcvaluex(), height: 48.calcvaluey()))
        qrbutton.layer.cornerRadius = 48.calcvaluey()/2
        
        qrbutton.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        
        confirmbutotn.setTitleColor(.white, for: .normal)
        confirmbutotn.setTitle("確認", for: .normal)
        confirmbutotn.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        confirmbutotn.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        confirmbutotn.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        shadowcontainer.addSubview(confirmbutotn)
        confirmbutotn.centerXInSuperview()
        confirmbutotn.anchor(top: nil, leading: nil, bottom: shadowcontainer.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        confirmbutotn.layer.cornerRadius = 48.calcvaluey()/2
       
        
        shadowcontainer.addSubview(xbutton)
        xbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popview)))
        xbutton.isUserInteractionEnabled = true
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: shadowcontainer.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.centerYAnchor.constraint(equalTo: titlelabel.centerYAnchor).isActive = true
    }
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
}
