//
//  ProductInfoController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
struct ProductInfo {
    var title:String
    var value:String
    var color:UIColor
    
    init(title:String,value:String,color:UIColor = UIColor.black) {
        self.title = title
        self.value = value
        self.color = color
    }
}
class ProductInfoController: SampleController {
    let shadowcontainer = UIView()
    var selectionview : UICollectionView!
    let leftline = UIView()
    let largetitle = UILabel()
    let sublabel = UILabel()
    let datacontainer = UIView()
    let newfix = UIButton(type: .system)
    let newmaintain = UIButton(type: .system)
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850"))
    
    let infoarray = [ProductInfo(title: "機台序號", value: "wksfpr109s"),ProductInfo(title: "購入日期", value: "2018年9月23日"),ProductInfo(title: "保固起訖", value: "2018年9月25日~2028年9月26日"),ProductInfo(title: "負責業務", value: "詹莉雅"),ProductInfo(title: "業務電話", value: "0987654321",color: #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)),ProductInfo(title: "業務信箱", value: "gtmc@gtmc.com.tw",color: #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1))]
    var infoarrayvstack = UIStackView()
    
    var datacontaineranchor:AnchoredConstraints!
    var imageviewanchor:AnchoredConstraints!
    
    var slideinrow = UITableView(frame: .zero, style: .plain)
    var slideinrowanchor: AnchoredConstraints!
    var isExpandarray = [false,false,false,false,false,false,false]
    var isShare = false
    
    var sharebutton = UIImageView(image: #imageLiteral(resourceName: "ic_tab_bar_share"))
    var sharebuttonanchor:AnchoredConstraints!
    
    var sharingbuttonstackview = UIStackView()
    var addButton = AddButton()
    var addButtonanchor:AnchoredConstraints!
    var isFixMode = false
    override func viewDidLoad() {
        super.viewDidLoad()
        //shadowcontainer.addshadowColor(color: #colorLiteral(red: 0.9086812139, green: 0.91102916, blue: 0.916521132, alpha: 1))
        shadowcontainer.layer.shadowColor = #colorLiteral(red: 0.8723884225, green: 0.8800089955, blue: 0.8848493099, alpha: 1)
        shadowcontainer.layer.shadowRadius = 2
        shadowcontainer.layer.shadowOffset = .init(width: 0, height: 4)
        shadowcontainer.layer.shadowOpacity = 1
        shadowcontainer.backgroundColor = .white
        shadowcontainer.layer.shouldRasterize = true
        shadowcontainer.layer.rasterizationScale = UIScreen.main.scale
        view.addSubview(shadowcontainer)
        shadowcontainer.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 65.calcvaluey()))
        
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        
        selectionview = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        
        view.addSubview(selectionview)
        selectionview.backgroundColor = .clear
        selectionview.anchor(top: shadowcontainer.topAnchor, leading: shadowcontainer.leadingAnchor, bottom: shadowcontainer.bottomAnchor, trailing: shadowcontainer.trailingAnchor,padding: .init(top: 0, left: 80.calcvaluex(), bottom: -3, right: 0))
        selectionview.register(SelectionViewCell.self, forCellWithReuseIdentifier: "cell")
        selectionview.delegate = self
        
        selectionview.dataSource = self
        
        
        view.addSubview(leftline)
        leftline.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        leftline.anchor(top: shadowcontainer.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 66.calcvaluey(), left: 56.calcvaluex(), bottom: 0, right: 0),size: .init(width: 6, height: 56.calcvaluey()))
        leftline.layer.cornerRadius = 3
        
        view.addSubview(largetitle)
        largetitle.text = "立式加工中心機"
        largetitle.font = UIFont(name: "Roboto-Bold", size: 42.calcvaluex())
        largetitle.anchor(top: leftline.topAnchor, leading: leftline.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: -4, left: 24.calcvaluex(), bottom: 0, right: 0))
        
        view.addSubview(sublabel)
        sublabel.text = "VCENTER-85B/102B"
        sublabel.font = UIFont(name: "Roboto-Light", size: 24.calcvaluex())
        sublabel.anchor(top: largetitle.bottomAnchor, leading: largetitle.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4, left: 0, bottom: 0, right: 0))
        
        view.addSubview(datacontainer)
        datacontainer.addshadowColor(color: #colorLiteral(red: 0.8441283107, green: 0.8483836651, blue: 0.858827889, alpha: 1))
        datacontainer.backgroundColor = .white
        datacontaineranchor = datacontainer.anchor(top: sublabel.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 44.calcvaluey(), left: -150.calcvaluex(), bottom: 0, right: 0),size: .init(width: 662.calcvaluex(), height: 318.calcvaluey()))
        datacontainer.layer.cornerRadius = 318.calcvaluey()/2
        
        view.addSubview(newfix)
        newfix.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        newfix.setTitle("新增報修", for: .normal)
        newfix.setTitleColor(.white, for: .normal)
        newfix.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        newfix.anchor(top: shadowcontainer.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 66.calcvaluey(), left: 0, bottom: 0, right: 59.calcvaluex()),size: .init(width: 144.calcvaluex(), height: 48.calcvaluey()))
        newfix.layer.cornerRadius = 48.calcvaluey()/2
        newfix.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        view.addSubview(newmaintain)
        newmaintain.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        newmaintain.setTitle("新增保養", for: .normal)
        newmaintain.setTitleColor(.white, for: .normal)
        newmaintain.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        newmaintain.anchor(top: shadowcontainer.bottomAnchor, leading: nil, bottom: nil, trailing: newfix.leadingAnchor,padding: .init(top: 66.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 144.calcvaluex(), height: 48.calcvaluey()))
        newmaintain.layer.cornerRadius = 48.calcvaluey()/2
        newmaintain.addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        
        view.addSubview(imageview)
        imageview.contentMode = .scaleAspectFill
       imageviewanchor = imageview.anchor(top: nil, leading: nil, bottom: nil, trailing:view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 46.calcvaluex()),size: .init(width: 341.calcvaluex(), height: 312.calcvaluey()))
        imageview.centerYAnchor.constraint(equalTo: datacontainer.centerYAnchor).isActive = true
        
        infoarrayvstack.axis = .vertical
        infoarrayvstack.spacing = 14.calcvaluey()
        infoarrayvstack.alignment = .leading
        for j in infoarray {
            let titlelabel = UILabel()
            titlelabel.text = j.title
            titlelabel.font = UIFont(name: "Roboto-Light", size: 20.calcvaluex())
            
            let valuelabel = UILabel()
            valuelabel.textColor = j.color
            valuelabel.text = j.value
            valuelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
            let hstack = UIStackView(arrangedSubviews: [titlelabel,valuelabel])
            hstack.axis = .horizontal
            hstack.spacing = 18.calcvaluex()
            
            infoarrayvstack.addArrangedSubview(hstack)
        }
        
        datacontainer.addSubview(infoarrayvstack)
        infoarrayvstack.centerYInSuperview()
        infoarrayvstack.centerXAnchor.constraint(equalTo: datacontainer.centerXAnchor,constant: 75.calcvaluex()).isActive = true
        slideinrow.contentInset.top = 26.calcvaluey()
//        slideinrow.backgroundColor = .red
        slideinrow.backgroundColor = .clear
        slideinrow.separatorStyle = .none
        slideinrow.delegate = self
        slideinrow.dataSource = self
        view.addSubview(slideinrow)
        slideinrowanchor = slideinrow.anchor(top: shadowcontainer.bottomAnchor, leading: view.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 462.calcvaluex(), height: 0))
        
        view.addSubview(sharebutton)
        sharebuttonanchor = sharebutton.anchor(top: nil, leading: nil, bottom: nil, trailing: shadowcontainer.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: -28.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        sharebutton.centerYAnchor.constraint(equalTo: shadowcontainer.centerYAnchor).isActive = true
        sharebutton.isUserInteractionEnabled = true
        sharebutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sharing)))
        
        sharingbuttonstackview.axis = .horizontal
        sharingbuttonstackview.spacing = 26.calcvaluex()
        sharingbuttonstackview.alignment = .center
        sharingbuttonstackview.distribution = .fillEqually
        for (index,k) in ["取消","全選","分享"].enumerated() {
            let button = UIButton(type: .system)
           
            button.tag = index
            button.setTitle(k, for: .normal)
            
            button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
            button.setTitleColor(.black, for: .normal)
            button.addTarget(self, action: #selector(sharingaction), for: .touchUpInside)
            sharingbuttonstackview.addArrangedSubview(button)
            
        }
        view.addSubview(sharingbuttonstackview)
        
        
        sharingbuttonstackview.anchor(top: nil, leading: nil, bottom: nil, trailing: shadowcontainer.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()))
        sharingbuttonstackview.centerYAnchor.constraint(equalTo: shadowcontainer.centerYAnchor).isActive = true
        sharingbuttonstackview.isHidden = true
        view.addSubview(addButton)
        addButton.image.backgroundColor = .black
        addButton.addshadowColor(color: #colorLiteral(red: 0.7947732806, green: 0.7968279123, blue: 0.801630199, alpha: 1))
        addButton.titles.text = "新增保養"
        addButton.titles.textColor = .white
        addButton.backgroundColor = .black
        
        addButtonanchor = addButton.anchor(top: nil, leading: nil, bottom: self.view.bottomAnchor, trailing: slideinrow.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 2),size: .init(width: 178.calcvaluex(), height: 48.calcvaluey()))
        addButton.layer.cornerRadius = 48.calcvaluey()/2
        addButton.addTarget(self, action: #selector(addNewRecord), for: .touchUpInside)
    }
    @objc func addNewRecord(){
        let vd = AddNewRecordController()
        vd.modalPresentationStyle = .overFullScreen
        vd.modalTransitionStyle = .crossDissolve
        vd.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.present(vd, animated: true, completion: nil)
    }
    @objc func sharingaction(){
        
        isShare = false
        self.slideinrow.beginUpdates()
        self.slideinrow.reloadData()
        self.slideinrow.endUpdates()
        sharingbuttonstackview.isHidden = true
        sharebutton.isHidden = false
        
        addButtonanchor.bottom?.constant = -26.calcvaluey()
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    @objc func sharing(){
        addButtonanchor.bottom?.constant = 74.calcvaluey()
        
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
        isShare = true
        
        self.slideinrow.beginUpdates()
        self.slideinrow.reloadData()
        self.slideinrow.endUpdates()
        sharingbuttonstackview.isHidden = false
        sharebutton.isHidden = true
        
    }
}
extension ProductInfoController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SelectionViewCell
        //cell.backgroundColor = .blue
        if indexPath.item == 0{
            cell.label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
            cell.vd.isHidden = false
        }
        else{
            cell.label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
            cell.vd.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 78.calcvaluex()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 80.calcvaluex(), height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SelectionViewCell
        cell.label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        cell.vd.isHidden = false
        if indexPath.item == 1{
            datacontaineranchor.leading?.constant = -datacontainer.frame.width
            imageviewanchor.trailing?.constant = -627.calcvaluex()
            newfix.isHidden = true
            newmaintain.isHidden = true
            slideinrowanchor.leading?.constant = -slideinrow.frame.width - 26.calcvaluex()
            sharebuttonanchor.trailing?.constant = -26.calcvaluex()
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            isFixMode = false
            addButton.titles.text = "新增保養"
            slideinrow.beginUpdates()
            slideinrow.reloadData()
            slideinrow.endUpdates()
            
        }
        else if indexPath.item == 0{
            datacontaineranchor.leading?.constant = -150.calcvaluex()
            imageviewanchor.trailing?.constant = -46.calcvaluex()

            slideinrowanchor.leading?.constant = 0
            sharebuttonanchor.trailing?.constant = 28.calcvaluex()

            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.newfix.isHidden = false
                self.newmaintain.isHidden = false
            }

        }
        else if indexPath.item == 2{
            sharebuttonanchor.trailing?.constant = 28.calcvaluex()
            datacontaineranchor.leading?.constant = -datacontainer.frame.width
            imageviewanchor.trailing?.constant = -627.calcvaluex()
            newfix.isHidden = true
            newmaintain.isHidden = true
            slideinrowanchor.leading?.constant = -slideinrow.frame.width - 26.calcvaluex()
            
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            isFixMode = true
            addButton.titles.text = "新增報修"
            slideinrow.beginUpdates()
            slideinrow.reloadData()
            slideinrow.endUpdates()
            
        }
    }
    
}
extension ProductInfoController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isFixMode {
            let cell = FixRecordCell(style: .default, reuseIdentifier: "fix")
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            return cell
        }
        else{
        let cell = SlideinRowCell(style: .default, reuseIdentifier: "cell")
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        if isShare{
            cell.selectbuttonanchor.leading?.constant = 26.calcvaluex()
            cell.datelabelanchor.leading?.constant = 66.calcvaluex()
           
        }
//        else{
//            cell.selectbuttonanchor.leading?.constant = -24.calcvaluex()
//            cell.datelabelanchor.leading?.constant = 46.calcvaluex()
//        }
        if isExpandarray[indexPath.section] {
        cell.extracontaineranchor.top?.constant = -272.calcvaluey()
                   cell.extracontainer.isHidden = false
            //cell.selectbutton.image = #imageLiteral(resourceName: "btn_check_box_pressed")
        }
        
        return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isFixMode {
            return 104.calcvaluey()
        }
        else{
        if isExpandarray[indexPath.section]
        {
            return 356.calcvaluey()
        }
        else{
        return 80.calcvaluey()
        }
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if isExpandarray[indexPath.section] {
            isExpandarray[indexPath.section] = false
        }
        else{
        isExpandarray[indexPath.section] = true
        }
        //isExpand = true
        tableView.beginUpdates()
        tableView.reloadData()
        tableView.endUpdates()
    }
}
class SelectionViewCell:UICollectionViewCell {
    let label = UILabel()
    let vd = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.text = "詳細資訊"
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        addSubview(label)
        label.centerInSuperview()
        
        vd.isHidden = true
        vd.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        
        addSubview(vd)
        vd.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: nil,size: .init(width: 50.calcvaluex(), height: 6))
        vd.centerXInSuperview()
        vd.layer.cornerRadius = 3
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddButton : UIButton {
    let image = UIImageView(image: #imageLiteral(resourceName: "ic_btn_add"))
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
        addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.textColor = .white
        titles.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddButton2 : UIButton {
    let image = UIImageView(image: #imageLiteral(resourceName: "ic_btn_add"))
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 20.calcvaluex(), bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.textColor = .white
        titles.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddButton3 : UIButton{
    let image = UIImageView(image: #imageLiteral(resourceName: "ic_btn_add"))
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 134.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.textColor = .white
        titles.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddButton4: UIButton {
        let image = UIImageView(image: #imageLiteral(resourceName: "ic_btn_add"))
    let titles = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .black
        addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        addSubview(image)
        image.centerYInSuperview()
        image.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        addSubview(titles)
        titles.centerYInSuperview()
        titles.textColor = .white
        titles.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        titles.anchor(top: nil, leading: image.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 2.calcvaluex(), bottom: 0, right: 32.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SlideinRowCell:UITableViewCell {
    let container = UIView()
    let datelabel = UILabel()
    let expandbutton = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_down"))
    let selectbutton = UIImageView(image: #imageLiteral(resourceName: "btn_check_box_normal"))
    var selectbuttonanchor:AnchoredConstraints!
    var datelabelanchor : AnchoredConstraints!
    var extracontainer = UIView()
    var extracontaineranchor:AnchoredConstraints!
    let circleview = UIView()
    let extralabel = UILabel()
    let extravstack = UIStackView()
    let editimage = UIImageView(image: #imageLiteral(resourceName: "ic_more"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(container)
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8527267575, green: 0.8549305797, blue: 0.8600837588, alpha: 1))
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        container.layer.cornerRadius = 15
        //container.clipsToBounds = true
        datelabel.text = "12月12日 下午3:25"
        datelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        container.addSubview(datelabel)
        datelabelanchor = datelabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0))
        //datelabel.centerYInSuperview()
        
        container.addSubview(expandbutton)
        //expandbutton.centerYInSuperview()
        expandbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        expandbutton.centerYAnchor.constraint(equalTo: datelabel.centerYAnchor).isActive = true
        
        container.addSubview(selectbutton)
        selectbuttonanchor = selectbutton.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 22.calcvaluey(), left: -24.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        selectbutton.centerYAnchor.constraint(equalTo: datelabel.centerYAnchor).isActive = true
        
        extracontainer.backgroundColor = #colorLiteral(red: 0.9842038751, green: 0.9843754172, blue: 0.9842056632, alpha: 1)
        extracontainer.isHidden = true
        container.addSubview(extracontainer)
        
        if #available(iOS 11.0, *) {
            extracontainer.layer.cornerRadius = 15
            extracontainer.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        extracontaineranchor = extracontainer.anchor(top: container.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,size: .init(width: 0, height: 272.calcvaluey()))
        extracontainer.addSubview(circleview)
        circleview.layer.cornerRadius = 10.calcvaluex()/2
        
        circleview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        circleview.anchor(top: extracontainer.topAnchor, leading: extracontainer.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 30.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 10.calcvaluex(), height: 10.calcvaluex()))
        
        extralabel.text = "機台兩個月一次例行性維修, 更換齒輪油、耗材, 全機檢查及上油."
        extralabel.numberOfLines = 0
        extralabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        extracontainer.addSubview(extralabel)
        extralabel.anchor(top: extracontainer.topAnchor, leading: circleview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 22.calcvaluey(), left: 6.calcvaluex(), bottom: 0, right: 70.calcvaluex()),size: .init(width: 324.calcvaluex(), height: 0))
        
        extravstack.spacing = 8.calcvaluey()
        extravstack.axis = .vertical
        extravstack.distribution = .fillEqually
        extravstack.alignment = .center
        for _ in 0...1 {
            let hstack = UIStackView()
            hstack.axis = .horizontal
            hstack.spacing = 8.calcvaluex()
            hstack.distribution = .fillEqually
            hstack.alignment = .center
            for _ in 0...2 {
                let image = UIImageView(image: #imageLiteral(resourceName: "image_rec_lib_banner"))
                image.contentMode = .scaleAspectFill
                
                image.clipsToBounds = true
                //image.layer.masksToBounds = true
                
                hstack.addArrangedSubview(image)
            }
            extravstack.addArrangedSubview(hstack)
            
        }
        
        extracontainer.addSubview(extravstack)
        extravstack.anchor(top: extralabel.bottomAnchor, leading: extracontainer.leadingAnchor, bottom: nil, trailing: extracontainer.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 156.calcvaluey()))
        
        extracontainer.addSubview(editimage)
        editimage.anchor(top: extracontainer.topAnchor, leading: nil, bottom: nil, trailing: extracontainer.trailingAnchor,padding: .init(top: 23.calcvaluey(), left: 0, bottom: 0, right: 32.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FixRecordCell:UITableViewCell {
    let container = UIView()
    let statuslabel = UILabel()
    let machineheader = UILabel()
    let subheader = UILabel()
    let timelabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(container)
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8527267575, green: 0.8549305797, blue: 0.8600837588, alpha: 1))
        container.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        container.layer.cornerRadius = 15
        
        statuslabel.layer.cornerRadius = 5
        statuslabel.layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.layer.borderWidth = 1
        
        statuslabel.text = "處理中"
        statuslabel.textAlignment = .center
        statuslabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        statuslabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        container.addSubview(statuslabel)
        statuslabel.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 30.calcvaluey()))
        statuslabel.centerYInSuperview()
        
        machineheader.text = "機台運作問題"
        machineheader.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        subheader.text = "久大行銷股份有限公司"
        subheader.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        
        timelabel.text = "10分鐘前"
        timelabel.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        
        let stackview = UIStackView(arrangedSubviews: [machineheader,subheader,timelabel])
        stackview.axis = .vertical
        stackview.alignment = .leading
        stackview.spacing = 1
        
        container.addSubview(stackview)
        stackview.anchor(top: nil, leading: statuslabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        stackview.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
