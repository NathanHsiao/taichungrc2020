//
//  FilterCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/19.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class CustomViewFlowLayout: UICollectionViewFlowLayout {

    var cellSpacing:CGFloat = 14.calcvaluex()

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
            self.minimumLineSpacing = 14.calcvaluey()
        self.sectionInset = UIEdgeInsets(top: 3.calcvaluey(), left: 8, bottom: 10, right: 6)
        let superArray = super.layoutAttributesForElements(in: rect)!
        
        let attributes = NSArray(array: superArray, copyItems: true) as! [UICollectionViewLayoutAttributes]
        

            var leftMargin = sectionInset.left
            var maxY: CGFloat = -1.0
        attributes.forEach { layoutAttribute in
                if layoutAttribute.frame.origin.y >= maxY {
                    leftMargin = sectionInset.left
                }
                layoutAttribute.frame.origin.x = leftMargin
                leftMargin += layoutAttribute.frame.width + cellSpacing
                maxY = max(layoutAttribute.frame.maxY , maxY)
            }
        return attributes
    }
}
class ComparisonFilterInnerCell : UICollectionViewCell {
           let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 19.calcvaluey()
       backgroundColor = .white
        addShadowColor(opacity: 0.05)
        
 
        label.textAlignment = .center
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(label)
        label.fillSuperview()
    }
    func didSelect(){
        backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        label.textColor = .white
    }
    func unSelect(){
        backgroundColor = .white
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol FilterOptionDelegate {
    func resetSelection(arr:[FilterOption],index:Int)
    func resetSelectionMode(mode:FilterMode,index:Int)
}
class ComparisonFilterOptionCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = array[indexPath.item].data.title.width(withConstrainedHeight: .infinity, font: UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!) + 64.calcvaluex()
        return .init(width: width, height: 38.calcvaluey())
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        
        if array[indexPath.row].selected {
            array[indexPath.row].selected = false
        }
        else{
            array[indexPath.row].selected = true
        }
        self.collectionview?.reloadData()
        delegate?.resetSelection(arr: array, index: self.tag)
        
        var count = true
        for j in array {
            if !j.selected {
                count = false
            }
        }
        if count {
            mode = .All
            selectionlabel.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
            selectall.setTitleColor(.black, for: .normal)
        }
        else{
            mode = .Selection
            selectionlabel.setTitleColor(.black, for: .normal)
            selectall.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
        }
        delegate?.resetSelectionMode(mode: self.mode ?? .All, index: self.tag)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! ComparisonFilterInnerCell
        //cell.layer.cornerRadius = collectionView.frame.height/5.8
        if array[indexPath.item].selected {
            cell.didSelect()
        }
        else{
            cell.unSelect()
        }
        cell.label.text = array[indexPath.item].data.title
        return cell
    }
    
    let selectionview = UIView()
    let selectionlabel = UIButton(type: .system)
    let selectall = UIButton(type: .system)
    let seperator = UIView()
    var collectionview : UICollectionView?
    var array = [FilterOption]()
    var mode : FilterMode? {
        didSet{
        if mode == .Selection {
            selectionlabel.setTitleColor(.black, for: .normal)
            selectall.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
        }
        else{
            selectionlabel.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
            selectall.setTitleColor(.black, for: .normal)
        }
        }
    }
    var delegate:FilterOptionDelegate?
    @objc func goSelection(sender:UIButton){
        if sender.tag == 0{
            mode = .Selection
            selectionlabel.setTitleColor(.black, for: .normal)
            selectall.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
        }
        else{
            mode = .All

            selectionlabel.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
            selectall.setTitleColor(.black, for: .normal)
            
            for (ind,_) in array.enumerated() {
                array[ind].selected = true
            
            }
            self.collectionview?.reloadData()
            delegate?.resetSelection(arr: array, index: self.tag)
            
        }
        delegate?.resetSelectionMode(mode: self.mode ?? .All, index: self.tag)
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionview.backgroundColor = .white
        selectionview.layer.cornerRadius = 12
        contentView.addSubview(selectionview)
        selectionview.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 160.calcvaluex(), height: 102.calcvaluey()))
 
        selectionview.addShadowColor(opacity: 0.05)
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        seperator.constrainHeight(constant: 0.7)
        seperator.constrainWidth(constant: 130.calcvaluex())
        //selectionlabel.setTitle("加工範圍", for: .normal)
        //selectionlabel.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
        selectionlabel.tag = 0
        selectionlabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
        selectall.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
        selectionlabel.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        selectall.tag = 1
        selectall.setTitle("全選", for: .normal)
        //selectall.setTitleColor(.black, for: .normal)
        selectall.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        let stackview = UIStackView(arrangedSubviews: [selectionlabel,seperator,selectall])
        stackview.axis = .vertical
        stackview.alignment = .center
        stackview.distribution = .fillProportionally
        selectionview.addSubview(stackview)
        stackview.fillSuperview()
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .vertical
        collectionview = UICollectionView(frame: .zero, collectionViewLayout: CustomViewFlowLayout())
        flowlayout.itemSize = UICollectionViewFlowLayout.automaticSize
        contentView.addSubview(collectionview!)
        self.layoutIfNeeded()
        collectionview?.anchor(top: selectionview.topAnchor, leading: selectionview.trailingAnchor, bottom: selectionview.bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 128.calcvaluex()))
        
        collectionview?.backgroundColor = .clear
        collectionview?.delegate = self
        collectionview?.dataSource = self
        collectionview?.register(ComparisonFilterInnerCell.self, forCellWithReuseIdentifier: "cellid")
        
//        if #available(iOS 11.0, *) {
//            collectionview?.contentInsetAdjustmentBehavior = .never
//        } else {
//            // Fallback on earlier versions
//        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
