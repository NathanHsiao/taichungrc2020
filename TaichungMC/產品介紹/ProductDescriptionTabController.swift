//
//  ProductDescriptionTabController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/21.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductDescriptionTabController:  UITabBarController{
    let backbutton = UIButton(type: .system)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.isHidden = true
        let buttonbar = DetailCustomTabBar()
        buttonbar.backgroundColor = .white
        buttonbar.layer.shadowColor = #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1)
        buttonbar.layer.shadowRadius = 2
        buttonbar.layer.shadowOffset = .zero
        buttonbar.layer.shadowOpacity = 1
        buttonbar.layer.rasterizationScale = UIScreen.main.scale
        buttonbar.layer.shouldRasterize = true
        view.addSubview(buttonbar)
        buttonbar.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 160.yscalevalue()))
        
        
        let topview = UIView()
         topview.isUserInteractionEnabled = true
         topview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        view.addSubview(topview)
        topview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 86))
        let slideinview = UIView()
        view.addSubview(slideinview)
        slideinview.backgroundColor = #colorLiteral(red: 0.8205059171, green: 0.4463779926, blue: 0.01516667381, alpha: 1)
        
        slideinview.anchor(top: view.topAnchor, leading: topview.leadingAnchor, bottom: nil, trailing: topview.trailingAnchor,size: .init(width: 0, height: UIApplication.shared.statusBarFrame.size.height))
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        
        backbutton.setImage(#imageLiteral(resourceName: "ic_back").withRenderingMode(.alwaysOriginal), for: .normal)
        topview.addSubview(backbutton)
        backbutton.contentMode = .scaleAspectFill
        backbutton.anchor(top: nil, leading: topview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.yscalevalue(), bottom: 0, right: 0),size: .init(width: 80.yscalevalue(), height: 80.yscalevalue()))
        backbutton.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.size.height/2).isActive = true
        backbutton.addTarget(self, action: #selector(popview), for: .touchUpInside)
    }
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
}
