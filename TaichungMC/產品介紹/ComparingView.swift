//
//  ComparingView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/18.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
protocol ComparingViewDelegate {
    func comparing()
}
class ComparingView: UIView {
    let stackview = UIStackView()
    var delegate:ComparingViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        let vi = ComparingLabel()
        vi.backgroundColor = .clear
        stackview.addArrangedSubview(vi)
        for _ in 0...2 {
            let j = ComparingCell()
            j.backgroundColor = .clear
            
            stackview.addArrangedSubview(j)
        }
        let button = UIButton(type: .system)
        button.setTitle("開始比較", for: .normal)
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(comparing), for: .touchUpInside)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 26)
        
        
        stackview.addArrangedSubview(button)
        
        if #available(iOS 11.0, *) {
            stackview.setCustomSpacing(22.xscalevalue(), after: stackview.arrangedSubviews[0])
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            stackview.setCustomSpacing(22.xscalevalue(), after: stackview.arrangedSubviews[3])
        } else {
            // Fallback on earlier versions
        }
        stackview.axis = .horizontal
        stackview.distribution = .fillEqually
        addSubview(stackview)
        stackview.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func comparing(){
        delegate?.comparing()
    }
}
class ComparingLabel : UIView {
    let label = UILabel()
    let vd = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.text = "產品比較"
        label.font = UIFont.systemFont(ofSize: 26)
        addSubview(label)
        label.centerInSuperview()
        
        addSubview(vd)
        vd.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        vd.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 24.yscalevalue(), left: 0, bottom: 24.yscalevalue(), right: 1),size: .init(width: 1, height: 0))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ComparingCell : UIView {
    let cv = UIView()
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        cv.backgroundColor = #colorLiteral(red: 0.8794258237, green: 0.8766200542, blue: 0.8816249371, alpha: 1)
        addSubview(cv)
        cv.fillSuperview(padding: .init(top: 18.xscalevalue(), left: 22.xscalevalue(), bottom: 18.xscalevalue(), right: 22.xscalevalue()))
        
        label.text = "+"
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        label.font = UIFont.boldSystemFont(ofSize: 60.yscalevalue())
        label.backgroundColor = .clear
        
        cv.addSubview(label)
        label.centerInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
