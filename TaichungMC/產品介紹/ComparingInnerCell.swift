//
//  ComparingInnerCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/18.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ComparingInnerCell: UIView {
    let imageview = UIImageView(image: #imageLiteral(resourceName: "machine_LV850").withRenderingMode(.alwaysOriginal))
    let firstbutton = UIButton(type: .system)
    let secondbutton = UIButton(type: .system)
    let thirdbutton = UIButton(type: .system)
    let cancelbutton = UIImageView(image: #imageLiteral(resourceName: "ic_close").withRenderingMode(.alwaysOriginal))
    override init(frame: CGRect) {
        super.init(frame: frame)

        addedmachine()
    }
    func addedmachine(){
        backgroundColor = .clear
        addSubview(imageview)
        imageview.contentMode = .scaleToFill
        imageview.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil)
        imageview.centerXInSuperview()
        imageview.heightAnchor.constraint(equalToConstant: 120.yscalevalue()).isActive = true
        imageview.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/2).isActive = true
        addSubview(cancelbutton)
        
        cancelbutton.contentMode = .scaleAspectFit
        cancelbutton.anchor(top: imageview.topAnchor, leading: nil, bottom: nil, trailing: imageview.trailingAnchor,padding: .init(top: -15, left: 0, bottom: 0, right: -22),size: .init(width: 30, height: 30))
        firstbutton.setTitle("立式加工中心機", for: .normal)
        firstbutton.setTitleColor(.black, for: .normal)
        secondbutton.setTitle("Vcenter-P76/106", for: .normal)
        secondbutton.setTitleColor(.black, for: .normal)
        thirdbutton.setTitle("請選擇型號", for: .normal)
        thirdbutton.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
        addSubview(firstbutton)
        let scaledpadding = 48.yscalevalue()
        firstbutton.anchor(top: imageview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 8.yscalevalue(), left: scaledpadding, bottom: 0, right: scaledpadding),size: .init(width: 0, height: 50.yscalevalue()))
        addSubview(secondbutton)
        secondbutton.anchor(top: firstbutton.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 8.yscalevalue(), left: scaledpadding, bottom: 0, right: scaledpadding),size: .init(width: 0, height: 50.yscalevalue()))
        addSubview(thirdbutton)
        thirdbutton.anchor(top: secondbutton.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 8.yscalevalue(), left: scaledpadding, bottom: 0, right: scaledpadding),size: .init(width: 0, height: 50.yscalevalue()))
        for i in [firstbutton,secondbutton,thirdbutton] {
            i.backgroundColor = .white
            i.layer.cornerRadius = 50.yscalevalue()/2
            i.layer.shadowColor = UIColor.black.cgColor
            i.layer.shadowRadius = 2
            i.layer.shadowOffset = .zero
            i.layer.shadowOpacity = 0.1
            i.layer.rasterizationScale = UIScreen.main.scale
            i.layer.shouldRasterize = true
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
