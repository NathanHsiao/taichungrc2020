//
//  InnerSelectionCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/14.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class InnerSelectionCell: UICollectionViewCell {
    let container = UIView()
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        container.backgroundColor = .white
        container.layer.shadowColor = UIColor.black.cgColor
        container.layer.shadowOpacity = 0.2
        container.layer.shadowRadius = 2
        container.layer.shadowOffset = .zero
        container.layer.rasterizationScale = UIScreen.main.scale
        container.layer.shouldRasterize = true
        
        addSubview(container)
        container.fillSuperview(padding: .init(top: 12, left: 36, bottom: 12, right: 36))
        self.layoutIfNeeded()
        container.layer.cornerRadius = (self.frame.height-24)/2
        container.addSubview(label)
        label.text = "VTURN-Q200"
        label.textAlignment = .center
        label.centerInSuperview()
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
