//
//  ProductPresentController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/14.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductPresentController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ComparingViewDelegate,MachineCollectionViewDelegate {
    func showDetail() {
        let vc = ProductDescriptionTabController()
        vc.modalPresentationStyle = .fullScreen
        vc.view.backgroundColor = .white
        self.present(vc, animated: true, completion: nil)
    }
    func comparing() {
        let vd = ComparisonController()
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    
    let button = UIButton(type: .system)
    let selectionview = UIView()
    var selectioncollectionview : UICollectionView!
    var innercollectionview:UICollectionView!
    
    let titlelabel = TitleView()
    
    var stackviewbuttonarray = [UIButton]()
    var machinecollectionview : UICollectionView!
    let pagecontrol = UIPageControl()
    let compareview = ComparingView()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
         let topview = UIView()
         topview.isUserInteractionEnabled = true
         topview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        view.addSubview(topview)
        topview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 86))
         button.setImage(#imageLiteral(resourceName: "ic_menu").withRenderingMode(.alwaysTemplate), for: .normal)
         //button.image = #imageLiteral(resourceName: "ic_menu").withRenderingMode(.alwaysTemplate)
         button.tintColor = .white
         topview.addSubview(button)
        view.layoutIfNeeded()
         button.isUserInteractionEnabled = true
         let buttonheight = topview.frame.height-UIApplication.shared.statusBarFrame.height-16
         button.anchor(top: nil, leading: topview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.xscalevalue(), bottom: 0, right: 0),size: .init(width: buttonheight, height: buttonheight))
        button.addTarget(self, action: #selector(showmenu), for: .touchUpInside)
         button.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.height/2).isActive = true
        // button.layer.zPosition = 2
         let collectionviewflowlayout = UICollectionViewFlowLayout()
         collectionviewflowlayout.scrollDirection = .horizontal
         selectioncollectionview = UICollectionView(frame: .zero, collectionViewLayout: collectionviewflowlayout)
        view.addSubview(selectionview)
         selectionview.backgroundColor = .white
         selectionview.layer.shadowColor = UIColor.black.cgColor
         selectionview.layer.shadowRadius = 2
         selectionview.layer.shadowOpacity = 0.3
         selectionview.layer.shadowOffset = .init(width: 0, height: 1)
        selectionview.layer.shouldRasterize = true
        selectionview.layer.rasterizationScale = UIScreen.main.scale
        selectionview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 80))
         
         
        view.addSubview(selectioncollectionview)
         selectioncollectionview.alwaysBounceHorizontal = true
         selectioncollectionview.delegate = self
         selectioncollectionview.dataSource = self
         selectioncollectionview.backgroundColor = .clear
         selectioncollectionview.register(OuterView2Cell.self, forCellWithReuseIdentifier: "cell")
         selectioncollectionview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 83))
        
        let newflowlayout = UICollectionViewFlowLayout()
        newflowlayout.scrollDirection = .horizontal
        innercollectionview = UICollectionView(frame: .zero, collectionViewLayout: newflowlayout)
        view.addSubview(innercollectionview)
        innercollectionview.backgroundColor = .clear
        innercollectionview.anchor(top: selectionview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 10.yscalevalue(), left: 36.yscalevalue(), bottom: 0, right: 0),size: .init(width: 0, height: 80.yscalevalue()))
        innercollectionview.dataSource = self
        innercollectionview.delegate = self
        innercollectionview.register(InnerSelectionCell.self, forCellWithReuseIdentifier: "cellid2")
        innercollectionview.decelerationRate = .fast
        

        
        
        let stackview = UIStackView()
        
        for i in ["建立訪談","建立報價","加入比較"] {
            let button = UIButton(type: .system)
            button.setTitle(i, for: .normal)
            button.setTitleColor(.white, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
            button.backgroundColor = .black
            button.layer.shadowRadius = 2
            button.layer.shadowColor = UIColor.darkGray.cgColor
            button.layer.shadowOpacity = 0.5
            button.layer.shadowOffset = .zero
            button.layer.shouldRasterize = true
            button.layer.rasterizationScale = UIScreen.main.scale
            stackview.addArrangedSubview(button)
            stackviewbuttonarray.append(button)
            button.addTarget(self, action: #selector(showcomparison), for: .touchUpInside)
        }
        stackview.axis = .vertical
        stackview.distribution = .fillEqually
        stackview.spacing = 20
        
        view.addSubview(stackview)
        stackview.anchor(top: nil, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: -28.yscalevalue()),size: .init(width: 170.yscalevalue(), height: 240.yscalevalue()))
        stackview.centerYInSuperview()
        view.layoutIfNeeded()
        for i in stackview.arrangedSubviews {
            //i.roundCorners(corners: [.topLeft,.bottomLeft], radius: i.frame.height/2)
            i.layer.cornerRadius = i.frame.height/2
        }
        
        let machineflow = UICollectionViewFlowLayout()
        machineflow.scrollDirection = .horizontal
        machinecollectionview = UICollectionView(frame: .zero, collectionViewLayout: machineflow)
        machinecollectionview.backgroundColor = .clear
        view.addSubview(machinecollectionview)
        machinecollectionview.anchor(top: innercollectionview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: stackview.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.xscalevalue()))
        machinecollectionview.register(MachineCollectionViewCell.self, forCellWithReuseIdentifier: "machine")
        machinecollectionview.delegate = self
        machinecollectionview.dataSource = self
        machinecollectionview.isPagingEnabled = true
        machinecollectionview.showsHorizontalScrollIndicator = false
        view.addSubview(pagecontrol)
        pagecontrol.centerXInSuperview()
        pagecontrol.numberOfPages = 5
        pagecontrol.pageIndicatorTintColor = .darkGray
        pagecontrol.currentPageIndicatorTintColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        pagecontrol.transform = CGAffineTransform(scaleX: 2, y: 2)
        pagecontrol.anchor(top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 24.yscalevalue(), right: 0))
        

        compareview.backgroundColor = .white
        compareview.delegate = self
        view.addSubview(compareview)
        compareview.layer.shadowColor = UIColor.black.cgColor
        compareview.layer.shadowRadius = 2
        compareview.layer.shadowOffset = .zero
        compareview.layer.shadowOpacity = 0.3
        compareview.layer.rasterizationScale = UIScreen.main.scale
        compareview.layer.shouldRasterize = true
        compareview.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 160.yscalevalue()))
            }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       

    }
    @objc func showcomparison() {
                UIView.animate(withDuration: 0.5) {
                    self.compareview.transform = CGAffineTransform(translationX: 0, y: -160.yscalevalue())
                }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         //compareanimate()
        selectioncollectionview.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .bottom)
        collectionView(self.selectioncollectionview, didSelectItemAt: IndexPath(item: 0, section: 0))
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == selectioncollectionview {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! OuterView2Cell
        cell.backgroundColor = .clear
        return cell
        }
        else if collectionView == machinecollectionview {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "machine", for: indexPath) as! MachineCollectionViewCell
            cell.delegate = self
            cell.backgroundColor = .clear
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid2", for: indexPath) as! InnerSelectionCell
            cell.backgroundColor = .clear
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == selectioncollectionview {
        let cell = collectionView.cellForItem(at: indexPath) as! OuterView2Cell
        cell.smallsep.isHidden = false
        cell.label.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        }
        else{
            let cell = collectionView.cellForItem(at: indexPath) as! InnerSelectionCell
            cell.container.backgroundColor = .black
            cell.label.textColor = .white
            cell.container.clipsToBounds = true
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int(machinecollectionview.contentOffset.x / machinecollectionview.frame.size.width)
        pagecontrol.currentPage = index
        
        innercollectionview.selectItem(at: IndexPath(item: index, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        collectionView(innercollectionview, didSelectItemAt: IndexPath(item: index, section: 0))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == selectioncollectionview  {
        return .init(width: (self.view.frame.width)/5, height: 83)
        }
        else if collectionView == machinecollectionview {
            return .init(width: collectionView.frame.width, height: collectionView.frame.height)
        }
        else{
            return .init(width: (self.view.frame.width)/5, height: 80.yscalevalue())
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    @objc func showmenu(){
        self.navigationController?.popViewController(animated: true)
    }
    
}
