//
//  ViewController.swift
//  TaichungMC
//
//  Created by Wilson on 2019/12/30.
//  Copyright © 2019 TaichungMC. All rights reserved.
//

import UIKit
import AVFoundation
import CoreTelephony
import Darwin
let xscale = UIScreen.main.bounds.width/1366
let yscale = UIScreen.main.bounds.height/1024
let expandwidth = (UIScreen.main.bounds.height + 40)/2 - (144*xscale)

class MainMenuButton : UIView {
    var imageview = UIImageView()
    let sublabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        imageview.contentMode = .scaleAspectFit
        
        addSubview(imageview)
        imageview.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).withAlphaComponent(0.5)
        imageview.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,size: .init(width: 60.calcvaluex(), height: 60.calcvaluex()))
        imageview.centerXInSuperview()
        
        sublabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        sublabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).withAlphaComponent(0.5)
        sublabel.text = "銷售展示"
        sublabel.textAlignment = .center
        sublabel.numberOfLines = 0
        sublabel.sizeToFit()
        addSubview(sublabel)
        sublabel.anchor(top: imageview.bottomAnchor, leading: nil, bottom: nil, trailing: nil,size: .init(width: 80.calcvaluex(), height: 50.calcvaluey()))
        sublabel.centerXInSuperview()
        isUserInteractionEnabled = true
        
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SubButton : UIView {
    var imageview = UIImageView()
    let sublabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        imageview.contentMode = .scaleAspectFit
        
        addSubview(imageview)
        imageview.tintColor = #colorLiteral(red: 0.7349141836, green: 0.7252627015, blue: 0.7209410667, alpha: 1)
        imageview.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,size: .init(width: 48.calcvaluex(), height: 48.calcvaluex()))
        imageview.centerXInSuperview()
        
        sublabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        sublabel.textColor = #colorLiteral(red: 0.7357886434, green: 0.7211242318, blue: 0.7212550044, alpha: 1)
        sublabel.text = "銷售展示"
        sublabel.numberOfLines = 0
        sublabel.textAlignment = .center
        addSubview(sublabel)
        sublabel.anchor(top: imageview.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0))
        sublabel.centerXInSuperview()
        isUserInteractionEnabled = true
        
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension CALayer {
    func addShadow(){
        self.shadowColor = #colorLiteral(red: 0.9129876494, green: 0.9153465629, blue: 0.9208646417, alpha: 1)
        self.shadowOffset = .init(width: 0, height: 0)
        self.shadowRadius = 5
        self.shadowOpacity = 1.0
        self.rasterizationScale = UIScreen.main.scale
        self.shouldRasterize = true
    }
    func addcircleshadow(){
        self.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        //self.shadowOffset = .init(width: 5, height: 0)
        self.shadowRadius = 5.calcvaluex()
        self.shadowOpacity = 0.2
        self.rasterizationScale = UIScreen.main.scale
        self.shouldRasterize = true
    }
}
extension Int {
    func xscalevalue()->CGFloat {
        return CGFloat(self) * xscale
    }
    func yscalevalue() -> CGFloat {
        return CGFloat(self) * yscale
    }
}
class NewTextField: UITextField {
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 32, dy: 0)
    }
}
class EditTextField : UITextField {
    var padding : UIEdgeInsets? {
        didSet {
            self.placeholderRect(forBounds: self.bounds)
            self.editingRect(forBounds: self.bounds)
            self.textRect(forBounds: self.bounds)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
       font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        autocorrectionType = .no
        autocapitalizationType = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding?.left ?? 0, dy: 0)
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding?.left ?? 0, dy: 0)
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: padding?.left ?? 0, dy: 0)
    }
    
}
class customimageview:UIImageView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let frame = self.bounds.insetBy(dx: -40, dy: -40)
        return frame.contains(point) ? self : nil
    }
}
struct Menubutton  {
    var selectedimage:UIImage?
    var deselectedimage:UIImage?
    var title:String?
    var arc:CAShapeLayer?
    var imageview:MainMenuButton?
    var previousIndex:Int?
    var currentIndex:Int?
    init(selectedimage:UIImage?,deselectedimage:UIImage?,title:String? = nil,arc:CAShapeLayer? = nil,imageview:MainMenuButton?=nil,previousIndex:Int?=nil,currentIndex:Int?=nil) {
        self.selectedimage = selectedimage
        self.deselectedimage = deselectedimage
        self.title = title
        self.arc = arc
        //self.arc?.isHidden = true
        self.imageview = imageview
        self.previousIndex = previousIndex
        self.currentIndex = currentIndex
    }
}

struct SubMenuButton  {
    var selectedimage:UIImage?
    var deselectedimage:UIImage?
    var title:String?
    var arc:CAShapeLayer?
    var imageview:SubButton?
    var previousIndex:Int?
    var currentIndex:Int?
    init(selectedimage:UIImage?,deselectedimage:UIImage?,title:String? = nil,arc:CAShapeLayer? = nil,imageview:SubButton?=nil,previousIndex:Int?=nil,currentIndex:Int?=nil) {
        self.selectedimage = selectedimage
        self.deselectedimage = deselectedimage
        self.title = title
        self.arc = arc
        //self.arc?.isHidden = true
        self.imageview = imageview
        self.previousIndex = previousIndex
        self.currentIndex = currentIndex
    }
}
extension ViewController{
    func addNavBar(){

        
        topview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        view.addSubview(topview)
        topviewanchor = topview.anchor(top: view.topAnchor, leading: nil, bottom: nil, trailing: view.leadingAnchor,size: .init(width: view.frame.width, height: 86))
        
        
        label.text = "最新消息"
        label.font = UIFont.systemFont(ofSize: 20)
        label.textColor = .white
        
        topview.addSubview(label)
        labelanchor = label.anchor(top: nil, leading: topview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: expandwidth, bottom: 0, right: 0))
        label.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.height/2).isActive = true
        
        self.view.layoutIfNeeded()
        
        button.isHidden = true
        //button.backgroundColor = .red
        button.setImage(#imageLiteral(resourceName: "ic_menu").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        topview.addSubview(button)
        let buttonheight = topview.frame.height-UIApplication.shared.statusBarFrame.height-16
        button.anchor(top: nil, leading: topview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.xscalevalue(), bottom: 0, right: 0),size: .init(width: buttonheight, height: buttonheight))
        button.centerYAnchor.constraint(equalTo: topview.centerYAnchor,constant: UIApplication.shared.statusBarFrame.height/2).isActive = true
        button.addTarget(self, action: #selector(showmenu), for: .touchUpInside)
    }
    @objc func showmenu(){
        self.circularviewanchor.leading?.constant = -539.calcvaluex()
        
        
       // self.innercircularviewanchor.constant = -180.xscalevalue()
        self.colorcircularviewanchor.leading?.constant = -439.xscalevalue()
        self.containeranchor.leading?.constant = expandwidth
        self.outerviewanchor.leading?.constant = 26.xscalevalue()
        self.outerviewanchor.trailing?.constant = 0
        self.containeranchor.trailing?.constant = -26.xscalevalue()
        self.labelanchor.leading?.constant = expandwidth
        self.button.isHidden = true
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            if self.mainbuttons[self.previousmainmenuindex].previousIndex != nil{
            self.newbuttons[self.previousmainmenuindex][ self.mainbuttons[self.previousmainmenuindex].previousIndex! ].arc?.isHidden = false
            }
            
        }
        table.reloadData()
    }
    func showmenu2(){
        self.circularviewanchor.leading?.constant = -539.calcvaluex()
        
        
       // self.innercircularviewanchor.constant = -180.xscalevalue()
        //self.colorcircularviewanchor.constant = -439.xscalevalue()
        self.containeranchor.leading?.constant = expandwidth
        self.outerviewanchor.leading?.constant = 26.xscalevalue()
        self.outerviewanchor.trailing?.constant = 0
        self.containeranchor.trailing?.constant = -26.xscalevalue()
        //self.labelanchor.leading?.constant = expandwidth
        self.button.isHidden = true
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            if self.mainbuttons[self.previousmainmenuindex].previousIndex != nil{
            self.newbuttons[self.previousmainmenuindex][ self.mainbuttons[self.previousmainmenuindex].previousIndex! ].arc?.isHidden = false
            }
            
        }
    }
}
class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        outerview.isHidden = false
       // self.circularviewanchor.constant = -self.view.frame.width
       // self.newbuttons[0].arc?.isHidden = true
        
        self.innercircularviewanchor.constant = -self.view.frame.width
       // self.colorcircularviewanchor.constant = -self.view.frame.width
        self.containeranchor.leading?.constant = 26.xscalevalue()
        //self.containeranchor.width?.constant = self.view.frame.width*0.33
        self.outerviewanchor.leading?.constant = 18.xscalevalue()
        self.outerviewanchor.trailing?.constant = -26.xscalevalue()
        self.containeranchor.trailing?.constant = -self.view.frame.width*(1-0.33)

        self.labelanchor.leading?.constant = self.button.frame.width + (26.xscalevalue()*2)
        table.collectionViewLayout.invalidateLayout()
        DispatchQueue.main.async {
            if self.mainbuttons[self.previousmainmenuindex].previousIndex != nil {
                
                self.newbuttons[self.previousmainmenuindex][self.mainbuttons[self.previousmainmenuindex].previousIndex!].arc?.isHidden = true
                
            }
            collectionView.reloadData()
        }
        
        
            self.button.isHidden = false

        UIView.animate(withDuration: 0.4, animations: {

            self.view.layoutIfNeeded()
        }) { (_) in
            //
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! NewsCell
        cell.backgroundColor = .white
        cell.layer.cornerRadius = 16
        cell.layer.addShadow()
        if indexPath.item == 0{
            cell.didscroll = true
        }
        else{
            cell.didscroll = false
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 24
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return .init(width: table.frame.width-10, height: collectionView.frame.height/6.5)
    }
    let circularview = UIView()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        } else {
            
        }
        return .lightContent
    }
    let stackview = UIStackView()
    var newbuttons = [[SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_new_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_new_normal"),title: "最新消息".localized),SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_product_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_product_normal"),title: "產品介紹".localized),SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_application_normal"), deselectedimage: #imageLiteral(resourceName: "ic_application_normal-1"),title: "應用資訊".localized),SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_success_story_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_success_story_normal"),title: "應用產業".localized)],[SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_customer_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_customer_normal"),title: "客戶管理".localized),SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_quotation_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_tab_spec_normal"),title: "訪談管理".localized),SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_folder_everyone_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_tab_folder_normal"),title: "業務檔案")],[SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_product_pressed2"), deselectedimage: #imageLiteral(resourceName: "ic_product_normal2"),title: "產品管理"),SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_content_work"), deselectedimage: #imageLiteral(resourceName: "ic_service_normal"),title: "報修服務"),SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_date_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_date_normal"),title: "工作排程"),SubMenuButton(selectedimage: #imageLiteral(resourceName: "ic_faq_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_faq_normal"),title: "常見問題")]]
    var labelanchor:AnchoredConstraints!
    let innercircularview = UIView()
    let colorcircularview = UIView()
    var mainbuttons = [Menubutton(selectedimage: #imageLiteral(resourceName: "ic_show_normal_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_show_normal"),title: "銷售展示".localized),Menubutton(selectedimage: #imageLiteral(resourceName: "ic_business_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_business_normal"),title: "業務管理".localized),Menubutton(selectedimage: #imageLiteral(resourceName: "ic_after_service_pressed"), deselectedimage: #imageLiteral(resourceName: "ic_after_service_normal"),title: "售後服務".localized)]
    var circularviewanchor : AnchoredConstraints!
    var innercircularviewanchor : NSLayoutConstraint!
    var colorcircularviewanchor : AnchoredConstraints!
    var containeranchor:AnchoredConstraints!
    let topview = UIView()
    let label = UILabel()
    let button = UIButton(type: .system)
    var menushow = false
    let outerview = OuterView()
    var outerviewanchor:AnchoredConstraints!
    var topviewanchor:AnchoredConstraints!
    var outerview2anchor:AnchoredConstraints!
    func setupcircularview() {
        circularview.backgroundColor = .white
        //-539.calcvaluex()
        view.addSubview(circularview)
        //circularview.centerXAnchor.constraint(equalTo: view.leadingAnchor,constant: -200 * xscale).isActive = true
        circularviewanchor = circularview.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: -836.calcvaluex(), bottom: 0, right: 0),size: .init(width: 836.calcvaluex(), height: 836.calcvaluex()))
        circularview.centerYInSuperview()
//        circularviewanchor = circularview.centerXAnchor.constraint(equalTo: view.leadingAnchor,constant: -view.frame.width)
//        circularviewanchor.isActive = true
//        circularview.widthAnchor.constraint(equalTo: view.heightAnchor,constant: 40).isActive = true
//        circularview.heightAnchor.constraint(equalTo: view.heightAnchor,constant: 40).isActive = true
//        circularview.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        circularview.layer.addcircleshadow()

//        view.addSubview(innercircularview)
//        innercircularview.backgroundColor = .white
//        innercircularview.layer.cornerRadius = view.frame.height/2
//        innercircularview.layer.addcircleshadow()
//        innercircularview.translatesAutoresizingMaskIntoConstraints = false
//        innercircularviewanchor = innercircularview.centerXAnchor.constraint(equalTo: view.leadingAnchor,constant: -180.xscalevalue())
//        innercircularviewanchor.isActive = true
//        innercircularview.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//        //let innerheight = view.frame.height/1.45
//        innercircularview.heightAnchor.constraint(equalTo: circularview.heightAnchor,multiplier: 1/1.29).isActive = true
//        innercircularview.widthAnchor.constraint(equalTo: circularview.heightAnchor,multiplier: 1/1.29).isActive = true
//        innercircularview.isUserInteractionEnabled = true

        view.addSubview(colorcircularview)
        colorcircularview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        colorcircularview.isUserInteractionEnabled = true
        colorcircularviewanchor = colorcircularview.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: -439.calcvaluex(), bottom: 0, right: 0),size: .init(width: 636.calcvaluex(), height: 636.calcvaluex()))
        colorcircularview.centerYInSuperview()
        colorcircularview.layer.borderColor = UIColor.white.cgColor
        colorcircularview.layer.borderWidth = 8.calcvaluex()
        colorcircularview.layer.addcircleshadow()
//        colorcircularviewanchor = colorcircularview.centerXAnchor.constraint(equalTo: view.leadingAnchor,constant: -180.xscalevalue())
//        colorcircularviewanchor.isActive = true
//        colorcircularview.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//        //let colorheight = view.frame.height/1.48
//        colorcircularview.heightAnchor.constraint(equalTo: circularview.heightAnchor,multiplier: 1/1.32).isActive = true
//        colorcircularview.widthAnchor.constraint(equalTo: circularview.heightAnchor,multiplier: 1/1.32).isActive = true
        
        stackview.axis = .vertical
        stackview.distribution = .fillEqually
        stackview.spacing = 50.calcvaluey()
        for i in 0..<mainbuttons.count {
            let mainbutton = MainMenuButton()
            mainbutton.imageview.image = mainbuttons[i].deselectedimage?.withRenderingMode(.alwaysTemplate)
            mainbutton.tag = i
            mainbutton.sublabel.text = mainbuttons[i].title
            mainbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(mainmenuselected)))
            
            mainbuttons[i].imageview = mainbutton
            
            stackview.addArrangedSubview(mainbutton)
        }
        colorcircularview.addSubview(stackview)
        
        stackview.anchor(top: colorcircularview.topAnchor, leading: nil, bottom: colorcircularview.bottomAnchor, trailing: colorcircularview.trailingAnchor,padding: .init(top: 141.calcvaluey(), left: 0, bottom: 140.calcvaluey() , right: 91.calcvaluex()),size: .init(width: 72.calcvaluex(), height: 0))
        

        
        
        
    }
    var previousmainmenuindex: Int!
    var currentmainmenuindex:Int!
    @objc func mainmenuselected(sender:UITapGestureRecognizer){
        print(sender.view!.tag)
         currentmainmenuindex = sender.view!.tag
        if previousmainmenuindex != nil {
            mainbuttons[previousmainmenuindex].imageview?.imageview.image = mainbuttons[previousmainmenuindex].deselectedimage?.withRenderingMode(.alwaysTemplate)
            mainbuttons[previousmainmenuindex].imageview?.sublabel.textColor = UIColor.white.withAlphaComponent(0.5)
        for i in newbuttons[previousmainmenuindex] {
            i.imageview?.removeFromSuperview()
            i.arc?.removeFromSuperlayer()
        }
        }


        //UIView.setAnimationsEnabled(false)
        positionViewsAlongArc()
       //self.view.layoutIfNeeded()
        mainbuttons[sender.view!.tag].imageview?.imageview.image = mainbuttons[sender.view!.tag].selectedimage
        mainbuttons[sender.view!.tag].imageview?.sublabel.textColor = .black
        
        self.circularviewanchor.leading?.constant = -539.calcvaluex()
       self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.4, animations: {
            //
        }) { (_) in
            if sender.view?.tag == 1{
                let m = self.addarc(startangle: -55, endAngle: -35)
                self.newbuttons[self.currentmainmenuindex][0].arc = m
                self.newbuttons[self.currentmainmenuindex][0].arc?.isHidden = true
                let b = self.addarc(startangle: -12, endAngle: 8)
                self.newbuttons[self.currentmainmenuindex][1].arc = b
                self.newbuttons[self.currentmainmenuindex][1].arc?.isHidden = true

                let c = self.addarc(startangle: 35, endAngle: 55)
                self.newbuttons[self.currentmainmenuindex][2].arc = c
                self.newbuttons[self.currentmainmenuindex][2].arc?.isHidden = true
            }
            else{
            let m = self.addarc(startangle: -55, endAngle: -35)
            self.newbuttons[self.currentmainmenuindex][0].arc = m
            self.newbuttons[self.currentmainmenuindex][0].arc?.isHidden = true
            let b = self.addarc(startangle: -30, endAngle: -10)
            self.newbuttons[self.currentmainmenuindex][1].arc = b
            self.newbuttons[self.currentmainmenuindex][1].arc?.isHidden = true

            let c = self.addarc(startangle: 10, endAngle: 30)
            self.newbuttons[self.currentmainmenuindex][2].arc = c
            self.newbuttons[self.currentmainmenuindex][2].arc?.isHidden = true

            let d = self.addarc(startangle: 35, endAngle: 55)

            self.newbuttons[self.currentmainmenuindex][3].arc = d
            self.newbuttons[self.currentmainmenuindex][3].arc?.isHidden = true
            }
            if self.mainbuttons[self.currentmainmenuindex].previousIndex != nil {
               
               
                self.newbuttons[self.currentmainmenuindex][self.mainbuttons[self.currentmainmenuindex].previousIndex!].arc?.isHidden = false
                self.newbuttons[self.currentmainmenuindex][self.mainbuttons[self.currentmainmenuindex].previousIndex!].imageview?.imageview.image = self.newbuttons[self.currentmainmenuindex][self.mainbuttons[self.currentmainmenuindex].previousIndex!].selectedimage
                self.newbuttons[self.currentmainmenuindex][self.mainbuttons[self.currentmainmenuindex].previousIndex!].imageview?.sublabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
                
            }
        }
//        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//
//
//        }, completion: {
//            _ in
//
//
//        })
        //UIView.setAnimationsEnabled(true)
        self.previousmainmenuindex = self.currentmainmenuindex
        
        
    }
    var table : UICollectionView!
    var player:AVPlayer!
    var playerLayer:AVPlayerLayer!
    func playvideo(){
        if let filePath = Bundle.main.path(forResource: "PIM", ofType:"mp4") {
            let filePathUrl = NSURL.fileURL(withPath: filePath)
            player = AVPlayer(url: filePathUrl)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.view.frame
            playerLayer.videoGravity = AVLayerVideoGravity.resize
            NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: nil) { (_) in
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            }
            self.view.layer.addSublayer(playerLayer)
            
        }
    }
    let searchbar = NewTextField()
    let newimageview = UIImageView()
    let alphaview = UIView()
    var newimageviewanchor:AnchoredConstraints!
    let outerview2 = OuterView2()
    let productmanager = ProductManagementController()
    var productmangeranchor:AnchoredConstraints!
    
    let maintaincontroller = MaintainController()
    var maintaincontrolleranchor : AnchoredConstraints!
    
    let scheduleworkcontroller = ScheduleWorkController()
    var scheduleworkcontrolleranchor:AnchoredConstraints!
    
    let frequentquestioncontroller = FrequentQuestionController()
    var frequentquestioncontrolleranchor:AnchoredConstraints!
    
    let salemanagementcontroller = SalesManagementController()
    var salemanagementcontrolleranchor:AnchoredConstraints!
    
    let priceManagementController = PriceManagementController()
    var priceManagementControlleranchor:AnchoredConstraints!
    
    let salesfilemangementcontroller = SalesFileManagementController()
    var sailesfilemanagementcontrolleranchor:AnchoredConstraints!
    
    let productIntroController = ProductIntroController()
    var productIntroControlleranchor:AnchoredConstraints!
    
    let newsController = NewsController()
    
    var newsControlleranchor:AnchoredConstraints!
    
    
    let bussinessController = BussinessController()
    
    var bussinessControlleranchor:AnchoredConstraints!
    
    let applicationController = ApplicationController()
    var applicationControlleranchor:AnchoredConstraints!
    var enterBefore = false
    
    func getUserData(id:String){
        
        NetworkCall.shared.getCall(parameter: "api-victor/v1/users/\(id)", decoderType: UserData.self) { (json) in
            if let json = json{
            do{
                let encode = try JSONEncoder().encode(json)
                UserDefaults.standard.saveUserData(data: encode, id: id)
            
                UserDefaults.standard.saveChosenCurrency(text: json.data?.attributes?.currencies.first ?? "")
            }
            catch{
                
            }
            }
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !enterBefore {
        player.play()
            enterBefore = true
        }
        getUserData(id: UserDefaults.standard.getUserId())
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    var previousIndex : Int?
    var previousMainIndex : Int?
    var previousAnchor:AnchoredConstraints!
    var previousController:UIViewController!
    
    func getInternet(){

        if #available(iOS 13.0, *) {
            if let statusBarManager = UIApplication.shared.keyWindow?.windowScene?.statusBarManager,
                let localStatusBar = statusBarManager.value(forKey: "createLocalStatusBar") as? NSObject,
                let statusBar = localStatusBar.value(forKey: "statusBar") as? NSObject,
                let _statusBar = statusBar.value(forKey: "_statusBar") as? UIView,
                let currentData = _statusBar.value(forKey: "currentData")  as? NSObject,
                let celluar = currentData.value(forKey: "cellularEntry") as? NSObject,
                let wifi = currentData.value(forKey: "wifiEntry") as? NSObject,
                let signalStrength = celluar.value(forKey: "displayValue") as? Int ,
                let wifiStrength = wifi.value(forKey: "rawValue") as? Int{
                print(77,wifiStrength)
            }
        } else {
            let app = UIApplication.shared
            if let mt = app.value(forKeyPath: "statusBar") as? NSObject,let foreground = mt.value(forKey: "foregroundView") as? UIView{
                for i in foreground.subviews {
                    print(i)
                    if i.isKind(of: NSClassFromString("UIStatusBarDataNetworkItemView")!) {
  
                       print( i.value(forKey:"wifiStrengthRaw"))
                    }
                    
                }
            }
            
            
//            if let mt = app.value(forKey: "statusBar" ) as? NSObject,let ht = mt.value(forKeyPath: "statusBar") as? NSObject, let zt = ht.value(forKeyPath: "foregroundView") as? UIView{
//
//
//                        let subviews = zt.subviews
//                                    for i in subviews{
//                                        print(i)
////                                        if i.isKind(of: NSClassFromString("_UIStatusBarWifiSignalView")!) {
////                                            print((i.value(forKey: "_numberOfActiveBars") as? Int))
////
////                                        }
////                                        else if i.isKind(of: )
//
//                                    }
//
//
//
//
//
//
//            }

            
//            if ((mt.value(forKeyPath: "_statusBar") as? NSObject)?.isKind(of: NSClassFromString("UIStatusBar_Modern")!))! {
//               let gt = mt.value(forKeyPath: "statusBar") as? NSObject)?.value(forKeyPath: "foregroundView") as! UIView
//            let subviews = gt.subviews
//            for i in subviews{
//                if i.isKind(of: NSClassFromString("_UIStatusBarWifiSignalView")!) {
//                    print((i.value(forKey: "_numberOfActiveBars") as? Int))
//                    break
//                }
//            }
//            }
//            else{
//
//            }
        }
        
//    let ct = CTGetSignalStrength()
//        print(ct)
        
    }
//    func getSignalStrength()->Int{
//           var result : Int = 0
//           //int CTGetSignalStrength();
//           let libHandle = dlopen ("/System/Library/Frameworks/CoreTelephony.framework/CoreTelephony", RTLD_NOW)
//           let CTGetSignalStrength2 = dlsym(libHandle, "CTGetSignalStrength")
//  
//           typealias CFunction = @convention(c) () -> Int
//
//           if (CTGetSignalStrength2 != nil) {
// 
//               let fun = unsafeBitCast(CTGetSignalStrength2!, to: CFunction.self)
//
//               let result = fun()
//                          print("!!!!result \(fun())")
//               return result;
// 
//           }
//        return -1
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //getSignalStrength()
        getInternet()
        playvideo()
        addNavBar()
        let slideinview = UIView()
        view.addSubview(slideinview)
        slideinview.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        slideinview.anchor(top: topview.bottomAnchor, leading: topview.leadingAnchor, bottom: view.bottomAnchor, trailing: topview.trailingAnchor)
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)

        
        setupcircularview()
        self.view.layoutIfNeeded()
        
        
        circularview.layer.cornerRadius = circularview.frame.height/2
        innercircularview.layer.cornerRadius = innercircularview.frame.height/2
        colorcircularview.layer.cornerRadius = colorcircularview.frame.height/2
        //previousselectedindex = 0
        circularview.isUserInteractionEnabled = true
        
      //  if #available(iOS 13.0, *) {
            let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
            statusBarView.backgroundColor = .black
           statusBarView.alpha = 0.25
            view.addSubview(statusBarView)
//        } else {
//            // Fallback on earlier versions
//        }
        registerLanguageChange()
    }
    override func changeLanguage() {
        for i in 0..<mainbuttons.count {
            if let m = stackview.arrangedSubviews[i] as? MainMenuButton {
                m.sublabel.text = mainbuttons[i].title
            }
        }
        
        for index in 0 ..< buttonArray.count {
            buttonArray[index].sublabel.text = newbuttons[currentmainmenuindex][index].title
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
        self.player.pause()
        //NotificationCenter.default.removeObserver(self)
    }
    @objc func hideouterview2(){
        

        self.outerview2anchor.leading?.constant = 0
        //self.outerview2.isHidden = true
        //self.outerview2.layer.zPosition = 0
        showmenu2()

    }
    @objc func slidenewview(){

        let viewd = ProductPresentController()
        viewd.view.backgroundColor = .white
        
            self.navigationController?.pushViewController(viewd, animated: true)
        
            
        
        
    }
    func createButton(size:CGFloat)->SubButton{
        let button = SubButton()
        
        button.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: size).isActive = true
        button.heightAnchor.constraint(equalToConstant: 75.calcvaluey()).isActive = true
        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectsection)))
        button.isUserInteractionEnabled = true
        return button
    }
    func addarc(startangle:CGFloat,endAngle:CGFloat) -> CAShapeLayer{
        let bb = UIBezierPath()
        bb.addArc(withCenter: circularview.center, radius: circularview.frame.width/2, startAngle: startangle * .pi / 180, endAngle: endAngle * .pi / 180, clockwise: true)
        let shapelayer = CAShapeLayer()
        shapelayer.lineWidth = 6
        shapelayer.strokeColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        shapelayer.lineCap = .round
        shapelayer.fillColor = UIColor.clear.cgColor
        shapelayer.path = bb.cgPath
        view.layer.addSublayer(shapelayer)
        return shapelayer
    }
    var previousselectedindex : Int?
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        
        

    }
    @objc func selectsection(sender:UITapGestureRecognizer){
        
        
        sender.isEnabled = false
        let current = newbuttons[currentmainmenuindex][sender.view!.tag]
        
        let previousindex = mainbuttons[currentmainmenuindex].previousIndex
        if previousindex == nil{
            current.arc?.isHidden = false
            current.imageview?.imageview.image =  current.selectedimage
            current.imageview?.sublabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        }
        else{
        if previousindex != sender.view!.tag {
            let previous = newbuttons[currentmainmenuindex][previousindex!]
            previous.arc?.isHidden = true
            previous.imageview?.imageview.image = previous.deselectedimage?.withRenderingMode(.alwaysTemplate)
            previous.imageview?.sublabel.textColor = #colorLiteral(red: 0.7349141836, green: 0.7252627015, blue: 0.7209410667, alpha: 1)

            current.arc?.isHidden = false
            current.imageview?.imageview.image =  current.selectedimage
            current.imageview?.sublabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)


            
        }
        }
        for i in 0..<mainbuttons.count {
            mainbuttons[i].previousIndex = nil
        }
        mainbuttons[currentmainmenuindex].previousIndex = sender.view?.tag
        if previousMainIndex == currentmainmenuindex {
            if previousIndex != sender.view!.tag {
                slideanimation(type: sender.view!.tag)
            }
        }
        else{
            slideanimation(type: sender.view!.tag)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            sender.isEnabled = true
        }
        
        
        
    }

    func returnselectedView(adding:Bool,type:Int) -> (AnchoredConstraints?,UIViewController?) {
        if currentmainmenuindex == 1 {
            if type == 0 {
                if adding{
                        addChild(salemanagementcontroller)
                    salemanagementcontroller.mainview = self
                        view.addSubview(salemanagementcontroller.view)
                    view.insertSubview(salemanagementcontroller.view, belowSubview: circularview)
                        salemanagementcontrolleranchor = salemanagementcontroller.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                        //salemanagementcontroller.maincontroller = self
                    self.view.layoutIfNeeded()
                }
                return (salemanagementcontrolleranchor,salemanagementcontroller)
            }
            else if type == 1{
                if adding{
                        addChild(priceManagementController)
                    priceManagementController.mainview = self
                        view.addSubview(priceManagementController.view)
                    view.insertSubview(priceManagementController.view, belowSubview: circularview)
                        priceManagementControlleranchor = priceManagementController.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                        //salemanagementcontroller.maincontroller = self
                    self.view.layoutIfNeeded()
                }
                return (priceManagementControlleranchor,priceManagementController)
            }
            else {
                if adding{
                        addChild(salesfilemangementcontroller)
                    //salemanagementcontroller.mainview = self
                        view.addSubview(salesfilemangementcontroller.view)
                    view.insertSubview(salesfilemangementcontroller.view, belowSubview: circularview)
                        sailesfilemanagementcontrolleranchor = salesfilemangementcontroller.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                        //salemanagementcontroller.maincontroller = self
                    self.view.layoutIfNeeded()
                }
                return (sailesfilemanagementcontrolleranchor,salesfilemangementcontroller)
            }
        }
        else if currentmainmenuindex == 0{
            if type == 1{
                
                if adding{
                    print(7899)
                        addChild(productIntroController)
                    //salemanagementcontroller.mainview = self
                        view.addSubview(productIntroController.view)
                    view.insertSubview(productIntroController.view, belowSubview: circularview)
                        productIntroControlleranchor = productIntroController.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                        //salemanagementcontroller.maincontroller = self
                    self.view.layoutIfNeeded()
                }
                return (productIntroControlleranchor,productIntroController)
            }
            else if type == 0{
                if adding{
                    print(7899)
                        addChild(newsController)
                    newsController.originCon = self
                        view.addSubview(newsController.view)
                    view.insertSubview(newsController.view, belowSubview: circularview)
                    newsControlleranchor = newsController.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                        //salemanagementcontroller.maincontroller = self
                    self.view.layoutIfNeeded()
                }
                return (newsControlleranchor,newsController)
            }
            else if type == 3{
                if adding{
                    
                        addChild(bussinessController)
                    bussinessController.originCon = self
                        view.addSubview(bussinessController.view)
                    view.insertSubview(bussinessController.view, belowSubview: circularview)
                    bussinessControlleranchor = bussinessController.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                        //salemanagementcontroller.maincontroller = self
                    self.view.layoutIfNeeded()
                }
                return (bussinessControlleranchor,bussinessController)
            }
            else{
                if adding{
                    
                        addChild(applicationController)
                    applicationController.originCon = self
                        view.addSubview(applicationController.view)
                    view.insertSubview(applicationController.view, belowSubview: circularview)
                    applicationControlleranchor = applicationController.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                        //salemanagementcontroller.maincontroller = self
                    self.view.layoutIfNeeded()
                }
                return (applicationControlleranchor,applicationController)
            }
        }
        else{
        if type == 0 {
            if adding {
            addChild(productmanager)
            view.addSubview(productmanager.view)
            view.insertSubview(productmanager.view, belowSubview: circularview)
            productmanager.delegate = self
            productmangeranchor = productmanager.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
            self.view.layoutIfNeeded()
            }
            return (productmangeranchor,productmanager)
            


        }
        else if type == 1{
            if adding{
                    addChild(maintaincontroller)
                    view.addSubview(maintaincontroller.view)
                view.insertSubview(maintaincontroller.view, belowSubview: circularview)
                    maintaincontroller.mainview = self
                    maintaincontrolleranchor = maintaincontroller.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                self.view.layoutIfNeeded()
            }
            return (maintaincontrolleranchor,maintaincontroller)
            

        }
        else if type == 2{
            if adding{
                    addChild(scheduleworkcontroller)
                    view.addSubview(scheduleworkcontroller.view)
                view.insertSubview(scheduleworkcontroller.view, belowSubview: circularview)
                    scheduleworkcontrolleranchor = scheduleworkcontroller.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                    scheduleworkcontroller.maincontroller = self
                self.view.layoutIfNeeded()
            }
            return (scheduleworkcontrolleranchor,scheduleworkcontroller)
            

        }
        else {
            if adding{
                    addChild(frequentquestioncontroller)
                    view.addSubview(frequentquestioncontroller.view)
                view.insertSubview(frequentquestioncontroller.view, belowSubview: circularview)
                    frequentquestioncontrolleranchor = frequentquestioncontroller.view.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.leadingAnchor,size: .init(width: UIScreen.main.bounds.width, height: 0))
                    frequentquestioncontroller.maincontroller = self
                self.view.layoutIfNeeded()
            }
            return (frequentquestioncontrolleranchor,frequentquestioncontroller)
        }
        }
        return (nil,nil)
    }
    func slideanimation(type:Int){
        playerLayer.removeFromSuperlayer()
    //    if currentmainmenuindex == 0 {
//        if type == 0 {
//            if newimageviewanchor.trailing?.constant == 0 {
//                newimageviewanchor.trailing?.constant = -view.frame.width
//            }
//            topviewanchor.trailing?.constant = view.frame.width
//            //productmangeranchor.trailing?.constant = view.frame.width
//           // newimageviewanchor.trailing?.constant = view.frame.width
//        }
//        else{
//            if topviewanchor.trailing?.constant == view.frame.width {
//                topviewanchor.trailing?.constant = 0
//            }
//            //topviewanchor.trailing?.constant = 0
//        newimageviewanchor.trailing?.constant = 0
//        }
       // }
       // else{
            
            let (vc,_) = returnselectedView(adding: true, type: type)
            vc?.trailing?.constant = view.frame.width


            
            
            
       // }
        self.previousIndex = type
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: {
            _ in
            let (vc,vd) = self.returnselectedView(adding: false, type: type)
            if self.previousAnchor != nil{
                self.previousAnchor.trailing?.constant = 0
                self.previousController.removeFromParent()
                self.previousController.view.removeFromSuperview()
            }
//            self.view.sendSubviewToBack(vd!.view)
            
            self.previousAnchor = vc
            self.previousController = vd
            self.previousIndex = type
            self.previousMainIndex = self.currentmainmenuindex
            self.player.pause()
        })
    }
    var buttonArray = [SubButton]()
    func positionViewsAlongArc() {
      //  let degrees = (-135 / (CGFloat(newbutton.count + 2)))
        var degrees:[CGFloat]!

            if currentmainmenuindex == 0 {
                degrees = [-45,-15,15,45]
            }
            else if currentmainmenuindex == 1 {
                degrees = [-45,0,42]
            }
            else{
                degrees = [-45,-15,15,45]
            }
        
        for i in 0 ..< degrees.count {
            buttonArray = []
            let button = createButton(size: 80.calcvaluex())
        button.tag = i
            button.sublabel.text = self.newbuttons[currentmainmenuindex][i].title
            buttonArray.append(button)
        //button.image = newbuttons[i].deselectedimage
        
        //button.setImage(selectdimage[i], for: .selected)
          self.view.addSubview(button)
            //view.insertSubview(button, belowSubview: colorcircularview)
          // use trig to compute offsets from center button
        let hOffset = circularview.bounds.height/2 * cos(degrees[i] * .pi / 180)
          let vOffset = (circularview.bounds.height/2 * sin(degrees[i] * .pi / 180))
          // set new button's center relative to the center button's
          // center using centerX and centerY anchors and offsets
          button.centerXAnchor.constraint(equalTo: circularview.centerXAnchor, constant: hOffset - (circularview.frame.height/2 - colorcircularview.frame.height/2)/2 ).isActive = true
        if i == 0 {
            button.centerYAnchor.constraint(equalTo: circularview.centerYAnchor, constant: vOffset + 25).isActive = true
        }
        else if i == 3 {
            button.centerYAnchor.constraint(equalTo: circularview.centerYAnchor, constant: vOffset - 25).isActive = true
        }
        else{
            if currentmainmenuindex == 1{
                button.centerYAnchor.constraint(equalTo: circularview.centerYAnchor, constant: vOffset - 25).isActive = true
            }
            else{
            button.centerYAnchor.constraint(equalTo: circularview.centerYAnchor, constant: vOffset).isActive = true
            }
        }
            
            newbuttons[currentmainmenuindex][i].imageview = button
            newbuttons[currentmainmenuindex][i].imageview?.imageview.image = newbuttons[currentmainmenuindex][i].deselectedimage?.withRenderingMode(.alwaysTemplate)
      }
        
        
    }
    func hidecircle(){
        self.circularviewanchor.leading?.constant = -836.calcvaluex()
        
         
        // self.innercircularviewanchor.constant = -self.view.frame.width
        self.colorcircularviewanchor.leading?.constant = -636.calcvaluex()
        
        DispatchQueue.main.async {
            if self.mainbuttons[self.previousmainmenuindex].previousIndex != nil {
                
                self.newbuttons[self.previousmainmenuindex][self.mainbuttons[self.previousmainmenuindex].previousIndex!].arc?.isHidden = true
                
            }
            
        }
        
        
        

        UIView.animate(withDuration: 0.4, animations: {

            self.view.layoutIfNeeded()
        }) { (_) in
            //
        }
    }
    func showcircle(){
        self.circularviewanchor.leading?.constant = -539.calcvaluex()
        
        
        //self.innercircularviewanchor.constant = -180.xscalevalue()
        self.colorcircularviewanchor.leading?.constant = -439.calcvaluex()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            if self.mainbuttons[self.previousmainmenuindex].previousIndex != nil{
            self.newbuttons[self.previousmainmenuindex][ self.mainbuttons[self.previousmainmenuindex].previousIndex! ].arc?.isHidden = false
            }
            
        }
    }
}
extension ViewController : ProductManagmentDelegate {
    func collapse() {
        
        productmanager.containeranchor.leading?.constant = 333.calcvaluex()
        productmanager.titleviewanchor.leading?.constant = 333.calcvaluex()
        productmanager.dataview.collectionViewLayout.invalidateLayout()
        showcircle()

        
        

       
    }
    
    func expand() {

        productmanager.containeranchor.leading?.constant = 48.calcvaluex()
        productmanager.selectionview.collectionViewLayout.invalidateLayout()
        productmanager.dataview.collectionViewLayout.invalidateLayout()
        hidecircle()
    }
}

