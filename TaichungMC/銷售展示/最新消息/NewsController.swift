//
//  NewsController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/27.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class NewsController: SampleController {
    let leftview = UIView()
    let searchText = SearchTextField()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var leftviewanchor:AnchoredConstraints!
    
    var prevIndex : Int? = nil
    var rightView = UIView()

    var rightViewAnchor:AnchoredConstraints!
    var rightTopViewShadow = UIView()
    var rightTopView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let bottomView = UIView()
    let bottomWebView = UIWebView()
    let expandPdfButton = UIButton(type: .custom)
    let goToPageButton = UIButton(type: .custom)
    var data = [NewsMessage]()
    var bottomViewAnchor : AnchoredConstraints?
    override func viewDidLoad() {
        super.viewDidLoad()
        settingButton.isHidden = false
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        titleview.text = "最新消息"
        titleviewanchor.leading?.constant = 350.calcvaluex()
        view.addSubview(leftview)
        //leftview.backgroundColor = .red
        leftviewanchor = leftview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 323.calcvaluex(), bottom: 0, right: 0))
        
        
        leftview.addSubview(searchText)
        searchText.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        searchText.anchor(top:leftview.topAnchor , leading: leftview.leadingAnchor, bottom: nil, trailing: leftview.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 10.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        searchText.layer.cornerRadius = 46.calcvaluey()/2
        searchText.addShadowColor(opacity: 0.05)
        searchText.attributedPlaceholder = NSAttributedString(string: "搜尋最新消息", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchText.isHidden = true
        leftview.addSubview(tableview)
        tableview.anchor(top: leftview.topAnchor, leading: leftview.leadingAnchor, bottom: leftview.bottomAnchor, trailing: leftview.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        
        
        view.addSubview(rightView)
        rightView.backgroundColor = .clear
        rightView.anchor(top: leftview.topAnchor, leading: tableview.trailingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: -26.calcvaluex(), bottom: 0, right:0),size: .init(width: 654.calcvaluex(), height: 0))
        
        
        rightTopViewShadow.addShadowColor(opacity: 0.1)
        rightTopViewShadow.backgroundColor = .clear
        rightView.addSubview(rightTopViewShadow)
        
        rightTopViewShadow.anchor(top: rightView.topAnchor, leading: rightView.leadingAnchor, bottom: nil, trailing: rightView.trailingAnchor,padding: .init(top: 26.calcvaluex(), left: 18.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 122.calcvaluey()))
        rightTopView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)

        rightTopView.layer.cornerRadius = 15.calcvaluex()
        rightTopViewShadow.addSubview(rightTopView)
        rightTopView.isHidden = true
        rightTopView.showsHorizontalScrollIndicator = false
        rightTopView.fillSuperview()
        rightTopView.delegate = self
        rightTopView.dataSource = self
        (rightTopView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = .horizontal
        rightTopView.register(NewsVideoCell.self, forCellWithReuseIdentifier: "cell")
        rightTopView.contentInset = .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex())
        
        
        rightView.addSubview(bottomView)
        bottomView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        bottomView.isHidden = true
        bottomView.layer.cornerRadius = 15.calcvaluex()
        bottomView.addShadowColor(opacity: 0.1)
        bottomViewAnchor =  bottomView.anchor(top: rightTopView.bottomAnchor, leading: rightTopView.leadingAnchor, bottom: nil, trailing: rightTopView.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 600.calcvaluey()))
        
        bottomWebView.backgroundColor = .clear

        bottomWebView.isHidden = true
        bottomView.addSubview(bottomWebView)
        bottomWebView.fillSuperview()
   
        

        bottomWebView.delegate = self
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        expandPdfButton.addTarget(self, action: #selector(expandPDF), for: .touchUpInside)

        view.addSubview(expandPdfButton)

        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
        

        rightView.addSubview(expandPdfButton)
        expandPdfButton.isHidden = true
        expandPdfButton.anchor(top: rightView.topAnchor, leading: nil, bottom: nil, trailing: rightView.trailingAnchor,padding: .init(top: 562.calcvaluey(), left: 0, bottom: 0, right: 44.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        
                goToPageButton.setImage(#imageLiteral(resourceName: "ic_fab_jump"), for: .normal)
                goToPageButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        goToPageButton.setImage(#imageLiteral(resourceName: "ic_fab_jump"), for: .normal)
                rightView.addSubview(goToPageButton)
        
                goToPageButton.layer.cornerRadius = 38.calcvaluex()/2
        goToPageButton.addTarget(self, action: #selector(turnPage), for: .touchUpInside)
        goToPageButton.anchor(top: expandPdfButton.bottomAnchor, leading: nil, bottom: nil, trailing: expandPdfButton.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        goToPageButton.isHidden = true
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    
    func fetchApi(){
        self.tableview.isHidden = true
        NetworkCall.shared.getCall(parameter: "api-or/v1/message_category/28", decoderType: NewsClas.self) { (json) in
            DispatchQueue.main.async {
                if (json?.data?.count ?? 0) > 0{
                
                    self.data = (json?.data?[0].messages ?? []).filter({ (mes) -> Bool in
                        return mes.isActive() && mes.active == 1
                    })
                    self.tableview.isHidden = false
                    print(331,self.data)
                self.tableview.reloadData()
                }
            }
            

        }
    }
    @objc func turnPage(){
        let con = TurnToViewController()
        con.modalPresentationStyle = .overFullScreen
        self.present(con, animated: false, completion: nil)
    }
    @objc func expandPDF(){
        let con = ExpandPDFController(text: expandFile)
        con.modalPresentationStyle = .overFullScreen
        self.present(con, animated: false, completion: nil)
    }
    func hideView(){
        self.rightTopView.isHidden = true
        self.bottomWebView.isHidden = true
        self.bottomView.isHidden = true
        self.expandPdfButton.isHidden = true
    }
    func unHideView(){
        self.rightTopView.isHidden = false
        self.bottomWebView.isHidden = false
        self.bottomView.isHidden = false
        self.expandPdfButton.isHidden = false
    }
    override func popview() {
        prevIndex = nil
        self.tableview.reloadData()
        self.view.layoutIfNeeded()
        
        originCon?.showcircle()
        titleviewanchor.leading?.constant = 350.calcvaluex()
        leftviewanchor.leading?.constant = 333.calcvaluex()
        leftviewanchor.trailing?.constant = 0
         
        UIView.animate(withDuration: 0.5, animations: {
           
            self.view.layoutIfNeeded()
        }) { (_) in
            self.hideView()
        }
        
        
    }
    
    var data_List = [String]() {
        didSet{
            self.rightTopView.reloadData()
        }
    }
    var expandFile : String = ""
}

extension NewsController:UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIWebViewDelegate{

    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        removeShadow(webView: webView)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data_List.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! NewsVideoCell
        cell.tag = indexPath.item
        let data = data_List[indexPath.item]
        if let url = URL(string: data) {
            if url.pathExtension == "mp4" {
                cell.button.isHidden = false
            }
            else{
                cell.button.isHidden = true
            }
            if url.pathExtension == "pdf" {
                cell.imgv.image = Thumbnail().pdfThumbnail(url: url, width: 123.calcvaluex())
            }
            else if url.pathExtension == "mp4"{
                cell.imgv.image = Thumbnail().getThumbnailImage(forUrl: url)
            }
            else{
                cell.imgv.sd_setImage(with: url, completed: nil)
            }
        }
       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 123.calcvaluex(), height: 74.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let con = ExpandVideoView()
        if let url = URL(string: data_List[indexPath.item]) {
            print(11,url.absoluteString)
            let request = URLRequest(url: url)
            con.webview.loadRequest(request)
        }
        
        con.modalPresentationStyle = .overFullScreen
        self.present(con, animated: false, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = NNewsCell(style: .default, reuseIdentifier: "cell")
        cell.topLabel.text = data[indexPath.row].getDetails()?.name
        cell.subLabel.text = data[indexPath.row].custom_active_date
        if let prev = prevIndex {
            if indexPath.row == prev {
                cell.setColor()
            }
            else{
                cell.unsetColor()
            }
        }
        else{
            cell.unsetColor()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        data_List = data[indexPath.row].get_SubValue()
        if data_List.count == 0{
            self.bottomViewAnchor?.top?.constant = -122.calcvaluey()
            self.rightTopView.isHidden = true
        }
        else{
            self.bottomViewAnchor?.top?.constant = 20.calcvaluey()
            self.rightTopView.isHidden = false
        }
        self.view.layoutIfNeeded()
        if let url = URL(string: data[indexPath.row].get_MainValue()) {
            print(21,url.absoluteString)
            expandFile = url.absoluteString
            var req = URLRequest(url: url)
            req.cachePolicy = URLRequest.CachePolicy.returnCacheDataElseLoad
            bottomWebView.loadRequest(req)
        }
        else{
            expandFile = "about:blank"
            let req = URLRequest(url: URL(string: "about:blank")!)
            bottomWebView.loadRequest(req)
        }
        

        prevIndex = indexPath.row
        originCon?.hidecircle()
        titleviewanchor.leading?.constant = 80.calcvaluex()
        leftviewanchor.leading?.constant = 26.calcvaluex()
        leftviewanchor.trailing?.constant = -628.calcvaluex()
        
        self.unHideView()
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.view.layoutIfNeeded()
        }) { (_) in
            self.tableview.reloadData()
                    
        }
    }
    
    func removeShadow(webView: UIWebView) {
        for subview:UIView in webView.scrollView.subviews {
            subview.layer.shadowOpacity = 0
            for subsubview in subview.subviews {
                subsubview.layer.shadowOpacity = 0
            }
        }
    }
    
    
}
protocol NewsDelegate {
    func showVideo(index:Int)
}
class NewsVideoCell: UICollectionViewCell {
    let imgv = UIImageView()
    let button = UIButton(type: .custom)
    var delegate:NewsDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        addSubview(imgv)
        imgv.clipsToBounds = true
        imgv.contentMode = .scaleAspectFill
        
        imgv.fillSuperview()
        imgv.layer.borderWidth = 1.calcvaluex()
        imgv.layer.borderColor = #colorLiteral(red: 0.8893501457, green: 0.8893501457, blue: 0.8893501457, alpha: 1)
        
        addSubview(button)
        button.setImage(#imageLiteral(resourceName: "ic_video_piay"), for: .normal)
        button.contentHorizontalAlignment = .fill
        button.contentVerticalAlignment = .fill

        button.centerInSuperview(size: .init(width: 48.calcvaluex(), height: 48.calcvaluex()))
//        button.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showVideo)))

    }
//    @objc func showVideo(){
//        delegate?.showVideo(index:self.tag)
//    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class NNewsCell:UITableViewCell{
    let vd = UIView()
    let topLabel = UILabel()
    let mc = UIView()
       let nd = UIView()
    let subLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
                contentView.addSubview(mc)
                mc.backgroundColor = .clear
                mc.addShadowColor(opacity: 0.05)
                mc.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 92.calcvaluey()))
        mc.centerYInSuperview()
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        vd.layer.cornerRadius = 15.calcvaluex()
        vd.clipsToBounds = true
        mc.addSubview(vd)
        vd.fillSuperview()
        
        vd.addSubview(nd)
         nd.backgroundColor =  #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
         nd.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
        
        topLabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        topLabel.text = "108年度股東常會召"
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        nd.addSubview(topLabel)
        topLabel.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 53.calcvaluey()/2))
        
        subLabel.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        subLabel.text = "10月20日 下午3:25"
        subLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(subLabel)
        subLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 53.calcvaluey()/2))
    }
    func setColor(){
        vd.backgroundColor = MajorColor().mainColor
        mc.addShadowColor(opacity: 0.3)
    }
    func unsetColor(){
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        mc.addShadowColor(opacity: 0.1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
