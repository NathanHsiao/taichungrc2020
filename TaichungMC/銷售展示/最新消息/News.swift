//
//  News.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/12/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation


struct NewsClas : Codable {
    var data : [News]?
}

struct News : Codable {
    var id : Int
    var details : [NewsDetails]?
    var messages : [NewsMessage]?
    var children : [News]?
    var title : String?
    func getDetails() -> NewsDetails? {
        
        let lang = UserDefaults.standard.getLanguage()
        
        if let firstDetail = details?.first(where: { (nt) -> Bool in
            if nt.language_code == "zh-TW" && lang == "zh-Hant" {
                return true
            }
            
            return nt.language_code == lang
            
        })
        {
            return firstDetail
        }
        return nil
    }

}

struct NewsDetails : Codable {
    var detail_id : Int?
    var field_id : Int?
    var active : Int
    var name : String?
    var language_code : String
}
struct DateFormat {
    func convertToTimeStamp(text:String) -> TimeInterval {
        var dfmatter = DateFormatter()
        dfmatter.dateFormat="yyyy-MM-dd"
        var date = dfmatter.date(from: text)
        var dateStamp:TimeInterval = date!.timeIntervalSince1970
        return dateStamp
    }
}
struct NewsMessage : Codable {
    var id : Int
    var custom_active_date : String?
    var active : Int
    var details : [NewsDetails]?
    var datas : [NewsData]?
    func isActive() -> Bool {
        if let dt = datas?.first(where: { (nt) -> Bool in
            return nt.field?.form_name == "active_range"
        }) {
            switch dt.value {
            case .arrayOFString(let ft):
                if let ft = ft{
                    var active : TimeInterval?
                    var end : TimeInterval?
                    let current = Date().timeIntervalSince1970
                    if ft.count == 1{
                        active = DateFormat().convertToTimeStamp(text: ft[0])
                        if current >= active ?? 0{
                            return true
                        }
                        else{
                            return false
                        }
                    }
                    else if ft.count == 2{
                        active = DateFormat().convertToTimeStamp(text: ft[0])
                        end = DateFormat().convertToTimeStamp(text: ft[1])
                        if current >= (active ?? 0) && current <= (end ?? 0){
                            return true
                        }
                        else{
                            return false
                        }
                    }
                
                
                
                

                }
            
            default :
                ()
            }
        }
        return true
    }
    func get_MainValue() -> String {
        
        if let dt = datas?.first(where: { (nt) -> Bool in
            if nt.language_code == "zh-TW" && UserDefaults.standard.getLanguage() == "zh-Hant" && nt.field?.form_name == "file_content" {
                return true
            }
            else if nt.language_code == UserDefaults.standard.getLanguage() && nt.field?.form_name == "file_content" {
                return true
            }
            return false
        }) {
            switch dt.value {
            case .string(let st) :
                return st ?? ""
            
            default :
                ()
            }
        }
        
        return ""
    }
    func get_SubValue() -> [String] {
        if let dt = datas?.first(where: { (nt) -> Bool in
            if nt.language_code == "zh-TW" && UserDefaults.standard.getLanguage() == "zh-Hant" && nt.field?.form_name == "file_list" {
                return true
            }
            else if nt.language_code == UserDefaults.standard.getLanguage() && nt.field?.form_name == "file_list" {
                return true
            }
            return false
        }) {
            switch dt.value {
            case .arrayString(let st) :
                var reString = [String]()
                
                if let mt = st {
                    for i in mt {
                        for (_,val) in i {
                            reString.append(val)
                        }
                    }
                    return reString
                }
            default :
                ()
            }
        }
        
        return []
    }
    
    
    func getDetails() -> NewsDetails? {
        
        let lang = UserDefaults.standard.getLanguage()
        
        if let firstDetail = details?.first(where: { (nt) -> Bool in
            if nt.language_code == "zh-TW" && lang == "zh-Hant" {
                return true
            }
            
            return nt.language_code == lang
            
        })
        {
            return firstDetail
        }
        return nil
    }
}
enum NewsValue : Codable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .arrayString(let x):
            try container.encode(x)
        case .arrayOFString(let x):
            try container.encode(x)
        }
        
        
    }
    
    case string(String?)
    case arrayString([[String:String]]?)
    case arrayOFString([String]?)
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String?.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode([[String:String]]?.self) {
            self = .arrayString(x)
            return
        }
        if let x = try? container.decode([String]?.self) {
            self = .arrayOFString(x)
            return
        }
        throw DecodingError.typeMismatch(FileType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }
}
struct NewsData : Codable {
    var data_id : Int
    var language_code : String
    var value : NewsValue?
    var field : NewsDataField?
    


}
struct NewsDataField : Codable {
    var field_id : Int
    var form_name : String
    var details : [NewsDetails]?
}
