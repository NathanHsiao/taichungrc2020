//
//  StockMatchController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/19/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit


class StockChooseTextField : UITextField {
    let arrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down").withRenderingMode(.alwaysTemplate))
    override init(frame: CGRect) {
        super.init(frame: frame)
        attributedPlaceholder = NSAttributedString(string: "請選擇", attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.5487020612, green: 0.5264604688, blue: 0.522194922, alpha: 1)])
        backgroundColor = #colorLiteral(red: 0.9727925658, green: 0.9729319215, blue: 0.9727620482, alpha: 1)
        layer.cornerRadius = 15.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        
        arrow.tintColor = #colorLiteral(red: 0.778042376, green: 0.7781553864, blue: 0.7780176401, alpha: 1)
        
        addSubview(arrow)
        arrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        arrow.centerYInSuperview()
        
        font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        textColor = #colorLiteral(red: 0.1371272504, green: 0.09268946201, blue: 0.0868159011, alpha: 1)
    }
    func isEnable(){
        arrow.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        backgroundColor = #colorLiteral(red: 0.9693912864, green: 0.9695302844, blue: 0.969360888, alpha: 1)
        attributedPlaceholder = NSAttributedString(string: "請選擇", attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.1371272504, green: 0.09268946201, blue: 0.0868159011, alpha: 1)])
    }
    func isDisable(){
        arrow.tintColor = #colorLiteral(red: 0.778042376, green: 0.7781553864, blue: 0.7780176401, alpha: 1)
        attributedPlaceholder = NSAttributedString(string: "請選擇", attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.5487020612, green: 0.5264604688, blue: 0.522194922, alpha: 1)])
        backgroundColor = #colorLiteral(red: 0.9727925658, green: 0.9729319215, blue: 0.9727620482, alpha: 1)
        text = nil
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum ChooseMode {
    case Series
    case Model
    case Shooting
    case Diameter
}
protocol StockChooseFieldDelegate {
    func showSelection(mode:ChooseMode)
}
class StockChooseField : UIView,UITextFieldDelegate {
    let nameLabel : UILabel = {
       let nL = UILabel()
        
        nL.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        nL.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return nL
    }()
    var mode : ChooseMode?
    let chooseField = StockChooseTextField()
    var delegate : StockChooseFieldDelegate?
    init(text:String,mode:ChooseMode) {
        super.init(frame: .zero)
        self.mode = mode
        nameLabel.text = text
        addSubview(nameLabel)
        nameLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        
        addSubview(chooseField)
        chooseField.anchor(top: nameLabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        chooseField.delegate = self
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if isSelected {
            if let mode = mode{
            delegate?.showSelection(mode: mode)
            }
        }
        return false
    }
    var isSelected = false{
        didSet{
            if isSelected {
                    setSelected()
            }
            else{
                setUnSelected()
            }
        }
    }
    func setSelected(){
        nameLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        chooseField.isEnable()
        
    }
    func setUnSelected(){
        nameLabel.textColor = #colorLiteral(red: 0.778042376, green: 0.7781553864, blue: 0.7780176401, alpha: 1)
        chooseField.isDisable()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class StockMatchController: UIViewController,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let vd = touch.view , let vde = self.vde{
            if vd.isDescendant(of: vde){
                return false
            }
        }
        return true
    }
    let vd = UIView()
    let xbutton = UIButton(type: .custom)
    let nameLabel : UILabel = {
       let nL = UILabel()
        nL.text = "加入機台"
        nL.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        nL.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        return nL
    }()
    let seriesField = StockChooseField(text: "系列",mode: .Series)
    let modelField = StockChooseField(text: "型號",mode: .Model)
    let shootingField = StockChooseField(text: "射座型式",mode: .Shooting)
    let diameterField = StockChooseField(text: "螺桿直徑",mode: .Diameter)
    var imageview = UIImageView()
    var imageviewtext : UILabel = {
    let lb = UILabel()
        lb.text = "可預覽機台"
        lb.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        lb.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return lb
    }()
    weak var con2 : InterviewController?
    var series = [Series]()
    var model = [Series]()
    let conButton = confirmButton(type: .custom)
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        vd.backgroundColor = .white
        vd.layer.cornerRadius = 15.calcvaluey()
        view.addSubview(vd)
        vd.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 580.calcvaluey()))
        
        
        vd.addSubview(xbutton)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small"), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        
        xbutton.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        xbutton.addTarget(self, action: #selector(popDismiss), for: .touchUpInside)
        
        vd.addSubview(nameLabel)
        nameLabel.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluex(), left: 0, bottom: 0, right: 0))
        nameLabel.centerXInSuperview()
        
        vd.addSubview(seriesField)
        seriesField.anchor(top: nameLabel.bottomAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 31.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 240.calcvaluex(), height: 90.calcvaluey()))
        seriesField.delegate = self
        vd.addSubview(modelField)
        vd.addSubview(shootingField)
        vd.addSubview(diameterField)
        
        modelField.anchor(top: seriesField.topAnchor, leading: seriesField.trailingAnchor, bottom: seriesField.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 0))
        modelField.widthAnchor.constraint(equalTo: seriesField.widthAnchor).isActive = true
        modelField.delegate = self
        shootingField.anchor(top: seriesField.bottomAnchor, leading: seriesField.leadingAnchor, bottom: nil, trailing: modelField.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0))
        shootingField.heightAnchor.constraint(equalTo: seriesField.heightAnchor).isActive = true
        shootingField.delegate = self
        diameterField.anchor(top: shootingField.bottomAnchor, leading: shootingField.leadingAnchor, bottom: nil, trailing: shootingField.trailingAnchor,padding: .init(top: 18.calcvaluex(), left: 0, bottom: 0, right: 0))
        diameterField.heightAnchor.constraint(equalTo: seriesField.heightAnchor).isActive = true
        diameterField.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissSelect))
        tapGesture.delegate = self
        vd.addGestureRecognizer(tapGesture)
        
        
        imageview.addSubview(imageviewtext)
        imageviewtext.centerInSuperview()
        
        imageview.layer.cornerRadius = 15.calcvaluex()
        imageview.layer.borderWidth = 1.calcvaluex()
        imageview.layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        
        imageview.backgroundColor = .white
        imageview.contentMode = .scaleAspectFit
        
        vd.addSubview(imageview)
        imageview.anchor(top: seriesField.topAnchor, leading: modelField.trailingAnchor, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 385.calcvaluey()))
        
        
        vd.addSubview(conButton)
        conButton.anchor(top: imageview.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        conButton.setTitle("完成", for: .normal)
        conButton.centerXInSuperview()
        conButton.addTarget(self, action: #selector(finishSelection), for: .touchUpInside)
        fetchAllApi()
    }
    func showAlert(text:String) {
        let alertController = UIAlertController(title: "請選擇一個\(text)", message: nil, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    @objc func finishSelection(){
        if selected_Series == nil{
            showAlert(text: "系列")
            return
        }
        
        if selected_Model == nil{
            showAlert(text: "型號")
            return
        }
        if selected_Product == nil{
            showAlert(text: "射座型式")
            return
        }
        if selected_Spec == nil{
            showAlert(text: "螺桿直徑")
            return
        }
        
        con?.bottomView.bottomView.selected_Series = self.selected_Series
        con?.bottomView.bottomView.selected_Model = self.selected_Model
        con?.bottomView.bottomView.selected_Product = self.selected_Product
        con?.bottomView.bottomView.selected_Spec = self.selected_Spec
        
        con?.bottomView.ss.isHidden = true
        con?.bottomView.bottomView.reloadData()
        
        con?.nextView.recommandLabel.isHidden = false
        con?.nextView.valueLabel.isHidden = false
        
        if let capacity = selected_Spec?.fields.first(where: { (ft) -> Bool in
            return ft.code == "calculated-injection-capacity"
        })?.value?.doubleValue {
            let mimum = String(format: "%0.2f", 0.25 * capacity)
            let maximum = String(format: "%0.2f", 0.65 * capacity)
            con?.nextView.valueLabel.text = "最小：\(mimum)、最大：\(maximum)"
        }
        
        if let ct = con2 , let model = selected_Model, let product = selected_Product, let spec = selected_Spec{
            ct.addMachine(series: model, product : product, spec: spec)
        }
        //con?.nextView
        popDismiss()
    }
    @objc func dismissSelect(){
        self.height?.constant = 0
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }completion: { (com) in
            if com {
                self.vde?.removeFromSuperview()
                self.vde = nil
            }
        }
        
        
    }
    func fetchAllApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/categories", decoderType: topData.self) { (json) in
            DispatchQueue.main.async {
              
            if let json = json {
                self.seriesField.isSelected = true
                self.series = (json.data ?? []).filter({ (sr) -> Bool in
                    return sr.status == 1
                }).sorted(by: { (s1, s2) -> Bool in
                    return  (s1.order ?? 0) < (s2.order ?? 0)
                })
            }
            else{

                                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)

                                    
                alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: { (_) in
                    self.popDismiss()
                }))
                                    self.present(alert, animated: true, completion: nil)
                                    return
                
            }
                
            }
        }
    }
    @objc func popDismiss(){
        UIView.animate(withDuration: 0.4) {
            self.vd.transform = .identity
        } completion: { (com) in
            if com {
                self.dismiss(animated: false, completion: nil)
            }
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.4) {
            self.vd.transform = CGAffineTransform(translationX: 0, y: -567.calcvaluey())
        }
    }
    
    var vde : sampleMatch?
    var height : NSLayoutConstraint?
    var product = [Product]()
    var spec = [Specification]()
    var selected_Series : Series? {
        didSet{
            self.seriesField.chooseField.text = self.selected_Series?.title

            if selected_Series != nil{
            imageviewtext.isHidden = true
            if let url = URL(string: selected_Series?.assets?.getMachinePath() ?? "") {
                
                imageview.sd_setImage(with: url, completed: nil)
            }
            }
            
        }
    }
    weak var con : StockContentController?
    var nonShooter = false
    var selected_Model : Series? {
        didSet{
            self.modelField.chooseField.text = self.selected_Model?.title
            if let vde = (selected_Model?.products ?? []).sorted(by: { (pr1, pr2) -> Bool in
                return (pr1.order ?? 0) < (pr2.order ?? 0)
            }).filter({ (pr) -> Bool in
                return pr.status == 1 && pr.is_disable == 0
            }).first {
                if vde.shooter_code == nil{
                    shootingField.nameLabel.text = "機台"
                    nonShooter = true
                }
                else{
                    shootingField.nameLabel.text = "射座型式"
                    nonShooter = false
                }
                
            }
            else{
                shootingField.nameLabel.text = "射座型式"
                nonShooter = false
            }
        }
    }
    var selected_Product : Product?
    {
       didSet{
        self.shootingField.chooseField.text = self.nonShooter ? self.selected_Product?.title : self.selected_Product?.shooter_code
       }
   }
    var selected_Spec : Specification?
    {
       didSet{
        self.diameterField.chooseField.text = self.selected_Spec?.getInjectionValue()
       }
   }
}


extension StockMatchController : StockChooseFieldDelegate,StockMatchTableViewDelegate {
    func sendData(spec: Specification) {
        
        dismissSelect()
        self.selected_Spec = spec
    }
    func sendData(product: Product) {
       
        dismissSelect()
        if product.id != selected_Product?.id {
            diameterField.isSelected = false

           
            selected_Spec = nil
        if let spec = product.specifications {
            self.spec = spec
        }
        else{
            self.spec = []
        }
        }
        diameterField.isSelected = true
        self.selected_Product = product
        
    }
    func sendData(model: Series) {
        
        dismissSelect()
        if model.id != selected_Model?.id {
            self.selected_Model = model
            diameterField.isSelected = false
            
            
            selected_Product = nil
            selected_Spec = nil
            print(nonShooter)
            if !nonShooter {
            if let product = model.products?.filter({ (pr) -> Bool in
                return pr.status == 1 && pr.shooter_code != nil
            }).sorted(by: { (p1, p2) -> Bool in
                return (p1.order ?? 0) < (p2.order ?? 0)
            }) {
                self.product = product
            }
            else{
                self.product = []
            }
            }
            else{
                print(771,model.products)
                if let product = model.products?.filter({ (pr) -> Bool in
                    return pr.status == 1
                }).sorted(by: { (p1, p2) -> Bool in
                    return (p1.order ?? 0) < (p2.order ?? 0)
                }) {
                    self.product = product
                }
                else{
                    self.product = []
                }
            }
        }
        shootingField.isSelected = true
        

        self.selected_Model = model
    }
    func sendData(data: Series) {
        
        
        dismissSelect()
        if data.id != self.selected_Series?.id {
            modelField.isSelected = false
            shootingField.isSelected = false
            diameterField.isSelected = false
            selected_Model = nil
            selected_Product = nil
            selected_Spec = nil
        fetchModel(series:data)
        
            
        }

        self.selected_Series = data
    }
    
    func fetchModel(series:Series){
        NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(series.id)", decoderType: topData.self) { (json) in
            DispatchQueue.main.async {
              
            if let json = json {
                self.modelField.isSelected = true
                self.model = (json.data ?? []).filter({ (sr) -> Bool in
                    return sr.status == 1
                }).sorted(by: { (s1, s2) -> Bool in
                    return  (s1.order ?? 0) < (s2.order ?? 0)
                })
                print(3331,self.model)
            }
            else{

                                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)

                                    
                alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: { (_) in
                    //self.popDismiss()
                }))
                                    self.present(alert, animated: true, completion: nil)
                                    return
                
            }
                
            }
        }
    }
    
    
    
    func showSelection(mode: ChooseMode) {
        if let _ = vde{
        self.height?.constant = 0
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }completion: { (com) in
            if com {
                self.vde?.removeFromSuperview()
                self.vde = nil
                self.animateSelection(mode: mode)
            }
        }
        }
        else{
            self.animateSelection(mode: mode)
        }
       
    }
    
    func animateSelection(mode : ChooseMode){
        
        if mode == .Series{
            self.vde = StockMatchTableView(items: self.series, mode: mode)
            
        }
        else if mode == .Model {
            self.vde = StockMatchTableView(items: self.model,mode: mode)
        }
        else if mode == .Shooting {
            
            self.vde = StockMatchTableView(items: self.product,mode: mode)
            if let vv = self.vde as? sampleMatch {
                vv.nonShooter = self.nonShooter
            }
        }
        else if mode == .Diameter{
            self.vde = StockMatchTableView(items: self.spec,mode: mode)
        }
        self.vde?.delegate = self
       
        self.vd.addSubview(self.vde!)
        self.vde?.translatesAutoresizingMaskIntoConstraints = false
        
        height = self.vde?.heightAnchor.constraint(equalToConstant: 0)
        height?.isActive = true
        
        var calc_height : CGFloat = 0
        var height_difference : CGFloat = 0
        if mode == .Series {
            self.vde?.anchor(top: seriesField.bottomAnchor, leading: seriesField.leadingAnchor, bottom: nil, trailing: seriesField.trailingAnchor)
           
           calc_height = CGFloat(series.count) * 84.calcvaluey()
            height_difference = 380.calcvaluey()
        }
        else if mode == .Model {
            self.vde?.anchor(top: modelField.bottomAnchor, leading: modelField.leadingAnchor, bottom: nil, trailing: modelField.trailingAnchor)
           
           calc_height = CGFloat(model.count) * 84.calcvaluey()
            height_difference = 380.calcvaluey()
        }
        else if mode == .Shooting {
            self.vde?.anchor(top: shootingField.bottomAnchor, leading: shootingField.leadingAnchor, bottom: nil, trailing: shootingField.trailingAnchor)
           
           calc_height = CGFloat(product.count) * 84.calcvaluey()
            height_difference = 272.calcvaluey()
        }
        else if mode == .Diameter {
            self.vde?.anchor(top: diameterField.bottomAnchor, leading: diameterField.leadingAnchor, bottom: nil, trailing: diameterField.trailingAnchor)
           
           calc_height = CGFloat(spec.count) * 84.calcvaluey()
            height_difference = 164.calcvaluey()
        }
        self.view.layoutIfNeeded()
        if calc_height >= height_difference {
            calc_height = height_difference - 10.calcvaluey()
        }
        height?.constant = calc_height
        
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    
    
}
