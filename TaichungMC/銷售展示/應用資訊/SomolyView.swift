//
//  SomolyView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/17/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class askLabel : UILabel {
    override init(frame: CGRect) {
        super.init(frame: .zero)
       
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class somolySelectionField : UITextField{
    let imageArrow : UIImageView = {
       let imageArrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
        imageArrow.contentMode = .scaleAspectFit
        return imageArrow
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 19.calcvaluey()
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        layer.borderWidth = 1.calcvaluex()
        autocapitalizationType = .none
        autocorrectionType = .no
        keyboardType = .decimalPad
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        attributedPlaceholder = NSAttributedString(string: "請選擇", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)])
        

        addSubview(imageArrow)
        imageArrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        imageArrow.centerYInSuperview()
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 50.calcvaluex()))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 50.calcvaluex()))
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 50.calcvaluex()))
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//extension String {
//     struct NumFormatter {
//         static let instance = NumberFormatter()
//     }
//
//     var doubleValue: Double? {
//         return NumFormatter.instance.number(from: self)?.doubleValue
//     }
//
//     var integerValue: Int? {
//         return NumFormatter.instance.number(from: self)?.intValue
//     }
//}
class somolyEnterField : UITextField {
    let unitLabel : UILabel = {
       let lb = UILabel()
        lb.text = "cm2"
        lb.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        lb.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return lb
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 19.calcvaluey()
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        layer.borderWidth = 1.calcvaluex()
        autocapitalizationType = .none
        autocorrectionType = .no
        keyboardType = .decimalPad
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        attributedPlaceholder = NSAttributedString(string: "請輸入", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 16.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)])
    
        addSubview(unitLabel)
        unitLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 0, height: 19.calcvaluey()))
        unitLabel.centerYInSuperview()
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 50.calcvaluex()))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 50.calcvaluex()))
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 50.calcvaluex()))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum SomolyMode {
    case Area
    case Selection
    case Meat
    case Length
}
class SomolyTextField : UIView,UITextFieldDelegate {
    var mode : SomolyMode?
    var topLabel = askLabel()
    var enterField = somolyEnterField()
    var delegate : SomolyDelegate?
    weak var con : ApplicationController?
    init(text:String,mode:SomolyMode?) {
        super.init(frame: .zero)
        self.mode = mode
        let mutableString = NSMutableAttributedString(string: "*", attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.8211830258, green: 0.0004545784032, blue: 0, alpha: 1)])
        mutableString.append(NSAttributedString(string: text, attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)]))
        topLabel.attributedText = mutableString
        
        addSubview(topLabel)
        topLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 25.calcvaluey()))
        addSubview(enterField)
        enterField.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 38.calcvaluey()))
        enterField.delegate = self
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil{
            
            delegate?.showTextToInt(mode: self.mode, data: nil)
            
        }
        else{
            if let double = textField.text?.doubleValue {
                con?.error = false

               
                enterField.text = "\(double)"
                delegate?.showTextToInt(mode: self.mode, data: double)
                
            }
            else{
                enterField.text = nil
                con?.error = true
                
                delegate?.showTextToInt(mode: self.mode, data: nil)
            }
        
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol SomolyDelegate {
    func showSomolySelection()
    func showTextToInt(mode:SomolyMode?,data:Double?)
}
class SomolySelectionField : UIView,UITextFieldDelegate {
    var mode : SomolyMode?
    var topLabel = askLabel()
    var enterField = somolySelectionField()
    var delegate : SomolyDelegate?
    init(text:String,mode:SomolyMode?) {
        super.init(frame: .zero)
        self.mode = mode
        let mutableString = NSMutableAttributedString(string: "*", attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.8211830258, green: 0.0004545784032, blue: 0, alpha: 1)])
        mutableString.append(NSAttributedString(string: text, attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)]))
        topLabel.attributedText = mutableString
        addSubview(topLabel)
        topLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 25.calcvaluey()))
        addSubview(enterField)
        enterField.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 38.calcvaluey()))
        enterField.delegate = self
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        delegate?.showSomolySelection()
        return false
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class underLineField : UITextField {
    let underLine : UIView = {
       let under = UIView()
        under.backgroundColor = #colorLiteral(red: 0.850099454, green: 0.850099454, blue: 0.850099454, alpha: 1)
        return under
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        textColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        isUserInteractionEnabled = false
        font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        addSubview(underLine)
        underLine.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0))
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CalculatedView : UIView {
    let topLabel : UILabel = {
       let tL = UILabel()
        tL.text = "計算結果"
        tL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        tL.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return tL
    }()
    
    let subLabel : UILabel = {
        let tL = UILabel()
         tL.text = "建議鎖模力需求區間"
         tL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
         tL.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
         return tL
    }()
    
    let firstLabel = underLineField()
    
    let betweensymbol : UILabel = {
       let bs = UILabel()
        bs.text = "～"
        bs.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        bs.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return bs
    }()
    
    let secondLabel = underLineField()
    
    let unitLabel : UILabel = {
        let bs = UILabel()
         bs.text = "噸"
         bs.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
         bs.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
         return bs
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(topLabel)
        topLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        
        addSubview(subLabel)
        subLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0))
    
        addSubview(firstLabel)
        firstLabel.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,size: .init(width: 150.calcvaluex(), height: 28.calcvaluey()))
    
        addSubview(betweensymbol)
        betweensymbol.anchor(top: nil, leading: firstLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0))
        betweensymbol.centerYAnchor.constraint(equalTo: firstLabel.centerYAnchor).isActive = true
    
        addSubview(secondLabel)
        secondLabel.anchor(top: firstLabel.topAnchor, leading: betweensymbol.trailingAnchor, bottom: firstLabel.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 150.calcvaluex(), height: 28.calcvaluey()))
    
        addSubview(unitLabel)
        unitLabel.anchor(top: nil, leading: secondLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        unitLabel.centerYAnchor.constraint(equalTo: firstLabel.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class RecommandMachineCell : UITableViewCell {

    
    let nameLabel : UILabel = {
       let nL = UILabel()
        nL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nL.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        return nL
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        let vd = UIView()
        vd.addshadowColor()
        vd.backgroundColor = #colorLiteral(red: 0.9999127984, green: 1, blue: 0.9998814464, alpha: 1)
        vd.layer.cornerRadius = 15.calcvaluey()
        contentView.addSubview(vd)
        vd.centerInSuperview(size: .init(width: 512.calcvaluex(), height: 64.calcvaluey()))
        

        
        vd.addSubview(nameLabel)
        nameLabel.anchor(top: nil, leading: vd.leadingAnchor, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
        nameLabel.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class Stock_MachineViewCell : UITableViewCell {
    let imageview : UIImageView = {
       let im = UIImageView()
        im.contentMode = .scaleAspectFit
        return im
    }()
    
    let nameLabel : UILabel = {
       let nL = UILabel()
        nL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nL.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        return nL
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        let vd = UIView()
        vd.addshadowColor()
        vd.backgroundColor = #colorLiteral(red: 0.9999127984, green: 1, blue: 0.9998814464, alpha: 1)
        vd.layer.cornerRadius = 15.calcvaluey()
        contentView.addSubview(vd)
        vd.centerInSuperview(size: .init(width: 441.calcvaluex(), height: 78.calcvaluey()))
        
        vd.addSubview(imageview)
        imageview.anchor(top: nil, leading: vd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.43.calcvaluex(), bottom: 0, right: 0),size: .init(width: 78.23.calcvaluex(), height: 70.56.calcvaluey()))
        imageview.centerYInSuperview()
        
        vd.addSubview(nameLabel)
        nameLabel.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()))
        nameLabel.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol Stock_MachineViewDelegate {
    func goNextData(index:Int,series:[SomolyMachineSeries])
    
    
}
class Stock_MachineView : UIView,UITableViewDelegate,UITableViewDataSource {
    let topview = UIView()
    let tableview = UITableView(frame: .zero, style: .grouped)
    var somolySeries = [SomolyMachineSeries]() {
        didSet{
            self.tableview.reloadData()
        }
    }
    let recommandLabel : UILabel = {
       let rcL = UILabel()
        rcL.text = "建議機台"
        rcL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        rcL.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return rcL
    }()
    var delegate : Stock_MachineViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        topview.backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        addSubview(topview)
        topview.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 49.calcvaluey()))
        if #available(iOS 11.0, *) {
            topview.layer.cornerRadius = 15.calcvaluey()
            topview.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        }
        
        topview.addSubview(recommandLabel)
        recommandLabel.centerInSuperview()
        
        tableview.separatorStyle = .none
        
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.anchor(top: topview.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 11.calcvaluey(), left: 12.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
    
        tableview.delegate = self
        tableview.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return somolySeries.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Stock_MachineViewCell(style: .default, reuseIdentifier: "cell")
        let sr = somolySeries[indexPath.row]
        if let urlString = sr.assets?.getMachinePath() {
            if let url = URL(string: urlString) {
                cell.imageview.sd_setImage(with: url, completed: nil)
            }
        }
        
        if let sr = sr.titles?.zhTW {
            cell.nameLabel.text = sr
            
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableview.cellForRow(at: indexPath)
        
        UIView.animate(withDuration: 0.2) {
            cell?.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        } completion: { (com) in
            if com {
                UIView.animate(withDuration: 0.1) {
                    cell?.transform = .identity
                } completion: { (com) in
                    if com {
                        self.delegate?.goNextData(index: indexPath.row, series: self.somolySeries)
                    }
                }
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 98.calcvaluey()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SomolyView: UIView,SomolyDelegate {
    func showSomolySelection() {
        //
    }
    
    func showTextToInt(mode: SomolyMode?, data: Double?) {
        if mode == .Area{
            firstData = data
        }
        else if mode == .Meat
        
        {
            thirdData = data
        }
        
        else if mode == .Length{
            
           fourthData = data
        }
    }
    
    let contentView = UIView()
    var contentViewAnchor:AnchoredConstraints?
    var firstViewAnchor : AnchoredConstraints?
    let firstView = SomolyTextField(text: "成品投影面積", mode: .Area)
    let secondView = SomolySelectionField(text: "射出樹酯種類" , mode: .Selection)
    let checkView = SomolyCheckField()
    let calcButton = confirmButton(type: .custom)
    var didCheck = false
    
    let thridView = SomolyTextField(text: "成品最小肉厚" , mode: .Meat)
    let fourthView = SomolyTextField(text: "成品末端流動長度" , mode: .Length)
    
    let machineView = Stock_MachineView()
    var calcButtonAnchor : AnchoredConstraints?
    
    var second_data : SomolyData?
    
    var firstData : Double? {
        didSet{
            print(firstData)
        }
    }
    weak var con : ApplicationController? {
        didSet{
            firstView.con = con
            thridView.con = con
            fourthView.con = con
        }
    }
    
    
    var fourthData : Double?
    var thirdData : Double?
    var calculatedView = CalculatedView()
    var calculatedViewAnchor:AnchoredConstraints?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        contentView.addshadowColor()
        contentView.backgroundColor = #colorLiteral(red: 0.9999127984, green: 1, blue: 0.9998814464, alpha: 1)
        
        addSubview(contentView)
        contentViewAnchor = contentView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 323.calcvaluex(), bottom: 26.calcvaluey(), right: 0),size: .init(width: 675.calcvaluex(), height: 0))
        contentView.layer.cornerRadius = 15.calcvaluey()
        
        addSubview(machineView)
        machineView.addshadowColor()
        machineView.backgroundColor = #colorLiteral(red: 0.9999127984, green: 1, blue: 0.9998814464, alpha: 1)
        machineView.layer.cornerRadius = 15.calcvaluey()
        machineView.anchor(top: contentView.topAnchor, leading: contentView.trailingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 0))
        machineView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        machineView.alpha = 0
        
        
        contentView.addSubview(firstView)
        
        firstViewAnchor = firstView.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 46.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 280.calcvaluex(), height: 71.calcvaluey()))
        
        contentView.addSubview(secondView)
        secondView.anchor(top: firstView.topAnchor, leading: firstView.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0))
        secondView.widthAnchor.constraint(equalTo: firstView.widthAnchor).isActive = true
        secondView.heightAnchor.constraint(equalTo: firstView.heightAnchor).isActive = true
    
        
        contentView.addSubview(checkView)
        checkView.anchor(top: firstView.bottomAnchor, leading: firstView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 110.calcvaluex(), height: 25.calcvaluey()))
        checkView.checkButton.addTarget(self, action: #selector(goCheck), for: .touchUpInside)
        
        

        
        contentView.addSubview(thridView)
        thridView.anchor(top: checkView.bottomAnchor, leading: firstView.leadingAnchor, bottom: nil, trailing: firstView.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 71.calcvaluey()))
        contentView.addSubview(fourthView)
        fourthView.anchor(top: thridView.topAnchor, leading: thridView.trailingAnchor, bottom: thridView.bottomAnchor, trailing: secondView.trailingAnchor,padding: .init(top: 0, left: 25.calcvaluex(), bottom: 0, right: 0))
        fourthView.enterField.unitLabel.isHidden = true
        
        thridView.alpha = 0
        fourthView.alpha = 0
        
        calcButton.setTitle("計算", for: .normal)
        calcButton.addshadowColor()
        contentView.addSubview(calcButton)
        calcButtonAnchor = calcButton.anchor(top: checkView.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        calcButton.centerXInSuperview()
        
        
        firstView.delegate = self
        thridView.delegate = self
        fourthView.delegate = self
        
        
        contentView.addSubview(calculatedView)
        calculatedViewAnchor = calculatedView.anchor(top: contentView.bottomAnchor, leading: nil, bottom: nil, trailing: nil,size: .init(width: 388.calcvaluex(), height: 112.calcvaluey()))
        calculatedView.alpha = 0
        calculatedView.centerXInSuperview()

    }
    func animateDown(){

        self.calcButtonAnchor?.top?.constant = 133.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded()
        } completion: { (_) in
            UIView.animate(withDuration: 0.2) {
                self.thridView.alpha = 1
                self.fourthView.alpha = 1
            }

        }
        

    }
    func animateUP(){
        UIView.animate(withDuration: 0.2) {
            self.thridView.alpha = 0
            self.fourthView.alpha = 0
            
        } completion: { (_) in
            self.calcButtonAnchor?.top?.constant = 36.calcvaluey()
            UIView.animate(withDuration: 0.4) {
                self.layoutIfNeeded()
            }

        }
    }
    @objc func goCheck(){
        self.endEditing(true)
        calculatedView.firstLabel.text = nil
        calculatedView.secondLabel.text = nil
        if !didCheck {
            checkView.setCheck()
            didCheck = true
            animateDown()
        }
        else{
            checkView.unSetCheck()
            didCheck = false
            animateUP()
        }
    }
    func slideToLeft(){
        contentViewAnchor?.leading?.constant = 26.calcvaluex()
        contentViewAnchor?.width?.constant = 473.calcvaluex()
        firstViewAnchor?.width?.constant = 178.calcvaluex()
        machineView.alpha = 1

    }
    func slideToRight(){
        contentViewAnchor?.leading?.constant = 323.calcvaluex()
        contentViewAnchor?.width?.constant = 675.calcvaluex()
        firstViewAnchor?.width?.constant = 280.calcvaluex()
        machineView.alpha = 0
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SomolyCheckField : UIView {
    let checkButton = UIButton(type: .custom)
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(checkButton)
        checkButton.setImage(#imageLiteral(resourceName: "btn_check_box_normal"), for: .normal)
        checkButton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluey()))
        checkButton.centerYInSuperview()
        
        label.text = "已有成品"
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        addSubview(label)
        
        label.anchor(top: nil, leading: checkButton.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        label.centerYInSuperview()
    }
    
    func setCheck(){
        checkButton.setImage(#imageLiteral(resourceName: "btn_check_box_pressed"), for: .normal)
        label.textColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
    }
    
    func unSetCheck(){
        checkButton.setImage(#imageLiteral(resourceName: "btn_check_box_normal"), for: .normal)
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
