//
//  StockWebViewController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/20/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class StockWebViewController: SampleController {
    let webview = UIWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "成品細部成型設定"
        
        view.addSubview(webview)
        
        webview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 30.calcvaluey(), left: 0, bottom: 0, right: 0))
        webview.isOpaque = false
        webview.backgroundColor = .clear
    }
}
