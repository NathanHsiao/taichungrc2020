import UIKit
protocol StockMatchTableViewDelegate {
    func sendData(data:Series)
    func sendData(model:Series)
    func sendData(product:Product)
    func sendData(spec:Specification)
    
    //func sendData(
}
class sampleMatch : UIView {
    var delegate : StockMatchTableViewDelegate?
    var nonShooter = false
}
class StockMatchTableView<T:Codable> : sampleMatch,UITableViewDelegate,UITableViewDataSource {
    let tb = UITableView(frame: .zero, style: .plain)
    
    var items = [T]()
    var mode = ChooseMode.Series
    //var delegate : StockMatchTableViewDelegate?
    init(items:[T],mode:ChooseMode) {
        super.init(frame: .zero)
        self.mode = mode
        self.items = items
        self.backgroundColor = .white
        
        self.addshadowColor()
        
        addSubview(tb)
        tb.backgroundColor = .white
        tb.fillSuperview()
        tb.separatorStyle = .none
        tb.delegate = self
        tb.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : selectViewCell?
        if let pitems = items[indexPath.item] as? Series {
            cell = selectViewCell(txt: pitems.title ?? "")
            if pitems.is_disable == 1{
                cell?.label.textColor = .lightGray
            }
            else{
                cell?.label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
            }
        }
        else if let pitems = items[indexPath.item] as? Product {
            cell = selectViewCell(txt: nonShooter ? pitems.title ?? "" : pitems.shooter_code ?? "")
            if pitems.is_disable == 1{
                cell?.label.textColor = .lightGray
            }
            else{
                cell?.label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
            }
        }
        else if let pitems = items[indexPath.item] as? Specification{

            cell = selectViewCell(txt: pitems.fields.first { (f1) -> Bool in
                f1.code == "screw-diameter"
            }?.value ?? "")
        }
        if indexPath.item == items.count - 1{
            cell?.seperator.isHidden = true
        }
        else{
            cell?.seperator.isHidden = false
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.calcvaluey()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if mode == .Series {
        if let pr = items[indexPath.row] as? Series {
            if pr.is_disable == 0{
                delegate?.sendData(data: pr)
            }

            
        }
        }
        else if mode == .Model {
            if let pr = items[indexPath.row] as? Series {
                if pr.is_disable == 0{
                delegate?.sendData(model: pr)
                }
            }
        }
        else if mode == .Shooting {
            if let pr = items[indexPath.row] as? Product {
                if pr.is_disable == 0{
                delegate?.sendData(product:pr)
                }
            }
        }
        else if mode == .Diameter {
            if let pr = items[indexPath.row] as? Specification {
            delegate?.sendData(spec:pr)
        }
            
        }

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class StockMatchTableViewCell : UITableViewCell {
    let seperator = UIView()
    let label = UILabel()
    init(txt:String) {
        super.init(style: .default, reuseIdentifier: "cell")
        selectionStyle = .none
        
        contentView.addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 21.5.calcvaluex(), bottom: 0, right: 21.5.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
        
        label.text = txt
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        contentView.addSubview(label)
        label.centerInSuperview()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

