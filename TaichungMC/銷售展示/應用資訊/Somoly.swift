//
//  Somoly.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/17/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct Somoly : Codable {
    var data: [SomolyData]?
}


struct SomolyData : Codable {
    var resin: String?
    var type : String?
    var b1 : String?
    var b2 : String?
    var e1 : String?
    var e2 : String?
}


struct Formula : Codable {
    var data :[FormulaData]?
}
struct FormulaData : Codable {
    var start : String?
    var end : String?
    var square_x : String?
    var minus_x : String?
    var append : String?
}


struct SomolyMachine : Codable{
    var data : [SomolyMachineData]?
}

struct SomolyMachineData : Codable{

    
    var id : String
    var order : Int
    var status : Int
    var is_disable : Int
    var titles : Lang?
    var shooter_code : String?
    var categories : [SomolyMachineCategory]?
    var specifications : [SomolyMachineSpecification]?
}


struct SomolyMachineCategory : Codable {
    var titles:Lang?
    
    var parent : [SomolyMachineSeries]?
}

struct SomolyMachineSpecification : Codable {
    var titles: Lang?
    var field_injection : String?
    var field_injection_rate : String?
    var field_clamping_force : String?
    var shooter_code : String?
    var machine_title : String?
    var field_screw_diameter : String?
    private enum CodingKeys: String,CodingKey {
        case titles,field_injection,field_injection_rate = "field_injetction-rate" ,field_clamping_force = "field_clamping-force",shooter_code,field_screw_diameter = "field_screw-diameter",machine_title
    }
    func getMachineData() -> [String:String] {
        var specdict = [String:String]()
        if let sep = titles?.zhTW?.split(separator: " ") {
            
            if sep.count > 0 {
                let kk = String(sep[0]).split(separator: "-")
                if kk.count > 1{
                    let joined = String(kk[1]).components(separatedBy: .letters).joined()
                    let series = String(kk[0])
                    let machine = "\(series)-\(joined)"
                    
                     specdict["name"] = machine
                }
                
                let second = String(sep[1])
                let radius = second.components(separatedBy: .letters).joined()
                
                specdict["radius"] = radius
            }
        }

        return specdict
    }
}

struct SomolyMachineSeries : Codable {
    var id : String
   // var model_number : String?
    var status: Int
    var is_disable : Int
    var titles : Lang?
    var assets: Assets?
}
