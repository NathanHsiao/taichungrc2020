//
//  StockContentController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/2/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class seperatorV : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.8591474891, green: 0.8592686057, blue: 0.8591094613, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FieldCell : UITableViewCell {
    let bottomSeperator = seperatorV()
    let centerSeperator = seperatorV()
    let header = UILabel()
    let value = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        selectionStyle = .none
        contentView.addSubview(bottomSeperator)
        bottomSeperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        contentView.addSubview(centerSeperator)
        centerSeperator.anchor(top: contentView.topAnchor, leading: nil, bottom: contentView.bottomAnchor, trailing: nil,size: .init(width: 1.calcvaluex(), height: 0))
        centerSeperator.centerXInSuperview()
        
        header.textColor = .black
        value.textColor = .black
        
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        value.font = UIFont(name: "Robto-Regular", size: 18.calcvaluex())
        

        
        contentView.addSubview(header)
        header.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        header.centerYInSuperview()
        contentView.addSubview(value)
        value.anchor(top: nil, leading: centerSeperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        value.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class turnCell : UITableViewCell {
    let sep1 = seperatorV()
    let sep2 = seperatorV()
    let sep3 = seperatorV()
    let sep4 = seperatorV()
    let header = UILabel()
    let subHeader = UILabel()
    let value1 = UILabel()
    let value2 = UILabel()
    
    let value3 = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        
        contentView.addSubview(sep1)
        
        
        contentView.addSubview(sep2)
        
        contentView.addSubview(sep3)
        
        
        sep1.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 242.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        sep2.anchor(top: contentView.topAnchor, leading: sep1.trailingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 210.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        sep3.anchor(top: contentView.topAnchor, leading: sep2.trailingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 210.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
    
        contentView.addSubview(sep4)
        sep4.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 16.5.calcvaluex(), bottom: 0, right: 16.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
    
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        header.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
        
        subHeader.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        subHeader.textColor = #colorLiteral(red: 0.5685855746, green: 0.5686681271, blue: 0.568559587, alpha: 1)
        
        contentView.addSubview(header)
        header.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        
        header.centerYInSuperview()
        contentView.addSubview(subHeader)
        subHeader.anchor(top: nil, leading: header.trailingAnchor, bottom: header.bottomAnchor, trailing: nil)
        
        contentView.addSubview(value1)
        contentView.addSubview(value2)
        value1.anchor(top: nil, leading: sep1.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        value1.centerYInSuperview()
        
        value2.anchor(top: nil, leading: sep2.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        value2.centerYInSuperview()
        
        value1.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
        value2.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
        value3.textColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        value1.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        value2.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        value3.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        contentView.addSubview(value3)
        value3.anchor(top: nil, leading: sep3.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        value3.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol turnHeaderDelegate{
    func removeProduct()
}
class turnHeader : UIView {
    let sep1 = seperatorV()
    let sep2 = seperatorV()
    let sep3 = seperatorV()
    let machineLabel = UILabel()
    let requireLabel = UILabel()
    let selectedLabel = UILabel()
    let xbutton = UIButton(type: .custom)
    var delegate : turnHeaderDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9318111539, green: 0.9319418073, blue: 0.9317700267, alpha: 1)
        xbutton.setImage(#imageLiteral(resourceName: "ic_multiplication").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.isHidden = true
        xbutton.tintColor = .black
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        
        addSubview(xbutton)
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 10.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        xbutton.centerYInSuperview()
        
        addSubview(sep1)
        
        
        addSubview(sep2)
        
        addSubview(sep3)
        
        
        sep1.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 242.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        sep2.anchor(top: topAnchor, leading: sep1.trailingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 210.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        sep3.anchor(top: topAnchor, leading: sep2.trailingAnchor, bottom: bottomAnchor, trailing: nil,padding: .init(top: 0, left: 210.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
    
        setLabel(label: machineLabel, text: "機台規格")
        setLabel(label: requireLabel, text: "實際成型需求")
        setLabel(label: selectedLabel, text: "目前選擇的搭配")
        
        addSubview(machineLabel)
        addSubview(requireLabel)
        addSubview(selectedLabel)
        machineLabel.anchor(top: nil, leading: sep1.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        machineLabel.centerYInSuperview()
        
        requireLabel.anchor(top: nil, leading: sep2.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        requireLabel.centerYInSuperview()
        selectedLabel.anchor(top: nil, leading: sep3.trailingAnchor, bottom: nil, trailing: xbutton.leadingAnchor,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        selectedLabel.centerYInSuperview()
        
        xbutton.addTarget(self, action: #selector(removeProduct), for: .touchUpInside)

    }
    @objc func removeProduct(){
        self.delegate?.removeProduct()
    }
    func setLabel(label:UILabel,text:String) {
        label.text = text
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FieldsView : UIView,UITableViewDelegate,UITableViewDataSource{
    
    var stock_fields = [Order_StockField]() {
        didSet{
            
            tableview.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stock_fields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FieldCell(style: .default, reuseIdentifier: "cell")
        cell.header.text = stock_fields[indexPath.row].name
        cell.value.text = stock_fields[indexPath.row].value
        if indexPath.row == stock_fields.count - 1{
            cell.bottomSeperator.isHidden = true
        }
        else{
            cell.bottomSeperator.isHidden = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 49.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    let tableview = UITableView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 6.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869761825, green: 0.8871009946, blue: 0.8869369626, alpha: 1).cgColor
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.bounces = false
        tableview.showsVerticalScrollIndicator = false
        tableview.backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        addSubview(tableview)
        tableview.fillSuperview(padding: .init(top: 0, left: 16.5.calcvaluex(), bottom: 0, right: 16.5.calcvaluex()))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class turningV : UIView {

    let ss = SelectMachineView()
    let bottomView = turnView(frame: .zero, style: .grouped)
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 6.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869761825, green: 0.8871009946, blue: 0.8869369626, alpha: 1)
        clipsToBounds = true
        addSubview(bottomView)
        bottomView.fillSuperview()
        
        addSubview(ss)
        ss.backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        ss.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 306.calcvaluex(), height: 106.calcvaluey()))
        bottomView.ss = ss
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol turnViewDelegate{
    func removeRecommand()
}
class turnView : UITableView,UITableViewDataSource,UITableViewDelegate,turnHeaderDelegate {
    func removeProduct() {
        selected_Series = nil
        selected_Model = nil
        selected_Product = nil

        selected_Spec = nil
        
        self.reloadData()
        s_delegate?.removeRecommand()
        ss?.isHidden = false
    }
    var s_delegate: turnViewDelegate?
    weak var ss: SelectMachineView?
    var lastFour = [Order_StockField](){
        didSet{
            
            self.reloadData()
        }
    }
    var selected_Series : Series?
    var selected_Model : Series?
    var selected_Product : Product?

    var selected_Spec : Specification? {
        didSet{
        print(331,self.selected_Spec?.fields)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = turnCell(style: .default, reuseIdentifier: "cell")
        if indexPath.row == 1{
            cell.sep4.isHidden = true
            cell.header.text = "最大射出率 "
            cell.subHeader.text = "cm3/sec"
            cell.value1.text = lastFour.first(where: { (st) -> Bool in
                return st.code == "machine-max-rate"
            })?.value
            cell.value2.text = lastFour.first(where: { (st) -> Bool in
                return st.code == "real-max-rate"
            })?.value
            if let spec = selected_Spec {
                cell.value3.text = spec.fields.first(where: { (st) -> Bool in
                    return st.code == "injetction-rate"
                })?.value
            }
            else{
                cell.value3.text = nil
            }
            
        }
        else{
            cell.sep4.isHidden = false
            cell.header.text = "最大射壓 "
            cell.subHeader.text = "bar"
            cell.value1.text = lastFour.first(where: { (st) -> Bool in
                return st.code == "machine-max-pressure"
            })?.value
            cell.value2.text = lastFour.first(where: { (st) -> Bool in
                return st.code == "real-max-pressure"
            })?.value
            if let spec = selected_Spec {
                cell.value3.text = spec.fields.first(where: { (st) -> Bool in
                    return st.code == "injection"
                })?.value
            }
            else{
                cell.value3.text = nil
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 53.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vv = turnHeader()
        if let st = selected_Model?.title {
            
            let attributedKey = [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)] as [NSAttributedString.Key : Any]
            let s_attributedKey = [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)] as [NSAttributedString.Key : Any]
            let attrText = NSMutableAttributedString(string: "目前選擇的搭配",attributes: attributedKey)
            attrText.append(NSAttributedString(string: " (\(st)) ", attributes: s_attributedKey))
            vv.selectedLabel.attributedText = attrText
            
            vv.xbutton.isHidden = false
            vv.delegate = self
        }
        return vv
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 49.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)

        separatorStyle = .none
       
        self.bounces = false
        delegate = self
        dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
struct Order_StockField {
    var name:String
    var value:String?
    var code: String
    var affix : String?
}

class StockRecommandView : UIView {
    let recommandButton : UIButton = {
        let rc = UIButton(type: .custom)
        rc.backgroundColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        rc.setTitle("建議機台", for: .normal)
        rc.setTitleColor(.white, for: .normal)
        rc.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        return rc
    }()
    let settingButton : UIButton = {
        let rc = UIButton(type: .custom)
        rc.backgroundColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        rc.setTitle("顯示成型設定", for: .normal)
        rc.setTitleColor(.white, for: .normal)
        rc.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        return rc
    }()
    let recommandLabel : UIView = {
       let vd = UIView()
        vd.backgroundColor = .clear
        let seperator = UIView()
        seperator.backgroundColor = #colorLiteral(red: 0.8887394859, green: 0.8887394859, blue: 0.8887394859, alpha: 1)
        
        vd.addSubview(seperator)
        seperator.anchor(top: vd.topAnchor, leading: nil, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 10.calcvaluey(), right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        
        let infoimage = UIImageView(image: #imageLiteral(resourceName: "ic_error").withRenderingMode(.alwaysTemplate))
        infoimage.tintColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        infoimage.contentMode = .scaleAspectFit
        vd.addSubview(infoimage)
        infoimage.anchor(top: nil, leading: vd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 20.calcvaluex(), height: 20.calcvaluex()))
        infoimage.centerYInSuperview()
        
        let label = UILabel()
         label.backgroundColor = .clear
         label.text = "建議射出總量cm3"
         label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
         label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        
        vd.addSubview(label)
        label.anchor(top: nil, leading: infoimage.trailingAnchor, bottom: nil, trailing: seperator.leadingAnchor,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        label.centerYInSuperview()
        return vd
    }()
    let valueLabel : UILabel = {
       let label = UILabel()
        label.backgroundColor = .clear
        label.text = "最小：、最大："
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1)
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addshadowColor()
        backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        
        
        addSubview(recommandLabel)
        recommandLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,size: .init(width: 237.calcvaluex(), height: 0))
        
        
        addSubview(recommandButton)
        recommandButton.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 200.calcvaluex(), height: 0))
        addSubview(settingButton)
        settingButton.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: recommandButton.leadingAnchor,size: .init(width: 200.calcvaluex(), height: 0))
        addSubview(valueLabel)
        valueLabel.anchor(top: topAnchor, leading: recommandLabel.trailingAnchor, bottom: bottomAnchor, trailing: settingButton.leadingAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        recommandLabel.isHidden = true
        valueLabel.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class StockContentController:SampleController,turnViewDelegate {
    func removeRecommand() {
        nextView.recommandLabel.isHidden = true
        nextView.valueLabel.isHidden = true
        nextView.valueLabel.text = "最小：、最大："
    }
    
    var stock:Stock? {
        didSet{
            order_f = []
            lastFour = []
            for i in StockApi.shared.fields ?? [] {
                
                if let s = stock?.fields?[i.code ?? ""] {
                    print(100,i.title,s?.zhTW)
                    if i.code == "machine-max-pressure" || i.code == "machine-max-rate" || i.code == "real-max-pressure" || i.code == "real-max-rate" {
                        lastFour.append(Order_StockField(name: i.title ?? "", value: s?.zhTW,code:i.code ?? "",affix: i.affix))
                    }
                    else{
                        order_f.append(Order_StockField(name: i.title ?? "", value: s?.zhTW,code: i.code ?? "",affix: i.affix))
                    }
                }
            }
            bottomView.bottomView.lastFour = lastFour
            fields_view.stock_fields = order_f
            
        }
    }


    var lastFour = [Order_StockField]()
    var order_f = [Order_StockField]()
    let full_image = UIImageView()
    let fields_view = FieldsView()
    let bottomView = turningV()
    let nextView = StockRecommandView()
    override func viewDidLoad() {
        super.viewDidLoad()
        backbutton.image = #imageLiteral(resourceName: "ic_faq_before").withRenderingMode(.alwaysTemplate)
        backbutton.tintColor = .white
        titleview.text = stock?.title
        
        full_image.backgroundColor = .white
        full_image.contentMode = .scaleAspectFill
        full_image.clipsToBounds = true
        full_image.layer.borderWidth = 1.calcvaluex()
        full_image.layer.borderColor = #colorLiteral(red: 0.8730852008, green: 0.8732081056, blue: 0.8730465174, alpha: 1).cgColor
        view.addSubview(full_image)
        full_image.sd_setImage(with: URL(string: stock?.assets?.getImage() ?? ""), completed: nil)
        
        full_image.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 530.calcvaluex(), height: 339.calcvaluey()))
        
        view.addSubview(fields_view)
        fields_view.anchor(top: full_image.topAnchor, leading: full_image.trailingAnchor, bottom: full_image.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    
        view.addSubview(bottomView)
        bottomView.anchor(top: full_image.bottomAnchor, leading: full_image.leadingAnchor, bottom: nil, trailing: fields_view.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 155.calcvaluey()))
        full_image.isUserInteractionEnabled = true
        full_image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(expandImage)))

        
        view.addSubview(nextView)
        nextView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 76.calcvaluey()))
        nextView.recommandButton.addTarget(self, action: #selector(goToRecommand), for: .touchUpInside)
        bottomView.ss.plusImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelect)))
        bottomView.bottomView.s_delegate = self
        nextView.settingButton.addTarget(self, action: #selector(goSettingPic), for: .touchUpInside)
    }
    @objc func goSettingPic(){
        
        
        if let file = stock?.assets?.getFile() {
            if let url = URL(string: file) {
                let vc = StockWebViewController()
                vc.webview.loadRequest(URLRequest(url: url))
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    func uniqueElementsFrom(array: [SomolyMachineData],calc1:Double,calc2:Double) -> [SomolyMachineSeries] {
       
        
        var uniquearray = [SomolyMachineSeries]()
        
        for i in array {
            for j in i.categories ?? [] {
                for k in j.parent ?? [] {
                    if !uniquearray.contains(where: { (dt) -> Bool in
                        return dt.titles?.zhTW == k.titles?.zhTW
                    }) {
                        uniquearray.append(k)
                    }
                }
            }
        }
        
                        let vd = SomolyRecommendationController()
        vd.app_mode = false
        vd.originData = origindata
        vd.first = calc1
        vd.second = calc2
        vd.series = uniquearray
                vd.modalPresentationStyle = .fullScreen
                self.present(vd, animated: true, completion: nil)
        return uniquearray
    }
    var origindata = [SomolyMachineData]()
    func callForProducts(calc1:Double,calc2:Double) {
        print(3319,calc1,calc2)
        NetworkCall.shared.getCall(parameter: "api-or/v1/products/0", decoderType: SomolyMachine.self) { (json) in
            DispatchQueue.main.async {
              //  group.leave()
                if let json = json{
                    
                    if let data = json.data {
                        self.origindata = data
                        
                        let match_data = data.filter({ (sm) -> Bool in
                            
                            if let _ = (sm.specifications ?? []).first(where: { (spec) -> Bool in
                                
                                print(33121,spec.field_injection,spec.field_injection_rate)
                                if let somoly_injection = spec.field_injection?.doubleValue, let somoly_rate = spec.field_injection_rate?.doubleValue {
                                    
                                    
                                    return (somoly_injection >= calc1 && somoly_rate >= calc2) && (sm.status == 1 && sm.categories?.first?.parent?.first?.status == 1)
                                }
                                return false
                            }) {
                               return true
                            }
                            return false
                        })
                        print(889,match_data)
                        self.uniqueElementsFrom(array: match_data,calc1:calc1,calc2:calc2)
                        
                                
                        
 
                    }
                }
                
            }

        }
    }
    
    
    @objc func goToRecommand(){
        
        
        if let pressure = lastFour.first(where: { (ot) -> Bool in
            return ot.code == "real-max-pressure"
        }) , let rate = lastFour.first(where: { (ot) -> Bool in
            return ot.code == "real-max-rate"
        }) {
            if let pressure_val = pressure.value?.doubleValue, let rate_val = rate.value?.doubleValue {
                self.callForProducts(calc1: pressure_val, calc2: rate_val)
 
            }
            else{
                let alertController = UIAlertController(title: "目前無建議機台", message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "確認", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
        else{
            let alertController = UIAlertController(title: "目前無建議機台", message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "確認", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }

    }
    @objc func goSelect(){
       let selectMachineController = StockMatchController()
        selectMachineController.modalPresentationStyle = .overCurrentContext
        selectMachineController.con = self
        self.present(selectMachineController, animated: false, completion: nil)
    }
    @objc func expandImage(){
        let img = StockImageExpandController()
        img.modalPresentationStyle = .fullScreen
        img.img.image = full_image.image
        self.present(img, animated: true, completion: nil)
    }
}


class SelectMachineView : UIView {
    let plusImage = UIImageView(image: #imageLiteral(resourceName: "ic_addition").withRenderingMode(.alwaysTemplate))
    let plusLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        plusImage.tintColor = .white
        plusImage.contentMode = .scaleAspectFit
        plusImage.backgroundColor = #colorLiteral(red: 0.9407916665, green: 0.5144688487, blue: 0.007106156554, alpha: 1)
        plusImage.layer.cornerRadius = 40.calcvaluex()/2
        addSubview(plusImage)
        plusImage.centerXInSuperview()
        
        plusImage.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 40.calcvaluex(), height: 40.calcvaluex()))
        
        plusLabel.text = "比對試算機台"
        plusLabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        plusLabel.textColor = #colorLiteral(red: 0.9407916665, green: 0.5144688487, blue: 0.007106156554, alpha: 1)
        
        addSubview(plusLabel)
        plusLabel.anchor(top: plusImage.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 5.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 22.calcvaluey()))
        plusLabel.centerXInSuperview()
        plusImage.isUserInteractionEnabled = true
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
