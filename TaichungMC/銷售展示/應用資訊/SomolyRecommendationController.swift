//
//  SomolyRecommendationController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/18/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
protocol RecommandDataDelegate {
    func goToProduct(spec:SomolyMachineSpecification)
}
class RecommandDataView : UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = RecommandMachineCell(style: .default, reuseIdentifier: "cell")
        let sr = sortedData[indexPath.row]
        //\(sr.shooter_code ?? "")
        if sr.shooter_code == nil || sr.shooter_code == ""{
            cell.nameLabel.text = "\(sr.machine_title ?? "") / 螺桿直徑：\(sr.field_screw_diameter ?? "")"
        }
        else{
        cell.nameLabel.text = "\(sr.machine_title ?? "") / 射座型式：\(sr.shooter_code ?? "") / 螺桿直徑：\(sr.field_screw_diameter ?? "")"
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76.calcvaluey()
    }
    var sortedData = [SomolyMachineSpecification]() {
        didSet{
            self.tableview.reloadData()
        }
    }
    var delegate : RecommandDataDelegate?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableview.cellForRow(at: indexPath)
        
        UIView.animate(withDuration: 0.2) {
            cell?.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        } completion: { (com) in
            if com {
                UIView.animate(withDuration: 0.1) {
                    cell?.transform = .identity
                } completion: { (com) in
                    if com {
                        self.delegate?.goToProduct(spec: self.sortedData[indexPath.row])
                    }
                }
            }
        }

        
        
    }
    let tableview = UITableView(frame: .zero, style: .plain)
    override init(frame: CGRect) {
        super.init(frame: frame)
        let vd = UIView()
        vd.addshadowColor()
        vd.layer.cornerRadius = 15.calcvaluey()
        vd.backgroundColor = .white
        
        addSubview(vd)
        vd.fillSuperview(padding: .init(top: 16.calcvaluey(), left: 12.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    
        vd.addSubview(tableview)
        tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        tableview.fillSuperview(padding: .init(top: 20.calcvaluey(), left: 20.calcvaluex(), bottom: 0, right: 20.calcvaluex()))
    
        tableview.delegate = self
        tableview.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SomolyRecommendationController: SampleController {
    var app_mode = true
    let leftview = UITableView(frame: .zero, style: .plain)
    let rightview = RecommandDataView()
    var series = [SomolyMachineSeries]() {
        didSet{
            self.series = self.series.filter({ (sr) -> Bool in
                return sr.status == 1
            })
            sortData()
            self.leftview.reloadData()
        }
    }
    var first : Double?
    var second : Double?
    func sortData(){
        sortedData = []
        for i in originData {
            if (i.categories ?? []).contains(where: { (smc) -> Bool in
                if (smc.parent ?? []).contains(where: { (sr) -> Bool in
                    if self.series.count != 0 {
                    return sr.titles?.zhTW == self.series[currentIndex].titles?.zhTW
                    }
                    return false
                }) {
                    return true
                }
                return false
            }) {
                for j in i.specifications ?? [] {
                    
                    if app_mode{
                    if let ft = first, let st = second, let clamp = j.field_clamping_force?.doubleValue {
                        
                        if clamp >= ft && clamp <= st {
                            var ft = j
                            ft.shooter_code = i.shooter_code
                            ft.machine_title = i.categories?.first?.titles?.zhTW ?? ""
                            sortedData.append(ft)
                        }
                        

                    }
                    }
                    else{
                        
                        if let ft = first, let st = second, let pressure = j.field_injection?.doubleValue , let rate = j.field_injection_rate?.doubleValue{
                            print(7778,pressure,rate,ft,st)
                            if pressure >= ft  && rate >= st {
                                var ft = j
                               ft.shooter_code = i.shooter_code
                                ft.machine_title = i.categories?.first?.titles?.zhTW ?? ""
                                sortedData.append(ft)
                            }
                            

                        }
                        
                    }
                    
                }
            }
        }
        
        
        rightview.sortedData = self.sortedData
    }
    var sortedData = [SomolyMachineSpecification]()
    var originData = [SomolyMachineData]()
    var currentIndex : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "建議機台"
        
        view.addSubview(leftview)
        leftview.backgroundColor = .clear
        leftview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 20.calcvaluex(), bottom: 0, right: 0),size: .init(width: 402.calcvaluex(), height: 0))
        leftview.separatorStyle = .none
        leftview.delegate = self
        leftview.dataSource = self
        view.addSubview(rightview)
        rightview.backgroundColor = .clear
        rightview.anchor(top: topview.bottomAnchor, leading: leftview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        rightview.delegate = self
    }
}

extension SomolyRecommendationController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return series.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = RecommandCell(style: .default, reuseIdentifier: "cell")
        let sr = series[indexPath.row]
        if let urlString = sr.assets?.getMachinePath() {
            if let url = URL(string: urlString) {
                cell.imageview.sd_setImage(with: url, completed: nil)
            }
        }
        if let sr = sr.titles?.zhTW {
            cell.label.text = sr
        }
        if indexPath.row == currentIndex {
            cell.setColor()
        }
        else{
            cell.unsetColor()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        self.sortData()
        self.leftview.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension SomolyRecommendationController: RecommandDataDelegate {
    
    func getSpec(series:Series){
       
        if let pr = series.products {
            if pr.count == 0{
                SpecArray.constructValue(field: [])
                return
            }
            for a in pr {
                if let spec = a.specifications {
                    for s in spec{
                       
                                SpecArray.constructValue(field: s.fields)
                                break
                        
                        }
                }

                    
                
            }
        }
        else{
            SpecArray.constructValue(field: [])
            return
        }

    }
    
    func goToProduct(spec: SomolyMachineSpecification) {
        let hud = JGProgressHUD()
         hud.show(in: self.view)
        
    let in_series = series[currentIndex]
    let seriesId = in_series.id
    let tokenDict = ["check_token": UserDefaults.standard.getUpdateToken(id: seriesId) ]
    if let category = UserDefaults.standard.getCatValueData(id: seriesId){
        NetworkCall.shared.postCall(parameter: "api-or/v1/category/\(seriesId )/check", dict: tokenDict, decoderType: UpdateClass.self) { (json) in
            DispatchQueue.main.async {
                if let json = json {
                        
                    if !json.data {
                            
                            NetworkCall.shared.getCall(parameter: "api-or/v1/categories", decoderType: topData.self) { (json) in

                                
                                if let json = json {

                                    if let dt = json.data?.first(where: { (st) -> Bool in
                                        return st.id == seriesId
                                    }) {
                                        UserDefaults.standard.saveUpdateToken(token: dt.update_token ?? "", id: dt.id )
                                        
                                }
                                }
                                else{
                                                    hud.dismiss()
                                                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)

                                                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))

                                                    self.present(alert, animated: true, completion: nil)
                                                    return
                                }
                                
                            }
                            
                            
                            NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(seriesId)", decoderType: topData.self) { (json) in
                                if let json = json{
                                do{
                                    let encode = try JSONEncoder().encode(json)
                                    UserDefaults.standard.saveCatValueData(data: encode, id: seriesId)
                                }
                                catch{
                                    
                                }
                                    DispatchQueue.main.async {
                                        //////
                                        hud.dismiss()
                                        if let data = json.data{
                                        if let first = data.first(where: { (sr) -> Bool in
                                            return sr.titles?.zhTW == spec.getMachineData()["name"]
                                        }) {
                                            let vc = ProductInnerInfoController()
                                            vc.modalPresentationStyle = .fullScreen
                                            
                                            vc.titleview.text = "\(in_series.titles?.zhTW ?? "")"
                                            vc.tabcon.allSeries = data
                                            vc.tabcon.selectedSeries = first
                                            self.getSpec(series:first)
                                            self.present(vc, animated: true, completion: nil)
                                        }
                                        }
                                      
                                    }
                                    
                                    
                                    
                                
                                }
                                else{
                                    do{
                                        let json = try JSONDecoder().decode(topData.self, from: category)
                                        
                                        DispatchQueue.main.async {
                                            /////
                                            hud.dismiss()
                                            if let data = json.data{
                                            if let first = data.first(where: { (sr) -> Bool in
                                                return sr.titles?.zhTW == spec.getMachineData()["name"]
                                            }) {
                                                let vc = ProductInnerInfoController()
                                                vc.modalPresentationStyle = .fullScreen
                                                
                                                vc.titleview.text = "\(in_series.titles?.zhTW ?? "")"
                                                vc.tabcon.allSeries = data
                                                vc.tabcon.selectedSeries = first
                                                self.getSpec(series:first)
                                                self.present(vc, animated: true, completion: nil)
                                            }
                                            }
                                        }
                                        
                                    }
                                    catch let error{
                                        print(371,error)
                                    }
                                }
                            }
                        }
                        else{
                            do{
                                let json = try JSONDecoder().decode(topData.self, from: category)
                                DispatchQueue.main.async {
                                    //////////
                                    hud.dismiss()
                                    if let data = json.data{
                                    if let first = data.first(where: { (sr) -> Bool in
                                        return sr.titles?.zhTW == spec.getMachineData()["name"]
                                    }) {
                                        let vc = ProductInnerInfoController()
                                        vc.modalPresentationStyle = .fullScreen
                                        
                                        vc.titleview.text = "\(in_series.titles?.zhTW ?? "")"
                                        vc.tabcon.allSeries = data
                                        vc.tabcon.selectedSeries = first
                                        self.getSpec(series:first)
                                        self.present(vc, animated: true, completion: nil)
                                    }
                                    }
                                }
                            }
                            catch let error{
                                print(371,error)
                            }
                        }
                    }
                    else{
                                        do{
                                            let json = try JSONDecoder().decode(topData.self, from: category)
                                            DispatchQueue.main.async {
                                                //////////
                                                hud.dismiss()
                                                if let data = json.data{
                                                if let first = data.first(where: { (sr) -> Bool in
                                                    return sr.titles?.zhTW == spec.getMachineData()["name"]
                                                }) {
                                                    let vc = ProductInnerInfoController()
                                                    vc.modalPresentationStyle = .fullScreen
                                                    
                                                    vc.titleview.text = "\(in_series.titles?.zhTW ?? "")"
                                                    vc.tabcon.allSeries = data
                                                    vc.tabcon.selectedSeries = first
                                                    self.getSpec(series:first)
                                                    self.present(vc, animated: true, completion: nil)
                                                }
                                                }
                                            }
                                                                                    }
                                        catch let error{
                                            print(371,error)
                                        }
                    }
                }
            }

        }
        else{

            NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(seriesId)", decoderType: topData.self) { (json) in
                
                if let json = json{
            do{
                
                let encode = try JSONEncoder().encode(json)
                UserDefaults.standard.saveCatValueData(data: encode, id: seriesId)
            }
            catch{
                
            }
                DispatchQueue.main.async {
                   ////////
                    hud.dismiss()
                    if let data = json.data{
                    if let first = data.first(where: { (sr) -> Bool in
                        return sr.titles?.zhTW == spec.getMachineData()["name"]
                    }) {
                        let vc = ProductInnerInfoController()
                        vc.modalPresentationStyle = .fullScreen
                        
                        vc.titleview.text = "\(in_series.titles?.zhTW ?? "")"
                        vc.tabcon.allSeries = data
                        vc.tabcon.selectedSeries = first
                        self.getSpec(series:first)
                        self.present(vc, animated: true, completion: nil)
                    }
                    }
                }
               
                
               
            
            }
            else{
                hud.dismiss()
                let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        }
    
    
    
    }
    
}
