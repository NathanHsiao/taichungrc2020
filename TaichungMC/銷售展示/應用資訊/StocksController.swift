//
//  StocksController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/28/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class StockCell : UICollectionViewCell {
    let image = UIImageView()
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        //image.backgroundColor = .red
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.backgroundColor = .white
        image.layer.borderWidth = 1.calcvaluex()
        image.layer.borderColor = #colorLiteral(red: 0.8869761825, green: 0.8871009946, blue: 0.8869369626, alpha: 1)
        addSubview(image)
        image.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 137.calcvaluey()))
        
        label.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.textAlignment = .center
        addSubview(label)
        label.anchor(top: image.bottomAnchor, leading: image.leadingAnchor, bottom: bottomAnchor, trailing: image.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol SelectionButtonDelegate {
    func deleteIndex(index:Int)
}
class SelectionButton : UIView {
    let label = UILabel()
    let xButton = UIButton(type: .custom)
    var delegate:SelectionButtonDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9407916665, green: 0.5144688487, blue: 0.007106156554, alpha: 1)
        layer.cornerRadius = 38.calcvaluey()/2
        
        label.textColor = .white
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        
        addSubview(label)
        label.fillSuperview(padding: .init(top: 7.calcvaluey(), left: 32.calcvaluex(), bottom: 7.calcvaluey(), right: 54.calcvaluex()))
        label.constrainHeight(constant: 25.calcvaluey())
        xButton.setImage(#imageLiteral(resourceName: "ic_multiplication").withRenderingMode(.alwaysTemplate), for: .normal)
        xButton.contentHorizontalAlignment = .fill
        xButton.contentVerticalAlignment = .fill
        xButton.tintColor = .white
        
        addSubview(xButton)
        xButton.anchor(top: nil, leading: label.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 7.calcvaluey(), left: 6.calcvaluex(), bottom: 7.calcvaluey(), right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        xButton.centerYInSuperview()
        xButton.addTarget(self, action: #selector(deleteData), for: .touchUpInside)
    }
    @objc func deleteData(){
        delegate?.deleteIndex(index: self.tag)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CatView : UIView {
    let header = UILabel()
    let selectionVd = SelectionButton()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(header)
       
        header.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        header.textColor = #colorLiteral(red: 0.1345235407, green: 0.09256006032, blue: 0.08165547997, alpha: 1)
        
        header.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 25.calcvaluey()))
        header.centerYInSuperview()
        
        addSubview(selectionVd)
        selectionVd.anchor(top: nil, leading: header.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 38.calcvaluey()))
        selectionVd.centerYInSuperview()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol StockCategoriesViewDelegate{
    func resetData(data:[[String:ApplicationData]]?)
}
class StocksCategoriesView : UIView,SelectionButtonDelegate {
    var delegate:StockCategoriesViewDelegate?
    func deleteIndex(index: Int) {
        print("delete",index)
        stackview.safelyRemoveArrangedSubviews()
        data?.remove(at: index)
        
        delegate?.resetData(data: data)
    }
    
    var stackview = UIStackView()
    let dropDownButton = UIButton(type: .custom)
    var data : [[String:ApplicationData]]? {
        didSet{
            stackview.safelyRemoveArrangedSubviews()
            if let data = data{
                for (index,i) in data.enumerated() {
                    for (key,val) in i {
                        var calculateWidth = 98.calcvaluex()
                        
                        calculateWidth += key.width(withConstrainedHeight: .infinity, font: UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!)
                        calculateWidth += val.title?.width(withConstrainedHeight: .infinity, font: UIFont(name: "Roboto-Medium", size: 18.calcvaluex())!) ?? 0
                        let cat_v = CatView()
                        cat_v.constrainWidth(constant: calculateWidth)
                        let headerdict = ["resin-type":"樹酯種類","stock-type":"成品類型","industry-kind":"產業類別","plastic-kind":"塑料類別"]
         
                        cat_v.header.text = headerdict[key]
                        cat_v.selectionVd.label.text = val.title
                        cat_v.selectionVd.tag = index
                        cat_v.selectionVd.delegate = self
                        stackview.addArrangedSubview(cat_v)
                    }
                }
                stackview.addArrangedSubview(UIView())
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        addshadowColor()
        backgroundColor = .white
        addSubview(stackview)
        stackview.axis = .horizontal
        stackview.spacing = 24.calcvaluex()
        stackview.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
        
        addSubview(dropDownButton)
        dropDownButton.setImage(#imageLiteral(resourceName: "ic_arrow_down").withRenderingMode(.alwaysTemplate), for: .normal)
        dropDownButton.contentHorizontalAlignment = .fill
        dropDownButton.contentVerticalAlignment = .fill
        
        dropDownButton.tintColor = #colorLiteral(red: 0.5685855746, green: 0.5686681271, blue: 0.568559587, alpha: 1)
        
        dropDownButton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 40.calcvaluex(), height: 40.calcvaluex()))
        dropDownButton.centerYInSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


enum StockSelectMode {
    case Select
    case None
    case Transforming
}
class StocksController:SampleController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,StockCategoriesViewDelegate,SelectionViewDelegate {
    func setSomolySelection(data: SomolyData) {
        //
    }
    func setSelect(current: Int, index: Int, data: ApplicationData) {
       // print(current,index,data)
        
        vd?.array[current][index] = data
    }
    
    weak var con : ApplicationController?
    var count : Int?
    func resetData(data: [[String : ApplicationData]]?) {

        var selectedarray : [ApplicationData?] = [nil,nil]
        if let data = data{
            for i in data {
                for (key,val) in i{
                    
                    if key == "resin-type" || key == "industry-kind"{
                        selectedarray[0] = val
                    }
                    else {
                    selectedarray[1] = val
                    }
                }
            }
        }
        
        
        if selectedarray[0] == nil && selectedarray[1] == nil {
            stock_data = []
            con?.array[count ?? 0] = selectedarray
            vd?.array = con?.array ?? [[nil,nil],[nil,nil]]
            return
        }
        
        con?.array[count ?? 0] = selectedarray
        vd?.array = con?.array ?? [[nil,nil],[nil,nil]]
        var o_filter = StockApi.shared.stocks
        
        for i in selectedarray {
            if let i = i{
                o_filter = o_filter?.filter({ (st) -> Bool in
                if let categories = st.categories {
                    
                    if categories.contains(i.id ?? "") {
                        return true
                    }
                }

                return false
            })
            }
            
        }
       
        stock_data = o_filter ?? []
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stock_data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StockCell
        cell.backgroundColor = #colorLiteral(red: 0.9795905948, green: 0.9797275662, blue: 0.9795475602, alpha: 1)
        cell.label.text = stock_data[indexPath.item].title
        if let url = URL(string: stock_data[indexPath.item].assets?.getImage() ?? "") {
            cell.image.sd_setImage(with: url, completed: nil)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vd = StockContentController()
        vd.modalPresentationStyle = .fullScreen
        vd.stock = stock_data[indexPath.item]
        self.present(vd, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = collectionView.frame.width - (36.calcvaluex()*3)
        return .init(width: width/4, height: 189.calcvaluey())
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 48.calcvaluey()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 36.calcvaluex()
    }
    var selectionView = StocksCategoriesView()
    var stock_data = [Stock]() {
        didSet{
            stock_data = stock_data.filter({ (st) -> Bool in
                return st.status == 1
            }).sorted(by: { (s1, s2) -> Bool in
                return s1.order < s2.order
            })
            self.stockcollectionViewController.reloadData()
        }
    }
    //var filter_data = [Stock]()
    var mode : StockSelectMode = .None
    lazy var stockcollectionViewController : UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        let lc = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        lc.backgroundColor = .clear
        lc.delegate = self
        lc.dataSource = self
        lc.register(StockCell.self, forCellWithReuseIdentifier: "cell")
        return lc
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "成型應用"
        

        
        view.addSubview(selectionView)
        selectionView.delegate = self
        selectionView.anchor(top: topview.bottomAnchor, leading: topview.leadingAnchor, bottom: nil, trailing: topview.trailingAnchor,size: .init(width: 0  , height: 65.calcvaluey()))
        view.addSubview(stockcollectionViewController)
        stockcollectionViewController.anchor(top: selectionView.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 36.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 40.calcvaluex()))
   
        selectionView.dropDownButton.addTarget(self, action: #selector(showApplicationView), for: .touchUpInside)
        
    }
    var vd : ApplicationView?
    @objc func showApplicationView() {
        if mode == .None {
            mode = .Transforming
        vd = ApplicationView()
            if let vs = vd{
            vs.adLabalAnchor?.trailing?.isActive = false
                vs.adLabel.leadingAnchor.constraint(equalTo: vs.s1.leadingAnchor).isActive = true
                vs.s1Anchor?.leading?.constant = 26.calcvaluex()
                vs.s1Anchor?.width?.constant = 473.calcvaluex()
                vs.s2Anchor?.width?.constant = 473.calcvaluex()

                vs.backgroundColor = #colorLiteral(red: 0.9999158978, green: 1, blue: 0.9998719096, alpha: 1)
        view.addSubview(vs)
                vs.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 617.calcvaluey()))
                vs.current = self.count ?? 0
                vs.array = con?.array ?? [[nil,nil],[nil,nil]]
                
                
                vs.s1.firstSelect.selectionView.tag = 0
                vs.s1.secondSelect.selectionView.tag = 1
                vs.s2.firstSelect.selectionView.tag = 2
                vs.s2.secondSelect.selectionView.tag = 3
                vs.s1.firstSelect.selectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choice)))
                vs.s1.secondSelect.selectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choice)))
                vs.s2.firstSelect.selectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choice)))
                vs.s2.secondSelect.selectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(choice)))
                vs.s1.confirmB.addTarget(self, action: #selector(goData), for: .touchUpInside)
                vs.s2.confirmB.addTarget(self, action: #selector(goData), for: .touchUpInside)
                
        self.view.layoutIfNeeded()
        selectionView.stackview.isHidden = true

        
        UIView.animate(withDuration: 0.4) {
            vs.transform = CGAffineTransform(translationX: 0, y: -617.calcvaluey())
        } completion: { (_) in
            self.selectionView.dropDownButton.setImage(#imageLiteral(resourceName: "ic_arrow_up").withRenderingMode(.alwaysTemplate), for: .normal)
            self.mode = .Select
        }
            }
        }
        else if mode == .Select{
            mode = .Transforming
            UIView.animate(withDuration: 0.4) {
                self.vd?.transform = .identity
            } completion: { (_) in
                self.selectionView.dropDownButton.setImage(#imageLiteral(resourceName: "ic_arrow_down").withRenderingMode(.alwaysTemplate), for: .normal)
                self.vd?.removeFromSuperview()
                self.selectionView.stackview.isHidden = false
                self.mode = .None
            }
        }

    }
    @objc func goData(sender:UIButton) {
        var fd = [[String:ApplicationData]]()
        if sender.tag == 0{
            if vd?.array[0][0] == nil && vd?.array[0][1] == nil{
                print("AA")
                
                return
            }
            
            if let at = vd?.array[0][0] {
                let vd = ["樹酯種類":at]
                 
                fd.append(vd)
            }
            if let at = vd?.array[0][1] {
                let vd = ["成品類型":at]
                fd.append(vd)
            }
        }
        else{
            if vd?.array[1][0] == nil && vd?.array[1][1] == nil{
                print("BB")
                
                return
            }
            
            if let at = vd?.array[1][0] {
                let vd = ["產業類別":at]
                fd.append(vd)
            }
            
            if let at = vd?.array[1][1] {
                let vd = ["塑料類別":at]
                fd.append(vd)
                
                
            }
        }
        
        if var filterdata = StockApi.shared.stocks {
        //self.filter_data = filterdata
            for i in vd?.array[sender.tag] ?? [nil,nil] {
            if let i = i{
            filterdata = filterdata.filter({ (st) -> Bool in
                if let categories = st.categories {
                    
                    if categories.contains(i.id ?? "") {
                        return true
                    }
                }

                return false
            })
            }
        }
        
            self.stock_data = filterdata
        }
        con?.array = vd?.array ?? [[nil,nil],[nil,nil]]
        self.selectionView.data = fd
        mode = .Transforming
        UIView.animate(withDuration: 0.4) {
            self.vd?.transform = .identity
        } completion: { (_) in
            self.selectionView.dropDownButton.setImage(#imageLiteral(resourceName: "ic_arrow_down").withRenderingMode(.alwaysTemplate), for: .normal)
            self.vd?.removeFromSuperview()
            self.selectionView.stackview.isHidden = false
            self.mode = .None
        }
    }
    @objc func choice(gesture:UITapGestureRecognizer){
        var title : String? = ""
        var count : Int?
        var index : Int?
        if let vt = gesture.view {
            switch vt.tag {
            case 0:
                title = vd?.s1.firstSelect.label.text
                count = 0
                index = 0
            case 1:
                title = vd?.s1.secondSelect.label.text
                count = 0
                index = 1
            case 2:
                title = vd?.s2.firstSelect.label.text
                count = 1
                index = 0
            case 3:
                title = vd?.s2.secondSelect.label.text
                count = 1
                index = 1
            default:
                ()
            }
        }
        
            if let dt = StockApi.shared.cjson {
                if let data = dt.first(where: { (ap) -> Bool in
                    if ap.title == title {
                        return true
                    }
                    return false
                }) {
                    
                    let vd = SelectionViewController()
                    vd.count = count
                    vd.index = index
                    
                    vd.popView.data = data.children
                    vd.popView.cdelegate = self
                    vd.modalPresentationStyle = .overCurrentContext
                    self.present(vd, animated: false, completion: nil)
                    
                }
            }
       // }

    }
}

extension UIView {
    
    public func removeAllConstraints() {
        var _superview = self.superview
        
        while let superview = _superview {
            for constraint in superview.constraints {
                
                if let first = constraint.firstItem as? UIView, first == self {
                    superview.removeConstraint(constraint)
                }
                
                if let second = constraint.secondItem as? UIView, second == self {
                    superview.removeConstraint(constraint)
                }
            }
            
            _superview = superview.superview
        }
        
        self.removeConstraints(self.constraints)
        self.translatesAutoresizingMaskIntoConstraints = true
    }
}
