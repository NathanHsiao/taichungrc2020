//
//  Application.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 1/27/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation

struct Application : Codable {
    var data: [ApplicationData]?
    var meta: Meta
}
struct Meta: Codable {
    var update_token : String
}
struct ApplicationData : Codable{
    var id : String?
    var parent_id: String?
    var key_id : String?
    var title : String?
    var titles : Lang?
    var children : [ApplicationData]?
    
}

struct Stocks : Codable {
    var data : [Stock]?
    var meta: Meta
}
struct Stock : Codable {
    var id : String
    var order : Int
    var status : Int

    var title : String
    var categories : [String]?
    var assets : Assets?
    var fields : [String:Lang?]?
}


struct StockFields : Codable {
    var data : [S_Fields]?
    var meta : Meta
}

struct S_Fields: Codable {
    var id: String?
    var title: String?
    var titles: Lang?
    var code : String?
    var affix : String?
}
