//
//  ProductModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/21/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
struct TopProducts : Codable {
    var data : [Product]
}
struct PLocation: Codable {
    var title : String
}
struct PRadius : Codable {
    var title : String
}
struct Audits : Codable {
    var id : Int
    var status : Int
    var content : [AuditContent]
    var assets : Assets?
}
struct AuditContent :Codable {
    var type : Int
    var content: String?
    var reply : String?
    var qty : Int?
    var price : String?
    var id : String?
    var files : [AddedOptionFile]?
}
struct Product : Codable {
    var company_code : String?
    var full_name : String?
    var id : String
    var order : Int?
    var shooter_code : String?
    var status : Int?
    var product_id : String?
    var is_disable : Int?
    var title:String?
    var titles: Lang?
    var description:[String]?
   var descriptions : LangArray?
    var options : [Option]?
    var assets: Assets?
    var model_number : String?
    var prices : [PriceGroup]?
    var price_groups : [String:String]?
    var specifications : [Specification]?
    var maps: [Map]?
    var audit : [Audits]?
    var qty : Int?
    private enum CodingKeys : String,CodingKey {
        case status,full_name,company_code,is_disable,product_id,id,order,title,titles,description,descriptions,model_number,options,prices,price_groups,specifications,maps,assets,audit,qty,shooter_code
    }
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try? container.decode(Int?.self,forKey: .status)
        full_name = try? container.decode(String?.self,forKey: .full_name)
        company_code = try? container.decode(String?.self,forKey: .company_code)
        is_disable = try? container.decode(Int?.self,forKey: .is_disable)
        product_id = try? container.decode(String?.self,forKey: .product_id)
        
        order = try? container.decode(Int?.self,forKey: .order)
        title = try? container.decode(String?.self,forKey: .title)
        titles = try? container.decode(Lang?.self,forKey: .titles)
        description = try? container.decode([String]?.self,forKey: .description)
        
        model_number = try? container.decode(String?.self,forKey: .model_number)
        options = try? container.decode([Option]?.self,forKey: .options)
        prices = try? container.decode([PriceGroup]?.self,forKey: .prices)
        price_groups = try? container.decode([String:String]?.self,forKey: .price_groups)
        specifications = try? container.decode([Specification]?.self,forKey: .specifications)
        maps = try? container.decode([Map]?.self,forKey: .maps)
        
        shooter_code = try? container.decode(String?.self, forKey: .shooter_code)
        audit = try? container.decode([Audits]?.self,forKey: .audit)
        qty = try? container.decode(Int?.self,forKey: .qty)
        do{
            assets = try container.decode(Assets?.self,forKey: .assets)
            
            
            id = try String(container.decode(Int.self, forKey: .id))
            
        }
        catch let error{
          
            id = try container.decode(String.self, forKey: .id)
        }
        
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(company_code, forKey: .company_code)
        try container.encode(order, forKey: .order)
        try container.encode(status, forKey: .status)
        try container.encode(shooter_code, forKey: .shooter_code)
        try container.encode(is_disable, forKey: .is_disable)
        try container.encode(product_id, forKey: .product_id)
        try container.encode(price_groups, forKey: .price_groups)
        try container.encode(audit, forKey: .audit)
        try container.encode(specifications, forKey: .specifications)
        try container.encode(maps, forKey: .maps)
        try container.encode(full_name, forKey: .full_name)
        
        
       
        try container.encode(title, forKey: .title)
        try container.encode(titles, forKey: .titles)
        
        try container.encode(description, forKey: .description)
        try container.encode(descriptions, forKey: .descriptions)
        
        try container.encode(model_number, forKey: .model_number)
        try container.encode(options, forKey: .options)
        
        try container.encode(prices, forKey: .prices)
        try container.encode(assets, forKey: .assets)
    }
    func getProductsPrices() -> PriceGroup?{
        let dt = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId())
        if let dt = dt{
        let json = try? JSONDecoder().decode(UserData.self, from: dt)
            if !(json?.data?.attributes?.currencies.isEmpty ?? true) && !(json?.data?.attributes?.currency_groups.isEmpty ?? true){
                if let matchingGroup = json?.data?.attributes?.currency_groups[0] {
                    let matchingCurrency = UserDefaults.standard.getChosenCurrency()
                    if let price = self.prices?.first(where: { (pr) -> Bool in
                        return pr.currency == matchingCurrency && pr.group == matchingGroup
                    }) {
                        return price
                    }
                    else if let price = self.prices?.first(where: { (pr) -> Bool in
                        return pr.currency == matchingCurrency && pr.group == "X"
                    }) {
                        return price
                    }
                }
            }
            
        }
        
        return nil
        
    }

}
struct Assets : Codable {
    var three360 : [FilePath]?
    var background : [FilePath]?
    var machine : [FilePath]?
    var specification : [FilePath]?
    var image : [FilePath]?
    var file : [FilePath]?
    private enum CodingKeys: String,CodingKey {
        case background,machine,three360 = "360",specification,image,file
    }
    func getFile() -> String{
        if let ab = file {
            if ab.count == 0{
                return ""
            }
            else{
                if let remote = ab[0].remote {
                    return remote
                }
                if let m = ab[0].files?.zhTW {
                    if m.count == 0{
                        return ""
                    }
                    else{
                        switch m[0].path {
                        case .arrayString(let array) :
                            if let f = array {
                                
                                if f.count == 0{
                                    return ""
                                }
                                else{
                                    return "\(AppURL().baseURL)uploads/\((f[0] ?? "").addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                                }
                            }

                        case .string(let st):
                            if let f = st {
                                return "\(AppURL().baseURL)uploads/\(f.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                            }
                        default:
                            ()
                            
                        }
                    }
                }
            }
        }
        return ""
    }
    func getImage() -> String {
        if let ab = image {
            if ab.count == 0{
                return ""
            }
            else{
                if let remote = ab[0].remote {
                    return remote
                }
                if let m = ab[0].files?.zhTW {
                    if m.count == 0{
                        return ""
                    }
                    else{
                        switch m[0].path {
                        case .arrayString(let array) :
                            if let f = array {
                                
                                if f.count == 0{
                                    return ""
                                }
                                else{
                                    return "\(AppURL().baseURL)uploads/\((f[0] ?? "").addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                                }
                            }

                        case .string(let st):
                            if let f = st {
                                return "\(AppURL().baseURL)uploads/\(f.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                            }
                        default:
                            ()
                            
                        }
                    }
                }
            }
        }
        return ""
    }
    func getMachinePath() -> String {
        if let ab = machine {
            if ab.count == 0{
                return ""
            }
            else{
                if let remote = ab[0].remote {
                    return remote
                }
                if let m = ab[0].files?.zhTW {
                    if m.count == 0{
                        return ""
                    }
                    else{
                        switch m[0].path {
                        case .arrayString(let array) :
                            if let f = array {
                                
                                if f.count == 0{
                                    return ""
                                }
                                else{
                                    return "\(AppURL().baseURL)uploads/\((f[0] ?? "").addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                                }
                            }

                        case .string(let st):
                            if let f = st {
                                return "\(AppURL().baseURL)uploads/\(f.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                            }
                        default:
                            ()
                            
                        }
                    }
                }
            }
        }
        return ""
    }
    
    func getBackgroundPath() -> String {
        if let ab = background {
            if ab.count == 0{
                return ""
            }
            else{
                if let remote = ab[0].remote {
                    return remote
                }
                if let m = ab[0].files?.zhTW {
                    if m.count == 0{
                        return ""
                    }
                    else{
                        switch m[0].path {
                        case .arrayString(let array) :
                            if let f = array {
                                
                                if f.count == 0{
                                    return ""
                                }
                                else{
                                    return "\(AppURL().baseURL)uploads/\((f[0] ?? "").addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                                }
                            }

                        case .string(let st):
                            if let f = st {
                                return "\(AppURL().baseURL)uploads/\(f.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                            }
                        default:
                            ()
                            
                        }

                    }
                }
            }
        }
        return ""
    }
    
}

struct FilePath : Codable {
    var id:String
    var title:String?
    var remote : String?
    var files : Files?
    
    func getUrlString() -> String {
        if remote != nil{
            return remote ?? ""
        }
        
        if let m = files?.zhTW {
            if let ab = m.first {
                switch ab.path {
                case .string(let fr):
                    return "\(AppURL().baseURL)uploads/\(fr?.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                case .arrayString(let fr):
                    if let mt = fr?.first {
                        return "\(AppURL().baseURL)uploads/\(mt?.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                    }
                    
                default:
                    ()
                }
                
            }
        }
        
        return ""
        
        
    }
}
struct Option : Codable {
    var id : String
    var selected : Bool?
    var title:String
    var titles:Lang?
    var order:Int?
    var prices : [PriceGroup]?
    var remote : String?
    var files : Files?
    var children : [Option]?
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(title, forKey: .title)
        try container.encode(id, forKey: .id)
        try container.encode(order, forKey: .order)
        try container.encode(titles, forKey: .titles)
        try container.encode(prices, forKey: .prices)
        try container.encode(remote, forKey: .remote)
        try container.encode(files, forKey: .files)
        try container.encode(children, forKey: .children)
        //try container.encode(remotes, forKey: .remotes)
        try container.encode(selected, forKey: .selected)
        
    }
    
    func getUrlString() -> String {
        if remote != nil{
            return remote ?? ""
        }
        
        if let m = files?.zhTW {
            if let ab = m.first {
                switch ab.path {
                case .string(let fr):
                    return "\(AppURL().baseURL)uploads/\((fr ?? "").addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"
                default:
                    ()
                }
                
            }
        }
        
        return ""
        
        
    }
    func getProductsPrices() -> PriceGroup?{
        let dt = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId())
        if let dt = dt{
        let json = try? JSONDecoder().decode(UserData.self, from: dt)
            if !(json?.data?.attributes?.currencies.isEmpty ?? true) && !(json?.data?.attributes?.currency_groups.isEmpty ?? true){
                if let matchingCurrency = json?.data?.attributes?.currencies[0], let matchingGroup = json?.data?.attributes?.currency_groups[0] {
                    if let price = self.prices?.first(where: { (pr) -> Bool in
                        return pr.currency == matchingCurrency && pr.group == matchingGroup
                    }) {
                        return price
                    }
                    else if let price = self.prices?.first(where: { (pr) -> Bool in
                        return pr.currency == matchingCurrency && pr.group == "X"
                    }) {
                        return price
                    }
                }
            }
            
        }
        
        return nil
        
    }
}
struct Specification : Codable {
    var code: String?
    var title: String?
    var titles : Lang?
    var fields : [Field]
    
    func getInjectionValue(nullValue:String="全部") -> String{
        let mt = self.fields.first { (f1) -> Bool in
            return f1.code == "screw-diameter"
        }
        
        return mt?.value ?? nullValue
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(title, forKey: .title)
        try container.encode(titles, forKey: .titles)
        try container.encode(fields, forKey: .fields)
        
    }
}
struct Field : Codable {
    var code : String?
    var title : String?
    var titles : Lang?
    var affix : String?
    var prefix:String?
    var value : String?
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(title, forKey: .title)
        try container.encode(titles, forKey: .titles)
        try container.encode(prefix, forKey: .prefix)
        try container.encode(affix, forKey: .affix)
        try container.encode(value, forKey: .value)
    }
}
struct LangArray : Codable {
    var en:[String]?
    var zhTW:[String]?
    
    private enum CodingKeys: String,CodingKey {
        case en, zhTW = "zh-TW"
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(en, forKey: .en)
        try container.encode(zhTW, forKey: .zhTW)
    }
}

struct PriceGroup : Codable {
    var group: String
    var currency: String
    var price : Double?
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(price, forKey: .price)
        try container.encode(group, forKey: .group)
        try container.encode(currency, forKey: .currency)
    }
}


struct Map : Codable {
    var id : String
    var image : String?
    var hotspots : [Hotspot]?
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(image, forKey: .image)
        try container.encode(hotspots, forKey: .hotspots)
        try container.encode(id, forKey: .id)
    }
    
}

struct Hotspot: Codable {
    var coordinate : HotspotLocation?
    var remote : String?
    var files: Files?
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(coordinate, forKey: .coordinate)
        try container.encode(remote, forKey: .remote)
        try container.encode(files, forKey: .files)
        //try container.encode(remotes, forKey: .remotes)
        
    }
}
struct HotspotLocation: Codable {
    var id : String
    var width : String
    var height : String
    var top : String
    var left : String
}
struct Files : Codable {
    var en:[File]?
    var zhTW:[File]?
    
    private enum CodingKeys: String,CodingKey {
        case en, zhTW = "zh-TW"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        do{
            zhTW = try container.decode([File].self, forKey: .zhTW)
        }
        catch{
            zhTW = nil
        }
        
        do{
            en = try container.decode([File].self, forKey: .en)
        }
        catch{
            en = nil
        }
    }
}

struct File : Codable {
    //var id : String
    var path: FileType?
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(path, forKey: .path)

        
    }
//    init(from decoder: Decoder) throws {
//        if let int = try? String?(from: decoder) {
//            path = int
//            return
//        }
//
//        path = try [String?]?(from: decoder)
//    }
}
enum FileType: Codable {
    case string(String?)
    case arrayString([String?]?)
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String?.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode([String?]?.self) {
            self = .arrayString(x)
            return
        }
        throw DecodingError.typeMismatch(FileType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }
    func encode(to encoder: Encoder) throws {
        //var container = encoder.container(keyedBy: CodingKeys.self)
       var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .arrayString(let x):
            try container.encode(x)
        }
    }
}
//extension FileType {
//    private enum CodingKeys: String, CodingKey {
//        case string
//        case arrayString
//    }
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        switch self {
//        case .string(let number):
//            try container.encode(number, forKey: .string)
//        case .arrayString(let value):
//            try container.encode(value, forKey: .arrayString)
//        }
//    }
//}
struct NewSpecField {
    var title:String
    var decodeId:String
    var value : [String?] = [nil,nil,nil,nil,nil,nil]
    var unit : String? = ""
    var prefix : String? = ""
    init(title:String,decodeId:String,unit:String? = "",prefix:String? = "") {
        self.title = title
        self.decodeId = decodeId
        self.unit = unit
        self.prefix = prefix
    }
}
struct compareSection {
    var title:String
    var array : [NewSpecField]
}
struct SpecArray {
    
    static var value = [compareSection]()

    static func constructValue(field:[Field]) {
        var fArray = field
        //fArray.removeFirst()
        var headerArray = [compareSection]()
        var count = -1
        
        for i in fArray {
            
            
            if i.value == "__" {
                headerArray.append(compareSection(title: i.title ?? "", array: []))
                count += 1
            }
            else{
                if count >= 0 && count < headerArray.count{
                headerArray[count].array.append(NewSpecField(title: i.title ?? "", decodeId: i.code ?? "",unit: i.affix ?? "",prefix: i.prefix ?? ""))
                }
            }
        }
        SpecArray.value = headerArray
    
    }

}
