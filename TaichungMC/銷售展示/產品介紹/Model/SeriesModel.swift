//
//  ProductModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/19/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import Foundation
struct topData: Codable {
    var data : [Series]?
}
struct TopLevelModel : Codable{
    var id : String
    
    var title : String?
    
    var children : [Series]?
}

struct Series : Codable {
    var isNone : Bool?
    var id : String
    var title : String?
    var titles : Lang?
    var is_disable : Int?
    var status : Int?
    var parent_id: String
    var update_token : String?
//
    var products : [Product]?
    var children : [Series]?
    var description : [String]?
    var assets: Assets?
    
    var selectedTitle:String?
    var selectedId:String?
    var order:Int?
    
}


