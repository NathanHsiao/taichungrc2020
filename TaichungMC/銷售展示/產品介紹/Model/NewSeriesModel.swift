//
//  NewSeriesModel.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 11/20/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

struct NewSeries: Codable {
    
    var data: [CommonData]?
    
    var included: [CommonData]?
}

struct CommonData : Codable{
    var id:String
    var attributes: Attributes?
    var relationships: Relationships?
    
 
    

}
enum FileType2: Codable {
    case string(Files?)
    case arrayString([Files?]?)
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Files?.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode([Files?]?.self) {
            self = .arrayString(x)
            return
        }
        throw DecodingError.typeMismatch(FileType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .arrayString(let x):
            try container.encode(x)
        }
    }
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .string(let x):
//            try container.encode(x)
//        case .arrayString(let x):
//            try container.encode(x)
//        }
//    }
}
struct Attributes : Codable {
    var parent_id : String?
    var title:String?
    var description:[String]?
    var tags : [String]?
    var remote : String?
    var files: FileType2?
    
    func searchWithTags(tag:String) -> URL?{
        if let _ = tags?.first(where: { (cm) -> Bool in
            return cm == tag
        }){
            if let rm = remote {
                return URL(string: rm)
            }
            else{
                if let ft = files {
                    switch ft {
                    case .string(let x):
                        if let t = x, let zh = t.zhTW?.first{
                            switch zh.path {
                            case .string(let m) :
                                if let k = m{
                                    return k.convertTofileUrl()

                                }
                            case .arrayString(let k):
                                if let mm = k?.first {
                                    return mm?.convertTofileUrl()

                                }
                            default:
                                ()
                            }
                            
                        }
                    case .arrayString(let g):
                        if let m = g?.first, let zh = m?.zhTW?.first{
                            switch zh.path {
                            case .string(let ff) :
                                if let k = ff{
                                    return k.convertTofileUrl()

                                }
                            case .arrayString(let k):
                                if let mm = k?.first {
                                    return mm?.convertTofileUrl()
                                }
                            default:
                                ()
                            }
                        }
                    default:
                        ()
                    }
                }
            }
        }
        return nil
    }
    
}
struct topCommonData : Codable {
    var data : [CommonData]?
    

}
struct Relationships : Codable {
    var assets: topCommonData
    
}
