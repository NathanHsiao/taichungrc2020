//
//  RcSpecDetailController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/15/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class RcSpecDetailController: UIViewController {
    let notShownView : UIView = {
       let vc = UIView()
        vc.backgroundColor = #colorLiteral(red: 0.9557610154, green: 0.9558981061, blue: 0.9557310939, alpha: 1)
        let vvd = UIView()
        vvd.backgroundColor = .white
        vvd.layer.cornerRadius = 15.calcvaluex()
        vvd.addshadowColor()
        vc.addSubview(vvd)
        vvd.fillSuperview(padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 0))
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: 26.calcvaluex())
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        label.text = "請先選擇機型"
        label.textAlignment = .center
        vvd.addSubview(label)
        label.centerXInSuperview()
        label.anchor(top: vvd.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 237.calcvaluey(), left: 0, bottom: 0, right: 0))
        return vc
    }()
    let js = SelectEquipController(nextvd: SpecRightView(), mode: .Equip)
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(notShownView)
        notShownView.fillSuperview()
        addChild(js)
        view.addSubview(js.view)
        js.view.fillSuperview()
    }
}
