//
//  SampleSpecTabController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/15/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class SpecSearchField : UITextField,UITextFieldDelegate {
    override init(frame: CGRect) {
        super.init(frame: frame)
        let img = UIImageView(image: #imageLiteral(resourceName: "ic_search").withRenderingMode(.alwaysTemplate))
        img.tintColor = #colorLiteral(red: 0.61385113, green: 0.6139419079, blue: 0.6138312817, alpha: 1)
        leftView = img
        backgroundColor = .white
        addshadowColor()
        layer.cornerRadius = 24.calcvaluey()
        leftViewMode = .unlessEditing
            delegate = self
        attributedPlaceholder = .init(string: "搜尋標配", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.61385113, green: 0.6139419079, blue: 0.6138312817, alpha: 1)])
        textColor = .black
    }
//
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 0))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 58.calcvaluex(), bottom: 0, right: 0))
    }
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.leftViewRect(forBounds: bounds)
        rect.origin.x += 26.calcvaluex()
        return rect
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SpecLeftCellContentView : UIView {
    let lb = UILabel()
    let seperator : UIView = {
       let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.9425347119, green: 0.9425347119, blue: 0.9425347119, alpha: 1)
        return sep
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        lb.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        lb.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        lb.text = "YUKE油壓系統220V"
        
        addSubview(lb)
        lb.centerYInSuperview()
        lb.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 28.calcvaluex(), bottom: 0, right: 0))
        
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol SpecLeftCellDelegate {
    func reloadTableWithIndex(index:Int)
}
class SpecLeftCell : UITableViewCell {
    let container = UIView()
    let stackview = UIStackView()
    let selectionCicle = UIImageView(image: #imageLiteral(resourceName: "btn_radio_pressed"))
    let leftLabel : UILabel = {
        let lfL = UILabel()
        lfL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        lfL.text = "半閉油壓系統380V"
        lfL.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        return lfL
    }()
    let seperator : UIView = {
       let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.8696922573, green: 0.8696922573, blue: 0.8696922573, alpha: 1)
        return sep
    }()
    func hideView(){
        seperator.isHidden = true
        stackview.isHidden = true
        selectionCicle.image = #imageLiteral(resourceName: "btn_radio_normal")
    }
    func showView(){
        seperator.isHidden = false
        stackview.isHidden = false
        selectionCicle.image = #imageLiteral(resourceName: "btn_radio_pressed")
    }
    var delegate:SpecLeftCellDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        selectionStyle = .none
        container.backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        container.addshadowColor()
        container.layer.cornerRadius = 15.calcvaluex()

        container.addSubview(selectionCicle)
        selectionCicle.contentMode = .scaleToFill
        selectionCicle.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 28.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        //container.layer.masksToBounds = true
        container.addSubview(leftLabel)
        leftLabel.anchor(top: selectionCicle.topAnchor, leading: selectionCicle.trailingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
        container.addSubview(seperator)
        seperator.anchor(top: selectionCicle.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 17.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 1.calcvaluey()))
        stackview.axis = .vertical
        stackview.distribution = .fillEqually
        
        for i in 0 ..< 5{
            let vd = SpecLeftCellContentView()
            if i == 4{
                vd.seperator.isHidden = true
            }
            else{
                vd.seperator.isHidden = false
            }
            stackview.addArrangedSubview(vd)
        }
        container.addSubview(stackview)
        stackview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 57.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0),size: .init(width: 0, height: 250.calcvaluey()))
        
        contentView.addSubview(container)
        container.fillSuperview(padding: .init(top: 6.calcvaluey(), left: 26.calcvaluex(), bottom: 6.calcvaluey(), right: 9.calcvaluex()))
        selectionCicle.isUserInteractionEnabled = true
        selectionCicle.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(itemSelected)))
        
    }
    @objc func itemSelected(){
        delegate?.reloadTableWithIndex(index:self.tag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SpecLeftView : UIView,UITableViewDelegate,UITableViewDataSource,SpecLeftCellDelegate{
    func reloadTableWithIndex(index: Int) {
        if selectedIndex != index {
        selectedIndex = index
            
            tb.reloadSections(IndexSet(integer: 0), with: .automatic)
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SpecLeftCell(style: .default, reuseIdentifier: "cell")
        cell.tag = indexPath.row
        cell.delegate = self
        if selectedIndex == indexPath.row {
            cell.showView()
        }
        else{
            cell.hideView()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == selectedIndex {
            return UITableView.automaticDimension
        
        }
        else{
            return 70.calcvaluey()
        }
    }
//
    let searchvd = SpecSearchField()
    let tb = UITableView(frame: .zero, style: .plain)
    var selectedIndex = 0
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        
        addSubview(searchvd)
        searchvd.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 9.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        
        addSubview(tb)
        tb.separatorStyle = .none
        tb.showsVerticalScrollIndicator = false
        tb.backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        tb.delegate = self
        tb.dataSource = self
        tb.anchor(top: searchvd.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0))
        tb.contentInset = .init(top: 0, left: 0, bottom: 70.calcvaluey(), right: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SampleSpecTabController: UIViewController {
    var leftview : UIView?
    var lwidth : CGFloat?
    var rightview: UIView?
    init(leftview:UIView,width:CGFloat,rightview:UIView) {
        super.init(nibName: nil, bundle: nil)
        
        self.leftview = leftview
        self.lwidth = width
        self.rightview = rightview
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if let leftview = leftview, let width = lwidth, let rightview = rightview {
            view.addSubview(leftview)
            leftview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: width, height: 0))
            
            view.addSubview(rightview)
            rightview.anchor(top: view.topAnchor, leading: leftview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        }
        print(991,self.superclass)
        
    }
}


