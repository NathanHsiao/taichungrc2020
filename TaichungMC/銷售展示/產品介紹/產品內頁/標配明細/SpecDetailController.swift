//
//  SpecDetailController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class SpecDetailController: UIViewController {
    let spec = SpecTableView()
    let machineLabel = UILabel()
    let selectLabel = SearchTextField()
    
    let selectContent = UIView()
    var selectContentConstraint : NSLayoutConstraint!
    var selectContentTableView = UITableView(frame: .zero, style: .grouped)
    //var tArray = titleArray().getTitle()
    var topOptions = [Option]()
    var originalOptions = [Option]()
    var selectedProducts:Product? {
        didSet{
            
            topOptions = selectedProducts?.options?[0].children ?? []
            originalOptions = topOptions
            textFieldDidEndEditing(selectLabel)
            //spec.topOptions = topOptions
            
            if let price = selectedProducts?.getProductsPrices() {
                
                
                    
                spec.priceLabel.text = "\(price.currency) \((price.price ?? 0).converToPrice() ?? "0元")"
                

            }
            else{
                
                spec.priceLabel.text = "0元"
            }
            
            
        }
    }
    var selectedSeries: Series? {
        didSet{
//            print(selectedSeries?.products?[0].options?[0].children)
           
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        spec.bottomTopAnchor.trailing?.constant = 0
        
        view.addSubview(spec)
        spec.fillSuperview()
        
        selectLabel.layer.cornerRadius = 46.calcvaluey()/2
        selectLabel.addshadowColor()
        selectLabel.backgroundColor = .white
        selectLabel.attributedPlaceholder = NSAttributedString(string: "搜尋標配", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        selectLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        selectLabel.autocapitalizationType = .none
        selectLabel.autocorrectionType = .no
        selectLabel.returnKeyType = .search
        selectLabel.clearButtonMode = .always
        view.addSubview(selectLabel)
        selectLabel.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 342.calcvaluex(), height: 46.calcvaluey()))
        view.addSubview(selectContent)
                selectContent.addShadowColor(opacity: 0.1)
         
                selectContent.anchor(top: selectLabel.bottomAnchor, leading: selectLabel.leadingAnchor, bottom: nil, trailing: selectLabel.trailingAnchor)
        selectContentConstraint = selectContent.heightAnchor.constraint(equalToConstant: 0)
                selectContentConstraint.isActive = true
                
                selectLabel.delegate = self
                
                selectContent.addSubview(selectContentTableView)
                selectContentTableView.layer.cornerRadius = 5.calcvaluex()
        selectContentTableView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
                selectContentTableView.fillSuperview()
                
                selectContentTableView.separatorStyle = .none
        let tap = UITapGestureRecognizer(target: self, action: #selector(popContent))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        popContent()
    }
}
extension SpecDetailController:UITextFieldDelegate {
    @objc func popContent(){
        selectContentConstraint.constant = 0
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func showContent(){
        selectContentConstraint.constant = 320.calcvaluey()
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func filterByText(textField:UITextField) {
        topOptions = originalOptions.filter({ (op) -> Bool in
            if let text = textField.text {
                if op.title.lowercased().contains(text.lowercased()) {
                    return true
                }
            }
            if let _ = op.children?.firstIndex(where: { (op1) -> Bool in
                if let text = textField.text {
                    if op1.title.lowercased().contains(text.lowercased()) {
                        return true
                    }
                }
                return false
            }) {
                return true
            }
            
            return false
        })
        print(topOptions)
        for (ind,i) in topOptions.enumerated() {
            topOptions[ind].children = i.children?.filter({ (op) -> Bool in
                if let txt = textField.text {
                    if op.title.lowercased().contains(txt.lowercased()) {
                        return true
                    }
                }
                return false
            })
            
        }

    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil{
            topOptions = originalOptions
        }
        else{
        filterByText(textField: textField)
        }
        spec.topOptions = topOptions
    }
}

