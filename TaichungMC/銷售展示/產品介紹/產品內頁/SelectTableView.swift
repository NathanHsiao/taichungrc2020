//
//  SelectTableView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/22/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class sView : UIView{
    weak var compareitem : ComparingItemController?
    var twoItemIndex : Int?
    var specCon : SampleRCTopViewController?
    var isNone = false
}
class SelectTableView<T:Codable> : sView,UITableViewDelegate,UITableViewDataSource {
    let tb = UITableView(frame: .zero, style: .plain)
    
    var items = [T]()
    
    init(items:[T]) {
        super.init(frame: .zero)
        self.items = items
        self.backgroundColor = .white
        
        self.addshadowColor()
        
        addSubview(tb)
        tb.backgroundColor = .white
        tb.fillSuperview()
        tb.separatorStyle = .none
        tb.delegate = self
        tb.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : selectViewCell?
        if let pitems = items[indexPath.item] as? Series {
            cell = selectViewCell(txt: pitems.title ?? "")
            if pitems.is_disable == 1{
                cell?.label.textColor = .lightGray
            }
            else{
                cell?.label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
            }
        }
        else if let pitems = items[indexPath.item] as? Product {
            cell = selectViewCell(txt: isNone == true ? pitems.title ?? "" : pitems.shooter_code ?? "")
            if pitems.is_disable == 1{
                cell?.label.textColor = .lightGray
            }
            else{
                cell?.label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
            }
        }
        else if let pitems = items[indexPath.item] as? Specification{

            cell = selectViewCell(txt: pitems.fields.first { (f1) -> Bool in
                f1.code == "screw-diameter"
            }?.value ?? "")
        }
        if indexPath.item == items.count - 1{
            cell?.seperator.isHidden = true
        }
        else{
            cell?.seperator.isHidden = false
        }
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.calcvaluey()
    }
    func doCompare(indexPath:IndexPath) {
        if let pr = items as? [Series] {
            
            if pr[indexPath.item].title != compareitem?.itemsArray[self.tag].nLabel.titleLabel?.text {
                compareitem?.itemsArray[self.tag].sLabel.setTitle("選擇", for: .normal)
                compareitem?.itemsArray[self.tag].sLabel.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
                
                compareitem?.itemsArray[self.tag].zLabel.fLabel.setTitle("選擇", for: .normal)
                compareitem?.itemsArray[self.tag].zLabel.nLabel.setTitle("選擇", for: .normal)
                compareitem?.itemsArray[self.tag].zLabel.fLabel.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
                compareitem?.itemsArray[self.tag].zLabel.nLabel.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
                print(99,compareitem?.compareDetailArray)

                for (index,j) in (compareitem?.compareDetailArray ?? []).enumerated() {
                    for (index2,_) in j.array.enumerated(){
                        if self.tag == 0 {
                            compareitem?.compareDetailArray[index].array[index2].value[0] = nil
                            compareitem?.compareDetailArray[index].array[index2].value[1] = nil
                        }
                        else if self.tag == 1{
                            compareitem?.compareDetailArray[index].array[index2].value[2] = nil
                            compareitem?.compareDetailArray[index].array[index2].value[3] = nil
                        }
                        else if self.tag == 2{
                            compareitem?.compareDetailArray[index].array[index2].value[4] = nil
                            compareitem?.compareDetailArray[index].array[index2].value[5] = nil
                        }
                    }

 
                }
                if compareitem?.compareDetailArray.count ?? 0 > 0 {
                compareitem?.comDetail.reloadSections(IndexSet(arrayLiteral: 0), with: .none)
                }
            }
            compareitem?.itemsArray[self.tag].nLabel.setTitle(pr[indexPath.item].title, for: .normal)
            compareitem?.itemsArray[self.tag].nLabel.layoutIfNeeded()
//            compareitem?.selectedProducts[self.tag] = pr[indexPath.item]
            compareitem?.analyzeToLocation(index:self.tag,series: pr[indexPath.item])
            
            
        }
        
        if let pr = items[indexPath.item] as? Product {
            let vd = compareitem?.itemsArray[self.tag].sLabel


                if pr.title != compareitem?.itemsArray[self.tag].sLabel.titleLabel?.text {
                    print(77799)

                    compareitem?.itemsArray[self.tag].zLabel.fLabel.setTitle("選擇", for: .normal)
                    compareitem?.itemsArray[self.tag].zLabel.nLabel.setTitle("選擇", for: .normal)
                    compareitem?.itemsArray[self.tag].zLabel.fLabel.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
                    compareitem?.itemsArray[self.tag].zLabel.nLabel.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
                    for (index,j) in (compareitem?.compareDetailArray ?? []).enumerated() {
                        for (index2,_) in j.array.enumerated(){
                            if self.tag == 0 {
                                compareitem?.compareDetailArray[index].array[index2].value[0] = nil
                                compareitem?.compareDetailArray[index].array[index2].value[1] = nil
                            }
                            else if self.tag == 1{
                                compareitem?.compareDetailArray[index].array[index2].value[2] = nil
                                compareitem?.compareDetailArray[index].array[index2].value[3] = nil
                            }
                            else if self.tag == 2{
                                compareitem?.compareDetailArray[index].array[index2].value[4] = nil
                                compareitem?.compareDetailArray[index].array[index2].value[5] = nil
                            }
                        }

     
                    }
                    compareitem?.comDetail.reloadSections(IndexSet(arrayLiteral: 0), with: .none)

                }
            vd?.setTitleColor(#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1), for: .normal)
                vd?.setTitle(pr.title, for: .normal)
                vd?.layoutIfNeeded()
                
            compareitem?.analyzeToRadius(index1: self.tag,product: pr)
        
            


        }


        if let pr = items[indexPath.item] as? Specification {
            let vd = compareitem?.itemsArray[self.tag].zLabel
            let txt = pr.fields.first { (f1) -> Bool in
                f1.code == "screw-diameter"
            }?.value ?? ""
            if twoItemIndex == 1{
                vd?.fLabel.setTitle(txt, for: .normal)
                vd?.fLabel.setTitleColor(#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1), for: .normal)
                vd?.fLabel.layoutIfNeeded()
                

            }
            else{
                vd?.nLabel.setTitle(txt, for: .normal)
                vd?.nLabel.setTitleColor(#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1), for: .normal)
                vd?.nLabel.layoutIfNeeded()
                
            }
           
            compareitem?.analyzeDetail(index1: self.tag, index2: twoItemIndex ?? 0,spec:pr)

        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(7771)
        UIView.setAnimationsEnabled(false)
        if let _ = compareitem {
            if let k = items[indexPath.row] as? Series {
                if k.is_disable == 1{
                    return
                }
            }
            print(2213)
            doCompare(indexPath:indexPath)
        }
        else{
            
            if let a = specCon {
                if let k = items[indexPath.row] as? Series {
                    if k.is_disable == 1 {
                        return
                    }
                    if a.vd.typeSelectionView.label.text != k.title {
                        a.selectedProducts = nil
                        a.selectedSpec1 = nil
                        a.selectedSpec2 = nil
                    }
                    a.selectedSeries = k
                    
                }
                else if let m = items[indexPath.row] as? Product {
                    if m.is_disable == 1 {
                        return
                    }
                    if isNone{
                        if a.vd.positionSelectionView.label.text != m.title {
                            a.selectedSpec1 = nil
                            a.selectedSpec2 = nil
                        }
                    }
                    else{
                    if a.vd.positionSelectionView.label.text != m.shooter_code {
                        a.selectedSpec1 = nil
                        a.selectedSpec2 = nil
                    }
                    }
                    
                    a.selectedProducts = m
                }
                else if let d = items[indexPath.row] as? Specification {
                    if self.tag == 2{
                        a.selectedSpec1 = d
                    }
                    else if self.tag == 3{
                        a.selectedSpec2 = d
                    }
                }
                a.reloadingData()
                
            }
        }
        UIView.setAnimationsEnabled(true)
        compareitem?.dismissAll()
        

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class selectViewCell : UITableViewCell {
    let seperator = UIView()
    let label = UILabel()
    init(txt:String) {
        super.init(style: .default, reuseIdentifier: "cell")
        selectionStyle = .none
        
        contentView.addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 21.5.calcvaluex(), bottom: 0, right: 21.5.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
        
        label.text = txt
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        contentView.addSubview(label)
        label.centerInSuperview()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
