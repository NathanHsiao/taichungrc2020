//
//  comparingView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/6/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class comparingView : UIView {
    lazy var downButton : UIButton = {
        let bt = UIButton(type: .custom)
        bt.setImage(#imageLiteral(resourceName: "ic_arrow_drop_down_circle"), for: .normal)
        bt.contentHorizontalAlignment = .center
        bt.contentVerticalAlignment = .center
        bt.addTarget(self, action: #selector(slideDown), for: .touchUpInside)
        return bt
    }()
    var slidedown = false
    @objc func slideDown(){
        print(12333)
        if !slidedown {
        UIView.animate(withDuration: 0.4, animations: {
            self.transform = .identity
        }) { (_) in
            self.downButton.setImage(#imageLiteral(resourceName: "ic_arrow_drop_up_circle"), for: .normal)
            self.cmTitle.transform = .init(translationX: 0, y: -133.calcvaluey())
            self.downButton.transform = .init(translationX: 0, y: -133.calcvaluey())
            self.slidedown = true
        }
        }
        else{
            UIView.animate(withDuration: 0.4, animations: {
                self.transform = .init(translationX: 0, y: -133.calcvaluey())
            }) { (_) in
                self.downButton.setImage(#imageLiteral(resourceName: "ic_arrow_drop_up_circle"), for: .normal)
                self.cmTitle.transform = .identity
                self.downButton.transform = .identity
                self.slidedown = false
            }
        }
    }
    let content : UIView = {
        let ct = UIView()
        ct.backgroundColor = .white
        ct.addshadowColor()
        return ct
    }()
    lazy var upButton : UIButton = {
        let bt = UIButton(type: .custom)
        bt.setImage(#imageLiteral(resourceName: "ic_arrow_drop_up_circle"), for: .normal)
        bt.contentHorizontalAlignment = .center
        bt.contentVerticalAlignment = .center
        bt.addTarget(self, action: #selector(slideDown), for: .touchUpInside)
        return bt
    }()
    lazy var cmTitle : UILabel = {
        let label = UILabel()
        label.text = "產品比較"
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluey())
        return label
    }()
    let seperator : UIView = {
       let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        
        return sep
    }()
    let firstItem = compareItemView()
    let secondItem = compareItemView()
    let thirdItem = compareItemView()
    let compareButton : UIButton = {
        let cm = UIButton(type: .custom)
        cm.backgroundColor = .black
        cm.setTitle("開始比較", for: .normal)
        cm.setTitleColor(.white, for: .normal)
        cm.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        return cm
    }()
    func cancelAll(){
        
        UIView.animate(withDuration: 0.4, animations: {
            self.downButton.setImage(#imageLiteral(resourceName: "ic_arrow_drop_down_circle"), for: .normal)
            self.cmTitle.transform = .init(translationX: 0, y: 133.calcvaluey())
            self.downButton.transform = .init(translationX: 0, y: 133.calcvaluey())

        }) { (_) in

            self.slidedown = false
        }
    }
    var selectedSeries = [Series]() {
        didSet{
            for i in 0 ..< 3 {
                if i < selectedSeries.count {
                    let series = selectedSeries[i]
                    if i == 0{
                        firstItem.showmachineView()
                        firstItem.machineView.seriesName.text = series.selectedTitle
                        firstItem.machineView.image.sd_setImage(with: URL(string: "\(series.assets?.getMachinePath() ?? "")"), completed: nil)
                    }
                    else if i == 1{
                        secondItem.showmachineView()
                        secondItem.machineView.seriesName.text = series.selectedTitle
                        secondItem.machineView.image.sd_setImage(with: URL(string: "\(series.assets?.getMachinePath() ?? "")"), completed: nil)
                    }
                    else if i == 2{
                        thirdItem.showmachineView()
                        thirdItem.machineView.seriesName.text = series.selectedTitle
                        thirdItem.machineView.image.sd_setImage(with: URL(string: "\(series.assets?.getMachinePath() ?? "")"), completed: nil)
                    }
                }
                else{
                    if i == 0{
                        firstItem.showaddView()
                    }
                    else if i == 1{
                        secondItem.showaddView()
                    }
                    else if i == 2{
                        thirdItem.showaddView()
                    }
                }
            }
        }
    }
//    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//        for subview in subviews as [UIView] {
//            if !subview.isHidden
//               && subview.alpha > 0
//               && subview.isUserInteractionEnabled
//               && subview.point(inside: convert(point, to: subview), with: event) {
//                 return true
//            }
//        }
//        return false
//    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {

        let translatedPoint = downButton.convert(point, from: self)

        if (downButton.bounds.contains(translatedPoint)) {
            print("Your button was pressed")
            if slidedown {
                UIView.animate(withDuration: 0.4, animations: {
                    self.downButton.setImage(#imageLiteral(resourceName: "ic_arrow_drop_down_circle"), for: .normal)
                    self.cmTitle.transform = .identity
                    self.downButton.transform = .identity
                    self.transform = .init(translationX: 0, y: -133.calcvaluey())
                }) { (_) in

                    self.slidedown = false
                }
            }
            return downButton.hitTest(translatedPoint, with: event)
        }
        return super.hitTest(point, with: event)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(content)
        content.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 115.calcvaluey()))

        
        addSubview(downButton)
        downButton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 48.calcvaluex(), bottom: 0, right: 0),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        downButton.centerYAnchor.constraint(equalTo: content.topAnchor).isActive = true
        
        addSubview(cmTitle)
        cmTitle.anchor(top: nil, leading: leadingAnchor, bottom: content.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 51.calcvaluey(), right: 0),size: .init(width: 80.calcvaluex(), height: 28.calcvaluey()))
        
        addSubview(seperator)
        seperator.anchor(top: content.topAnchor, leading: cmTitle.trailingAnchor, bottom: content.bottomAnchor, trailing: nil,padding: .init(top: 13.5.calcvaluey(), left: 26.calcvaluex(), bottom: 24.5.calcvaluey(), right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        addSubview(firstItem)
        firstItem.anchor(top: content.topAnchor, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 6.calcvaluey(), left: 52.calcvaluex(), bottom: 0, right: 0),size: .init(width: 176.calcvaluey(), height: 82.calcvaluey()))
        firstItem.machineView.xbutton.tag = 0
        addSubview(secondItem)
        secondItem.anchor(top: firstItem.topAnchor, leading: firstItem.trailingAnchor, bottom: firstItem.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 52.calcvaluex(), bottom: 0, right: 0),size: .init(width: 176.calcvaluex(), height: 0))
        secondItem.machineView.xbutton.tag = 1
        addSubview(thirdItem)
        thirdItem.anchor(top: secondItem.topAnchor, leading: secondItem.trailingAnchor, bottom: secondItem.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 52.calcvaluex(), bottom: 0, right: 0),size: .init(width: 176.calcvaluex(), height: 0))
        thirdItem.machineView.xbutton.tag = 2
        addSubview(compareButton)
        compareButton.anchor(top: content.topAnchor, leading: thirdItem.trailingAnchor, bottom: content.bottomAnchor, trailing: content.trailingAnchor,padding: .init(top: 0, left: 42.calcvaluex(), bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol compareItemViewDelegate {
    func clearMachine(index:Int)
}
class compareItemView : UIView {
    let addView : UIButton = {
        let aV = UIButton(type: .custom)
        aV.backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        aV.setImage(#imageLiteral(resourceName: "ic_addition").withRenderingMode(.alwaysTemplate), for: .normal)
        aV.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return aV
    }()
    let machineView = addedMachineView()
    var delegate : compareItemViewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(addView)
        addView.fillSuperview(padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        addSubview(machineView)
        machineView.fillSuperview()
        machineView.isHidden = true
        machineView.xbutton.addTarget(self, action: #selector(clearMachine), for: .touchUpInside)
    }
    @objc func clearMachine(button:UIButton){
        delegate?.clearMachine(index: button.tag)
    }
    func showaddView(){
        addView.isHidden = false
        machineView.isHidden = true
    }
    func showmachineView(){
        addView.isHidden = true
        machineView.isHidden = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class addedMachineView : UIView {
    let xbutton : UIButton = {
        let bt = UIButton(type: .custom)
        bt.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        bt.contentVerticalAlignment = .fill
        bt.contentHorizontalAlignment = .fill
        return bt
    }()
    let image : UIImageView = {
       let im = UIImageView()
        im.contentMode = .scaleToFill
        
        return im
    }()
    let seriesName : UILabel = {
        let sr = UILabel()
        //sr.text = "RC系列"
        sr.textColor = .black
        sr.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        return sr
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(xbutton)
        xbutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        addSubview(image)
        
        image.anchor(top: xbutton.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 5.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 87.calcvaluex(), height: 51.calcvaluey()))
        image.centerXInSuperview()
        
        addSubview(seriesName)
        seriesName.anchor(top: image.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 20.calcvaluey()))
        seriesName.centerXInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
