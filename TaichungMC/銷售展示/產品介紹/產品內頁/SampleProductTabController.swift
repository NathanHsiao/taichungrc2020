//
//  SampleProductTabController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/22.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class ProductTabField:UITextField {
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 26.calcvaluex(), dy: 0)
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 26.calcvaluex(), dy: 0)
    }
}
enum ControllerMode {
    case Spec
    case Equip
    case Functionality
    case Precision
    case Diagram
    
}
class SampleProductTabController: UIViewController {
    let leftview = UIView()
    let rightview = UIView()
    var nextvd : UIView?
    var mode:ControllerMode?
    init(nextvd:UIView,mode:ControllerMode) {
        super.init(nibName: nil, bundle: nil)
        self.nextvd = nextvd
        self.mode = mode
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let machineLabel = UILabel()
    //let selectLabel = ProductTabField()
    let searchFild = SearchTextField()
    let tableview = UITableView(frame: .zero, style: .grouped)
    
    let selectContent = UIView()
    var selectContentConstraint : NSLayoutConstraint!
    var selectContentTableView = UITableView(frame: .zero, style: .grouped)
    var currentIndex = 0
    lazy var tap = UITapGestureRecognizer(target: self, action: #selector(popContent))
    var leftviewAnchor : AnchoredConstraints?
    var tableviewanchor : AnchoredConstraints?
    var fileArray = [FilePath](){
        didSet{
            
            if searchFild.text == "" || searchFild.text == nil{
                self.fileArray = self.originalFileArray
            }
            else{
                self.fileArray = self.originalFileArray.filter { (op) -> Bool in
                    if let txt = searchFild.text,let title = op.title{
                        if title.lowercased().contains(txt.lowercased()) {
                            return true
                        }
                    }
                return false
            }
                
            }
            setRightView()
            //self.tableview.reloadData()

        }
    }
    var originalFileArray = [FilePath]()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        leftview.backgroundColor = .clear
        view.addSubview(leftview)
        leftviewAnchor = leftview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 312.calcvaluex(), height: 0))
        
        rightview.backgroundColor = .clear
        view.addSubview(rightview)
        rightview.anchor(top: view.topAnchor, leading: leftview.trailingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)

        rightview.addSubview(nextvd!)
        nextvd?.fillSuperview()
        searchFild.addshadowColor(color: .white)
        searchFild.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        searchFild.autocorrectionType = .no
        searchFild.autocapitalizationType = .none
        searchFild.returnKeyType = .search
        searchFild.clearButtonMode = .always
        searchFild.delegate = self
        leftview.addSubview(searchFild)
        searchFild.layer.cornerRadius = 24.calcvaluey()
        searchFild.anchor(top: leftview.topAnchor, leading: leftview.leadingAnchor, bottom: nil, trailing: leftview.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 18.calcvaluex()),size: .init(width: 0, height: 48.calcvaluey()))
        var searchText = ""
        switch mode {
        case .Spec:
            searchText = "搜尋產品規格"
        case .Equip:
            searchText = "搜尋選購配備"
        case .Functionality:
            searchText = "搜尋產品性能"
        case .Precision:
            searchText = "搜尋精度標準"
        case .Diagram:
            searchText = "搜尋配置圖"
        
        case .none:
            ()
        }
        searchFild.attributedPlaceholder = NSAttributedString(string: searchText, attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)])
        searchFild.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        searchFild.isHidden = true
        leftview.addSubview(tableview)
        tableviewanchor = tableview.anchor(top: leftview.topAnchor, leading: leftview.leadingAnchor, bottom: leftview.bottomAnchor, trailing: leftview.trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0))
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.showsVerticalScrollIndicator = false
        tableview.contentInset = .init(top: 0, left:0, bottom: 72.calcvaluey(), right: 0)
        
        selectContent.backgroundColor = .clear
//        selectContent.delegate = self
//        selectContent.dataSource = self
        leftview.addSubview(selectContent)
        selectContent.addShadowColor(opacity: 0.1)

        selectContent.anchor(top: leftview.topAnchor, leading: leftview.leadingAnchor, bottom: nil, trailing: leftview.trailingAnchor)
        selectContentConstraint = selectContent.heightAnchor.constraint(equalToConstant: 0)
        selectContentConstraint.isActive = true
        


        tap.cancelsTouchesInView = false
        tap.delegate = self
        view.addGestureRecognizer(tap)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        popContent()
    }
}
extension SampleProductTabController:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: selectContentTableView.self))!{
            return false
        }
        return true
    }
    @objc func popContent(){
        self.view.endEditing(true)
        selectContentConstraint.constant = 0
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func showContent(){
        selectContentConstraint.constant = 320.calcvaluey()
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fileArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == selectContentTableView {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "selection")
            cell.selectionStyle = .none
            cell.backgroundColor = .white
            return cell
        }
        let cell = ProductTabCell(style: .default, reuseIdentifier: "cell")
        cell.label.text = fileArray[indexPath.row].title
        if indexPath.row == currentIndex{
            cell.setColor()
        }
        else{
            cell.unsetColor()
        }
        return cell
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text == "" || textField.text == nil{
            self.fileArray = self.originalFileArray
        }
        else{
            self.fileArray = self.originalFileArray.filter { (op) -> Bool in
                if let txt = textField.text,let title = op.title{
                    if title.lowercased().contains(txt.lowercased()) {
                        return true
                    }
                }
            return false
        }
            
        }
        //setOptions()
        self.tableview.reloadData()
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 139.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableview {
        currentIndex = indexPath.row
        setRightView()
        self.tableview.reloadData()
        }
        
       
    }
    func setRightView(){
        rightview.subviews.forEach({$0.removeFromSuperview()})
        nextvd = nil
        print(fileArray)

        if fileArray.count > 0 {
            if currentIndex >= fileArray.count {
                currentIndex = 0
            }
        if fileArray[currentIndex].id == "-1x" {
            
            nextvd = RCSpecTableView()
            
            if let pp = parent as? SampleRCTopViewController {
                if let ab = nextvd as? RCSpecTableView {
                    ab.selectedSeries = pp.selectedSeries
                    ab.selectedProducts = pp.selectedProducts
                    ab.selectedSpec1 = pp.selectedSpec1
                    ab.selectedSpec2 = pp.selectedSpec2
                    ab.products = pp.selectedSeries?.products ?? []
                }
            }


        }
        else{
        nextvd = SpecRightView2()
        if let av = nextvd as? SpecRightView2 {
            //av.product = self.selectedProduct
            av.webview.loadRequest(URLRequest(url: URL(string: "about:blank")!))
            if currentIndex < fileArray.count {
            if let url = URL(string: fileArray[currentIndex].getUrlString()) {
                av.url = url
                General().showActivity(vd: av.webview)
                av.webview.loadRequest(URLRequest(url: url))
            }
            }

        }
        }

        }
        else{
            nextvd = SpecRightView()
        }
        UIView.setAnimationsEnabled(false)
        rightview.addSubview(nextvd!)
        nextvd?.fillSuperview()
        
        self.selectContentTableView.reloadData()
        self.tableview.reloadData()
        //print(789,selectedProduct?.assets?.performance)
        
        //UIView.setAnimationsEnabled(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.setAnimationsEnabled(true)
        }
    }

}

class RecommandCell : UITableViewCell {
    let vd = UIView()
    let mc = UIView()
    let imageview = UIImageView()
    let nd = UIView()
    let label = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        contentView.addSubview(mc)
        mc.backgroundColor = .clear
        mc.addShadowColor(opacity: 0.05)
//        mc.centerInSuperview(size: .init(width: 268.calcvaluex(), height: 72.calcvaluey()))
        mc.fillSuperview(padding: .init(top: 6.calcvaluey(), left: 26.calcvaluex(), bottom: 6.calcvaluey(), right: 18.calcvaluex()))
        mc.addSubview(vd)
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        vd.fillSuperview()
        vd.layer.cornerRadius = 15.calcvaluex()
        

        vd.clipsToBounds = true
        vd.addSubview(nd)
        nd.backgroundColor =  #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        nd.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
        nd.addSubview(imageview)
        imageview.anchor(top: nil, leading: nd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 38.43.calcvaluex(), bottom: 0, right: 0),size: .init(width: 78.23.calcvaluex(), height: 70.56.calcvaluey()))
        imageview.centerYInSuperview()
        imageview.contentMode = .scaleAspectFit
        label.text = "機械規格"
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        nd.addSubview(label)
        label.anchor(top: nil, leading: imageview.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 29.33.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
        label.centerYInSuperview()
    }
    func setColor(){
        vd.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        mc.addShadowColor(opacity: 0.2)
    }
    func unsetColor(){
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        mc.addShadowColor(opacity: 0.05)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ProductTabCell: UITableViewCell {
    let vd = UIView()
    let mc = UIView()
    let nd = UIView()
    let label = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        selectionStyle = .none
        contentView.addSubview(mc)
        mc.backgroundColor = .clear
        mc.addShadowColor(opacity: 0.05)
//        mc.centerInSuperview(size: .init(width: 268.calcvaluex(), height: 72.calcvaluey()))
        mc.fillSuperview(padding: .init(top: 6.calcvaluey(), left: 26.calcvaluex(), bottom: 6.calcvaluey(), right: 18.calcvaluex()))
        mc.addSubview(vd)
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        vd.fillSuperview()
        vd.layer.cornerRadius = 15.calcvaluex()
        

        vd.clipsToBounds = true
        vd.addSubview(nd)
        nd.backgroundColor =  #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        nd.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 0, left: 10.calcvaluex(), bottom: 0, right: 0))
        
        label.text = "機械規格"
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        nd.addSubview(label)
        label.anchor(top: nil, leading: nd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
        label.centerYInSuperview()
        
    }
    func setColor(){
        vd.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        mc.addShadowColor(opacity: 0.2)
    }
    func unsetColor(){
        vd.backgroundColor = #colorLiteral(red: 0.9920461774, green: 0.9922190309, blue: 0.9920480847, alpha: 1)
        mc.addShadowColor(opacity: 0.05)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
