//
//  ComparingItemController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/17.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension UIStackView {

    func safelyRemoveArrangedSubviews() {

        // Remove all the arranged subviews and save them to an array
        let removedSubviews = arrangedSubviews.reduce([]) { (sum, next) -> [UIView] in
            self.removeArrangedSubview(next)
            return sum + [next]
        }

//        // Deactive all constraints at once
//        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))

        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
class ComparingItemController:SampleController,UIGestureRecognizerDelegate {
    weak var con : RcProductSelectionController?
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if touch.view?.isDescendant(of: self.comDetail) == true{
            return false
        }
        if let vd = vde{
        if touch.view?.isDescendant(of: vd) == true{
            return false
        }
        }
        return true
    }
    let stackview = UIStackView()
    let bottomTop = UIView()
    let comDetail = UITableView(frame: .zero, style: .plain)
    var comDetailAnchor : AnchoredConstraints!
    private var lastContentOffset: CGFloat = 0
    var isBottom = false
    let filterButton = UIButton(type: .custom)
    let contentTopBackground = UIView()
    var itemsArray = [ItemView]()
    var selectedProducts : [Product?] = [nil,nil,nil]
    
    var positionArray : [[Product]?] = [nil,nil,nil]
    var radiusArray : [[[Specification]?]] = [[nil,nil],[nil,nil],[nil,nil]]
    var compareDetailArray = SpecArray.value
    var nonShooter = [false,false,false]
    func analyzeToLocation(index:Int,series:Series){
        selectedSeries[index].selectedId = series.id
        selectedSeries[index].selectedTitle = series.title
        if let fd = series.products, let mt = fd.first, let spec = mt.specifications?.first {
            if let field = spec.fields.first(where: { (ft) -> Bool in
                return ft.code == "injection-model"
            }) {
                if field.value == nil {
                    if let st = stackview.arrangedSubviews[index] as? ItemView {
                        st.sLabel.isUserInteractionEnabled = false
                        st.sLabel.setTitle("-", for: .normal)
                    }
                    
                    for i in fd{
                        if index == 0{
                            analyzeToRadius(index1: 0, product: i,shot: false,index2:0)
                            analyzeToRadius(index1: 0, product: i,shot: false,index2: 1)
                        }
                        else if index == 1{
                            analyzeToRadius(index1: 1, product: i,shot: false,index2: 0)
                            analyzeToRadius(index1: 1, product: i,shot: false,index2: 1)
                        }
                        else if index == 2{
                            analyzeToRadius(index1: 2, product: i,shot: false,index2: 0)
                            analyzeToRadius(index1: 2, product: i,shot: false,index2: 1)
                        }
                    }
                    return
                }
            }
            
        }
        positionArray[index] = series.products?.filter({ (pr) -> Bool in
            return pr.status == 1
        }).sorted(by: { (p1, p2) -> Bool in
            return (p1.order ?? 0) < (p2.order ?? 0)
        })

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        con?.selectedSeries = self.selectedSeries
    }
    func analyzeToRadius(index1:Int,product:Product,shot:Bool = true,index2:Int=0){
        if shot {
        let pr = product.model_number ?? ""
        var st = pr.split(separator: "&")
  
            if st.count == 1{
                st.append(st[0])
            }
        for (ind,i) in st.enumerated() {

            var pArray = [Specification]()
            let t = String(i).trimmingCharacters(in: .whitespacesAndNewlines)
            
            for j in product.specifications ?? []{
                
                for k in j.fields {
                    
                    if (k.code ?? "") == "injection-model" {
                        
                        
                        if t.contains(k.value ?? ""){

                        pArray.append(j)
                        }
                    }
                }
            }

            radiusArray[index1][ind] = pArray
        }
        }
        else{
            var pArray = [Specification]()
            for j in product.specifications ?? []{
                
                pArray.append(j)
            }
            print(index1,index2)
            radiusArray[index1][index2] = pArray
        }
        print(371,radiusArray)
        

        
    }
    var specifications : [Specification?] = [nil,nil,nil,nil,nil,nil]
    func analyzeDetail(index1:Int,index2:Int,spec:Specification){
        
        if index1 == 0 && index2 == 1{
            specifications[1] = spec
        }
        else if index1 == 0 && index2 == 0{
            specifications[0] = spec
        }
        else if index1 == 1 && index2 == 0 {
            specifications[2] = spec
        }
        else if index1 == 1 && index2 == 1{
            specifications[3] = spec
        }
        else if index1 == 2 && index2 == 0 {
            specifications[4] = spec
        }
        else if index1 == 2 && index2 == 1 {
            specifications[5] = spec
        }
     
        
        convertToDetailArray(reloadSection:true)
    }
    func convertToDetailArray(reloadSection:Bool){
        
        for (index,j) in specifications.enumerated() {

                for f in j?.fields ?? [] {
                    for (index2,k) in compareDetailArray.enumerated() {
                        if let index3 = k.array.firstIndex(where: { (n1) -> Bool in
                            return n1.decodeId == f.code
                        }) {
                            compareDetailArray[index2].array[index3].value[index] = f.value
                        }
                        else{
                            continue
                        }
                    }

                }
            
        }
 
        //self.comDetail.reloadData()
//        for (ind,_) in self.compareDetailArray.enumerated() {
//            self.comDetail.reloadSections(IndexSet(arrayLiteral: ind), with: .none)
//        }
        if reloadSection {
        self.comDetail.reloadSections(IndexSet(integersIn: 0...compareDetailArray.count-1), with: .none)
        }
        else{
            self.comDetail.reloadData()
        }

        
       
    }
    let positionLabel : UILabel = {
       let pos = UILabel()
        pos.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        pos.textColor = #colorLiteral(red: 0.1351079345, green: 0.09290169924, blue: 0.0822731033, alpha: 1)
        pos.text = "射座型式"
        return pos
    }()
    let diameterLabel : UILabel = {
       let pos = UILabel()
        pos.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        pos.textColor = #colorLiteral(red: 0.1351079345, green: 0.09290169924, blue: 0.0822731033, alpha: 1)
        pos.text = "螺桿直徑"
        return pos
    }()
    var selectedSeries = [Series]() {
        didSet{
            
            for (index,_) in selectedSeries.enumerated() {
                self.selectedSeries[index].children = self.selectedSeries[index].children?.filter({ (sr) -> Bool in
                    return sr.status == 1
                }).sorted(by: { (or1, or2) -> Bool in
                    return (or1.order ?? 0) < (or2.order ?? 0)
                })
                
                if let first = self.selectedSeries[index].children?.first(where: { (sr) -> Bool in
                    return sr.id == self.selectedSeries[index].selectedId
                }) {
                    print(3312,first)
                    if let vd = (first.products ?? []).sorted(by: { (pr1, pr2) -> Bool in
                        return (pr1.order ?? 0) < (pr2.order ?? 0)
                    }).filter({ (pr) -> Bool in
                        return pr.status == 1 && pr.is_disable == 0
                    }).first {
                        if vd.shooter_code == nil{
                            nonShooter[index] = true
                        }
                        else{
                            nonShooter[index] = false
                        }
                    }
                    else{
                        nonShooter[index] = false
                    }
                }
//                if let vde = (self.selectedSeries[index].children?.products ?? []).sorted(by: { (pr1, pr2) -> Bool in
//                    return (pr1.order ?? 0) < (pr2.order ?? 0)
//                }).filter({ (pr) -> Bool in
//                    return pr.status == 1 && pr.is_disable == 0
//                }).first {
//
//                }
                    
            }
        }
        }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        NetworkCall.shared.getCall(parameter: "test-v2/products", decoderType: TopProducts.self) { (json) in
//            print(7888,json?.data[6])
//        }
    }
    var height : NSLayoutConstraint?
    var vde : sView?
    func animateSelection(sender:UIView,prev:UIView?){
        print(sender.superview?.tag)
        if sender.tag == 4{
            self.vde = SelectTableView(items:selectedSeries[sender.superview?.tag ?? 0].children ?? [])
        }
        else if sender.tag == 5{
            self.vde = SelectTableView(items:positionArray[sender.superview?.tag ?? 0] ?? [])
            self.vde?.twoItemIndex = prev?.tag
            if let ac = self.vde {
                print(nonShooter[sender.superview?.tag ?? 0])
                ac.isNone = nonShooter[sender.superview?.tag ?? 0]
            }
        }
        else{
            self.vde = SelectTableView(items: radiusArray[sender.superview?.tag ?? 0][prev?.tag ?? 0] ?? [])
            self.vde?.twoItemIndex = prev?.tag
        }
        self.vde?.compareitem = self
        self.vde?.tag = sender.superview?.tag ?? 0
        self.view.addSubview(self.vde!)
        if let prev = prev{
            self.vde?.anchor(top: prev.bottomAnchor, leading: prev.leadingAnchor, bottom: nil, trailing: prev.trailingAnchor)
        }
        else{
            self.vde?.anchor(top: sender.bottomAnchor, leading: sender.leadingAnchor, bottom: nil, trailing: sender.trailingAnchor)
        }

        self.height = self.vde?.heightAnchor.constraint(equalToConstant: 0)
        self.height?.isActive = true
        
        self.view.layoutIfNeeded()
        if sender.tag == 4{
            if let av = selectedSeries[sender.superview?.tag ?? 0].children {
                self.height?.constant = CGFloat(av.count) * 84.calcvaluey()
            }
        }
        else if sender.tag == 5{
            if let av = positionArray[sender.superview?.tag ?? 0] {
                self.height?.constant = CGFloat(av.count) * 84.calcvaluey()
            }
        }
        else{
            if let av = radiusArray[sender.superview?.tag ?? 0][prev?.tag ?? 0] {
                self.height?.constant = CGFloat(av.count) * 84.calcvaluey()
            }
        }
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
            
        }
    }
    @objc func goSelection(sender:UIButton){
       
        print(sender.superview?.tag)
        var sending : UIView?
        var twoview = false
        if sender.superview?.isKind(of: TwoItemView.self) == true{
            sending = sender.superview
            twoview = true
        }
        else{
            sending = sender
            twoview = false
        }
        if selectedSeries[sending?.superview?.tag ?? 0].children == nil || selectedSeries[sending?.superview?.tag ?? 0].children?.count == 0{
            if sending?.tag == 4{

               

                let alert = UIAlertController(title: "沒有機台可以選擇", message: nil, preferredStyle: .alert)
                let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            return
               
            }
        }
        
        
        if vde != nil{
            dismissAll()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if twoview{
                    self.animateSelection(sender:sending ?? UIView(),prev:sender)
                }
                else{
                    self.animateSelection(sender:sending ?? UIView(),prev: nil)
                }
                
            }
        }
        else{
            if twoview{
                self.animateSelection(sender:sending ?? UIView(),prev:sender)
            }
            else{
                self.animateSelection(sender:sending ?? UIView(),prev: nil)
            }
            
        }

    }
    @objc func deleteSeries(sender:UIButton){
        selectedSeries.remove(at: sender.tag)
        
        switch sender.tag {
        case 0:
        
            let v1 = stackview.arrangedSubviews[1] as? ItemView
            let v2 = stackview.arrangedSubviews[2] as? ItemView
            let p1 = positionArray[1]
            let p2 = positionArray[2]
            let r1 = radiusArray[1]
            let r2 = radiusArray[2]
            positionArray = [nil,nil,nil]
            radiusArray = [[nil,nil],[nil,nil],[nil,nil]]
            positionArray[0] = p1
            positionArray[1] = p2
            
            radiusArray[0] = r1
            radiusArray[1] = r2
            
            let s1 = specifications[2]
            let s2 = specifications[3]
            let s3 = specifications[4]
            let s4 = specifications[5]
            specifications = [nil,nil,nil,nil,nil,nil]
            specifications[0] = s1
            specifications[1] = s2
            specifications[2] = s3
            specifications[3] = s4
            v1?.cancelButton.tag = 0
            v1?.imView.tag = 0
            
            v2?.cancelButton.tag = 1
            v2?.imView.tag = 1
            stackview.safelyRemoveArrangedSubviews()
            stackview.addArrangedSubview(v1!)
            stackview.addArrangedSubview(v2!)
            addSingleSubview(i: 2)


        case 1:
            let v1 = stackview.arrangedSubviews[0] as? ItemView
            let v2 = stackview.arrangedSubviews[2] as? ItemView
            let p1 = positionArray[0]
            let p2 = positionArray[2]
            let r1 = radiusArray[0]
            let r2 = radiusArray[2]
            positionArray = [nil,nil,nil]
            radiusArray = [[nil,nil],[nil,nil],[nil,nil]]
            positionArray[0] = p1
            positionArray[1] = p2
            
            radiusArray[0] = r1
            radiusArray[1] = r2
            let s1 = specifications[0]
            let s2 = specifications[1]
            let s3 = specifications[4]
            let s4 = specifications[5]
            specifications = [nil,nil,nil,nil,nil,nil]
            specifications[0] = s1
            specifications[1] = s2
            specifications[2] = s3
            specifications[3] = s4
            v1?.cancelButton.tag = 0
            v1?.imView.tag = 0
            v2?.cancelButton.tag = 1
            v2?.imView.tag = 1
            stackview.safelyRemoveArrangedSubviews()
            stackview.addArrangedSubview(v1!)
            stackview.addArrangedSubview(v2!)
            addSingleSubview(i: 2)
        case 2:
            let v1 = stackview.arrangedSubviews[0] as? ItemView
            let v2 = stackview.arrangedSubviews[1] as? ItemView
            let p1 = positionArray[0]
            let p2 = positionArray[1]
            let r1 = radiusArray[0]
            let r2 = radiusArray[1]
            positionArray = [nil,nil,nil]
            radiusArray = [[nil,nil],[nil,nil],[nil,nil]]
            positionArray[0] = p1
            positionArray[1] = p2
            
            radiusArray[0] = r1
            radiusArray[1] = r2
            let s1 = specifications[0]
            let s2 = specifications[1]
            let s3 = specifications[2]
            let s4 = specifications[3]
            specifications = [nil,nil,nil,nil,nil,nil]
            specifications[0] = s1
            specifications[1] = s2
            specifications[2] = s3
            specifications[3] = s4
            v1?.cancelButton.tag = 0
            v1?.imView.tag = 0
            v2?.cancelButton.tag = 1
            v2?.imView.tag = 1
            stackview.safelyRemoveArrangedSubviews()
            stackview.addArrangedSubview(v1!)
            stackview.addArrangedSubview(v2!)
            addSingleSubview(i: 2)
        default:
            ()
        }
        itemsArray.removeAll()
        for i in stackview.arrangedSubviews {
            if let iv = i as? ItemView {
                itemsArray.append(iv)
            }
            
        }
        for i in 0 ..< 3 {
            if i < selectedSeries.count {
                
                let series = selectedSeries[i]
                
                itemsArray[i].showMachineView()
                itemsArray[i].imgv.sd_setImage(with: URL(string: "\(series.assets?.getMachinePath() ?? "")"), completed: nil)
                
                
                

            }
            else{
                
                itemsArray[i].showAddView()
            }
        }
        
        for (index, j) in compareDetailArray.enumerated() {
            for (index2,_) in j.array.enumerated() {
                switch sender.tag {
                case 0 :
                    compareDetailArray[index].array[index2].value[0] = compareDetailArray[index].array[index2].value[2]
                    compareDetailArray[index].array[index2].value[1] = compareDetailArray[index].array[index2].value[3]
                    compareDetailArray[index].array[index2].value[2] = compareDetailArray[index].array[index2].value[4]
                    compareDetailArray[index].array[index2].value[3] = compareDetailArray[index].array[index2].value[5]
                    compareDetailArray[index].array[index2].value[4] = nil
                    compareDetailArray[index].array[index2].value[5] = nil
                case 1:
                    compareDetailArray[index].array[index2].value[2] = compareDetailArray[index].array[index2].value[4]
                    compareDetailArray[index].array[index2].value[3] = compareDetailArray[index].array[index2].value[5]
                    compareDetailArray[index].array[index2].value[4] = nil
                    compareDetailArray[index].array[index2].value[5] = nil
                case 2:
                    compareDetailArray[index].array[index2].value[4] = nil
                    compareDetailArray[index].array[index2].value[5] = nil
                default:
                   continue
                }
            }


        }
        comDetail.reloadData()
    }
    func addSingleSubview(i:Int) {
        let vd = ItemView()
        //vd.backgroundColor = .red
        vd.imView.tag = i
        vd.cancelButton.tag = i
        vd.cancelButton.addTarget(self, action: #selector(deleteSeries), for: .touchUpInside)
        
        print(vd.tag)
        vd.nLabel.tag = 4
        vd.sLabel.tag = 5
        vd.zLabel.tag = 6
        vd.nLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
        vd.sLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
//            vd.sLabel.fLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
        vd.zLabel.nLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
        vd.zLabel.fLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
        //itemsArray.append(vd)
        stackview.addArrangedSubview(vd)
    }
    func addStackview(){
        for i in 0 ..< 3{
            let vd = ItemView()
            //vd.backgroundColor = .red
            vd.imView.tag = i
            vd.cancelButton.tag = i
            vd.cancelButton.addTarget(self, action: #selector(deleteSeries), for: .touchUpInside)
            
            print(vd.tag)
            vd.nLabel.tag = 4
            vd.sLabel.tag = 5
            vd.zLabel.tag = 6
            vd.nLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
            vd.sLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
//            vd.sLabel.fLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
            vd.zLabel.nLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
            vd.zLabel.fLabel.addTarget(self, action: #selector(goSelection), for: .touchUpInside)
            itemsArray.append(vd)
            stackview.addArrangedSubview(vd)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        compareDetailArray = SpecArray.value
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        titleview.text = "產品比較"
        view.addSubview(contentTopBackground)
        contentTopBackground.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 270.calcvaluey()))
        stackview.axis = .horizontal
        stackview.spacing = 34.calcvaluex()
        stackview.distribution = .fillEqually
        
        self.addStackview()
        for i in 0 ..< 3 {
            if i < selectedSeries.count {
                let series = selectedSeries[i]
                itemsArray[i].nLabel.setTitle(series.selectedTitle, for: .normal)
                if let series = series.children?.first(where: { (sr) -> Bool in
                    return sr.id == series.selectedId
                }) {
                    for k in series.products ?? []{
                        
                        if k.title?.contains("&") ?? false{
                            itemsArray[i].zLabel.setTwo()
                        }
                        else{
                            itemsArray[i].zLabel.setOne()
                        }
                    }
                    analyzeToLocation(index: i, series: series)
                }

                itemsArray[i].showMachineView()
                itemsArray[i].imgv.sd_setImage(with: URL(string: "\(series.assets?.getMachinePath() ?? "")"), completed: nil)
//                itemsArray[i].nLabel.setTitle("\(series.title ?? "")-請選擇機型", for: .normal)
            }
            else{
                
                itemsArray[i].showAddView()
            }
        }
        contentTopBackground.addSubview(stackview)
        stackview.anchor(top: contentTopBackground.topAnchor, leading: nil, bottom: nil, trailing: contentTopBackground.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 19.calcvaluex()),size: .init(width: 782.calcvaluex(), height: 270.calcvaluey()))
        
        contentTopBackground.addSubview(positionLabel)
        contentTopBackground.addSubview(diameterLabel)
        diameterLabel.anchor(top: nil, leading: contentTopBackground.leadingAnchor, bottom: stackview.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 15.calcvaluey(), right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        
        positionLabel.anchor(top: nil, leading: diameterLabel.leadingAnchor, bottom: diameterLabel.topAnchor, trailing: diameterLabel.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 23.calcvaluey(), right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        
        bottomTop.backgroundColor = .clear

        bottomTop.addshadowColor(color: .white)
 

        view.addSubview(bottomTop)
        comDetailAnchor = bottomTop.anchor(top: stackview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: -10.calcvaluey(), right: 0))
        comDetail.layer.cornerRadius = 15.calcvaluey()
        comDetail.layer.masksToBounds = true
        bottomTop.addSubview(comDetail)
        comDetail.separatorStyle = .none
        comDetail.fillSuperview()
        comDetail.delegate = self
        comDetail.dataSource = self
        //comDetail.bounces = false
        
        settingButton.isHidden = true
        topview.addSubview(filterButton)
        filterButton.setImage(#imageLiteral(resourceName: "ic_filter"), for: .normal)
        filterButton.contentHorizontalAlignment = .fill
        filterButton.contentVerticalAlignment = .fill
        
        filterButton.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        filterButton.centerYAnchor.constraint(equalTo: titleview.centerYAnchor).isActive = true
        filterButton.addTarget(self, action: #selector(goFilter), for: .touchUpInside)
        view.bringSubviewToFront(contentTopBackground)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissAll))
        tap.delegate = self
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        
    }
    @objc func dismissAll(){
        if let height = height {
            height.constant = 0
            
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
            } completion: { (_) in
                self.vde?.removeFromSuperview()
                self.vde = nil
                self.height = nil
            }

           
            
        }
    }
    var filterOptionArray : [FilterSelection]? {
        didSet{
            if let _ = filterOptionArray {
                filterButton.setImage(UIImage(named: "ic_filter_selected"), for: .normal)
            }
            else{
                filterButton.setImage(UIImage(named: "ic_filter"), for: .normal)
            }
        }
    }
    @objc func goFilter(){
        let con = ComparisonFilterController()
        con.modalPresentationStyle = .fullScreen
        con.con = self
        con.filterOptionArray = self.filterOptionArray ?? []
        self.present(con, animated: true, completion: nil)
    }
}
extension ComparingItemController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return compareDetailArray[section].array.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return compareDetailArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CompareCell(spec: compareDetailArray[indexPath.section].array[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = CompareHeader()
        vd.headerLabel.text = compareDetailArray[section].title
        return vd
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.calcvaluey()
    }
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        if scrollView.contentOffset.y < 0 {
//            scrollView.contentOffset.y = 0
//        }
//        if (self.lastContentOffset > scrollView.contentOffset.y) {
//            // move up
//            print(scrollView.contentOffset.y)
//            if scrollView.contentOffset.y <= 0{
//                if isBottom {
//                    self.changeStackView(down:false)
//                    isBottom = false
//                comDetailAnchor.top?.constant = 12.calcvaluey()
//                    UIView.animate(withDuration: 0.4, animations: {
//                        self.view.layoutIfNeeded()
//                    }) { (_) in
//
//                                   }
//
//                }
//            }
//
//        }
//        else if (self.lastContentOffset < scrollView.contentOffset.y) {
//           // move down
//            print("abc")
//            if !isBottom {
//                self.changeStackView(down:true)
//                isBottom = true
//            comDetailAnchor.top?.constant = -82.calcvaluey()
//
//
//
//                UIView.animate(withDuration: 0.4, animations: {
//                    self.view.layoutIfNeeded()
//                }) { (_) in
//
//                               }
//
//            }
//        }
//
//
//        // update the new position acquired
//        self.lastContentOffset = scrollView.contentOffset.y
//    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //dismissAll()
    }
    
    func changeStackView(down:Bool){
        for i in stackview.arrangedSubviews {
            if let vd = i as? ItemView{
                if down{
                vd.nLabel.setTitle("NT$ 145萬", for: .normal)
                vd.nLabel.setTitleColor(#colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1), for: .normal)
                vd.nLabel.titleLabel?.font = UIFont(name: "Roboto-Bold", size: 18.calcvaluex())
                }
                else{
                    vd.nLabel.setTitle("高效率速軌", for: .normal)
                    vd.nLabel.setTitleColor(#colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1), for: .normal)
                    vd.nLabel.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
                }
            }
        }
    }
    
}
class CompareHeader: UIView {
    let headerLabel = UILabel()
    let seperator = UIView()
    let valueLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        addSubview(headerLabel)
        headerLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: centerXAnchor,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        headerLabel.centerYInSuperview()
        //headerLabel.text = "射出單元"
        headerLabel.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(valueLabel)
        valueLabel.anchor(top: nil, leading: centerXAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        valueLabel.centerYInSuperview()
       
        valueLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        valueLabel.textColor = #colorLiteral(red: 0.4391633868, green: 0.4392449856, blue: 0.4391643405, alpha: 1)
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CompareLabel : UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        textColor = #colorLiteral(red: 0.1358637214, green: 0.09255879372, blue: 0.0801673159, alpha: 1)
        textAlignment = .center
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CompareCell:UITableViewCell {
    let headerLabel = UILabel()
     let seperator = UIView()
    let unitLabel = UILabel()
    var labelArray = [CompareLabel(),CompareLabel(),CompareLabel(),CompareLabel(),CompareLabel(),CompareLabel()]

    init(spec:NewSpecField) {
        super.init(style: .default, reuseIdentifier: "cell")
        let vd = UIView()
        vd.backgroundColor = #colorLiteral(red: 0.9913000464, green: 0.9481928945, blue: 0.8962452412, alpha: 1)
        selectedBackgroundView = vd
        backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        contentView.addSubview(headerLabel)
        headerLabel.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        headerLabel.centerYInSuperview()
        headerLabel.text = spec.title
        headerLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.4391633868, green: 0.4392449856, blue: 0.4391643405, alpha: 1)
        
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        contentView.addSubview(seperator)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        for (index,i) in labelArray.enumerated() {
            contentView.addSubview(i)
            i.constrainHeight(constant: 25.calcvaluey())
            i.constrainWidth(constant: 106.calcvaluex())
            i.centerYInSuperview()
            i.text = spec.value[index]
            switch index {
            case 0 :
                i.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 230.calcvaluex()).isActive = true
            case 1:
                i.leadingAnchor.constraint(equalTo: labelArray[0].trailingAnchor,constant: 20.calcvaluex()).isActive = true
            case 2:
                i.leadingAnchor.constraint(equalTo: labelArray[1].trailingAnchor,constant: 33.calcvaluex()).isActive = true
            case 3:
                i.leadingAnchor.constraint(equalTo: labelArray[2].trailingAnchor,constant: 20.calcvaluex()).isActive = true
            case 4:
                i.leadingAnchor.constraint(equalTo: labelArray[3].trailingAnchor,constant: 33.calcvaluex()).isActive = true
            case 5:
                i.leadingAnchor.constraint(equalTo: labelArray[4].trailingAnchor,constant: 20.calcvaluex()).isActive = true
            default:
                ()
            }
            
        }
        //layoutIfNeeded()
    }
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        let vd = UIView()
//        vd.backgroundColor = #colorLiteral(red: 0.9913000464, green: 0.9481928945, blue: 0.8962452412, alpha: 1)
//        selectedBackgroundView = vd
//        backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
//        addSubview(headerLabel)
//        headerLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
//        headerLabel.centerYInSuperview()
//        headerLabel.text = "床面旋徑"
//        headerLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
//        headerLabel.textColor = #colorLiteral(red: 0.4391633868, green: 0.4392449856, blue: 0.4391643405, alpha: 1)
//
//        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
//        addSubview(seperator)
//        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
//
//        addSubview(unitLabel)
//        unitLabel.text = "mm"
//        unitLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
//        unitLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
//        unitLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 179.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 16.calcvaluey()))
//        unitLabel.centerYInSuperview()
//
//        firstLabel.text = "Ø570"
//        secondLabel.text = "Ø650"
//        thirdLabel.text = "Ø650"
//
//        firstLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
//        secondLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
//        thirdLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
//
//        firstLabel.textColor = #colorLiteral(red: 0.1358637214, green: 0.09255879372, blue: 0.0801673159, alpha: 1)
//        secondLabel.textColor = #colorLiteral(red: 0.1358637214, green: 0.09255879372, blue: 0.0801673159, alpha: 1)
//        thirdLabel.textColor = #colorLiteral(red: 0.1358637214, green: 0.09255879372, blue: 0.0801673159, alpha: 1)
//
//        addSubview(firstLabel)
//        firstLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 322.calcvaluex(), bottom: 0, right: 0))
//        firstLabel.centerYInSuperview()
//
//        addSubview(secondLabel)
//        secondLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing:     nil,padding: .init(top: 0, left: 560.calcvaluex(), bottom: 0, right: 0))
//        secondLabel.centerYInSuperview()
//
//        addSubview(thirdLabel)
//        thirdLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 798.calcvaluex(), bottom: 0, right: 0))
//        thirdLabel.centerYInSuperview()
//
//    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SpecCell:UITableViewCell {
    let headerLabel = UILabel()
     let seperator = UIView()
    let unitLabel = UILabel()
    let firstLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let vd = UIView()
        vd.backgroundColor = #colorLiteral(red: 0.9913000464, green: 0.9481928945, blue: 0.8962452412, alpha: 1)
        selectedBackgroundView = vd
        backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        addSubview(headerLabel)
        headerLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        headerLabel.centerYInSuperview()
       
        headerLabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.4391633868, green: 0.4392449856, blue: 0.4391643405, alpha: 1)
        
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        
        addSubview(unitLabel)
        
        unitLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        unitLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        unitLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 179.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 16.calcvaluey()))
        unitLabel.centerYInSuperview()
        
        

        
        firstLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())

        
        firstLabel.textColor = #colorLiteral(red: 0.1358637214, green: 0.09255879372, blue: 0.0801673159, alpha: 1)

        
        addSubview(firstLabel)
        firstLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 392.calcvaluex(), bottom: 0, right: 0))
        firstLabel.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ItemLabel : UIButton {

    init(text:String,price:Bool = false,textColor : UIColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)) {
        super.init(frame: .zero)
        if price {
            setTitleColor(#colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1), for: .normal)
            titleLabel?.font = UIFont(name: "Roboto-Bold", size: 18.calcvaluex())
        }
        else{
            setTitleColor(textColor, for: .normal)
            titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        }
        setTitle(text, for: .normal)

        
        titleLabel?.textAlignment = .center
        
        isUserInteractionEnabled = true
        layer.cornerRadius = 20.calcvaluey()
        backgroundColor = .white
        addshadowColor(color: .white)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ItemView : UIView,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    let imgv = UIImageView()
        let cancelButton = UIButton(type: .custom)
    let nLabel = ItemLabel(text: "")
    let sLabel = ItemLabel(text: "選擇",textColor: #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1))
    let zLabel = TwoItemView()
    let imView = UIView()
    let addView : UIButton = {
        let aV = UIButton(type: .custom)
        aV.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        aV.addshadowColor()
        aV.setImage(#imageLiteral(resourceName: "ic_addition").withRenderingMode(.alwaysTemplate), for: .normal)
        aV.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)

        return aV
    }()
    func showAddView(){
        imView.isHidden = true
        addView.isHidden = false
    
    }
    func showMachineView(){
        imView.isHidden = false
        addView.isHidden = true
    }
//    lazy var selectMachineTableView : UIView = {
//       let vd = UIView()
//        vd.backgroundColor = .white
//        vd.addshadowColor()
//        vd.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelectMachine)))
//        return vd
//    }()
    var selectMachineView : UIImageView = {
       let vd = UIImageView()
        vd.backgroundColor = .white
        vd.addshadowColor()
        vd.isUserInteractionEnabled = true

        return vd
    }()
    @objc func goSelectMachine(){
        print(7765)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        addSubview(addView)
        addView.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 212.calcvaluex(), height: 88.calcvaluey()))
        addView.centerXInSuperview()
        addSubview(imView)
        imView.fillSuperview()
        imView.isHidden = true

        imgv.contentMode = .scaleAspectFill
        imView.addSubview(imgv)
        imgv.anchor(top: imView.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 157.calcvaluex(), height: 92.calcvaluey()))
        imgv.centerXInSuperview()
        
        cancelButton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        imView.addSubview(cancelButton)
         cancelButton.contentHorizontalAlignment = .fill
         cancelButton.contentVerticalAlignment = .fill
        cancelButton.anchor(top: imView.topAnchor, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        
        


        
        imView.addSubview(nLabel)
        nLabel.anchor(top: imgv.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 232.calcvaluex(), height: 40.calcvaluey()))
        

        nLabel.centerXInSuperview()
        //nLabel.addTarget(self, action: #selector(goToSelectMachine), for: .touchUpInside)
        imView.addSubview(sLabel)
        sLabel.anchor(top: nLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 232.calcvaluex(), height: 40.calcvaluey()))
        sLabel.centerXInSuperview()
        imView.addSubview(zLabel)
        zLabel.anchor(top: sLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 232.calcvaluex(), height: 40.calcvaluey()))
        zLabel.centerXInSuperview()
        
//        imView.addSubview(selectMachineView)
//        selectMachineView.anchor(top: nLabel.bottomAnchor, leading: nLabel.leadingAnchor, bottom: nil, trailing: nLabel.trailingAnchor,size: .init(width: 0, height: 300.calcvaluey()))
//        let tap = UITapGestureRecognizer(target: self, action: #selector(goToSelectMachine))
//        tap.delegate = self
//        selectMachineView.addGestureRecognizer(tap)
        
    }
    @objc func goToSelectMachine(){

        let vd = SelectionController(parentFrame: General.shared.getTopVc()?.view.convert(nLabel.frame, from: self))
        vd.modalPresentationStyle = .overCurrentContext
        vd.view.backgroundColor = .clear
        
        General.shared.getTopVc()?.present(vd, animated: true, completion: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TwoItemView : UIView {
    let nLabel = ItemLabel(text: "選擇",textColor: #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1))
    let andLabel = UILabel()
    let fLabel = ItemLabel(text: "選擇",textColor: #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1))
    var nLabelAnchor : AnchoredConstraints?
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(nLabel)
        addSubview(andLabel)
        addSubview(fLabel)
        nLabel.tag = 0
        nLabelAnchor = nLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 106.calcvaluex(), height: 40.calcvaluey()))
        nLabel.centerYInSuperview()
        
        andLabel.text = "&"
        andLabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        andLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        andLabel.anchor(top: nil, leading: nLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0))
        andLabel.centerYInSuperview()
        fLabel.tag = 1
        fLabel.anchor(top: nil, leading: andLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 106.calcvaluex(), height: 40.calcvaluey()))
        fLabel.centerYInSuperview()
    }
    func setOne(){
        nLabelAnchor?.width?.constant = 232.calcvaluex()
        andLabel.isHidden = true
        fLabel.isHidden = true
        self.layoutIfNeeded()
    }
    func setTwo(){
        nLabelAnchor?.width?.constant = 106.calcvaluex()
        andLabel.isHidden = false
        fLabel.isHidden = false
        self.layoutIfNeeded()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
