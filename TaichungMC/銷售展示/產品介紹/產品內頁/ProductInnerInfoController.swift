//
//  ProductInfoController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
struct ProductButtonModel {
    var name:String
    var selectedImage:UIImage
    var unselectedImage:UIImage
}
class ProductButton : UIView{
    let imgv = UIImageView()
    let seperator = UIView()
    let nLabel = UILabel()
    var pbm:ProductButtonModel?
    init(pbm:ProductButtonModel) {
        super.init(frame: .zero)
        self.pbm = pbm
        imgv.contentMode = .scaleAspectFit
        addSubview(imgv)
        imgv.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing:nil,padding: .init(top: 20.calcvaluey(), left: 78.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
       // imgv.centerYInSuperview()
        imgv.image = self.pbm?.unselectedImage
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)

        addSubview(seperator)
        seperator.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 10.5.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 45.calcvaluey()))
        //seperator.centerYInSuperview()
        nLabel.text = self.pbm?.name
        nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)

        addSubview(nLabel)
        nLabel.anchor(top: topAnchor, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        //nLabel.centerYInSuperview()
    }

    func setSelect(){
        backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        nLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nLabel.textColor = .white
        imgv.image = self.pbm?.selectedImage
        seperator.isHidden = true
    }
    func unSelect(){
        backgroundColor = .clear
        nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        imgv.image = self.pbm?.unselectedImage
        if tag != 6{
        seperator.isHidden = false
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ProductInnerTabBarController: UITabBarController{

    var series: Series?
    var productIndex: Int?
    var selectedSpec1: Specification?
    var selectedSpec2: Specification?
    var selectedProduct : Product?
    var allSeries = [Series]()
    var selectedSeries : Series?
    let vd = UIView()
    let stackview = UIStackView()
    let productButtons = [ProductButton(pbm: ProductButtonModel(name: "機台展示", selectedImage: #imageLiteral(resourceName: "ic_tab_features_pressed"), unselectedImage: #imageLiteral(resourceName: "ic_tab_features_normal"))),ProductButton(pbm: ProductButtonModel(name: "機械規格", selectedImage: #imageLiteral(resourceName: "ic_tab_list_pressed"), unselectedImage: #imageLiteral(resourceName: "ic_tab_list_normal"))),ProductButton(pbm: ProductButtonModel(name: "標配明細", selectedImage: #imageLiteral(resourceName: "ic_tab_spec_pressed"), unselectedImage: #imageLiteral(resourceName: "ic_tab_spec_normal"))),ProductButton(pbm: ProductButtonModel(name: "選購項目", selectedImage: #imageLiteral(resourceName: "ic_tab_optional_pressed"), unselectedImage: #imageLiteral(resourceName: "ic_tab_optional_normal")))]
    var prevIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.isHidden = true
        vd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vd.addshadowColor(color: .white)

        view.addSubview(vd)
        vd.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 72.calcvaluey()))

        let vwidth = UIScreen.main.bounds.width/CGFloat(productButtons.count)
        for (index,i) in productButtons.enumerated() {
            
        vd.addSubview(i)
            if index == 0{
                i.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,size: .init(width: vwidth, height: 0))
            }
            else{
                i.anchor(top: vd.topAnchor, leading: productButtons[index-1].trailingAnchor, bottom: vd.bottomAnchor, trailing: nil,size: .init(width: vwidth, height: 0))
            }
            i.tag = index
            i.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goButton)))
            
        }
        productButtons[productButtons.count-1].seperator.isHidden = true
        productButtons[0].setSelect()

        let vd = ProductFeatureController()
        
        //let mt = SampleProductTabController(nextvd: RCSpecTableView())
        let mt = SampleRCTopViewController(bottomView: SampleProductTabController(nextvd: RCSpecTableView(), mode: .Spec))
        viewControllers = [vd,mt,SampleRCTopViewController(bottomView: SpecDetailController()),SampleRCTopViewController(bottomView: SelectEquipController(nextvd: SpecRightView(), mode: .Equip))]
        
        
    }
    @objc func goButton(sender:UITapGestureRecognizer){
        if prevIndex != 0{
        productButtons[prevIndex-1].seperator.isHidden = false
        }
        productButtons[prevIndex].unSelect()
        if let tag = sender.view?.tag {
            if tag != 0 {
            productButtons[tag - 1].seperator.isHidden = true
            }
            productButtons[tag].setSelect()
            
            self.selectedIndex = tag
            prevIndex = tag
            
            
        }
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
                
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        
    }
}
class ProductInnerInfoController: UIViewController {
        override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    var series: Series?
    
    var productIndex: Int?
    var isShooter = false
    let topview = UIView()
    let backbutton = UIImageView(image: #imageLiteral(resourceName: "ic_back"))
    let titleview = UILabel()
    var titleviewanchor:AnchoredConstraints!
    var tabcon = ProductInnerTabBarController()
    var optionarray = [OptionObj]()
    var selectedSpec1: Specification?
    var selectedSpec2: Specification?
    var selectedSeries : Series?
    var selectedProduct : Product?
    @objc func goCreateInterview(){

        if selectedProduct == nil{
            let alertController = UIAlertController(title: "請選擇射座與螺桿直徑", message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        if selectedSpec1 == nil{
            let alertController = UIAlertController(title: "請選擇射座與螺桿直徑", message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        if selectedSpec2 == nil && isShooter{
            let alertController = UIAlertController(title: "請選擇射座與螺桿直徑", message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        if (selectedProduct?.getProductsPrices()?.price == 0) || (selectedProduct?.getProductsPrices()?.price == nil){
            let alertController = UIAlertController(title: "此產品不支援您的貨幣群組，請通知管理員", message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        else{

            let vd = InterviewController()
            vd.modalPresentationStyle = .fullScreen
            vd.mode = .New
            var pr = selectedProduct
            let spec1 = selectedSpec1?.getInjectionValue() ?? ""
            let spec2 = selectedSpec2?.getInjectionValue() ?? ""
          let fullname = "\(selectedSeries?.title ?? "") / 射座型式：\(pr?.shooter_code ?? "") / 螺桿直徑：\(spec1)&\(spec2)"
            pr?.full_name = fullname
            vd.addedMachine.append(AddedProduct(series: nil, sub_series: selectedSeries,product: pr, spec: selectedSpec1, options: [], customOptions: [customForm(id: 0, status: -1, is_Cancel: false, statusArray: [], options: [])],fileAssets:[]))

       vd.savedOptions[0] = self.optionarray
        vd.addedMachine[0].options = []
        for i in self.optionarray {
            if i.selectedRow != 0{
                if let option = i.option?.children?[i.selectedRow]{
                    vd.addedMachine[0].options.append(SelectedOption(name: "\(i.option?.title ?? "") / \(option.title)", option: option))
                }
            }
        }
            vd.fieldsArray = [.Info,.Machine,.Discount]
        vd.createEditInterviewCell()
        vd.createMachineCell()
        self.present(vd, animated: true, completion: nil)
        }
    }
    let stackview = UIStackView()
    override func viewDidLoad() {
        topview.isUserInteractionEnabled = true
         topview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        view.addSubview(topview)
        topview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 86.calcvaluey()))
        let slideinview = UIView()
        view.addSubview(slideinview)
        slideinview.backgroundColor = #colorLiteral(red: 0.8205059171, green: 0.4463779926, blue: 0.01516667381, alpha: 1)
        
        slideinview.anchor(top: view.topAnchor, leading: topview.leadingAnchor, bottom: nil, trailing: topview.trailingAnchor,size: .init(width: 0, height: UIApplication.shared.statusBarFrame.size.height))
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        
        stackview.spacing = 16.calcvaluex()
        stackview.axis = .horizontal
        stackview.distribution = .fillEqually
        for (index,i) in ["","","建立訪談"].enumerated(){
            if index == 0 || index == 1{
                stackview.addArrangedSubview(UIView())
            }
            else{
            let button = UIButton(type: .custom)
            button.setTitle(i, for: .normal)
            button.setTitleColor(.white, for: .normal)
            button.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
            button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
            button.layer.cornerRadius = 19.calcvaluey()
                button.addTarget(self, action: #selector(goCreateInterview), for: .touchUpInside)
            stackview.addArrangedSubview(button)
            }
        }
        
        topview.addSubview(stackview)
        stackview.anchor(top: topview.topAnchor, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 440.calcvaluex(), height: 38.calcvaluey()))
 
        
        topview.addSubview(backbutton)
        backbutton.anchor(top: nil, leading: topview.leadingAnchor, bottom: topview.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 17.calcvaluey(), right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        backbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popview)))
        backbutton.isUserInteractionEnabled = true
        
        topview.addSubview(titleview)
        
        titleview.textColor = .white
        titleview.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        titleviewanchor = titleview.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 80.calcvaluex(), bottom: 17.calcvaluey(), right: 0))
        titleview.centerYAnchor.constraint(equalTo: backbutton.centerYAnchor).isActive = true
        
        view.addSubview(tabcon.view)
        addChild(tabcon)
        tabcon.view.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)

    }
    @objc func goCustomer(){
        let con = SelectCustomerController()
        
        con.modalPresentationStyle = .overFullScreen
        
        self.present(con, animated: false, completion: nil)
    }
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
}
