//
//  RcProductSelectionController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/5/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class RcProductSelectionController:SampleController {
    var mode = CompareMode.Normal {
        didSet{
            typeCollection.mode = self.mode
        }
    }
    let bottomselectionview = UIView()
    var TopSeries = [Series]() {
        didSet{
            productarray = TopSeries
            self.selectionCollectionview.reloadData()
            typeCollection.productArray = productarray[selectedIndex].children ?? []
            self.setMainView()
        }
    }
    //let selectionCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    lazy var selectionCollectionview : UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        let collectionview = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        collectionview.register(RcSelectionCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionview.contentInset = .init(top: 0, left: 80.calcvaluex(), bottom: 0, right: 80.calcvaluex())
        collectionview.backgroundColor = .clear
        collectionview.delegate = self
        collectionview.dataSource = self
        return collectionview
    }()
    var productarray = [Series]()
    var selectedSeries = [Series]() {
        didSet{
           // self.setMainView()
            cmView.selectedSeries = self.selectedSeries
//            if selectedSeries.count == 0{
//                UIView.animate(withDuration: 0.4) {
//
//                    self.cmView.transform = .identity
//                }
//            }
        }
    }
    var selectedIndex = 0 {
        didSet{
            //setMainView()
        }
    }
    var bottomContentView = UIView()
    lazy var mainPic = UIImageView(image: #imageLiteral(resourceName: "im_machine_rc"))
    lazy var productLabel : UILabel = {
       let label = UILabel()
        label.font = UIFont(name: "Roboto-Bold", size: 42.calcvaluey())
        label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        label.numberOfLines = 0
        //label.text = productarray[selectedIndex].name
        return label
    }()
    var leftPad : UIView = {
        let vd = UIView()
        vd.backgroundColor = #colorLiteral(red: 0.9392504096, green: 0.5119799972, blue: 0.02642006986, alpha: 1)
        vd.layer.cornerRadius = 3.calcvaluex()
        return vd
    }()
    var subLabel : UILabel = {
       let label = UILabel()
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        //label.text = "精密製造、高速塑膠雙射出成型機種"
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return label
    }()
    lazy var descriptionTableView : UITableView = {
        let tb = UITableView(frame: .zero, style: .plain)
        tb.backgroundColor = .clear
        tb.separatorStyle = .none
        tb.delegate = self
        tb.dataSource = self
        return tb
    }()
    let typeLabel : UILabel = {
       let lb = UILabel()
        lb.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        lb.text = "機型"
        return lb
    }()
    let typeCollection: typeCollectionView  = {
        let typ = typeCollectionView()
        
        return typ
    }()
    lazy var compareButton : UIButton = {
        let cb = UIButton(type: .custom)
        cb.setTitle("加入比較", for: .normal)
        cb.backgroundColor = .black
        cb.setTitleColor(.white, for: .normal)
        cb.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        cb.layer.cornerRadius = 21.calcvaluey()
        cb.addTarget(self, action: #selector(addCompare), for: .touchUpInside)
        return cb
    }()
    
    @objc func addCompare(){
       
        if compareButton.titleLabel?.text == "加入比較"{
            ErrorChecking.ErrorView(message: "請點選您想加入比較的機型")
           compareButton.setTitle("取消比較", for: .normal)
            mode = .Compare
//            if selectedSeries.count < 3 {
//        selectedSeries.append(TopSeries[selectedIndex])

//            }
//            else{
//                let alert = UIAlertController(title: "最多只能加三個系列", message: "請先取消其他系列的比較", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
//                self.present(alert, animated: true, completion: nil)
//            }
        }
        else{
            mode = .Normal
            compareButton.setTitle("加入比較", for: .normal)
//            if let index = selectedSeries.firstIndex(where: { (s1) -> Bool in
//                return s1.title == TopSeries[selectedIndex].title
//            }) {
//                selectedSeries.remove(at: index)
//            }
            
        }

        
        
        cmView.selectedSeries = selectedSeries
//        if selectedSeries.count == 0{
//
//            UIView.animate(withDuration: 0.4) {
//
//                self.cmView.transform = .identity
//            }
//
//            self.cmView.cancelAll()
//
//        }
      //  else{
        if mode == .Compare{
        self.cmView.downButton.setImage(#imageLiteral(resourceName: "ic_arrow_drop_down_circle"), for: .normal)
        self.cmView.downButton.transform = .identity
        self.cmView.cmTitle.transform = .identity
            self.typeCollection.contentInset = .init(top: 0, left: 0, bottom: 133.calcvaluey(), right: 0)
        UIView.animate(withDuration: 0.4) {
            
            self.cmView.transform = CGAffineTransform(translationX: 0, y: -133.calcvaluey())
        }
        }
        else{
            self.typeCollection.contentInset = .zero
                        UIView.animate(withDuration: 0.4) {
            
                            self.cmView.transform = .identity
                        }
        }
       // }
    }
    let cmView = comparingView()
    lazy var backgroundImage : UIImageView = {
        let im = UIImageView(image: #imageLiteral(resourceName: "bg_machine_rc"))
        im.contentMode = .scaleAspectFill
        im.clipsToBounds = true
        return im
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "產品介紹"
        bottomselectionview.backgroundColor = .white
        bottomselectionview.addshadowColor(offset:.init(width: 0, height: 2))
        view.addSubview(bottomselectionview)
        
        bottomselectionview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .zero, size: .init(width: 0, height: 65.calcvaluey()))
        
        
        view.addSubview(selectionCollectionview)
        selectionCollectionview.anchor(top: bottomselectionview.topAnchor, leading: bottomselectionview.leadingAnchor, bottom: bottomselectionview.bottomAnchor, trailing: bottomselectionview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: -3.calcvaluey(), right: 0))
        selectionCollectionview.showsHorizontalScrollIndicator = false
        view.addSubview(bottomContentView)
        bottomContentView.backgroundColor = .clear
        
        bottomContentView.anchor(top:bottomselectionview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        view.sendSubviewToBack(bottomContentView)
        bottomContentView.addSubview(backgroundImage)
        backgroundImage.fillSuperview()
        bottomContentView.addSubview(mainPic)
        mainPic.contentMode = .scaleAspectFill
        mainPic.backgroundColor = .clear
        mainPic.anchor(top: bottomContentView.topAnchor, leading: bottomselectionview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 175.calcvaluey(), left: 55.calcvaluex(), bottom: 0, right: 0),size: .init(width: 457.calcvaluex(), height: 267.calcvaluey()))
        
        bottomContentView.addSubview(compareButton)
        compareButton.anchor(top: bottomContentView.topAnchor, leading: nil, bottom: nil, trailing: bottomContentView.trailingAnchor,padding: .init(top: 51.calcvaluey(), left: 0, bottom: 0, right: -18.calcvaluex()),size: .init(width: 132.calcvaluex(), height: 42.calcvaluey()))

        
        bottomContentView.addSubview(leftPad)
        leftPad.anchor(top: bottomContentView.topAnchor, leading: bottomContentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 55.5.calcvaluey(), left: 540.calcvaluex(), bottom: 0, right: 18.calcvaluex()),size: .init(width: 6.calcvaluex(), height: 40.calcvaluey()))
        bottomContentView.addSubview(productLabel)
        productLabel.anchor(top: bottomContentView.topAnchor, leading: leftPad.trailingAnchor, bottom: nil, trailing: compareButton.leadingAnchor,padding: .init(top: 51.calcvaluey(), left: 8.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        bottomContentView.addSubview(subLabel)
        subLabel.anchor(top: productLabel.bottomAnchor, leading: productLabel.leadingAnchor, bottom: nil, trailing: bottomContentView.trailingAnchor,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        bottomContentView.addSubview(descriptionTableView)
        descriptionTableView.showsVerticalScrollIndicator = false
        descriptionTableView.anchor(top: subLabel.bottomAnchor, leading: subLabel.leadingAnchor, bottom: nil, trailing: bottomContentView.trailingAnchor,padding: .init(top: 41.calcvaluey(), left: 0, bottom: 0, right: 94.calcvaluex()),size: .init(width: 0, height: 173.calcvaluey()))
        
        bottomContentView.addSubview(typeLabel)
        typeLabel.anchor(top: descriptionTableView.bottomAnchor, leading: nil, bottom: nil, trailing: bottomContentView.trailingAnchor,padding: .init(top: 33.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 466.calcvaluex(), height: 25.calcvaluey() ))
        
        bottomContentView.addSubview(typeCollection)
        typeCollection.anchor(top: typeLabel.bottomAnchor, leading: typeLabel.leadingAnchor, bottom: bottomContentView.bottomAnchor, trailing: bottomContentView.trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 34.calcvaluey()))
        typeCollection.mdelegate = self
        view.addSubview(cmView)
        cmView.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 18.calcvaluex(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 122.calcvaluey()))
        cmView.compareButton.addTarget(self, action: #selector(goCompare), for: .touchUpInside)
        cmView.firstItem.delegate = self
        cmView.secondItem.delegate = self
        cmView.thirdItem.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        

    }
    @objc func goCompare(){
        let mt = ComparingItemController()
        mt.con = self
        mt.selectedSeries = self.selectedSeries
        if let first = self.selectedSeries.first {
            if let selectedFirst = first.children?.first(where: { (sr) -> Bool in
                return sr.id == first.selectedId
            }) {
                getSpec(series: selectedFirst)
            }
        }
        
        mt.modalPresentationStyle = .fullScreen
        self.present(mt, animated: true, completion: nil)
    }
    var desscript = [String]()
}
enum CompareMode {
    case Normal
    case Compare
}
extension RcProductSelectionController : UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , typeCollectionViewDelegate,compareItemViewDelegate{
    func setSeries(seriesTitle: String?, seriesId: String) {
        var pr = TopSeries[selectedIndex]
        pr.selectedId = seriesId
        pr.children = typeCollection.productArray
        pr.selectedTitle = seriesTitle
        if selectedSeries.count < 3{
        selectedSeries.append(pr)
        cmView.selectedSeries = selectedSeries
        }
        else{
                            let alert = UIAlertController(title: "最多只能加三種機型", message: "請先取消其他機型的比較", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
        }
    }
    

    
    func clearMachine(index: Int) {

        selectedSeries.remove(at: index)
        
    }
    
    func goToProduct(index:Int) {
        let vc = ProductInnerInfoController()
        vc.modalPresentationStyle = .fullScreen
        
        vc.titleview.text = "\(TopSeries[selectedIndex].title ?? "")"
        vc.tabcon.allSeries = typeCollection.productArray
        vc.tabcon.selectedSeries = typeCollection.productArray[index]
        self.getSpec(series:typeCollection.productArray[index])
        self.present(vc, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TopSeries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RcSelectionCollectionViewCell
        cell.setData(series: TopSeries[indexPath.item])
        if selectedIndex == indexPath.item {
            cell.showSelected()
        }
        else{
            cell.showUnSelected()
        }
       
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = "\(TopSeries[indexPath.item].title ?? "")".width(withConstrainedHeight: collectionView.frame.height, font: UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!) 
        if width <= 89.calcvaluex() {
            width = 89.calcvaluex()
        }
        else{
            width += 20.calcvaluex()
        }
        
        return  .init(width: width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 88.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if TopSeries[indexPath.item].is_disable != 1{
        selectedIndex = indexPath.item

        setMainView()
        }

    }
    func getSpec(series:Series){
       // for i in self.typeCollection.productArray {
        print(331,series)
        if let pr = series.products {
            print(113,pr)
            if pr.count == 0{
                SpecArray.constructValue(field: [])
                return
            }
            for a in pr {
                if let spec = a.specifications {
                    for s in spec{
                       
                                SpecArray.constructValue(field: s.fields)
                                break
                        
                        }
                }

                    
                
            }
        }
        else{
            SpecArray.constructValue(field: [])
            return
        }
    

        //}
    }
    func setMainView(){

        let pr = TopSeries[selectedIndex]
        productLabel.text = "\(pr.title ?? "")"
        if let asset = pr.assets {
            backgroundImage.sd_setImage(with: URL(string: "\(asset.getBackgroundPath())") , completed: nil)
            mainPic.sd_setImage(with: URL(string: "\(asset.getMachinePath())") , completed: nil)
            
        }
        self.desscript = pr.description ?? []
        self.descriptionTableView.reloadData()
       
        self.typeCollection.productArray = []
        self.typeCollection.isHidden = true
        if let category = UserDefaults.standard.getCatValueData(id: TopSeries[selectedIndex].id){
            NetworkCall.shared.postCall(parameter: "api-or/v1/category/\(TopSeries[selectedIndex].id )/check", dict: ["check_token": UserDefaults.standard.getUpdateToken(id: TopSeries[selectedIndex].id) ], decoderType: UpdateClass.self) { (json) in
                DispatchQueue.main.async {
                    if let json = json {
                        
                        if !json.data {
                            
                            NetworkCall.shared.getCall(parameter: "api-or/v1/categories", decoderType: topData.self) { (json) in

                         
                                if let json = json {

                                    if let dt = json.data?.first(where: { (st) -> Bool in
                                        return st.id == self.TopSeries[self.selectedIndex].id
                                    }) {
                                        UserDefaults.standard.saveUpdateToken(token: dt.update_token ?? "", id: dt.id )
                                        
                                }
                                }
                                else{
                                                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)

                                                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))

                                                    self.present(alert, animated: true, completion: nil)
                                                    return
                                }
                                
                            }
                            
                            
                            NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(self.TopSeries[self.selectedIndex].id)", decoderType: topData.self) { (json) in
                                if let json = json{
                                do{
                                    let encode = try JSONEncoder().encode(json)
                                    UserDefaults.standard.saveCatValueData(data: encode, id: self.TopSeries[self.selectedIndex].id )
                                }
                                catch{
                                    
                                }
                                    DispatchQueue.main.async {
                                        self.typeCollection.productArray = (json.data ?? []).filter({ (sr) -> Bool in
                                            return sr.status == 1
                                        }).sorted(by: { (s1, s2) -> Bool in
                                            return (s1.order ?? 0) < (s2.order ?? 0)
                                        })
                                        self.typeCollection.isHidden = false
                                    }
                                    
                                    
                                    
                                
                                }
                                else{
                                    do{
                                        let json = try JSONDecoder().decode(topData.self, from: category)
                                        self.typeCollection.productArray = (json.data ?? []).filter({ (sr) -> Bool in
                                            return sr.status == 1
                                        }).sorted(by: { (s1, s2) -> Bool in
                                            return (s1.order ?? 0) < (s2.order ?? 0)
                                        })
                                        self.typeCollection.isHidden = false
                                    }
                                    catch let error{
                                        print(371,error)
                                    }
                                }
                            }
                        }
                        else{
                            do{
                                let json = try JSONDecoder().decode(topData.self, from: category)
                                self.typeCollection.productArray = (json.data ?? []).filter({ (sr) -> Bool in
                                    return sr.status == 1
                                }).sorted(by: { (s1, s2) -> Bool in
                                    return (s1.order ?? 0) < (s2.order ?? 0)
                                })
                                self.typeCollection.isHidden = false
                            }
                            catch let error{
                                print(371,error)
                            }
                        }
                    }
                    else{
                                        do{
                                            let json = try JSONDecoder().decode(topData.self, from: category)
                                            self.typeCollection.productArray = (json.data ?? []).filter({ (sr) -> Bool in
                                                return sr.status == 1
                                            }).sorted(by: { (s1, s2) -> Bool in
                                                return (s1.order ?? 0) < (s2.order ?? 0)
                                            })
                                            self.typeCollection.isHidden = false
                                                                                    }
                                        catch let error{
                                            print(371,error)
                                        }
                    }
                }
            }

        }
        else{

        NetworkCall.shared.getCall(parameter: "api-or/v1/category/\(self.TopSeries[selectedIndex].id)", decoderType: topData.self) { (json) in
            if let json = json{
            do{
                let encode = try JSONEncoder().encode(json)
                UserDefaults.standard.saveCatValueData(data: encode, id: self.TopSeries[self.selectedIndex].id)
            }
            catch{
                
            }
                DispatchQueue.main.async {
                    self.typeCollection.productArray = (json.data ?? []).filter({ (sr) -> Bool in
                        return sr.status == 1
                    }).sorted(by: { (s1, s2) -> Bool in
                        return (s1.order ?? 0) < (s2.order ?? 0)
                    })
                    self.typeCollection.isHidden = false
                }
               
                
               
            
            }
            else{
                let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        }
        
        self.selectionCollectionview.reloadData()
        
        
    }
}

extension RcProductSelectionController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return desscript.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = descriptionCell(style: .default, reuseIdentifier: "cell")
       // cell.dLabel.text = productarray[selectedIndex].descriptions[indexPath.row]
        cell.dLabel.text = desscript[indexPath.row]
        return cell
    }
    
    
}
class descriptionCell : UITableViewCell {
    let cirView : UIView = {
        let vd = UIView()
        vd.backgroundColor = #colorLiteral(red: 0.9427522421, green: 0.5120122433, blue: 0.02646821924, alpha: 1)
        vd.layer.cornerRadius = 5.calcvaluex()
        return vd
    }()
    let dLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Roboto-Light", size: 16.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1351079345, green: 0.09290169924, blue: 0.0822731033, alpha: 1)
        label.numberOfLines = 0
        return label
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle = .none
        contentView.addSubview(cirView)
        cirView.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 10.calcvaluex(), height: 10.calcvaluex()))
        
        contentView.addSubview(dLabel)
        dLabel.anchor(top: contentView.topAnchor, leading: cirView.trailingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 10.calcvaluey(), right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class RcSelectionCollectionViewCell : UICollectionViewCell {
    let label = UILabel()
    let underline = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        label.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 0))
        label.centerXInSuperview()
        
        addSubview(underline)
        underline.backgroundColor = .clear
        underline.anchor(top: nil, leading: nil, bottom: bottomAnchor, trailing: nil,size: .init(width: 50.calcvaluex(), height: 6.calcvaluey()))
        underline.layer.cornerRadius = 3.calcvaluey()
        underline.centerXInSuperview()
    }
    func setData(series:Series) {
        self.label.text = "\(series.title ?? "")"

    }
    func showSelected(){
        self.label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        self.label.textColor = #colorLiteral(red: 0.9427522421, green: 0.5120122433, blue: 0.02646821924, alpha: 1)
        self.underline.backgroundColor = #colorLiteral(red: 0.9427522421, green: 0.5120122433, blue: 0.02646821924, alpha: 1)
    }
    func showUnSelected(){
        self.label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        self.label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        self.underline.backgroundColor = .clear
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol typeCollectionViewDelegate {
    func goToProduct(index:Int)
    func setSeries(seriesTitle:String?,seriesId:String)
}
class typeCollectionView : UICollectionView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var mode = CompareMode.Normal
    var mdelegate: typeCollectionViewDelegate?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! typeCollectionViewCell
        cell.label.text = productArray[indexPath.item].title
        if productArray[indexPath.item].is_disable == 1{
            cell.alphabackground.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1).withAlphaComponent(0.4)
        }
        else{
            cell.alphabackground.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1).withAlphaComponent(0)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: (collectionView.frame.width - 36.calcvaluex())/3 , height: 64.calcvaluey())
    }
    var productArray = [Series]() {
        didSet{
            UIView.performWithoutAnimation {
                self.reloadData()
            }
            
        }
    }
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .vertical
        
        super.init(frame: .zero, collectionViewLayout: flowlayout)
        self.backgroundColor = .clear
        self.delegate = self
        self.dataSource = self
        self.showsVerticalScrollIndicator = false
        self.register(typeCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if productArray[indexPath.item].is_disable != 1{
        if mode == .Normal{
        mdelegate?.goToProduct(index:indexPath.item)
        }
        else{
            mdelegate?.setSeries(seriesTitle: productArray[indexPath.item].title, seriesId: productArray[indexPath.item].id)
        }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 18.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 18.calcvaluex()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class typeCollectionViewCell : UICollectionViewCell {
    let contenView : UIView = {
       let ct = UIView()
        ct.backgroundColor = #colorLiteral(red: 0.9427522421, green: 0.5120122433, blue: 0.02646821924, alpha: 1)
        ct.layer.cornerRadius = 10.calcvaluex()
        
        return ct
    }()
    let label : UILabel = {
      let lt = UILabel()
        lt.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        lt.textColor = .white
        lt.textAlignment = .center
        lt.text = "RC-180"
        return lt
    }()
    let alphabackground = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        
        addSubview(contenView)
        contenView.addshadowColor(opacity: 0.3)
        contenView.fillSuperview(padding: .init(top: 2.calcvaluey(), left: 2.calcvaluey(), bottom: 2.calcvaluey(), right: 2.calcvaluey()))
        
        contenView.addSubview(label)
        label.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 4.calcvaluex()))
        label.centerYInSuperview()
        contenView.addSubview(alphabackground)
        //alphabackground.backgroundColor = UIColor.white.withAlphaComponent(0)
        alphabackground.layer.cornerRadius = 10.calcvaluex()
        alphabackground.fillSuperview()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
