//
//  SampleRCTopViewController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/12/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class RcLabel : UILabel {
    init(stext:String){
        super.init(frame: .zero)
        font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        text = stext
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class RcSelectionView : UIView {
    let label = UILabel()
    let downArrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down").withRenderingMode(.alwaysTemplate))

    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        label.text = "全部"
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1351745725, green: 0.08770362288, blue: 0.08223239332, alpha: 1)
        
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        layer.cornerRadius = 20.calcvaluey()
        addshadowColor()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        downArrow.contentMode = .scaleAspectFit
        addSubview(downArrow)
        downArrow.tintColor = #colorLiteral(red: 0.2033203298, green: 0.2033203298, blue: 0.2033203298, alpha: 1)
        downArrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 14.calcvaluex()),size: .init(width: 24.calcvaluey(), height: 24.calcvaluey()))
        downArrow.centerYInSuperview()
        addSubview(label)
        label.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: downArrow.leadingAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0))
        label.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class RcTopView : UIView {
    let typeLabel = RcLabel(stext: "機型")
    let typeSelectionView = RcSelectionView()
    
    let positionLabel = RcLabel(stext: "射座形式")
    let positionSelectionView = RcSelectionView()
//    let positionAndLabel = RcLabel(stext: "&")
//    let positionSelectionView2 = RcSelectionView()
    
    let screwLabel = RcLabel(stext: "螺桿直徑")
    var screwSelectionView1 = RcSelectionView()
    let screwAndLabel = RcLabel(stext: "&")
    let screwSelectionView2 = RcSelectionView()
    
    var deleteAllButton : UIButton = {
        let dB = UIButton(type: .custom)
        dB.setTitle("清除", for: .normal)
        dB.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
        dB.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        dB.backgroundColor = .clear
        return dB
    }()
    var screwLabelAnchor:AnchoredConstraints?
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addshadowColor()
        
        addSubview(typeLabel)
        typeLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
        typeLabel.centerYInSuperview()
        addSubview(typeSelectionView)
        typeSelectionView.anchor(top: nil, leading: typeLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0),size: .init(width: 166.calcvaluex(), height: 38.calcvaluey()))
        typeSelectionView.centerYInSuperview()
        typeSelectionView.tag = 0
        addSubview(positionLabel)
        positionLabel.anchor(top: nil, leading: typeSelectionView.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
        positionLabel.centerYInSuperview()
        
        addSubview(positionSelectionView)
//        addSubview(positionAndLabel)
//        addSubview(positionSelectionView2)
        positionSelectionView.tag = 1
        positionSelectionView.anchor(top: nil, leading: positionLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 224.calcvaluex(), height: 38.calcvaluey()))
        positionSelectionView.centerYInSuperview()
//        positionAndLabel.anchor(top: nil, leading: positionSelectionView1.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 12.calcvaluex(), height: 24.calcvaluey()))
//        positionAndLabel.centerYInSuperview()
//
//        positionSelectionView2.anchor(top: nil, leading: positionAndLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 106.calcvaluex(), height: 38.calcvaluey()))
//        positionSelectionView2.centerYInSuperview()
        
        
        addSubview(screwLabel)
        screwLabel.anchor(top: nil, leading: positionSelectionView.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 82.calcvaluex(), height: 28.calcvaluey()))
        screwLabel.centerYInSuperview()
        screwSelectionView1.tag = 2
        screwSelectionView2.tag = 3
        addSubview(screwSelectionView1)
        addSubview(screwAndLabel)
        addSubview(screwSelectionView2)
        
        screwLabelAnchor = screwSelectionView1.anchor(top: nil, leading: screwLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 106.calcvaluex(), height: 38.calcvaluey()))
        screwSelectionView1.centerYInSuperview()
        screwAndLabel.anchor(top: nil, leading: screwSelectionView1.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 12.calcvaluex(), height: 24.calcvaluey()))
        screwAndLabel.centerYInSuperview()
        
        screwSelectionView2.anchor(top: nil, leading: screwAndLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 106.calcvaluex(), height: 38.calcvaluey()))
        screwSelectionView2.centerYInSuperview()
        
        addSubview(deleteAllButton)
        deleteAllButton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 40.calcvaluex(), height: 28.calcvaluey()))
        deleteAllButton.centerYAnchor.constraint(equalTo: screwSelectionView2.centerYAnchor).isActive = true
        
    }
    func setOne(){
        screwLabelAnchor?.width?.constant = 232.calcvaluex()
        screwAndLabel.isHidden = true
        screwSelectionView2.isHidden = true
        self.layoutIfNeeded()
    }
    func setTwo(){
        print(8889)
        screwLabelAnchor?.width?.constant = 106.calcvaluex()
        screwAndLabel.isHidden = false
        screwSelectionView2.isHidden = false
        self.layoutIfNeeded()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class pleaseSelectLabell : UIView {
    let pL = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
         pL.text = "請先選擇機型,射座"
         pL.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
         pL.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())

         backgroundColor = .white
         addshadowColor()
         layer.cornerRadius = 15.calcvaluey()
         addSubview(pL)
         pL.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 237.calcvaluey(), left: 0, bottom: 0, right: 0))
         pL.centerXInSuperview()
         isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SampleRCTopViewController: UIViewController,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view?.isDescendant(of: vde ?? sView()))! {
            return false
        }

        return true
    }
    let vd = RcTopView()
    //let mt = SampleProductTabController(nextvd: RCSpecTableView())
    var selectionView : UIView = {
       let sV = UIView()
        sV.backgroundColor = .white
        sV.addshadowColor()
        return sV
    }()
    var bottomvc : UIViewController?
    var vde : sView?
    init(bottomView:UIViewController) {
        super.init(nibName: nil, bundle: nil)
        bottomvc = bottomView

    }
    @objc func popContent(){
        
    }
    func hideView(){
      // let hiding = true
//        vd.positionSelectionView.isHidden = hiding
//        vd.screwSelectionView1.isHidden = hiding
//        vd.screwSelectionView2.isHidden = hiding
        vd.positionLabel.text = "機台"
        vd.setOne()
//        vd.screwLabel.isHidden = hiding
//        vd.screwAndLabel.isHidden = hiding
    }
    func unHideView(){
        //let hiding = false
        vd.positionLabel.text = "射座"
        vd.setTwo()
//        vd.screwLabel.isHidden = hiding
//        vd.screwAndLabel.isHidden = hiding
//        vd.positionSelectionView.isHidden = hiding
//        vd.screwSelectionView1.isHidden = hiding
//        vd.screwSelectionView2.isHidden = hiding
    }
    var allSeries = [Series]()
    var selectedSpec1: Specification?
    var selectedSpec2: Specification?
    var selectedSeries : Series? {
        didSet{
            
            if let vde = (selectedSeries?.products ?? []).sorted(by: { (pr1, pr2) -> Bool in
                return (pr1.order ?? 0) < (pr2.order ?? 0)
            }).filter({ (pr) -> Bool in
                return pr.status == 1 && pr.is_disable == 0
            }).first {
                print(991,vde.title,vde.shooter_code)
                if vde.shooter_code == nil {
                    hideView()
                    self.selectedSeries?.isNone = true
                    self.pleaseSelectLabel.pL.text = "請先選擇機型,機台"
                }
                else{
                    unHideView()
                    self.selectedSeries?.isNone = false
                    self.pleaseSelectLabel.pL.text = "請先選擇機型,射座"
                }
            }
            else{
                hideView()
                self.selectedSeries?.isNone = true
                self.pleaseSelectLabel.pL.text = "請先選擇機型,機台"
            }
            

        }
    }
    var selectedProducts : Product? {
        didSet{
            print(self.selectedProducts?.getProductsPrices())
        }
    }
    
    var pleaseSelectLabel = pleaseSelectLabell()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let tab = tabBarController as? ProductInnerTabBarController {
           
            allSeries = tab.allSeries
            self.selectedSeries = tab.selectedSeries
            self.selectedProducts = tab.selectedProduct
            self.selectedSpec1 = tab.selectedSpec1
            self.selectedSpec2 = tab.selectedSpec2
            for i in selectedSeries?.products ?? [] {
                if i.shooter_code?.contains("&") ?? false{
                    vd.setTwo()
                    if let tab = tabBarController as? ProductInnerTabBarController {

                        if let parent = tab.parent as? ProductInnerInfoController {
                            parent.isShooter = true
                        }
                    }
                }
                else{
                    vd.setOne()
                    if let tab = tabBarController as? ProductInnerTabBarController {

                        if let parent = tab.parent as? ProductInnerInfoController {
                            parent.isShooter = false
                        }
                    }
                
                }
            }
            vd.typeSelectionView.label.text = self.selectedSeries?.title
            if let ft = bottomvc as? SpecDetailController ?? bottomvc as? SelectEquipController {
                
                if self.selectedProducts != nil{
                    vd.positionSelectionView.label.text = self.selectedSeries?.isNone == true ? self.selectedProducts?.title : self.selectedProducts?.shooter_code
                    vd.screwSelectionView1.label.text = self.selectedSpec1?.getInjectionValue() ?? "全部"
                    vd.screwSelectionView2.label.text = self.selectedSpec2?.getInjectionValue() ?? "全部"
                    ft.view.isHidden = false
                    pleaseSelectLabel.isHidden = true
                }
                else{
                    vd.positionSelectionView.label.text = "全部"
                    vd.screwSelectionView1.label.text = "全部"
                    vd.screwSelectionView2.label.text = "全部"
                    ft.view.isHidden = true
                    pleaseSelectLabel.isHidden = false
                }
            }
            else{
                if self.selectedProducts != nil{
                    vd.positionSelectionView.label.text = self.selectedSeries?.isNone == true ? self.selectedProducts?.title : self.selectedProducts?.shooter_code
                    vd.screwSelectionView1.label.text = self.selectedSpec1?.getInjectionValue() ?? "全部"
                    vd.screwSelectionView2.label.text = self.selectedSpec2?.getInjectionValue() ?? "全部"

                }
                else{
                    vd.positionSelectionView.label.text = "全部"
                    vd.screwSelectionView1.label.text = "全部"
                    vd.screwSelectionView2.label.text = "全部"

                }
            }
            if let jc = bottomvc as? SelectEquipController {
                jc.selectedProduct = self.selectedProducts
               // jc.selectedSeries = self.selectedSeries
            }
            else{

            if let jc = bottomvc as? SampleProductTabController {
                var sc = (self.selectedSeries?.products?.sorted(by: { (pr1, pr2) -> Bool in
                    return (pr1.order ?? 0) < (pr2.order ?? 0)
                }).first?.assets?.specification ?? [])
                print(334,sc)
                sc.insert(FilePath(id: "-1x", title: "機械規格", remote: nil, files: nil), at: 0)
                jc.originalFileArray = sc
                if let k = jc.nextvd as? RCSpecTableView {
                   // k.isNone = self.selectedSeries?.isNone
                    k.selectedSeries = self.selectedSeries
                    k.selectedProducts = self.selectedProducts
                    k.selectedSpec1 = self.selectedSpec1
                    k.selectedSpec2 = self.selectedSpec2
                    k.products = self.selectedSeries?.products ?? []
                    
                }
                //else{

                jc.fileArray = sc
                //}
                
            }
            }
            if let jc = bottomvc as? SpecDetailController {
                jc.selectedProducts = self.selectedProducts
                //jc.selectedSeries = self.selectedSeries
            }

        }
        vd.typeSelectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelection)))
        vd.positionSelectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelection)))
        vd.screwSelectionView1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelection)))
        vd.screwSelectionView2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelection)))
        
        vd.deleteAllButton.addTarget(self, action: #selector(resetAll), for: .touchUpInside)
    }
    @objc func resetAll(){
        self.selectedProducts = nil
        //self.selectedSeries = nil
        self.selectedSpec1 = nil
        self.selectedSpec2 = nil
        self.reloadingData()
    }
    var height : NSLayoutConstraint?
    @objc func dismissAll(){
        if let height = height {
            height.constant = 0
            
            UIView.animate(withDuration: 0.1) {
                self.view.layoutIfNeeded()
            } completion: { (_) in
                self.vde?.removeFromSuperview()
                self.vde = nil
                self.height = nil
            }

           
            
        }
    }
    func reloadingData(){
        
        if let tab = tabBarController as? ProductInnerTabBarController {
            tab.selectedSeries = self.selectedSeries
            tab.selectedProduct = self.selectedProducts
            tab.selectedSpec1 = self.selectedSpec1
            tab.selectedSpec2 = self.selectedSpec2
            if let parent = tab.parent as? ProductInnerInfoController {
                parent.selectedSeries = self.selectedSeries
                parent.selectedProduct = self.selectedProducts
                parent.selectedSpec1 = self.selectedSpec1
                parent.selectedSpec2 = self.selectedSpec2
            }
        }
        vd.typeSelectionView.label.text = self.selectedSeries?.title
        if let ft = bottomvc as? SpecDetailController ?? bottomvc as? SelectEquipController {
            
            if self.selectedProducts != nil{
                vd.positionSelectionView.label.text = self.selectedSeries?.isNone == true ? self.selectedProducts?.title : self.selectedProducts?.shooter_code
                vd.screwSelectionView1.label.text = self.selectedSpec1?.getInjectionValue() ?? "全部"
                vd.screwSelectionView2.label.text = self.selectedSpec2?.getInjectionValue() ?? "全部"
                ft.view.isHidden = false
                pleaseSelectLabel.isHidden = true
            }
            else{
                vd.positionSelectionView.label.text = "全部"
                vd.screwSelectionView1.label.text = "全部"
                vd.screwSelectionView2.label.text = "全部"
                ft.view.isHidden = true
                pleaseSelectLabel.isHidden = false
            }
        }
        else{
            if self.selectedProducts != nil{
                vd.positionSelectionView.label.text = self.selectedSeries?.isNone == true ? self.selectedProducts?.title : self.selectedProducts?.shooter_code
                
                vd.screwSelectionView1.label.text = self.selectedSpec1?.getInjectionValue() ?? "全部"
                vd.screwSelectionView2.label.text = self.selectedSpec2?.getInjectionValue() ?? "全部"

            }
            else{
                vd.positionSelectionView.label.text = "全部"
                vd.screwSelectionView1.label.text = "全部"
                vd.screwSelectionView2.label.text = "全部"
            }
        }
        if let jc = bottomvc as? SpecDetailController {
            jc.selectedProducts = self.selectedProducts
            //jc.selectedSeries = self.selectedSeries
        }
        if let jc = bottomvc as? SelectEquipController {
            jc.selectedProduct = self.selectedProducts
           // jc.selectedSeries = self.selectedSeries
            
        }
        else{

        if let jc = bottomvc as? SampleProductTabController {
            var sc = (self.selectedSeries?.products?.sorted(by: { (pr1, pr2) -> Bool in
                return (pr1.order ?? 0) < (pr2.order ?? 0)
            }).first?.assets?.specification ?? [])
            print(331,sc)
            sc.insert(FilePath(id: "-1x", title: "機械規格", remote: nil, files: nil), at: 0)
            jc.originalFileArray = sc
            if let k = jc.nextvd as? RCSpecTableView {
                k.selectedSeries = self.selectedSeries
                k.selectedProducts = self.selectedProducts
                k.selectedSpec1 = self.selectedSpec1
                k.selectedSpec2 = self.selectedSpec2
                k.products = self.selectedSeries?.products ?? []
                

                
            }
           // else{

                jc.fileArray = sc
            //}

            
        }
        }
        dismissAll()
    }
    func convertToRadius(index:Int) -> [Specification]{
        if let mt = selectedProducts, let spec = mt.specifications?.first {
            if let field = spec.fields.first(where: { (ft) -> Bool in
                return ft.code == "injection-model"
            }) {
                if field.value == nil {
                    var pArray = [Specification]()
                    for j in selectedProducts?.specifications ?? []{
                        
                        pArray.append(j)
                    }
                    return pArray
                   
                }
            }
            
        }
        let pr = selectedProducts?.title ?? ""
        
        let st = pr.split(separator: "&")
  
        
        
       
            var pArray = [Specification]()

            
            for j in selectedProducts?.specifications ?? []{
                
                for k in j.fields {
                    
                    if (k.code ?? "") == "injection-model" {
                        if index >= st.count {
                            if st[0] == (k.value ?? ""){
                                pArray.append(j)
                            }
                        }
                        else{
                        if st[index] == (k.value ?? ""){

                        pArray.append(j)
                        }
                        }
                    }
                }
            }
        
        return pArray


        
    }
    @objc func goSelection(sender: UITapGestureRecognizer){
       
        if self.vde != nil{
            dismissAll()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            if let sender = sender.view {
                if sender.tag == 0{
            self.vde = SelectTableView(items: self.allSeries)
                }
                else if sender.tag == 1{
                    if self.selectedSeries?.isNone ?? true {
                        self.vde = SelectTableView(items: (self.selectedSeries?.products ?? []).filter({ (p1) -> Bool in
                        
                            return p1.status == 1
                        }).sorted(by: { (pr1, pr2) -> Bool in
                            return (pr1.order ?? 0) < (pr2.order ?? 0)
                        }))
                        if let att = self.vde as? sView {
                            att.isNone = true
                        }
                        
                    }
                    else{
                        self.vde = SelectTableView(items: (self.selectedSeries?.products ?? []).filter({ (p1) -> Bool in
                        
                            return p1.shooter_code != nil && p1.status == 1
                        }).sorted(by: { (pr1, pr2) -> Bool in
                            return (pr1.order ?? 0) < (pr2.order ?? 0)
                        }))
                    }

                }
                else if sender.tag == 2{
                    
                    self.vde = SelectTableView(items: self.convertToRadius(index: 0))
                    self.vde?.tag = 2
                }
                else if sender.tag == 3{

                    self.vde = SelectTableView(items: self.convertToRadius(index: 1))
                    self.vde?.tag = 3
                }
            self.vde?.backgroundColor = .white
                self.vde?.specCon = self
            self.view.addSubview(self.vde!)
            
            
                self.vde?.anchor(top: sender.bottomAnchor, leading: sender.leadingAnchor, bottom: nil, trailing: sender.trailingAnchor)
            self.height = self.vde?.heightAnchor.constraint(equalToConstant: 0)
            self.height?.isActive = true
            
            self.view.layoutIfNeeded()

                if sender.tag == 0 {
                self.height?.constant = CGFloat(self.allSeries.count) * 84.calcvaluey()
                }
                else if sender.tag == 1{
                    if self.selectedSeries?.isNone ?? true {
                        self.height?.constant = CGFloat((self.selectedSeries?.products ?? []).filter({ (p1) -> Bool in
                            return p1.status == 1
                        }).sorted(by: { (pr1, pr2) -> Bool in
                            return (pr1.order ?? 0) < (pr2.order ?? 0)
                        }).count) * 84.calcvaluey()
                    }
                    else{
                    self.height?.constant = CGFloat((self.selectedSeries?.products ?? []).filter({ (p1) -> Bool in
                        return p1.shooter_code != nil && p1.status == 1
                    }).sorted(by: { (pr1, pr2) -> Bool in
                        return (pr1.order ?? 0) < (pr2.order ?? 0)
                    }).count) * 84.calcvaluey()
                    }
                }
                else if sender.tag == 2{
                    self.height?.constant = CGFloat(self.convertToRadius(index: 0).count) * 84.calcvaluey()
                }
                else if sender.tag == 3{
                    self.height?.constant = CGFloat(self.convertToRadius(index: 1).count) * 84.calcvaluey()
                }
            
            
            UIView.animate(withDuration: 0.4) {
                self.view.layoutIfNeeded()
                
            }
            }
        }

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var heightanchor:NSLayoutConstraint?
    override func viewDidLoad() {
        super.viewDidLoad()

        
        view.addSubview(vd)
        vd.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 82.calcvaluey()))
        if let mt = bottomvc {
        addChild(mt)
            if let m = mt as? SampleProductTabController{
                m.tap.isEnabled = false
            }
        view.addSubview(mt.view)
        mt.view.anchor(top: vd.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 5.calcvaluey(), left: 0, bottom: 0, right: 0))
        }
        
        view.addSubview(pleaseSelectLabel)
        pleaseSelectLabel.anchor(top: vd.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        vd.typeSelectionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goSelect)))
        view.addSubview(selectionView)
        
        selectionView.anchor(top: vd.typeSelectionView.bottomAnchor, leading: vd.typeSelectionView.leadingAnchor, bottom: nil, trailing: vd.typeSelectionView.trailingAnchor)
        
        heightanchor = selectionView.heightAnchor.constraint(equalToConstant: 0)
        heightanchor?.isActive = true
        selectionView.isHidden = true
//        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissing)))
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissAll))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        self.view.addGestureRecognizer(tap)
    }
    
//    @objc func dismissing(){
//        heightanchor?.constant = 0
//        UIView.animate(withDuration: 0.4) {
//            self.view.layoutIfNeeded()
//        } completion: { (_) in
//            self.selectionView.isHidden = true
//        }
//
//    }
    @objc func goSelect(){
        
        selectionView.isHidden = false
        heightanchor?.constant = 240.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
}
