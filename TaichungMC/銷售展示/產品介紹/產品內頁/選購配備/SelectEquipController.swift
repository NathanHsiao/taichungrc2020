//
//  SelectEquipController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension Double{

        var clean: String {
            return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%0.2f", self)
        }
    
    func converToPrice() -> String?{
        let price = self
        var dilutePrice : String = ""
        var unit = ""
        if price < 100{
            dilutePrice = "\(Int(price))"
        }
        else if price >= 100 && price < 1000{
            dilutePrice = "\(Int(price / 100))"
            unit = "百"
        }
        else if price >= 1000 && price < 10000{
            dilutePrice = "\(Int(price / 1000))"
            unit = "千"
        }
        else {
            
            dilutePrice = (price/10000).clean
            
            unit = "萬"
        }
        
        return "\(dilutePrice)\(unit)元"
    }
}
protocol EquipDelegate {
    func goEquip(index:Int,innerTag:Int)
}
struct OptionObj {
    var selectedRow : Int = 0
    var option : Option?
}
class SelectEquipController: SampleProductTabController,EquipDelegate {
    func goEquip(index:Int,innerTag: Int) {
        optionsArray[index].selectedRow = innerTag
        //innerIndex[index] = innerTag
        
        for i in optionsArray {
            if let index = originaloptions.firstIndex(where: { (obj) -> Bool in
                return obj.option?.id == i.option?.id
            }) {
                originaloptions[index].selectedRow = i.selectedRow
            }
        }
        currentIndex = index
        loadFile(inIndex: innerTag)
        calculatePrice()
        if let tab = tabBarController as? ProductInnerTabBarController {
            
            
            if let k = tab.parent as? ProductInnerInfoController {
                k.optionarray = optionsArray
            }
        }
        self.tableview.reloadData()
    }
    
    override func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == "" || textField.text == nil{
            self.optionsArray = self.originaloptions
        }
        else{
            self.optionsArray = self.originaloptions.filter { (op) -> Bool in
                if let option = op.option,let txt = textField.text{
                    if option.title.lowercased().contains(txt.lowercased()) {
                        return true
                    }
                }
            
                if let _ = op.option?.children?.firstIndex(where: { (op) -> Bool in
                return op.title.lowercased().contains((textField.text ?? "").lowercased())
            }) {
                return true
            }
            return false
        }
        }
        //setOptions()
        self.tableview.reloadData()
    }
    func loadFile(inIndex:Int) {

        
        if let nt = nextvd as? SpecRightView{
            nt.webview.loadRequest(URLRequest(url: URL(string: "about:blank")!))
            if currentIndex < self.optionsArray.count{
            if let url = URL(string: self.optionsArray[currentIndex].option?.children?[inIndex].getUrlString() ?? "") {
                nt.url = url
                let request = URLRequest(url: url)
                General().showActivity(vd: nt.webview)
                nt.webview.loadRequest(request)
            }
            }
            //nt.webview.loadRequest()
        }
    }
    var optionsArray = [OptionObj]()
    //var innerIndex = [Int]()
//    var options = [Option]() {
//        didSet{
//            self.tableview.reloadData()
//        }
//    }
    var originaloptions = [OptionObj]()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableview.estimatedRowHeight = UITableView.automaticDimension;
        self.tableview.rowHeight = UITableView.automaticDimension;
        
        
    }
    var currency = ""
    func calculatePrice(){
        var total = currentPrice
        
        if originaloptions.count == 0{
            return
        }
        for (ind,val) in originaloptions.enumerated() {

            if let pr = originaloptions[ind].option?.children?[val.selectedRow].getProductsPrices() {
                
                if let price = pr.price {
                        total += price
                    }
               
                
            
            }
        }
        print(1123,currency)
        if let right = nextvd as? SpecRightView {
            if total != 0{
            right.priceLabel.text = "\(currency) \(total.converToPrice() ?? "0元")"
            }
            else{
                right.priceLabel.text = currency == "" ? "0元" : "\(currency) 0元"
            }
            
        }
        print(currentPrice)


    }
    var topOptions = [Option]()
    var preSelectedProduct : Product?
    var selectedProduct:Product?{
        didSet{
            
            self.topOptions = selectedProduct?.options?[0].children ?? []
            if preSelectedProduct == nil || preSelectedProduct?.id != selectedProduct?.id{
                textFieldDidEndEditing(searchFild)
                optionsArray = []
                self.setOptions()
            }
            preSelectedProduct = selectedProduct
            
        }
    }
    var currentPrice : Double = 0
    func setOptions(){
        let options = self.topOptions.sorted { (op1, op2) -> Bool in
            return op1.order ?? 0 < op2.order ?? 0
        }.filter { (op) -> Bool in
            if (op.children?.count ?? 0) > 1{
                return true
            }
            return false
        }
        
         
        if optionsArray.isEmpty {
        for var i in options {
            i.children = i.children?.sorted(by: { (op1, op2) -> Bool in
                return (op1.order ?? 0) < (op2.order ?? 0)
            })
            optionsArray.append(OptionObj(selectedRow: 0, option: i))
        }
        }
        self.originaloptions = optionsArray
        
        textFieldDidEndEditing(searchFild)
        
        if let right = nextvd as? SpecRightView {
            
            if let price = selectedProduct?.getProductsPrices() {
                currency = price.currency
                currentPrice = price.price ?? 0
                right.priceLabel.text = "\(price.currency) \((price.price ?? 0).converToPrice() ?? "0元")"

            }
            else{
                currentPrice = 0
                right.priceLabel.text = "0元"
            }
            
            
            
        }
        self.loadFile(inIndex: 0)
        
        self.calculatePrice()
        self.tableview.reloadData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        leftviewAnchor?.width?.constant = 361.calcvaluex()
        searchFild.isHidden = false
        tableviewanchor?.top?.constant = 68.calcvaluey()
        self.view.layoutIfNeeded()
        setOptions()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == selectContentTableView {
            return 84.calcvaluey()
        }
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if tableView == selectContentTableView {
//            return productArray.count
//        }
        return optionsArray.count
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if tableView == selectContentTableView {
//            let cell = selectTableCell(style: .default, reuseIdentifier: "selection")
//            cell.title.text = productArray[indexPath.row].title
//            if indexPath.row == productArray.count - 1{
//                cell.seperator.isHidden = true
//            }
//            else{
//                cell.seperator.isHidden = false
//            }
//            return cell
//        }
        if tableView == tableview {
            let cell = EquipCell(option: self.optionsArray[indexPath.row].option!)
        cell.tag = indexPath.row
        cell.delegate = self

        return cell
        }
        else{
            let cell = UITableViewCell(style: .default, reuseIdentifier: "ct")
            cell.backgroundColor = .white
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? EquipCell {
            if indexPath.row == currentIndex{
                cell.setColor()
            }
            else{
                cell.unsetColor()
            }
            cell.setSelected(index: optionsArray[indexPath.row].selectedRow)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableview {
        currentIndex = indexPath.row
            loadFile(inIndex: optionsArray[currentIndex].selectedRow)
        self.tableview.reloadData()
        }
//        else{
//            if selectedProduct?.id != productArray[indexPath.row].id{
//                self.currentIndex = 0
//            self.selectedProduct = productArray[indexPath.row]
//            self.selectLabel.text = self.selectedProduct?.title
//                setOptions()
//            }
//            self.popContent()
//            
//            //self.setRightView()
//        }
    }
}
class EquipCellSelectView : UIView {
    let imgv = UIButton(type: .custom)
    let nLabel = UILabel()
    let priceLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        imgv.setImage(#imageLiteral(resourceName: "btn_radio_normal"), for: .normal)
        imgv.setImage(#imageLiteral(resourceName: "btn_radio_pressed"), for: .selected)
        addSubview(imgv)
        imgv.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        //imgv.centerYInSuperview()
        
        nLabel.text = "6’’億川"
        nLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        
        addSubview(nLabel)
        nLabel.numberOfLines = 0
        nLabel.anchor(top: imgv.topAnchor, leading: imgv.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        nLabel.trailingAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        
        priceLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        priceLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(priceLabel)
        priceLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 22.calcvaluex()),size: .init(width: 0, height: 25.calcvaluey()))
        priceLabel.centerYInSuperview()
    }
    
    func setImgvSelected(){
        imgv.isSelected = true
        nLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        priceLabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
    }
    func unSelectedImgv(){
        imgv.isSelected = false
        nLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        priceLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class EquipCell:ProductTabCell {
    let topView = UIView()
    let topLabel = UILabel()
    let stackview = UIStackView()
    var delegate:EquipDelegate?
    init(option:Option) {
        //super.init(style: style, reuseIdentifier: reuseIdentifier)
        super.init(style: .default, reuseIdentifier: "eq")
        label.removeFromSuperview()
        topView.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        
        nd.addSubview(topView)
        nd.clipsToBounds = false
        topView.addSubview(topLabel)

        topLabel.centerYInSuperview()
        
        topLabel.text = option.title
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        topLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        topLabel.numberOfLines = 2
        topLabel.anchor(top: topView.topAnchor, leading: topView.leadingAnchor, bottom: topView.bottomAnchor, trailing: topView.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 36.calcvaluex(), bottom: 16.calcvaluey(), right: 36.calcvaluex()))
        //,size: .init(width: 0, height: topLabel.text?.height(withConstrainedWidth: 235.calcvaluex(), font: topLabel.font) ?? 0)
        
        topView.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: vd.trailingAnchor,size: .init(width: 0, height: (topLabel.text?.height(withConstrainedWidth: 235.calcvaluex(), font: topLabel.font) ?? 0)+32.calcvaluey()))
        vd.addSubview(stackview)
        stackview.axis = .vertical
        stackview.distribution = .fill
        //stackview.alignment = .leading
        stackview.spacing = 14.calcvaluey()
        
        for (ind,i) in (option.children ?? []).enumerated() {
            let vc = EquipCellSelectView()
            vc.nLabel.text = i.title
//            if i.prices != nil && i.prices?.count != 0{
//                let st = NumberFormatter()
//                st.numberStyle = .decimal
//                vc.priceLabel.text = "+\(st.string(from: NSNumber(value: i.prices?[0].price ?? 0)) ?? "")元"
//            }
            if let i = i.getProductsPrices() {
                let st = NumberFormatter()
                st.numberStyle = .decimal
                vc.priceLabel.text = "+\(st.string(from: NSNumber(value: i.price ?? 0)) ?? "")元"
            }
            //vc.priceLabel.text =
            vc.nLabel.tag = ind
            vc.nLabel.isUserInteractionEnabled = true
            vc.nLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(textSelect)))
            vc.imgv.tag = ind
            vc.setImgvSelected()
            vc.imgv.addTarget(self, action: #selector(buttonSelected), for: .touchUpInside)
            //vc.priceLabel.isHidden = true
            vc.constrainHeight(constant: i.title.height(withConstrainedWidth: 115.calcvaluex(), font: vc.nLabel.font))
            stackview.addArrangedSubview(vc)

        }
        stackview.anchor(top: topView.bottomAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 36.calcvaluex(), bottom: 16.calcvaluey(), right: 0))
    }
    @objc func textSelect(sender:UITapGestureRecognizer) {
        delegate?.goEquip(index: self.tag, innerTag: sender.view?.tag ?? 0)
    }
    @objc func buttonSelected(sender:UIButton){
        delegate?.goEquip(index: self.tag, innerTag: sender.tag)
    }
    func setSelected(index:Int){
        for (indext,i) in stackview.arrangedSubviews.enumerated() {
            if let vd = i as? EquipCellSelectView {
                if indext == index{
                    vd.setImgvSelected()
                }
                else{
                    vd.unSelectedImgv()
                }
                
            }
        }

    }
    func setUnselected(index:Int){
        if let vd = stackview.arrangedSubviews[index] as? EquipCellSelectView {
            vd.unSelectedImgv()
        }
    }
    override func setColor() {
        super.setColor()
        nd.clipsToBounds = true
    }
    override func unsetColor() {
        super.unsetColor()
        nd.clipsToBounds = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
