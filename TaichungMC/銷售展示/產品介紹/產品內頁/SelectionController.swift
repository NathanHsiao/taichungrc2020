//
//  SelectionController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/21/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class SelectionController: UIViewController {
    var parentFrame : CGRect?
    var vd = UIView()
    init(parentFrame:CGRect?){
        super.init(nibName: nil, bundle: nil)
        self.parentFrame = parentFrame

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        vd.backgroundColor = .white
        vd.addshadowColor()
        if let parent = parentFrame {
            //print(parent.convert(parent.center, to: self.view))

            vd.frame = .init(x: parent.origin.x, y: parent.origin.y + parent.height, width: parent.width, height: 0)
            self.view.addSubview(vd)
        }
        
        

        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissview)))


    }
    @objc func dismissview(){
        self.dismiss(animated: false, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            UIView.animate(withDuration: 0.4) {
                
                   

                    self.vd.frame.size.height += 300.calcvaluey()
                  

                
        }
        }


    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.perform(#selector(performAnimation), with: nil, afterDelay: 0)

    }
    @objc func performAnimation(){

        
    }
}
