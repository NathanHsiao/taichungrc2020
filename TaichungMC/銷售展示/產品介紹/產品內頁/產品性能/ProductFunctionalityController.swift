//
//  ProductFunctionalityController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class ProductFunctionalityController: SampleProductTabController {
    override func viewDidLoad() {
        super.viewDidLoad()
        if let nx = nextvd as? VideoPlayerView{
            nx.videolayer.isUserInteractionEnabled = true

                    nx.playButton.addTarget(self, action: #selector(goPlay), for: .touchUpInside)
        }

    }
    @objc func goPlay(){
        print(999)
        
        let videoview = ExpandVideoView()
        
        videoview.modalPresentationStyle = .overFullScreen
        
        self.present(videoview, animated: false, completion: nil)
    }
}
