//
//  FilteringViewController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/15.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
protocol FilteringViewDelegate {
    func sendData(txt:String,index:Int)
}
class FilteringViewController:UIViewController {
    let vd = UIView()
    let backbutton = UIImageView(image: #imageLiteral(resourceName: "ic_close_small"))
    let titleLabel = UILabel()
    let tableview = UITableView(frame: .zero, style: .plain)
    let confirmButton = UIButton(type: .custom)
    var selectedFill : [String?] = [nil,nil,nil]
    var con:InnerProductIntroController?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        vd.backgroundColor = .white
        vd.addshadowColor(color: .white)
        vd.layer.cornerRadius = 15.calcvaluex()
        
        view.addSubview(vd)
        vd.centerInSuperview(size: .init(width: 624.calcvaluex(), height: 496.calcvaluey()))
        
        vd.addSubview(backbutton)
        backbutton.contentMode = .scaleAspectFit
        
        backbutton.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        backbutton.isUserInteractionEnabled = true
        backbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeView)))
        
        titleLabel.text = "篩選"
        titleLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        titleLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(titleLabel)
        titleLabel.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        titleLabel.centerXInSuperview()
        
        vd.addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.anchor(top: titleLabel.bottomAnchor, leading: vd.leadingAnchor, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 42.calcvaluey(), left: 44.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 315.calcvaluey()))
        
        confirmButton.layer.cornerRadius = 48.calcvaluey()/2
        
        confirmButton.setTitle("確認", for: .normal)
        confirmButton.setTitleColor(.white, for: .normal)
        confirmButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        vd.addSubview(confirmButton)
        confirmButton.anchor(top: tableview.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        confirmButton.centerXInSuperview()
        confirmButton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        confirmButton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
        
    }
    @objc func closeView(){
        con?.addFilter(data: selectedFill)
        self.dismiss(animated: false, completion: nil)
    }
}

extension FilteringViewController: UITableViewDelegate,UITableViewDataSource,FilteringViewDelegate {
    func sendData(txt: String,index:Int) {
        print(index)
        selectedFill[index] = txt
        print(selectedFill)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FilteringViewCell(style: .default, reuseIdentifier: "cell")
        cell.headerLabel.text = ["切削直徑（mm）","兩頂心間距（mm）","型態"][indexPath.row]
        cell.tag = indexPath.row
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105.calcvaluey()
    }
    
    
}

class FilteringViewCell:UITableViewCell{
    let headerLabel = UILabel()
    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var currentIndex : Int?
    var delegate:FilteringViewDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        headerLabel.text = "切削直徑（mm）"
        headerLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        headerLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        addSubview(headerLabel)
        headerLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 25.calcvaluey()))
        
        (collectionview.collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        (collectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        addSubview(collectionview)
        collectionview.anchor(top: headerLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 42.calcvaluey()))
        collectionview.backgroundColor = .clear
        collectionview.dataSource = self
        collectionview.delegate = self
        collectionview.alwaysBounceHorizontal = true
        collectionview.register(FilteringCellCollectionCell.self, forCellWithReuseIdentifier: "cell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension FilteringViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FilteringCellCollectionCell
        cell.label.text = ["< 600","601~1000","1001~2000","< 2001"][indexPath.item]
        if let cI = currentIndex {
            if indexPath.item == cI {
                cell.getSelected = true
            }
            else{
                cell.getSelected = false
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12.calcvaluex()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ct = collectionView.cellForItem(at: indexPath) as! FilteringCellCollectionCell
        delegate?.sendData(txt: "\(["切削直徑（mm）","兩頂心間距（mm）","型態"][self.tag])： \(ct.label.text ?? "")", index: self.tag)
        ct.getSelected = true
        if let cI = currentIndex {
            let mt = collectionView.cellForItem(at: IndexPath(item: cI, section: 0)) as! FilteringCellCollectionCell
            mt.getSelected = false
        }
        currentIndex = indexPath.item
    }

    
}
class FilteringCellCollectionCell: UICollectionViewCell {
    let label = UILabel()
    var getSelected : Bool = false{
        didSet{
        if getSelected {
            backgroundColor = #colorLiteral(red: 0.1279973984, green: 0.08465168625, blue: 0.07670681924, alpha: 1)
            label.textColor = .white
        }
        else{
            backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
            label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        layer.cornerRadius = 21.calcvaluey()
        layer.borderColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        layer.borderWidth = 1.calcvaluex()
        label.text = "< 350"
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        label.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        addSubview(label)
        label.fillSuperview(padding: .init(top: 11.calcvaluey(), left: 32.calcvaluex(), bottom: 12.calcvaluey(), right: 32.calcvaluex()))
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
