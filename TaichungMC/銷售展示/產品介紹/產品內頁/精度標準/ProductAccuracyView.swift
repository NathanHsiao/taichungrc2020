//
//  ProductAccuracyView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductAccuracyView: UIView {
    let vd = UIView()
    let webview = UIWebView()
    let expandPdfButton = UIButton(type: .custom)
    let goToPageButton = UIButton(type: .custom)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        vd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vd.addShadowColor(opacity: 0.15)
        vd.layer.cornerRadius = 15.calcvaluex()
        addSubview(vd)
        vd.fillSuperview(padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()))
        
        vd.addSubview(webview)
        webview.fillSuperview(padding: .init(top: 4.calcvaluey(), left: 4.calcvaluey(), bottom: 4.calcvaluey(), right: 4.calcvaluey()))
        webview.delegate = self
        
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        

        addSubview(expandPdfButton)
        expandPdfButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 504.calcvaluey(), left: 0, bottom: 0, right: 44.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
        
        goToPageButton.setImage(#imageLiteral(resourceName: "ic_fab_jump"), for: .normal)
        goToPageButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        addSubview(goToPageButton)
        goToPageButton.anchor(top: expandPdfButton.bottomAnchor, leading: expandPdfButton.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        goToPageButton.layer.cornerRadius = 38.calcvaluex()/2
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension ProductAccuracyView:UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        removeShadow(webView: webview)
    }
    func removeShadow(webView: UIWebView) {
        for subview:UIView in webView.scrollView.subviews {
            subview.layer.shadowOpacity = 0
            for subsubview in subview.subviews {
                subsubview.layer.shadowOpacity = 0
            }
        }
    }
}
