//
//  Show360Controller.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/22.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import SDWebImage
class Show360Controller: UIViewController {
    let vd = UIView()
    var vdanchor:AnchoredConstraints!
    let xbutton = UIButton(type: .custom)
    let productImgae = UIImageView()
    let nLabel = UILabel()
    var downloadArray = [String]()
    var imageArray = [UIImage?]()
    var featureProduct : FeatureProduct? {
        didSet{
            nLabel.text = "\(featureProduct?.title ?? "")(\(featureProduct?.product?.title ?? ""))"
        }
    }
    var assets: Assets? {
        didSet{

            
            for i in assets?.three360 ?? [] {
                for j in i.files?.zhTW ?? []{
                    switch j.path {
                    case .arrayString(let array) :
                        if let m = array {
                            for f in m {
                                downloadArray.append(f ?? "")
                            }
                            
                        }

                    case .string(let st):
                        if let f = st {
                            downloadArray.append(f)
                        }
                    default:
                        ()
                        
                    }

                }
            }
            downloadImage()

        }
    }
    func downloadImage(){
        
        if downloadArray.count > 0{
            let manager = SDWebImageManager.shared
            manager.loadImage(with: URL(string: "\(AppURL().baseURL)uploads/\(downloadArray.first ?? "")"), options: .continueInBackground, progress: nil) { (img, _, _, _, complete, _) in
                if complete {
                    self.imageArray.append(img)
                    self.downloadArray.remove(at: 0)
                    self.downloadImage()
                }
            }
        }
        else if downloadArray.count == 0{
            if self.imageArray.count > 0{
            productImgae.image = self.imageArray[0]
            }
            General().stopActivity(vd: self.view)
        }
    }
    var currentIndex: Int = 0 {
        didSet{
            if imageArray.count > 0{
            productImgae.image = imageArray[currentIndex]
            }
        }
    }

    var lastPoint: CGPoint = CGPoint.zero
    let sensitivity: CGFloat = 50
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        vd.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        vd.layer.cornerRadius = 15.calcvaluex()
        
        view.addSubview(vd)
        vdanchor = vd.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 649.calcvaluey()))
        
        xbutton.setImage(#imageLiteral(resourceName: "ic_multiplication"), for: .normal)
        xbutton.contentVerticalAlignment = .fill
        xbutton.contentHorizontalAlignment = .fill
        vd.addSubview(xbutton)
        xbutton.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing: vd.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        xbutton.addTarget(self, action: #selector(closeView), for: .touchUpInside)
        
        
        productImgae.contentMode = .scaleAspectFit
        
        vd.addSubview(productImgae)
        productImgae.anchor(top: vd.topAnchor, leading: nil, bottom: nil, trailing:    nil,padding: .init(top: 56.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 384.75.calcvaluex(), height: 417.84.calcvaluey()))
        productImgae.centerXInSuperview()

        nLabel.font = UIFont(name: "Roboto-Bold", size: 34.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(nLabel)
        nLabel.anchor(top: nil, leading: nil, bottom: vd.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 65.calcvaluey(), right: 0))
        nLabel.centerXInSuperview()
        self.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(goPanning)))
    }
    @objc func goPanning(sender:UIPanGestureRecognizer){
        let currentPoint = sender.location(in: productImgae)
        if sender.state == .began{
           lastPoint = currentPoint
        }else if sender.state == .changed {
           let velocity = sender.velocity(in: productImgae)
           if velocity.x > 0 && currentPoint.x > lastPoint.x + sensitivity{
              currentIndex = currentIndex > 0 ? currentIndex - 1 : imageArray.count - 1
              lastPoint = currentPoint
           }else{
              if currentPoint.x < lastPoint.x - sensitivity {
                 currentIndex = currentIndex < imageArray.count - 1 ? currentIndex + 1 : 0
                 lastPoint = currentPoint
              }
           }
        }
    }
    @objc func closeView(){
        vdanchor.top?.constant = 0
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        vdanchor.top?.constant = -649.calcvaluey()
        
//        UIView.animate(withDuration: 0.4) {
//            self.view.layoutIfNeeded()
//        }
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        } completion: { (_) in
            if self.downloadArray.count != 0{
            General().showActivity(vd: self.view)
            }
        }

    }
}
