//
//  ProductFeatureController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/21.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
protocol ExpandProductDelegate {
    func expandImage(index:Int,mapIndex:Int)
    func go360(index:Int,mapIndex:Int)
    func didScrolling()
    func endScrolling()
    func setNewProduct(series:Series)
}
struct FeatureProduct {
    var title:String
    var product:Product?
}
extension String{
    func convertTofileUrl() -> URL?{
        if let url = URL(string: "\(AppURL().baseURL)uploads/\(self.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? "")"){
            return url
        }
        return nil
    }
}
class ProductFeatureController:UIViewController {

    let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let pdfView = UIView()
    let webview = UIWebView()
    let expandPdfButton = UIButton(type: .custom)
    let goToPageButton = UIButton(type: .custom)
    let pageControl = UIPageControl()
    var delegate:ExpandProductDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        

        view.backgroundColor = #colorLiteral(red: 0.9959673285, green: 0.9961408973, blue: 0.9959691167, alpha: 1)

        
        view.addSubview(collectionview)
        collectionview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,size: .init(width: 461.calcvaluex(), height: 0))
        collectionview.backgroundColor = .clear
        collectionview.delegate = self
        collectionview.isScrollEnabled = false
        collectionview.dataSource = self
        collectionview.isPagingEnabled = true
        collectionview.register(FeatureCell.self, forCellWithReuseIdentifier: "cell")
        (collectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        
        pdfView.backgroundColor = .white
        pdfView.addshadowColor(color: .white)
        pdfView.layer.cornerRadius = 15.calcvaluex()
        
        view.addSubview(pdfView)
        pdfView.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 537.calcvaluex(), height: 656.calcvaluey()))
        
        
        pdfView.addSubview(webview)
        
        webview.backgroundColor = .clear
        webview.delegate = self
        webview.scalesPageToFit = true
        webview.fillSuperview(padding: .init(top: 24.calcvaluey(), left: 0, bottom: 0, right: 0))
        webview.layer.shadowOpacity = 0
//        if let pdf = Bundle.main.url(forResource: "samplePDF", withExtension: "pdf", subdirectory: nil, localization: nil)  {
//           let req = NSURLRequest(url: pdf)
//           webview.loadRequest(req as URLRequest)
//         }
        webview.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        expandPdfButton.addTarget(self, action: #selector(expandPDF), for: .touchUpInside)

        view.addSubview(expandPdfButton)
        expandPdfButton.anchor(top: pdfView.topAnchor, leading: nil, bottom: nil, trailing: pdfView.trailingAnchor,padding: .init(top: 478.calcvaluey(), left: 0, bottom: 0, right: 18.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
        
        goToPageButton.setImage(#imageLiteral(resourceName: "ic_fab_jump"), for: .normal)
        goToPageButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        view.addSubview(goToPageButton)
        goToPageButton.anchor(top: expandPdfButton.bottomAnchor, leading: expandPdfButton.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 18.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        goToPageButton.layer.cornerRadius = 38.calcvaluex()/2
        
        goToPageButton.isHidden = true
       
        
    }
    var topSeries : Series?
    var products = [FeatureProduct]()
    var selectedIndex : Int?
    var allSeries = [Series]()
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let tab = self.tabBarController as? ProductInnerTabBarController {
            if let kk = tab.parent as? ProductInnerInfoController {
                kk.stackview.isHidden = false
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let tab = self.tabBarController as? ProductInnerTabBarController {
            if let kk = tab.parent as? ProductInnerInfoController {
                kk.stackview.isHidden = true
            }
            topSeries = tab.selectedSeries
            products = [FeatureProduct]()
            print(topSeries?.title)
            for i in tab.allSeries {
                if i.title == topSeries?.title {
                    //if i.products?.count ?? 0 > 0{
                    selectedIndex = products.count
                   // }
                }
                if (i.products ?? []).isEmpty {
                    products.append(FeatureProduct(title: i.title ?? "", product: nil))
                }
                
                for j in i.products ?? [] {
                    products.append(FeatureProduct(title: i.title ?? "", product: j))
                }
            }
            allSeries = tab.allSeries

            self.collectionview.reloadData()
            self.pageControl.numberOfPages = self.products.count
            if let sr = selectedIndex {
                self.pageControl.currentPage = sr
            }
            self.collectionview.isHidden = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                if let sr = self.selectedIndex {
  
                    self.collectionview.scrollToItem(at: IndexPath(item: sr, section: 0), at: .right, animated: false)
                    self.collectionview.isHidden = false
                    
                }

            }

        }
        
    }
    @objc func expandPDF(){
        print(fileString)
        let con = ExpandPDFController(text: fileString)
        con.modalPresentationStyle = .overFullScreen
        self.tabBarController?.present(con, animated: false, completion: nil)
    }
    func removeShadow(webView: UIWebView) {
        for subview:UIView in webView.scrollView.subviews {
            subview.layer.shadowOpacity = 0
            for subsubview in subview.subviews {
                subsubview.layer.shadowOpacity = 0
            }
        }
    }
    var fileString = ""
}
extension ProductFeatureController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIWebViewDelegate,ExpandProductDelegate,HotSpotDelegate {
    func sendFile(remote: String) {
        webview.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        guard let url = URL(string: remote) else {return}
        
        if url.lastPathComponent == ".pdf" {
            goToPageButton.isHidden = false
        }
        else{
            goToPageButton.isHidden = true
        }
        fileString = url.absoluteString
        let request = URLRequest(url: url)
        General().showActivity(vd: self.webview)
        webview.loadRequest(request)
    }
    
    func setNewProduct(series: Series) {
        if let tab = self.tabBarController as? ProductInnerTabBarController {
            if tab.selectedSeries?.id != series.id {
            tab.selectedSeries = series
                tab.selectedProduct = nil
                tab.selectedSpec1 = nil
                tab.selectedSpec2 = nil
            }
        }
        topSeries = series
        print(topSeries?.title)
        products = [FeatureProduct]()
        for i in allSeries {
            if i.title == topSeries?.title {
                //if i.products?.count ?? 0 > 0{
                selectedIndex = products.count
                //}
            }
            if (i.products ?? []).isEmpty {
                products.append(FeatureProduct(title: i.title ?? "", product: nil))
            }
            for j in i.products ?? [] {
                products.append(FeatureProduct(title: i.title ?? "", product: j))
            }
        }
        
        self.collectionview.reloadData()
        self.pageControl.numberOfPages = self.products.count
        if let sr = selectedIndex {
            self.pageControl.currentPage = sr
        }
       // self.collectionview.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            if let sr = self.selectedIndex {

                self.collectionview.scrollToItem(at: IndexPath(item: sr, section: 0), at: .right, animated: false)
                self.collectionview.isHidden = false
                
            }

        }
        
    }
    
    func sendFile(file: Files?) {
        print(file)

        webview.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        if let mt = file {
            if let g = mt.zhTW {
                if g.count > 0 {
                    switch g[0].path {
                    case .string(let ft):
                        if let a = ft {
                            //fileString = a
                            guard let url = a.convertTofileUrl() else {return}
                            fileString = url.absoluteString
                            let request = URLRequest(url: url)
                            General().showActivity(vd: webview)
                            webview.loadRequest(request)
                        }
                    default:
                        ()
                    }
                }
            }
        }
        else{
            webview.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        }
    }
    
    func didScrolling() {
        collectionview.isScrollEnabled = false
    }
    
    func endScrolling() {
        collectionview.isScrollEnabled = true
    }
    
    func go360(index:Int,mapIndex:Int) {
        let con = Show360Controller()
        con.featureProduct = self.products[index]
        con.assets = self.products[index].product?.assets
        
        con.modalPresentationStyle = .overFullScreen
        self.tabBarController?.present(con, animated: false, completion: nil)
    }
    func expandImage(index:Int,mapIndex:Int) {
        let con = ExpandProductController()

        con.imgv.img.sd_setImage(with: (self.products[index].product?.maps?[mapIndex].image ?? "").convertTofileUrl(), completed: nil)
        
        if let hotspot =
            self.products[index].product?.maps?[mapIndex].hotspots {
            
            con.hotspot = hotspot
        }
        
        con.modalPresentationStyle = .overFullScreen
        self.tabBarController?.present(con, animated: false, completion: nil)
    }
//    func expandImage(index:Int,mapIndex:Int) {
//        let con = ExpandProductController()
//        con.imgv.sd_setImage(with: URL(string: "\(AppURL().baseURL)uploads/\(self.products[index].product.maps?[mapIndex].image ?? "")"), completed: nil)
//        con.modalPresentationStyle = .overFullScreen
//        self.tabBarController?.present(con, animated: false, completion: nil)
//    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        General().stopActivity(vd: self.webview)
        let a = webView.scrollView.subviews
        for var view in a{
            if view.isKind(of: NSClassFromString("UIWebPDFView")!) {
                print("YES")
                let b = view.subviews
                print(view.bounds.size.height)
                print(b.count)
//                for var iview in b{
//                    if iview.isKind(of: NSClassFromString("UIPDFPageView")!) {
//                        print(iview.bounds.size.height)
//                    }
//                }
            }
        }
        removeShadow(webView: webView)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
//    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
//
//        let index = Int(self.collectionview.contentOffset.x / self.collectionview.frame.width)
//        //print(333,index)
//        pageControl.currentPage = index
//
//
//        //self.collectionview.reloadItems(at: [IndexPath(item: pageControl.currentPage, section: 0)])
//    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FeatureCell
        cell.productImgv.delegate = self
        cell.tag = indexPath.item
        cell.delegate = self
        cell.productLabel.text = "\(products[indexPath.item].title)"
        cell.totalProduct = allSeries
        if products[indexPath.item].product?.maps == nil{
            cell.product = nil
        }
        else{
        cell.product = products[indexPath.item].product
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return
            .init(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print(123,Int(scrollView.contentOffset.x)/Int(scrollView.frame.width))
        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        pageControl.currentPage = index
        
        if let cell = collectionview.cellForItem(at: IndexPath(item: index, section: 0)) as? FeatureCell {
            if cell.productImgv.hotspots.count > 0{
                cell.productImgv.delegate?.sendFile(file: cell.productImgv.hotspots[cell.productImgv.selectedIndex].files)
            }
            else{
                cell.productImgv.delegate?.sendFile(file: nil)
            }

        }
    }
//
//    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
//
//        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//    }
}
class selectTableCell : UITableViewCell {
    let title = UILabel()
    let seperator = UIView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .white
        self.selectionStyle = .none
        contentView.addSubview(title)
        title.textColor = .black
        title.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        title.centerInSuperview()
        
        contentView.addSubview(seperator)
        seperator.backgroundColor = #colorLiteral(red: 0.864822649, green: 0.864822649, blue: 0.864822649, alpha: 1)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()),size: .init(width: 0, height: 1.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class FeatureCell:UICollectionViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: selectContentTableView.self))!{
            return false
        }
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if totalProduct?[indexPath.row].is_disable != 1{
        popContent()
        if let pr = totalProduct?[indexPath.row] {
            //delegate?.setNewProduct(product: pr)
            delegate?.setNewProduct(series: pr)
 
        }
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalProduct?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = selectTableCell(style: .default, reuseIdentifier: "selection")
        cell.title.text = totalProduct?[indexPath.row].title
        if indexPath.row == (totalProduct?.count ?? 0) - 1{
            cell.seperator.isHidden = true
        }
        else{
            cell.seperator.isHidden = false
        }
        if totalProduct?[indexPath.row].is_disable == 1{
            cell.title.textColor = .lightGray
        }
        else{
            cell.title.textColor = .black
        }
        return cell

    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 139.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product?.maps?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 198.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.calcvaluex()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! mapCollectionViewCell
        if indexPath.item == selectedIndex {
            cell.backgroundColor = #colorLiteral(red: 0.7637601495, green: 0.7638711333, blue: 0.7637358308, alpha: 1)
        }
        else{
            cell.backgroundColor = #colorLiteral(red: 0.9018597007, green: 0.902017653, blue: 0.9018613696, alpha: 1)
        }
        cell.image.sd_setImage(with: (product?.maps?[indexPath.row].image ?? "").convertTofileUrl(), completed: nil)
        
        return cell
    }
    var product : Product? {
        didSet{
            
            if let pr = product?.assets?.three360 {
                let kt = pr.filter { (file) -> Bool in
                    return file.files != nil || file.remote != nil
                }
                if kt.count == 0 {
                    featureButton.setUnable()
                }
                else{
                    featureButton.setEnable()
                }
            }
            else{
                featureButton.setUnable()
            }
//            productLabel.text = product?.title
            if self.product != nil{
//                if self.product?.assets?.three360[] != nil{
//
//                }
//                else{
//
//                }
                productImgv.img.sd_setImage(with: (product?.maps?[selectedIndex].image ?? "").convertTofileUrl()) { (image, _, _, _) in
                if let _ = image {
                    self.productImgv.hotspots = self.product?.maps?[self.selectedIndex].hotspots ?? []
                    self.mapCollectionView.reloadData()
                }
            }
                pageControl.numberOfPages = product?.maps?.count ?? 0
                expandPdfButton.isHidden = false
            }
            else{
                expandPdfButton.isHidden = true
                //featureButton.isHidden = true
                productImgv.delegate?.sendFile(file: nil)
                productImgv.removeDotAndImage()
                self.mapCollectionView.reloadData()
            }

            
        }
    }
    var selectedIndex: Int = 0
    let labelSeperator = UIView()
    let productLabel = UILabel()
    let productImgv = hotSpotView()
    let featureButton = Feature360Button(type: .custom)
        let expandPdfButton = UIButton(type: .custom)
    var mapCollectionView : UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        let map = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        map.backgroundColor = .clear
        return map
    }()
    var pageControl = UIPageControl()
    var delegate:ExpandProductDelegate?
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        collectionView.reloadData()
        productImgv.img.sd_setImage(with: (product?.maps?[selectedIndex].image ?? "").convertTofileUrl(), completed: nil)
        
        productImgv.selectedIndex = 0
        productImgv.hotspots = product?.maps?[selectedIndex].hotspots ?? []
        pageControl.currentPage = indexPath.item
    }
    var menuButton : UIButton = {
        let menuButton = UIButton(type: .custom)
        menuButton.setImage(#imageLiteral(resourceName: "ic_arrow_down"), for: .normal)
        menuButton.contentVerticalAlignment = .fill
        menuButton.contentHorizontalAlignment = .fill
        
        return menuButton
    }()
    let selectContent = UIView()
    var selectContentConstraint : NSLayoutConstraint!
    var selectContentTableView = UITableView(frame: .zero, style: .grouped)
    var selectHeight : CGFloat = 0
    var totalProduct: [Series]? {
        didSet{
            
            selectHeight = CGFloat(totalProduct?.count ?? 0) * 84.calcvaluey()
            selectContentTableView.reloadData()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
                labelSeperator.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
                labelSeperator.layer.cornerRadius = 6.calcvaluex()/2
                addSubview(labelSeperator)
                labelSeperator.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 32.5.calcvaluey(), left: 18.calcvaluex(), bottom: 0, right: 0),size: .init(width: 6.calcvaluex(), height: 46.5.calcvaluey()))
        addSubview(featureButton)
        featureButton.addTarget(self, action: #selector(go360), for: .touchUpInside)
        featureButton.addshadowColor(color: .white)
        featureButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 37.calcvaluey(), left: 0, bottom: 0, right: 18.calcvaluex()),size: .init(width: 122.calcvaluex(), height: 40.calcvaluey()))
                productLabel.text = "NP16(2軸)"
                productLabel.font = UIFont(name: "Roboto-Bold", size: 42.calcvaluex())
                productLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(menuButton)
        menuButton.anchor(top: nil, leading: nil, bottom: nil, trailing: featureButton.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 40.calcvaluex(), height: 40.calcvaluex()))
        menuButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
                addSubview(productLabel)
        productLabel.numberOfLines = 2
        productLabel.anchor(top: topAnchor, leading: labelSeperator.trailingAnchor, bottom: nil, trailing: featureButton.leadingAnchor,padding: .init(top: 26.calcvaluey(), left: 18.calcvaluex(), bottom: 0, right: 4.calcvaluex()))
        menuButton.centerYAnchor.constraint(equalTo: productLabel.centerYAnchor).isActive = true
                productImgv.contentMode = .scaleAspectFill
        
                addSubview(productImgv)
        
        
        productImgv.anchor(top: productLabel.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 45.calcvaluey(), left: 24.calcvaluex(), bottom: 0, right: 24.calcvaluex()),size: .init(width: 0, height: 303.calcvaluey()))
        

        
        expandPdfButton.setImage(#imageLiteral(resourceName: "ic_fab_full"), for: .normal)
        expandPdfButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        

        addSubview(expandPdfButton)
        expandPdfButton.anchor(top: nil, leading: nil, bottom: productImgv.bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 23.calcvaluex()),size: .init(width: 38.calcvaluex(), height: 38.calcvaluex()))
        expandPdfButton.layer.cornerRadius = 38.calcvaluex()/2
        expandPdfButton.addTarget(self, action: #selector(expandData), for: .touchUpInside)
        
        addSubview(mapCollectionView)
        mapCollectionView.anchor(top: productImgv.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 121.calcvaluey()))
        mapCollectionView.contentInset = .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0)
        mapCollectionView.delegate = self
        mapCollectionView.dataSource = self
        mapCollectionView.bounces = true
        mapCollectionView.register(mapCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        mapCollectionView.showsHorizontalScrollIndicator = false
        
        addSubview(pageControl)
        //pageControl.numberOfPages = 2
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        pageControl.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 86.calcvaluey(), right: 0),size: .init(width: 0, height: 8.calcvaluey()))
        
        selectContent.backgroundColor = .clear
 
        addSubview(selectContent)
        selectContent.addShadowColor(opacity: 0.1)
 
        selectContent.anchor(top: productLabel.bottomAnchor, leading: productLabel.leadingAnchor, bottom: nil, trailing: menuButton.trailingAnchor)
        
        selectContentConstraint = selectContent.heightAnchor.constraint(equalToConstant: 0)
        selectContentConstraint.isActive = true
        
        selectContent.addSubview(selectContentTableView)
        selectContentTableView.layer.cornerRadius = 5.calcvaluex()
        selectContentTableView.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        selectContentTableView.fillSuperview()
        selectContentTableView.delegate = self
        selectContentTableView.dataSource = self
        
        selectContentTableView.separatorStyle = .none
        let tap = UITapGestureRecognizer(target: self, action: #selector(popContent))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        addGestureRecognizer(tap)
    }
    @objc func popContent(){
        selectContentConstraint.constant = 0
        
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded()
        }
    }
    func showContent(){
        selectContentConstraint.constant = selectHeight
        
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded()
        }
    }
    @objc func showMenu(){
        showContent()
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        delegate?.didScrolling()
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.endScrolling()
    }
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        delegate?.endScrolling()
//    }
    @objc func go360(){
        delegate?.go360(index:self.tag,mapIndex:self.selectedIndex)
    }
    @objc func expandData(){
        delegate?.expandImage(index:self.tag,mapIndex:self.selectedIndex)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class dotView : UIView {
    let label = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 2.calcvaluex()
        addshadowColor()
        
        backgroundColor = .white
        label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        label.textAlignment = .center
        addSubview(label)
        label.centerInSuperview()
        
    }
    func isSelected(){
        backgroundColor = #colorLiteral(red: 0.9427522421, green: 0.5120122433, blue: 0.02646821924, alpha: 1)
        label.textColor = .white
    }
    func unSelected(){
        backgroundColor = .white
        label.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol HotSpotDelegate {
    func sendFile(file:Files?)
    func sendFile(remote:String)
    
}
class hotSpotView : UIView {
    let img = UIImageView()
    var delegate:HotSpotDelegate?
    var scaleRatio : CGFloat = 1
    
    var moveRatio : CGFloat = 1.5
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(img)
        img.fillSuperview()
        img.contentMode = .scaleAspectFit
    }
    var dotArray = [dotView]()
    
    @objc func didSelect(sender:UITapGestureRecognizer){
        
        if let vd = sender.view as? dotView {
            print(vd.tag)
            //selectedIndex = vd.tag
            for i in dotArray {
                if vd.label.text == i.label.text {
                    i.isSelected()
                    if let remote = hotspots[vd.tag].remote {
                        
                        self.delegate?.sendFile(remote: remote)
                    }
                    else{
                    self.delegate?.sendFile(file: hotspots[vd.tag].files)
                    }
                    
                }
                else{
                    i.unSelected()
                }
            }
        }
    }
    func removeDotAndImage(){
        self.img.image = nil
        for i in dotArray{
            i.removeFromSuperview()
        }
        dotArray.removeAll()
    }
    var selectedIndex = 0
    var hotspots = [Hotspot](){
        didSet{
            self.layoutIfNeeded()
            print(img.image?.size.width,img.image?.size.height)
            let rect = AVMakeRect(aspectRatio: img.image?.size ?? .zero, insideRect: img.bounds)
            print(4445,rect)
            //print(445,img.contentClippingRect.width,original_image.size.width)
            let widthratio = (rect.width)/(img.image?.size.width ?? 0)
            
            let heightratio = (rect.height)/(img.image?.size.height ?? 0)
            
            for i in dotArray{
                i.removeFromSuperview()
            }
            dotArray.removeAll()

            for (index,i) in hotspots.enumerated() {

                let dot = dotView()
                dot.label.text = i.coordinate?.id
                dot.label.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex() * scaleRatio)
                dot.tag = index
                if index == selectedIndex{
                    dot.isSelected()
                    if let remote = hotspots[0].remote {
                        self.delegate?.sendFile(remote: remote)
                    }
                    else{
                    self.delegate?.sendFile(file: hotspots[0].files)
                    }
                }
                else{
                    dot.unSelected()
                }
                addSubview(dot)
                let x = CGFloat(NumberFormatter().number(from: i.coordinate?.left ?? "0") ?? 0)
                
                let y = CGFloat(NumberFormatter().number(from: i.coordinate?.top ?? "0") ?? 0)
                let width = 40.calcvaluex()
                let height = 40.calcvaluex()
                dot.frame = .init(x: rect.origin.x + (x * widthratio)  , y: rect.origin.y + (y * heightratio) , width: width * scaleRatio , height: height * scaleRatio)
                dot.layer.cornerRadius = (height * scaleRatio)/2
                dot.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didSelect)))
                dotArray.append(dot)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class mapCollectionViewCell: UICollectionViewCell {
    let image = UIImageView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9018597007, green: 0.902017653, blue: 0.9018613696, alpha: 1)
        
        addSubview(image)
        image.fillSuperview(padding: .init(top: 8.calcvaluex(), left: 8.calcvaluex(), bottom: 8.calcvaluex(), right: 8.calcvaluex()))
        image.contentMode = .scaleAspectFit
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class Feature360Button : UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitle("360°展示", for: .normal)
        setTitleColor(#colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1), for: .normal)
        titleLabel?.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        
        backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        
        layer.cornerRadius = 20.calcvaluey()
        layer.borderColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        layer.borderWidth = 1.calcvaluex()
    }
    func setEnable(){
        setTitleColor(MajorColor().mainColor, for: .normal)
        layer.borderColor = MajorColor().mainColor.cgColor
        self.isUserInteractionEnabled = true
    }
    func setUnable(){
        setTitleColor(#colorLiteral(red: 0.8160102403, green: 0.8160102403, blue: 0.8160102403, alpha: 1), for: .normal)
        layer.borderColor = #colorLiteral(red: 0.8160102403, green: 0.8160102403, blue: 0.8160102403, alpha: 1)
        self.isUserInteractionEnabled = false
     
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
