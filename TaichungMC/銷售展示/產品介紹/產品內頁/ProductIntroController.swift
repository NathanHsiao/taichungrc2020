//
//  ProductIntroController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/14.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
struct Tags{
    static let machine = "machine"
    static let background = "background"
}
class ProductIntroController : SampleController {
    let tableview = UITableView(frame: .zero, style: .plain)
    var seriesArray = [Series]()
    let activityController = UIActivityIndicatorView(style: .whiteLarge)
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        topview.isHidden = true
        
        
        view.addSubview(tableview)
        tableview.anchor(top: view.topAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 669.calcvaluex(), height: 0))
        tableview.contentInset = .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0)
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        //tableview.clipsToBounds = true
        tableview.dataSource = self
        tableview.delegate = self
        
        view.addSubview(activityController)
        activityController.backgroundColor = #colorLiteral(red: 0.8848801295, green: 0.8848801295, blue: 0.8848801295, alpha: 1)
        activityController.constrainWidth(constant: 60.calcvaluex())
        activityController.constrainHeight(constant: 60.calcvaluey())
        activityController.centerYInSuperview()
        activityController.centerXAnchor.constraint(equalTo: tableview.centerXAnchor).isActive = true
        activityController.hidesWhenStopped = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print(12334)
        fetchApi()
    }

    func fetchApi(){
        activityController.startAnimating()
        self.tableview.isHidden = true

        NetworkCall.shared.getCall(parameter: "api-or/v1/categories", decoderType: topData.self) { (json) in
            if let json = json {
            self.seriesArray = (json.data ?? []).filter({ (sr) -> Bool in
                return sr.status == 1
            }).sorted(by: { (sr1, sr2) -> Bool in
                return (sr1.order ?? 0) < (sr2.order ?? 0)
            })
            for i in self.seriesArray {
                if UserDefaults.standard.getUpdateToken(id: i.id) == ""{
                    UserDefaults.standard.saveUpdateToken(token: i.update_token ?? "", id: i.id)
                }
            }
            DispatchQueue.main.async {
                
                self.tableview.reloadData()
                self.tableview.isHidden = false
                self.activityController.stopAnimating()
            }
            }
            else{

                                    let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)

                                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))

                                    self.present(alert, animated: true, completion: nil)
                                    return
                
            }
        }
        
    }
    
}
extension ProductIntroController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            if self.seriesArray[indexPath.item].is_disable != 1{
            let vd = RcProductSelectionController()
            vd.selectedIndex = indexPath.item
            vd.TopSeries = self.seriesArray
            
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
            }
        }

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return seriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ProductIntroCell(series: seriesArray[indexPath.row])
        if seriesArray[indexPath.row].is_disable == 1{
            cell.alphabackground.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1).withAlphaComponent(0.7)
        }
        else{
            cell.alphabackground.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1).withAlphaComponent(0)
        }


        return cell
//        let cell = ProductIntroCell(data: child[indexPath.row], included: self.included)
//        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.calcvaluey()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }

}

class ProductIntroCell:UITableViewCell{
    let container = UIView()
    let img = UIImageView()
    let topLabel = UILabel()
    let detailLabel = UILabel()
    let nextarrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next"))
    var alphabackground = UIView()

    init(series:Series) {
        super.init(style: .default, reuseIdentifier: "cell")
        selectionStyle = .none
        backgroundColor = .clear
        container.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        container.layer.cornerRadius = 15.calcvaluex()
        container.layer.shadowColor = UIColor.black.cgColor
        container.layer.shadowOffset = .init(width: 8.calcvaluex(), height: 0)
        container.layer.shadowRadius = 3.calcvaluex()
        container.layer.shadowOpacity = 0.1
        container.layer.shouldRasterize = true
        container.layer.rasterizationScale = UIScreen.main.scale
        contentView.addSubview(container)
        container.anchor(top: contentView.topAnchor, leading: nil, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 593.calcvaluex(), height: 108.calcvaluey()))
        
        img.contentMode = .scaleAspectFit
        
        if let machine = series.assets{

            
            img.sd_setImage(with: URL(string: "\(machine.getMachinePath())"), completed: nil)
                
            
        }
        

        container.addSubview(img)
        img.anchor(top: container.topAnchor, leading: nil, bottom: container.bottomAnchor, trailing: nil,size: .init(width: 119.calcvaluex(), height: 0))
        img.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
        img.centerXAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        
        container.addSubview(nextarrow)
        nextarrow.contentMode = .scaleAspectFit
        nextarrow.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        nextarrow.centerYInSuperview()
        container.addSubview(topLabel)
        topLabel.text = "\(series.title ?? "")"
        topLabel.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        topLabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nextarrow.leadingAnchor,padding: .init(top: 25.calcvaluey(), left: 101.calcvaluex(), bottom: 0, right: 16.calcvaluex()))
        
        container.addSubview(detailLabel)
        detailLabel.text = "\(series.title ?? "")"
        detailLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        detailLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        detailLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: topLabel.trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 0))
    
        container.addSubview(alphabackground)
        //alphabackground.backgroundColor = UIColor.white.withAlphaComponent(0)
        alphabackground.layer.cornerRadius = 15.calcvaluex()
        alphabackground.fillSuperview()
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
