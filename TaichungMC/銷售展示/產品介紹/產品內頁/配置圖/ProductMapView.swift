//
//  ProductMapController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ProductMapView: UIView {
    let vd = UIView()
    let imgv = UIImageView(image: #imageLiteral(resourceName: "image_rec_lib_banner"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        vd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vd.addShadowColor(opacity: 0.15)
        vd.layer.cornerRadius = 15.calcvaluex()
        addSubview(vd)
        vd.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 638.calcvaluex(), height: 572.calcvaluey()))
        
        imgv.contentMode = .scaleAspectFill
        imgv.clipsToBounds = true
        
        vd.addSubview(imgv)
        imgv.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor,padding: .init(top: 78.calcvaluey(), left: 24.calcvaluex(), bottom: 77.calcvaluey(), right: 24.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
