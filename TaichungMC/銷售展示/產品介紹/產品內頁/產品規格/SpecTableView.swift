//
//  SpecTableView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/23.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class SpecTableView: UIView {
    let vd = UIView()
    let label = UILabel()
    let priceLabel = UILabel()
    let bottomTop = UIView()
    let comDetail = UITableView(frame: .zero, style: .grouped)
    var bottomTopAnchor:AnchoredConstraints!
    var topOptions = [Option]() {
        didSet{
            topOptions = topOptions.sorted { (op1, op2) -> Bool in
                return op1.order ?? 0 < op2.order ?? 0
            }
            comDetail.reloadData()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        vd.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        vd.addShadowColor(opacity: 0.05)
        
        addSubview(vd)
        vd.layer.cornerRadius = 46.calcvaluey()/2
        
        vd.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 250.calcvaluex(), height: 46.calcvaluey()))
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        label.text = "標配價格："
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        vd.addSubview(label)
        label.anchor(top: nil, leading: vd.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
        label.centerYInSuperview()
        
        priceLabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        priceLabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        priceLabel.text = "-"
        vd.addSubview(priceLabel)
        priceLabel.anchor(top: nil, leading: label.trailingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 28.calcvaluey()))
        priceLabel.centerYInSuperview()
        
               bottomTop.backgroundColor = .clear

               bottomTop.addshadowColor(color: .white)
        

            addSubview(bottomTop)
        bottomTopAnchor = bottomTop.anchor(top: vd.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()))
               comDetail.layer.cornerRadius = 15.calcvaluey()
               comDetail.layer.masksToBounds = true
        comDetail.backgroundColor = .white
               bottomTop.addSubview(comDetail)
               comDetail.separatorStyle = .none
               comDetail.fillSuperview()
        comDetail.contentInset = .init(top: 0, left: 0, bottom: 70.calcvaluey(), right: 0)
        comDetail.delegate = self
        comDetail.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension SpecTableView:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return topOptions.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = SpecCell(style: .default, reuseIdentifier: "cell")
        cell.headerLabel.text = topOptions[indexPath.section].children?[indexPath.row].title
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = CompareHeader()
        vd.headerLabel.text = topOptions[section].title
        if topOptions[section].children?.count ?? 0 > 0 {
        vd.valueLabel.text = topOptions[section].children?.sorted(by: { (op1, op2) -> Bool in
            return op1.order ?? 0 < op2.order ?? 0
        })[0].title
       }
        return vd
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}
