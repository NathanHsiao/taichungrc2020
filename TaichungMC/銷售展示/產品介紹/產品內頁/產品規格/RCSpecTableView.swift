//
//  RCSpecTableView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/13/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class RCSpecTableView : UIView {
//    var isNone = false
    var selectedProducts : Product?
    var selectedSpec1: Specification?
    var selectedSpec2: Specification?
    var selectedSeries: Series? {
        didSet{
            if let series = selectedSeries {
                getSpec(series: series)
            }
            
        }
    }
    func getSpec(series:Series){
       // for i in self.typeCollection.productArray {
        if let pr = series.products {
            if pr.count == 0{
                SpecArray.constructValue(field: [])
                return
            }
            for a in pr {
                if let spec = a.specifications {
                    for s in spec{
                       
                                SpecArray.constructValue(field: s.fields)
                                break
                        
                        }
                }

                    
                
            }
        }
        else{
            SpecArray.constructValue(field: [])
            return
        }
    

        //}
    }
    var products = [Product]() {
        didSet{
            
            var shot = true
            if let mt = products.first, let spec = mt.specifications?.first {
                if let field = spec.fields.first(where: { (ft) -> Bool in
                    return ft.code == "injection-model"
                }) {
                    if field.value == nil {
                        shot = false
                    }
                }
                
            }
            
            var spec = [Specification]()
            if shot {
            for i in products {
                for j in i.specifications ?? []{
                    let inj = j.fields.first { (f) -> Bool in
                        f.code == "injection-model"
                    }
                    let scr = j.fields.first { (f) -> Bool in
                        f.code == "screw-diameter"
                    }
                    let isNone = selectedSeries?.isNone ?? false
                    if let sr = selectedProducts {
                        if isNone == true ? sr.title?.contains("&") ?? false : sr.shooter_code?.contains("&") ?? false{
                            if let tt = isNone == true ? sr.title?.split(separator: "&") : sr.shooter_code?.split(separator: "&") {
                                if tt.count == 2{
                                    
                                    if String(tt[0]) != inj?.value && String(tt[1]) != inj?.value{
                                        print("YES",inj?.value)
                                        continue
                                    }
                                   
                                    if let st = selectedSpec1 {
                                        if String(tt[0]) == inj?.value && String(tt[1]) == inj?.value{
                                            if scr?.value != st.getInjectionValue() {
                                                if let ss = selectedSpec2 {
                                                    if ss.getInjectionValue() != scr?.value {
                                                        continue
                                                    }
                                                }
                                                else{
                                                    continue
                                                }
                                                
                                            }
                              
                                        }
                                        else if String(tt[0]) == inj?.value {
                                            if scr?.value != st.getInjectionValue() {
                                                continue
                                                
                                            }
                                        }
                                        
                       
                   
                                    }
                                    if let st = selectedSpec2 {
                                        if String(tt[0]) == inj?.value && String(tt[1]) == inj?.value{
                                            if scr?.value != st.getInjectionValue() {
                                                if let ss = selectedSpec1 {
                                                    if ss.getInjectionValue() != scr?.value {
                                                        continue
                                                    }
                                                }
                                                else{
                                                    continue
                                                }
                                                
                                            }
                              
                                        }
                                        else if String(tt[1]) == inj?.value {
                                            if scr?.value != st.getInjectionValue() {
                                                continue
                                                
                                            }
                                        }
                                        
                       
                   
                                    }
                                    
                                }
                                else if tt.count == 1{
                                    if String(tt[0]) != inj?.value{
                                        continue
                                    }
                                }
                                
                            }
                          //  print(99,selectedSpec1?.getInjectionValue(),selectedSpec2?.getInjectionValue())
                   
                            
                        }
                        else{
                        let isNone = selectedSeries?.isNone ?? false
                            if isNone == true ? sr.title != inj?.value : sr.shooter_code != inj?.value {
                            continue
                        }
                        if let st = selectedSpec1 {
                            
                            if scr?.value != st.getInjectionValue() {
                                continue
                            }
                        }
                        }
                    }

                    
                    if !spec.contains(where: { (sp) -> Bool in
                        let specInj = sp.fields.first { (f) -> Bool in
                            f.code == "injection-model"
                        }
                        
                        let specScr = sp.fields.first { (f) -> Bool in
                            f.code == "screw-diameter"
                        }
                        return inj?.value == specInj?.value && scr?.value == specScr?.value
                    }) {
                        spec.append(j)
                    }
                }
            }
            }
            else{
                for i in products {
                    for j in i.specifications ?? []{
                        let inj = j.fields.first { (f) -> Bool in
                            f.code == "injection-model"
                        }
                        let scr = j.fields.first { (f) -> Bool in
                            f.code == "screw-diameter"
                        }
                        let isNone = selectedSeries?.isNone ?? false
                        if let sr = selectedProducts {
                            if isNone == true ? sr.title?.contains("&") ?? false : sr.shooter_code?.contains("&") ?? false{
                                if let tt = isNone == true ? sr.title?.split(separator: "&") : sr.shooter_code?.split(separator: "&"){
                                    if tt.count == 2{
                                        if String(tt[0]) != inj?.value && String(tt[1]) != inj?.value{
                                            continue
                                        }
                                    }
                                    else if tt.count == 1{
                                        if String(tt[0]) != inj?.value{
                                            continue
                                        }
                                    }
                                    
                                }
                                var count = 0
                                if let st = selectedSpec1 {
                                    if scr?.value != st.getInjectionValue() {
                                        count = 1
                                    }
                                    else{
                                        count = 0
                                    }
                                }
                                
                                if let st = selectedSpec2 {
                                    if scr?.value != st.getInjectionValue() {
                                        count = 1
                                    }
                                    else{
                                        count = 0
                                    }
                                }
                                if count == 1{
                                    continue
                                }
                                
                            }
                            else{
                                let isNone = selectedSeries?.isNone ?? false
                                if isNone == true ? sr.title != inj?.value : sr.shooter_code != inj?.value{
                                continue
                            }
                            if let st = selectedSpec1 {
                                
                                if scr?.value != st.getInjectionValue() {
                                    continue
                                }
                            }
                            }
                        }
                        spec.append(j)
                    }
                }
            }
            for i in spec {
                print(888,i.title)
            }
            spec = spec.sorted(by: { (sp1, sp2) -> Bool in
                let inj = sp1.fields.first { (f) -> Bool in
                    f.code == "injection-model"
                }
                let inj2 = sp2.fields.first { (f) -> Bool in
                    f.code == "injection-model"
                }
                if inj?.value == inj2?.value {
                    let specScr = sp1.fields.first { (f) -> Bool in
                        f.code == "screw-diameter"
                    }
                    let specScr2 = sp2.fields.first { (f) -> Bool in
                        f.code == "screw-diameter"
                    }
                    return (specScr?.value ?? "") < (specScr2?.value ?? "")
                }
                
                return (inj?.value ?? "") < (inj2?.value ?? "")
            })
            width = CGFloat(spec.count) * 95.calcvaluex()
            
            var val = [String?]()
            for _ in spec{
                val.append(nil)
            }
 
            for (ind,j) in data.enumerated(){
                for (ind2,_) in j.array.enumerated() {
                    data[ind].array[ind2].value = val
                }
                
            }
            
            for (index,j) in spec.enumerated() {

                for f in j.fields {
                    for (index2,k) in data.enumerated(){
                        
                        if let index3 = k.array.firstIndex(where: { (n1) -> Bool in
                            return n1.decodeId == f.code
                        }) {
                            data[index2].array[index3].value[index] = f.value
                        }
                        else{
                            continue
                        }
                    }

                    }
                
            }
           // print(361,data)
            rightCollection.reloadData()

        }
    }
    let vd = UIView()
    var data = SpecArray.value
    var width : CGFloat?
    let leftTable = UITableView(frame: .zero, style: .plain)
    lazy var rightCollection : UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.scrollDirection = .horizontal
        let rC = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        rC.delegate = self
        rC.dataSource = self
        rC.backgroundColor = .white
        rC.showsVerticalScrollIndicator = false
        rC.register(RightViewCell.self, forCellWithReuseIdentifier: "cell")
        rC.contentInset = .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex())
        return rC
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        vd.backgroundColor = .white
        vd.addshadowColor()
        vd.layer.cornerRadius = 15.calcvaluex()
        addSubview(vd)
        vd.fillSuperview(padding: .init(top: 16.calcvaluey(), left: 0, bottom: 0, right: -16.calcvaluex()))
        
        vd.addSubview(leftTable)
        leftTable.anchor(top: vd.topAnchor, leading: vd.leadingAnchor, bottom: vd.bottomAnchor, trailing: nil,size: .init(width: 246.calcvaluex(), height: 0))
        leftTable.backgroundColor = .white
        leftTable.layer.cornerRadius = 15.calcvaluex()
        leftTable.clipsToBounds = true
        leftTable.separatorStyle = .none

        leftTable.showsVerticalScrollIndicator = false
        leftTable.delegate = self
        leftTable.contentInset = .init(top: 0, left: 0, bottom: 66.calcvaluey(), right: 0)
        leftTable.dataSource = self
        vd.addSubview(rightCollection)
        rightCollection.anchor(top: vd.topAnchor, leading: leftTable.trailingAnchor, bottom: vd.bottomAnchor, trailing: vd.trailingAnchor)
        NotificationCenter.default.addObserver(self, selector: #selector(scrollingDown), name: NSNotification.Name("ScrollingDown2"), object: nil)
        


    }
    
    @objc func scrollingDown(notification:Notification) {
        if let table = notification.object as? UITableView {
            self.leftTable.contentOffset = table.contentOffset
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RCSpecTableView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].array.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = LabelCell(text: data[indexPath.section].array[indexPath.row].title)
        cell.unitLabel.text = data[indexPath.section].array[indexPath.row].unit
        return cell
        

    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return LeftHeaderView(text: data[section].title)
        

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 57.calcvaluey()
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.calcvaluey()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NotificationCenter.default.post(name: Notification.Name("ScrollDown"), object: leftTable, userInfo: nil)

    }
    
    
}
extension RCSpecTableView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! RightViewCell
        cell.tag = indexPath.item
        cell.backgroundColor = indexPath.item % 2 == 0 ? #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1) : .white
        cell.selectedSeries = self.selectedSeries
        cell.spec = self.data
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: width ?? 0, height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
class RightViewCell : UICollectionViewCell,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spec[section].array.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return spec.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0 && indexPath.section == 0{
            let cell = valueCell(ItemArray:spec[indexPath.section].array[indexPath.row].value ,distribution: .fill)
              
            return cell
        }
        else{
            let cell = valueCell(ItemArray: spec[indexPath.section].array[indexPath.row].value )
            return cell
        }

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
        return 57.calcvaluey()
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vd = RightHeaderView(text: self.selectedSeries?.title ?? "")
        vd.backgroundColor = self.tag % 2 == 0 ? #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1) : .white
        return vd
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.calcvaluey()
    }
    let tableview = UITableView(frame: .zero, style: .plain)
    var selectedSeries : Series?
    var spec = [compareSection]() {
        didSet{
            self.tableview.reloadData()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        addSubview(tableview)
        tableview.backgroundColor = .clear
        tableview.dataSource = self
        tableview.delegate = self
        tableview.separatorStyle = .none
        tableview.showsVerticalScrollIndicator = false
        tableview.contentInset = .init(top: 0, left: 0, bottom: 66.calcvaluey(), right: 0)
        tableview.fillSuperview()
        NotificationCenter.default.addObserver(self, selector: #selector(scrollingDown), name: NSNotification.Name("ScrollDown"), object: nil)
    }
    @objc func scrollingDown(notification:Notification) {
        if let table = notification.object as? UITableView {
            self.tableview.contentOffset = table.contentOffset
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NotificationCenter.default.post(name: NSNotification.Name("ScrollingDown2"), object: self.tableview)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class RightHeaderView : UIView {
    let hLabel : UILabel = {
       let hL = UILabel()
        hL.text = "RC-180"
        hL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        hL.textColor = #colorLiteral(red: 0.1351079345, green: 0.09290169924, blue: 0.0822731033, alpha: 1)
        return hL
    }()
    let seperator : UIView = {
       let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        
        return sep
    }()
    init(text:String) {
        super.init(frame: .zero)
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)

        
        
        addSubview(hLabel)
        hLabel.centerInSuperview()
        hLabel.text = text
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 0.5.calcvaluey()))
   }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class LeftHeaderView : UIView {
    let hLabel : UILabel = {
       let hL = UILabel()
        hL.text = "機型"
        hL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        hL.textColor = #colorLiteral(red: 0.1351079345, green: 0.09290169924, blue: 0.0822731033, alpha: 1)
        return hL
    }()
    let seperator : UIView = {
       let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        
        return sep
    }()
    init(text:String) {
        super.init(frame: .zero)
        backgroundColor = .white
        layer.cornerRadius = 15.calcvaluex()
        clipsToBounds = false
        
        addSubview(hLabel)
        hLabel.centerYInSuperview()
        hLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 32.5.calcvaluex(), bottom: 0, right: 0))
        
        addSubview(seperator)
        seperator.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 0.5.calcvaluey()))
   }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class columnView : UIView {
    let seperator1 : UIView = {
       let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        
        return sep
    }()
    let seperator2 : UIView = {
       let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        
        return sep
    }()
    let valLabel : UILabel = {
       let vL = UILabel()
        vL.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        vL.textColor = #colorLiteral(red: 0.1351079345, green: 0.09290169924, blue: 0.0822731033, alpha: 1)
        vL.text = "Y"
        return vL
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(seperator1)
        addSubview(seperator2)
        seperator1.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,size: .init(width: 1.calcvaluex(), height: 0))
        
        seperator2.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
        addSubview(valLabel)
        valLabel.centerInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class valueCell : UITableViewCell {
    
    init(ItemArray:[String?] , distribution: UIStackView.Distribution = .fillEqually) {
        super.init(style: .default, reuseIdentifier: "cell")
        let vd = UIView()
        vd.backgroundColor = #colorLiteral(red: 0.9913000464, green: 0.9481928945, blue: 0.8962452412, alpha: 1)
        selectedBackgroundView = vd
//        selectionStyle = .none
        backgroundColor = .clear
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = distribution
        if distribution == .fillEqually {
        
        for i in ItemArray {
            let vd = columnView()
            vd.valLabel.text = i
            stack.addArrangedSubview(vd)
        }
        }
        else{
            
            var counts: [String: Int] = [:]
            for item in ItemArray {
                counts[item ?? ""] = (counts[item ?? ""] ?? 0) + 1
            }
            for (x,y) in counts.sorted(by: {$0.key < $1.key}){
                let vd = columnView()
                vd.valLabel.text = x
                vd.constrainWidth(constant: CGFloat(y) * 95.calcvaluex())
                stack.addArrangedSubview(vd)
            }
            stack.addArrangedSubview(UIView())
        }
        contentView.addSubview(stack)
        stack.fillSuperview()
        
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class LabelCell : UITableViewCell {
    let hLabel : UILabel = {
       let hL = UILabel()
        hL.text = "螺桿直徑"
        hL.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        hL.textColor = #colorLiteral(red: 0.1351079345, green: 0.09290169924, blue: 0.0822731033, alpha: 1)
        return hL
    }()
    let unitLabel : UILabel = {
       let unit = UILabel()
        unit.text = "mm"
        unit.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        unit.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return unit
    }()
    let seperator : UIView = {
       let sep = UIView()
        sep.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        
        return sep
    }()
    init(text:String) {
        super.init(style: .default, reuseIdentifier: "cell")
        selectionStyle = .none
        contentView.addSubview(hLabel)
        hLabel.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 32.5.calcvaluex(), bottom: 0, right: 0))
        hLabel.centerYInSuperview()
        hLabel.text = text
        contentView.addSubview(unitLabel)
        unitLabel.anchor(top: nil, leading: nil, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()))
        unitLabel.centerYInSuperview()
        
        contentView.addSubview(seperator)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,size: .init(width: 0, height: 1.calcvaluey()))
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
