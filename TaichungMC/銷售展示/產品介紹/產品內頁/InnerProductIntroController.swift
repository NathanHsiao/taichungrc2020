//
//  InnerProductIntroController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/14.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension String {

    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func sizeOfString(usingFont font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
}
class ProductFilterView : UIView {
    let nLabel = UILabel()
    let vArrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_down").withRenderingMode(.alwaysTemplate))
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        layer.cornerRadius = 21.calcvaluey()
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.calcvaluex()
        addSubview(nLabel)
        nLabel.text = "篩選"
        nLabel.textColor = .white
        nLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 0))
        nLabel.centerYInSuperview()
        addSubview(vArrow)
        vArrow.tintColor = .white
        vArrow.contentMode = .scaleAspectFit
        vArrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        vArrow.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class ProductSFilterView : UIView {
    let nLabel = UILabel()
    let vArrow = UIImageView(image: #imageLiteral(resourceName: "ic_multiplication").withRenderingMode(.alwaysTemplate))
    var width: CGFloat?
    init(text:String) {
        super.init(frame: .zero)
        backgroundColor = .clear
        layer.cornerRadius = 21.calcvaluey()
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.calcvaluex()
        addSubview(nLabel)
        nLabel.text = text
        nLabel.textColor = .white
        nLabel.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nLabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 24.calcvaluex(), bottom: 0, right: 46.calcvaluex()))
        nLabel.centerYInSuperview()
        addSubview(vArrow)
        vArrow.tintColor = .white
        vArrow.contentMode = .scaleAspectFit
        vArrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 16.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        vArrow.centerYInSuperview()
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CompareItemView : UIView{
    let imagev = UIImageView(image: #imageLiteral(resourceName: "im_compare_machine_1"))
    let topLabel = UILabel()
    let bottomLabel = UILabel()
    let cancelButton = UIButton(type: .custom)
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        addSubview(imagev)
        imagev.contentMode = .scaleAspectFit
        
        imagev.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 63.calcvaluex(), height: 50.calcvaluey()))
        imagev.centerYInSuperview()
        
        cancelButton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        addSubview(cancelButton)
        cancelButton.contentHorizontalAlignment = .fill
        cancelButton.contentVerticalAlignment = .fill
        cancelButton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 8.calcvaluex(), left: 0, bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        
        topLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        bottomLabel.font = UIFont(name: "Roboto-Regular", size: 14.calcvaluex())
        
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        bottomLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        addSubview(topLabel)
        topLabel.anchor(top: topAnchor, leading: imagev.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 33.calcvaluey(), left: 0, bottom: 0, right: 0))
        topLabel.text = "高效率速軌"
        
        addSubview(bottomLabel)
        bottomLabel.anchor(top: topLabel.bottomAnchor, leading: topLabel.leadingAnchor, bottom: nil, trailing: nil)
        bottomLabel.text = "NP16-2軸"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CompareView : UIView {
    let bottomView = UIView()
    let downArrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down_circle"))
    var showing = true
    let startButton = UIButton(type: .custom)
    let nLabel = UILabel()
    let seperator = UIView()
    let vLabel = UILabel()
    let stackview = UIStackView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        bottomView.backgroundColor = .white
        bottomView.addshadowColor(color: .white)
        addSubview(bottomView)
        bottomView.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 0, height: 102.calcvaluey()))
        
        addSubview(downArrow)
        downArrow.contentMode = .scaleAspectFit
        downArrow.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 48.calcvaluex(), bottom: 0, right: 0),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        downArrow.centerYAnchor.constraint(equalTo: bottomView.topAnchor).isActive = true
        downArrow.isUserInteractionEnabled = true
        
        bottomView.addSubview(startButton)
        startButton.anchor(top: bottomView.topAnchor, leading: nil, bottom: bottomView.bottomAnchor, trailing: bottomView.trailingAnchor,size: .init(width: 170.calcvaluex(), height: 0))
        startButton.backgroundColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        startButton.setTitle("開始比較", for: .normal)
        startButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        startButton.setTitleColor(.white, for: .normal)
        
        nLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        nLabel.text = "產品比較"
        
        bottomView.addSubview(nLabel)
        nLabel.anchor(top: nil, leading: bottomView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 80.calcvaluex(), height: 28.calcvaluey()))
        nLabel.centerYInSuperview()
        seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
        
        bottomView.addSubview(seperator)
        seperator.anchor(top: nil, leading: nLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 66.calcvaluey()))
        seperator.centerYInSuperview()
        
        bottomView.addSubview(vLabel)
 
        vLabel.text = "最多比較三台機型"
        vLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        vLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        vLabel.anchor(top: nil, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 25.calcvaluey()))
        vLabel.centerYInSuperview()
        
        bottomView.addSubview(stackview)
        stackview.distribution = .fill
        stackview.spacing = 24.calcvaluex()
        stackview.axis = .horizontal
        stackview.isHidden = true
        stackview.anchor(top: bottomView.topAnchor, leading: seperator.trailingAnchor, bottom: bottomView.bottomAnchor, trailing: startButton.leadingAnchor,padding: .init(top: 0, left: 18.calcvaluex(), bottom: 0, right: 0))
//        addItem()
//        addItem()
//
//        stackview.addArrangedSubview(UIView())
    }
    func addItem(){

        let com = CompareItemView()
        com.constrainWidth(constant: 171.calcvaluex())
            stackview.addArrangedSubview(com)
        
        
        
        

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum InnerMode {
    case Normal
    case Compare
}
protocol InnerProductDelegate {
    func addNewItem()
    func goToProduct()
}
class InnerProductIntroController: SampleController {
    let imgv = UIImageView(image: #imageLiteral(resourceName: "bg_product"))
    let nameLabel = UILabel()
    let cusFilter = ProductFilterView()
    //let cusFilter2 = ProductSFilterView()
    let tableview = UITableView(frame: .zero, style: .plain)
    var stackviewanchor : AnchoredConstraints!
    let stackview = UIStackView()
    let compareButton = UIButton(type: .custom)
    var comparingView = CompareView()
    var mode = InnerMode.Normal
    var comparingViewAnchor:AnchoredConstraints!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        titleview.text = "CNC車床"
        
        imgv.contentMode = .scaleAspectFill
        imgv.clipsToBounds = true
        
        view.addSubview(imgv)
        imgv.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 172.calcvaluey()))
        
        nameLabel.font = UIFont(name: "Roboto-Medium", size: 36.calcvaluex())
        nameLabel.textColor = .white
        nameLabel.text = "CNC車床"
        imgv.addSubview(nameLabel)
        nameLabel.anchor(top: imgv.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 36.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 50.calcvaluey()))
        nameLabel.centerXInSuperview()
        
//        imgv.addSubview(cusFilter)
//        cusFilter.anchor(top: nameLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 108.calcvaluex(), height: 42.calcvaluey()))
//        cusFilter.centerXInSuperview()
        
        cusFilter.constrainWidth(constant: 108.calcvaluex())
        cusFilter.constrainHeight(constant: 42.calcvaluey())
        

        

        stackview.addArrangedSubview(cusFilter)
        stackview.axis = .horizontal
        stackview.distribution = .fill
        stackview.spacing = 12.calcvaluex()
        stackview.alignment = .fill
        view.addSubview(stackview)
        //imgv.isUserInteractionEnabled = true
        stackviewanchor = stackview.anchor(top: nameLabel.bottomAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 108.calcvaluex(), height: 42.calcvaluey()))
        stackview.centerXInSuperview()
        
        view.addSubview(tableview)
        tableview.separatorStyle = .none
        tableview.delegate = self
        tableview.dataSource = self
        tableview.anchor(top: imgv.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        cusFilter.isUserInteractionEnabled = true
        cusFilter.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goFilter)))
        
        compareButton.setTitle("加入比較", for: .normal)
        compareButton.setTitleColor(.white, for: .normal)
        compareButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
        compareButton.backgroundColor = .black
        compareButton.layer.cornerRadius = 21.calcvaluey()
        view.addSubview(compareButton)
        compareButton.addTarget(self, action: #selector(addToCompare), for: .touchUpInside)
        compareButton.anchor(top: imgv.bottomAnchor, leading: nil, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 48.calcvaluey(), left: 0, bottom: 0, right: -18.calcvaluex()),size: .init(width: 128.calcvaluex(), height: 42.calcvaluey()))
        compareButton.addshadowColor(color: .white)
        

        

    }
    @objc func dismissCompare(){
        //mode = .Normal
        if comparingView.showing{
        comparingViewAnchor.top?.constant = -24.calcvaluey()
        UIView.animate(withDuration: 0.4, animations:{
            self.view.layoutIfNeeded()
        }) { (_) in
            self.comparingView.downArrow.image = #imageLiteral(resourceName: "ic_arrow_drop_up_circle")
        }
            comparingView.showing = false
        }
        else{
            comparingViewAnchor.top?.constant = -120.calcvaluey()

            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.comparingView.downArrow.image = #imageLiteral(resourceName: "ic_arrow_drop_down_circle")
            }
            comparingView.showing = true
        }
        //self.tableview.reloadData()
    }
    @objc func goComparing(){
        if comparingView.stackview.arrangedSubviews.count > 0{
        let vd = ComparingItemController()
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
        }
    }
    @objc func addToCompare(){
        print(mode)
        if mode == .Normal {
            comparingView = CompareView()
            comparingView.startButton.addTarget(self, action: #selector(goComparing), for: .touchUpInside)
            view.addSubview(comparingView)
            
            comparingViewAnchor = comparingView.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 120.calcvaluey()))
            comparingView.downArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissCompare)))
        mode = .Compare
            self.view.layoutIfNeeded()
        comparingViewAnchor.top?.constant = -120.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
            compareButton.setTitle("取消比較", for: .normal)
            comparingView.showing = true
        }
        else{
            mode = .Normal
            
            comparingViewAnchor.top?.constant = 0

            UIView.animate(withDuration: 0.4, animations: {
                self.view.layoutIfNeeded()
            }) { (_) in
                self.comparingView.removeFromSuperview()
            }
            compareButton.setTitle("加入比較", for: .normal)
        }
        
        self.tableview.reloadData()
    }
    func addFilter(data:[String?]) {
        var twidth = 108.calcvaluex()
        for i in data {
            if let mt = i {
            let fv = ProductSFilterView(text: mt)
                let width = mt.widthOfString(usingFont: fv.nLabel.font) + 72.calcvaluex()
                fv.constrainHeight(constant: 42.calcvaluey())
                fv.constrainWidth(constant: width)
                stackview.addArrangedSubview(fv)
                twidth += width + 12.calcvaluex()
                fv.width = width + 12.calcvaluex()

            }
        }
        cusFilter.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        cusFilter.layer.borderColor = UIColor.clear.cgColor
        stackviewanchor.width?.constant = twidth
        
        self.view.layoutIfNeeded()
        
        
    }
    @objc func goFilter(){
        print("abc")
        let md = FilteringViewController()
        md.modalPresentationStyle = .overCurrentContext
        md.con = self
        self.present(md, animated: false, completion: nil)
    }
}
extension InnerProductIntroController : UITableViewDelegate,UITableViewDataSource,InnerProductDelegate{
    func goToProduct() {
        let vd2 = ProductInnerInfoController()
        vd2.modalPresentationStyle = .fullScreen
        //vd2.tabcon.allSeries = 
        self.present(vd2, animated: true, completion: nil)
    }
    
    func addNewItem() {
 

 
        if comparingView.stackview.arrangedSubviews.count != 4 {
        if comparingView.stackview.arrangedSubviews.count != 0{
        comparingView.stackview.removeArrangedSubview(comparingView.stackview.arrangedSubviews[comparingView.stackview.arrangedSubviews.count - 1])
        }
        comparingView.vLabel.isHidden = true
        comparingView.stackview.isHidden = false
        comparingView.addItem()
        comparingView.stackview.addArrangedSubview(UIView())
        }
        else{
            let con = UIAlertController(title: "提示", message: "只能選擇三個產品", preferredStyle: .alert)
            con.addAction(UIAlertAction(title: "確認", style: .cancel, handler: nil))
            
            self.present(con, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = InnerProductIntroCell(mode: self.mode,delegate:self)
        if indexPath.row % 2 == 0{
            cell.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        }
        else{
            cell.backgroundColor = .white
        }

        return cell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.calcvaluey()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
}
class InnerProductIntroCell:UITableViewCell{
    let imgv = UIImageView(image: #imageLiteral(resourceName: "im_compare_machine_1"))
    let seperator = UIView()
    let headerLabel = UILabel()
    let subLabel = UILabel()
    lazy var innerData = [InnerStackView(title: "S26/60", data: ["2軸"],mode: mode,delegate:self.delegate!),InnerStackView(title: "S26/110", data: ["2軸","C軸"],mode: mode,delegate:self.delegate!),InnerStackView(title: "S35", data: ["2軸","2軸","C軸","2軸","C軸","2軸","C軸","2軸","C軸","2軸","C軸"],mode: mode,delegate:self.delegate!),InnerStackView(title: "S35", data: ["2軸"],mode: mode,delegate:self.delegate!)]
    let stackview = UIStackView()
    var mode = InnerMode.Normal
    var delegate:InnerProductDelegate?
    init(mode:InnerMode,delegate:InnerProductDelegate) {
        super.init(style: .default, reuseIdentifier: "cell")
        self.delegate = delegate
        self.mode = mode
        selectionStyle = .none
         imgv.contentMode = .scaleAspectFit
         contentView.addSubview(imgv)
         imgv.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 106.calcvaluex(), bottom: 0, right: 0),size: .init(width: 242.calcvaluex(), height: 153.calcvaluey()))
         imgv.centerYInSuperview()
         
         seperator.backgroundColor = #colorLiteral(red: 0.8587269187, green: 0.8588779569, blue: 0.8587286472, alpha: 1)
         
         contentView.addSubview(seperator)
         seperator.anchor(top: contentView.topAnchor, leading: imgv.trailingAnchor, bottom: contentView.bottomAnchor, trailing: nil,padding: .init(top: 46.calcvaluey(), left: 42.calcvaluex(), bottom: 46.calcvaluey(), right: 0),size: .init(width: 1.calcvaluex(), height: 0))
         
         headerLabel.text = "高效率速軌"
         headerLabel.font = UIFont(name: "Roboto-Medium", size: 24.calcvaluex())
         headerLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
         
         contentView.addSubview(headerLabel)
         headerLabel.anchor(top: contentView.topAnchor, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 52.calcvaluey(), left: 74.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 33.calcvaluey()))
         
         subLabel.text = "Vturn-NP series"
         subLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
         subLabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
         
         contentView.addSubview(subLabel)
         subLabel.anchor(top: nil, leading: headerLabel.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 24.calcvaluey()))
         subLabel.centerYAnchor.constraint(equalTo: headerLabel.centerYAnchor).isActive = true

         
         stackview.distribution = .fill
         stackview.alignment = .fill
         stackview.axis = .vertical
         stackview.spacing = 22.calcvaluey()
                 for j in innerData {
                      stackview.addArrangedSubview(j)
                  }

                 stackview.addArrangedSubview(UIView())

         contentView.addSubview(stackview)
         stackview.anchor(top: headerLabel.bottomAnchor, leading: headerLabel.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 30.calcvaluey(), left: 0, bottom: 52.calcvaluey(), right: 0))

    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class InnerStackView : UIView {
    let hstack = UIStackView()
    let gstack = UIStackView()
    let label = UILabel()
    

    var mode = InnerMode.Normal
    var delegate : InnerProductDelegate?
    init(title:String,data:[String],mode:InnerMode = .Normal,delegate:InnerProductDelegate) {
        
        super.init(frame: .zero)
        self.delegate = delegate
        self.mode = mode
        label.text = title
        label.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(label)
        if data.count > 3{
            gstack(data:data)
            
            label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: gstack.leadingAnchor,padding: .init(top: 5.calcvaluey(), left: 0, bottom: 0, right: 8.calcvaluex()),size: .init(width: 0, height: 24.calcvaluey()))
        }
        else{
            _ = hstack(stack: hstack,data: data)


        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: hstack.leadingAnchor,padding: .init(top: 5.calcvaluey(), left: 0, bottom: 0, right: 8.calcvaluex()),size: .init(width: 0, height: 24.calcvaluey()))
        }

    }
    func gstack(data:[String]){
        gstack.axis = .vertical
        gstack.distribution = .fillEqually
        gstack.spacing = 22.calcvaluey()
        var kt = [[String]]()

        var numberoftime = 0.0
        if data.count % 3 == 0 {
            numberoftime = Double(data.count / 3)
        }
        else{
            numberoftime = ceil(Double(data.count/3) + 0.5)
        }
        

        for _ in 0 ..< Int(numberoftime) {
            kt.append([])
        }
        for (index,i) in data.enumerated() {
            


            kt[index/3].append(i)
            
        }
        for i in kt {
            let stackview = UIStackView()
            gstack.addArrangedSubview(hstack(stack: stackview, data: i,add: false))
        }
        addSubview(gstack)
        gstack.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 450.calcvaluex(), height: 0))
    }
    func hstack(stack:UIStackView,data:[String],add:Bool=true) -> UIStackView{
        
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = 32.calcvaluex()
        for (_,i) in data.enumerated() {
            let button = SelectButton(mode:self.mode,delegate: self.delegate!)
            button.button.setTitle(i, for: .normal)
            if mode == .Compare{
            button.constrainWidth(constant: 117.calcvaluex())
            }
            else{
                button.constrainWidth(constant: 89.calcvaluex())
            }
            button.constrainHeight(constant: 32.calcvaluey())
            stack.addArrangedSubview(button)
        }
        //if data.count < 3{
            stack.addArrangedSubview(UIView())
        //}
        
        if add {
            
        addSubview(stack)
            stack.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,size: .init(width: 450.calcvaluex(), height: 32.calcvaluey()))
        }
        return stack
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SelectButton : UIView {
    let button = UIButton(type: .custom)
    let box = UIImageView(image: #imageLiteral(resourceName: "btn_check_box_normal").withRenderingMode(.alwaysTemplate))
    var boxanchor : AnchoredConstraints!
    var buttonanchor:AnchoredConstraints!
    var delegate: InnerProductDelegate?
    init(mode:InnerMode,delegate:InnerProductDelegate){
        super.init(frame: .zero)
        self.delegate = delegate
        button.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16.calcvaluex())
         button.setTitleColor(.white, for: .normal)
         button.addshadowColor(color: .white)
         button.layer.cornerRadius = 16.calcvaluey()
         button.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        button.addTarget(self, action: #selector(goProductInfo), for: .touchUpInside)
         addSubview(box)
         box.contentMode = .scaleAspectFit
        if mode == .Compare{
            box.isHidden = false
            box.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        }
        else{
         box.isHidden = true
         boxanchor = box.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 24.calcvaluex()))
        }
        box.isUserInteractionEnabled = true
        box.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkBox)))
         box.centerYInSuperview()
        box.tintColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
         addSubview(button)
        if mode == .Compare{
            button.anchor(top: nil, leading: box.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 4.calcvaluex(), bottom: 0, right: 0),size: .init(width: 89.calcvaluex(), height: 32.calcvaluey()))
        }
        else{
         buttonanchor = button.anchor(top: nil, leading: box.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 89.calcvaluex(), height: 32.calcvaluey()))
        }
    }
    @objc func goProductInfo(){
        delegate?.goToProduct()
    }
    @objc func checkBox(){
        self.box.tintColor = .clear
        
        self.box.image = #imageLiteral(resourceName: "btn_check_box_pressed")
        delegate?.addNewItem()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
