//
//  General.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/21/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class General {
    static let shared = General()
    
    func getTopVc() -> UIViewController?{
        if #available(iOS 13, *)
        {
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

            if var topController = keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }

                return topController
            }
        }
        else{
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }

                return topController
            }
        
        }
        return nil
    }
    let activity : UIActivityIndicatorView = {
        let act = UIActivityIndicatorView(style: .whiteLarge)
        act.backgroundColor = #colorLiteral(red: 0.8848801295, green: 0.8848801295, blue: 0.8848801295, alpha: 1)
        
        act.hidesWhenStopped = true
        
        return act
    }()
    func showActivity(vd:UIView){
        
        vd.addSubview(activity)
        activity.centerInSuperview(size: .init(width: 50.calcvaluex(), height: 50.calcvaluex()))
        
        activity.startAnimating()
    }
    func stopActivity(vd:UIView){
        
        for i in vd.subviews {
            if let act = i as? UIActivityIndicatorView {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    act.stopAnimating()
                    act.removeFromSuperview()
                }

            }
        }
        
    }
}
