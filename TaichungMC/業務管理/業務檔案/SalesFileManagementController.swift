//
//  SalesFileManagementController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/4/1.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class SalesFileManagementController: SampleController {
    let searchcon = SearchTextField()
    let filecollectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let newCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "業務檔案"
        titleviewanchor.leading?.constant = 333.calcvaluex()
        
        searchcon.attributedPlaceholder = "搜尋檔案".convertoSearchAttributedString()
        
        view.addSubview(searchcon)
        searchcon.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluex(), left: 333.calcvaluex(), bottom: 0, right: 103.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        searchcon.backgroundColor = .white
        searchcon.addshadowColor(color: #colorLiteral(red: 0.9105209708, green: 0.9128736854, blue: 0.9183767438, alpha: 1))
        searchcon.layer.cornerRadius = 46.calcvaluey()/2
        
        view.addSubview(filecollectionview)
        filecollectionview.backgroundColor = .clear
        filecollectionview.anchor(top: searchcon.bottomAnchor, leading: searchcon.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 150.calcvaluey()))
        filecollectionview.delegate = self
        filecollectionview.dataSource = self
        filecollectionview.register(FileCell.self, forCellWithReuseIdentifier: "cellid")
        (filecollectionview.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        filecollectionview.showsHorizontalScrollIndicator = false
        
        view.addSubview(newCollectionView)
        newCollectionView.backgroundColor = .clear
        newCollectionView.anchor(top: filecollectionview.bottomAnchor, leading: filecollectionview.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        newCollectionView.delegate = self
        newCollectionView.dataSource = self
        newCollectionView.showsVerticalScrollIndicator = false
        newCollectionView.register(nFileCell.self, forCellWithReuseIdentifier: "newCell")
        newCollectionView.contentInset = .init(top: 46.calcvaluex(), left: 10.calcvaluex(), bottom: 20.calcvaluey(), right: 19.calcvaluex())
    }
    
}
class nFileCell : UICollectionViewCell {
    let imgv = UIImageView(image: #imageLiteral(resourceName: "image_rec_lib_banner"))
    let fileType = UIImageView(image: #imageLiteral(resourceName: "ic_video"))
    let downloadFile = UIImageView(image: #imageLiteral(resourceName: "ic_download"))
    let nLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        imgv.clipsToBounds = true
        imgv.contentMode = .scaleAspectFill
        
        addSubview(imgv)
        imgv.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 137.calcvaluey()))
        
        fileType.contentMode = .scaleAspectFit
        
        downloadFile.contentMode = .scaleAspectFit
        
        addSubview(fileType)
        fileType.anchor(top: imgv.bottomAnchor, leading: imgv.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        addSubview(downloadFile)
        downloadFile.anchor(top: fileType.topAnchor, leading: nil, bottom: nil, trailing: imgv.trailingAnchor,size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        
        addSubview(nLabel)
        nLabel.anchor(top: downloadFile.topAnchor, leading: fileType.trailingAnchor, bottom: nil, trailing: downloadFile.leadingAnchor,padding: .init(top: 0, left: 15.calcvaluex(), bottom: 0, right: 14.calcvaluex()))
        nLabel.numberOfLines = 2
        nLabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        nLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        nLabel.text = "191203台中精機 外觀展示影片歡迎參觀"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension SalesFileManagementController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == newCollectionView {
                return 6
        }
        
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == newCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newCell", for: indexPath)
            
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as! FileCell
        if indexPath.item == 0{
            cell.imagev.image = #imageLiteral(resourceName: "ic_folder_lock_pressed")
            cell.lb.text = "我的檔案"
            cell.lb.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        }
        else{
        cell.imagev.image = #imageLiteral(resourceName: "ic_folder_everyone_normal")
        cell.lb.text = "機台型錄"
            
            cell.lb.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        }
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == newCollectionView {
            return 28.calcvaluey()
        }
        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return 16.calcvaluex()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == newCollectionView {
            return .init(width: 210.calcvaluex(), height: 197.calcvaluey())
        }
        return .init(width: 136.calcvaluex(), height: collectionView.frame.height)
    }
    
    
}

class FileCell : UICollectionViewCell {
    let imagev = UIImageView(image: #imageLiteral(resourceName: "ic_folder_lock_pressed"))
    let lb = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        addSubview(imagev)
        imagev.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 136.calcvaluey()))
        imagev.contentMode = .scaleAspectFit
        
        addSubview(lb)
        lb.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        lb.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        lb.text = "我的檔案"
        lb.textAlignment = .center
        lb.anchor(top: imagev.bottomAnchor, leading: imagev.leadingAnchor, bottom: bottomAnchor, trailing: imagev.trailingAnchor,padding: .init(top: -14.calcvaluey(), left: 0, bottom: 0, right: 0))
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
