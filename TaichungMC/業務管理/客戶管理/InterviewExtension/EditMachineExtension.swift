//
//  EditMachineExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/1/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

extension InterviewController:EditMachineDelegate {
    @objc func goAddMachine(){
        let vd = StockMatchController()
        vd.modalPresentationStyle = .overFullScreen
        vd.con2 = self
       vd.modalTransitionStyle = .crossDissolve
        self.present(vd, animated: true, completion: nil)
    }
}


