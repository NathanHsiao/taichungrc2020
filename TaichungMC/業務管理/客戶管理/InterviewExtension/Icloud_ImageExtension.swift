//
//  Icloud_ImageExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
struct FileManagerUility {

    static func getFileSize(for key: FileAttributeKey) -> Int64? {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)

        guard
            let lastPath = paths.last,
            let attributeDictionary = try? FileManager.default.attributesOfFileSystem(forPath: lastPath) else { return nil }

        if let size = attributeDictionary[key] as? NSNumber {
            return size.int64Value
        } else {
            return nil
        }
    }

    static func convert(_ bytes: Int64, to units: ByteCountFormatter.Units = .useMB) -> String? {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = units
        formatter.countStyle = ByteCountFormatter.CountStyle.decimal
        formatter.includesUnit = false
        return formatter.string(fromByteCount: bytes)
    }
    
    static func getFileSize(data:Data) -> String{
      
      print("There were \(data.count) bytes")
      let bcf = ByteCountFormatter()
      bcf.allowedUnits = [.useMB] // optional: restricts the units to MB only
      bcf.countStyle = .file
      let string = bcf.string(fromByteCount: Int64(data.count))
      return string
      
    }
    
    static func checkIfFileSizeExceed(data:Data) -> Bool{
        if let freeSpaceInBytes = FileManagerUility.getFileSize(for: .systemFreeSize),let freeMB = FileManagerUility.convert(freeSpaceInBytes)  {
            let modifyMB = Double((freeMB.replacingOccurrences(of: ",", with: ""))) ?? 0.0
            let fileSize = Double(getFileSize(data: data)) ?? 0.0

            return modifyMB > fileSize
        }
        
        return false
    }
    
    static func checkOver25MB(data:Data) -> Bool {
        let fileSize = Double(getFileSize(data: data)) ?? 0.0
        
        return fileSize < 25
    }

}
extension InterviewController:UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIDocumentPickerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print(991,info)
        if let img =  info[UIImagePickerController.InfoKey.originalImage] as? UIImage , let dt = img.jpegData(compressionQuality: 0.1){
            if !FileManagerUility.checkIfFileSizeExceed(data: dt) {
                let alertController = UIAlertController(title: "您的裝置的空間不足以上傳", message: "請空出空間再上傳", preferredStyle: .alert)
                let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
                return
            }

            let uuid = "\(UUID().uuidString).jpg"
            writeDataToFolder(data: dt, name: uuid)
            if customAddFileMode {
                addedMachine[customSubIndex].customOptions[customSubIndex].options[customSubSubIndex].files = [AddedOptionFile(id: nil, path: uuid, pathUrl: nil,image: dt,fileUrl: nil)]
                print(778,addedMachine[customSubIndex].customOptions)
                dismiss(animated: true) {
                    
                    self.reloadSectionWithIndex(index: 1)
                }
                
            }
            else{
            interInfo.files.append( interViewFile(id: "", tag: ["file"], path: uuid, path_url: "", image: img) )

                dismiss(animated: true, completion: {
                    self.createEditInterviewCell()
                    self.rightview.beginUpdates()
                    self.rightview.reloadData()
                    self.rightview.endUpdates()
                })
            }
            
            customAddFileMode = false
        }

    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        customAddFileMode = false
        print(77799,customAddFileMode)
        dismiss(animated: true, completion: nil)
    }
    
    
    func goCameraAlbumAction(type:Int) {
                let picker = UIImagePickerController()
                picker.delegate = self
        if type == 0{
            picker.sourceType = .photoLibrary
        }
        else if type == 1{
            picker.sourceType = .camera
        }
                
                picker.modalPresentationStyle = .fullScreen
                self.present(picker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        customAddFileMode = false
        print(77799,customAddFileMode)
        //dismiss(animated: true, completion: nil)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let uuidName = "\(UUID().uuidString).\(url.pathExtension)"
        
        guard let data = try? Data(contentsOf: url) else {return}
        if !FileManagerUility.checkOver25MB(data: data) {
            let alertController = UIAlertController(title: "上傳檔案容量限制為25MB", message: "請重新選擇", preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        if !FileManagerUility.checkIfFileSizeExceed(data: data) {
            let alertController = UIAlertController(title: "您的裝置的空間不足以上傳", message: "請空出空間再上傳", preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
            writeDataToFolder(data: data, name: uuidName)
            var image : UIImage = UIImage()
            if url.pathExtension == "pdf" {
                image = Thumbnail().pdfThumbnail(url: url) ?? UIImage()
            }
            else if url.pathExtension == "mp4"{
                image = Thumbnail().getThumbnailImage(forUrl: url) ?? UIImage()
            }
            
        if customAddFileMode {
            addedMachine[customSubIndex].customOptions[customSubIndex].options[customSubSubIndex].files = [AddedOptionFile(id: nil, path: uuidName,fileUrl: url.absoluteString)]
            self.reloadSectionWithIndex(index: 1)
        }
        else{
            interInfo.files.append( interViewFile(id: "", tag: ["file"], path: uuidName, path_url: "", image: image,fileUrl: url.absoluteString) )
            self.reloadSectionWithIndex(index: 0)
        }
        
        customAddFileMode = false


    }
    
}
