//
//  SelectMachineExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/1/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
extension InterviewController: SelectedMachineCellDelegate {
    func customFileAction(textField:UITextField){
        let controller = UIAlertController(title: "請選擇上傳方式", message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿", style: .default) { (_) in
            self.customAddFileMode = true
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        let cameraAction = UIAlertAction(title: "拍照", style: .default) { (_) in
            self.customAddFileMode = true
            self.goCameraAlbumAction(type: 1)
        }
        controller.addAction(cameraAction)
        
        let cloudAction = UIAlertAction(title: "雲端", style: .default) { (_) in
            self.customAddFileMode = true
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
                    documentPickerController.modalPresentationStyle = .fullScreen
            self.present(documentPickerController, animated: true, completion: nil)
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = textField
                presenter.sourceRect = textField.bounds
            presenter.permittedArrowDirections = .any
            }
        self.present(controller, animated: true, completion: nil)
    }
    func showFile(filePaths:[FilePath]) {
        let vd = AttachFileWindowController(attachFiles: filePaths)
        vd.modalPresentationStyle = .overCurrentContext
        self.present(vd, animated: false, completion: nil)
    }
    
    func addCustomFile(index: Int, subIndex: Int, thirdIndex: Int, textField: UITextField) {
        customFirstIndex = index
        customSubIndex = subIndex
        customSubSubIndex = thirdIndex
        customFileAction(textField: textField)
    }
    
    func removeCustomFile(index: Int, subIndex: Int, thirdIndex: Int, textField: UITextField,addedFile: AddedOptionFile?) {
        let controller = UIAlertController(title: "請選擇", message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "刪除附檔", style: .default) { (_) in
            self.addedMachine[index].customOptions[subIndex].options[thirdIndex].files = []
            if let id = addedFile?.id , id != ""{
                let delFile = [id]
                self.addedMachine[index].customOptions[subIndex].options[thirdIndex].del_files = delFile
            }
            self.reloadSectionWithIndex(index: 1)
        }
        let cancel = UIAlertAction(title: "取消", style: .default, handler: nil)
        controller.addAction(albumAction)
        controller.addAction(cancel)
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = textField
                presenter.sourceRect = textField.bounds
            presenter.permittedArrowDirections = .any
            }
        self.present(controller, animated: true, completion: nil)
        
        
    }
    
    func setMachineQty(index: Int, qty: Int) {
        addedMachine[index].qty = qty
        self.reloadSectionWithIndex(index: 1)
    }
    
    func showCustomAttachFile(file:AddedOptionFile) {
        let vd = ExpandFileController()
        vd.view.backgroundColor = .clear

        print(8897,file.path,file.pathUrl)
        vd.imgv.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        
        if let image = file.image{
            vd.imagview.isHidden = false
            vd.imagview.image = UIImage(data: image)

        }
        else if let urlString = file.fileUrl {
            guard let url = URL(string: urlString) else {return}
            
            print(url.absoluteString)
            if url.pathExtension == "jpg" || url.pathExtension == "png" || url.pathExtension == "jpeg" {
                vd.imagview.isHidden = false
                vd.imagview.sd_setImage(with: url, completed: nil)
            }
            else{
            vd.imgv.loadRequest(URLRequest(url: url))
            }
        }
        else{
            guard let url = URL(string: "\(AppURL().baseURL)\(file.pathUrl ?? "")") else {return}
            
            print(url.absoluteString)
            if url.pathExtension == "jpg" || url.pathExtension == "png" || url.pathExtension == "jpeg" {
                vd.imagview.isHidden = false
                vd.imagview.sd_setImage(with: url, completed: nil)
            }
            else{
            vd.imgv.loadRequest(URLRequest(url: url))
            }
            
        }
        vd.modalPresentationStyle = .overFullScreen
        
        self.present(vd, animated: false, completion: nil)
    }
    
    func saveSignitureImage(image:UIImage,index:Int) {
        
        signitureArray[index] = interViewFile(id: "", tag: ["sign"], path: SignitureMode.Customer.rawValue, path_url: "", image: image)
        writeDataToFolder(data: image.jpegData(compressionQuality: 0.1), name: SignitureMode.Customer.rawValue)
        
        reloadSectionWithIndex(index: 2)
    }
 
    func setEquipOption(index : Int,options:[OptionObj]) {
        self.savedOptions[index] = options
        addedMachine[index].options = []
        for i in options {
            if i.selectedRow != 0{
                if let option = i.option?.children?[i.selectedRow]{
                    addedMachine[index].options.append(SelectedOption(name: "\(i.option?.title ?? "") / \(option.title)", option: option))
                }
            }
        }
        reloadSectionWithIndex(index: 1)
    }
    func addMachine(series:Series,product:Product,spec:Specification) {
        if (product.getProductsPrices()?.price == 0) || (product.getProductsPrices()?.price == nil){
            let alertController = UIAlertController(title: "此產品不支援您的貨幣群組，請通知管理員", message: nil, preferredStyle: .alert)
            let action = UIAlertAction(title: "確定", style: .cancel, handler: nil)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        var pr = product
        
        pr.full_name = "\(series.title ?? "") / 射座型式：\(product.shooter_code ?? "") / 螺桿直徑：\(spec.getInjectionValue())"
        addedMachine.append(AddedProduct(series: nil, sub_series: series,product: pr, spec: spec, options: [], customOptions: [customForm(id: 0, status: -1, is_Cancel: false, statusArray: [], options: [])],fileAssets:[]))
        
        fieldsArray = [.Info,.Machine,.Discount]
        reloadSectionWithIndex(index: 1,reloadAll: true)
 
    }
    func removeMachine(index: Int) {
        //要加入
        let dt = self.addedMachine.remove(at: index)
        self.savedOptions[index] = []
        if let id = dt.product?.id, dt.product?.product_id != nil{
            self.deleteProductArray.append(id)
        }
        if self.addedMachine.count == 0{
            fieldsArray = [.Info,.Machine]
        }
        reloadSectionWithIndex(index: 1,reloadAll: self.addedMachine.count == 0)
    }
    func modifyCustomStatus(index: Int, subIndex: Int, textField: UITextField) {
        modeselection = CustomizeModeSelectionController()
        modeselection?.isStatus = true
        modeselection?.firstIndex = index
        modeselection?.secondIndex = subIndex
        
        if mode == .New {
            modeselection?.statusArray = ["草稿","新合審"]
        }
        else {
            print(12,index,subIndex)
            if subIndex >= addedMachine[index].customOptions.count {
                modeselection?.statusArray = ["草稿","新合審","已取消"]
               // addedMachine[index].customOptions
            }
            else {
                let vt = addedMachine[index].customOptions[subIndex]
                if vt.status == 0 {
                    modeselection?.statusArray = ["草稿","新合審","已取消"]
                    addedMachine[index].customOptions[subIndex].statusArray = ["草稿","新合審","已取消"]
                }
                else if !vt.is_Cancel{
                    if vt.statusArray.isEmpty {
                    addedMachine[index].customOptions[subIndex].statusArray = [dictStatus[vt.status] ?? "","已取消"]
                    modeselection?.statusArray = [dictStatus[vt.status] ?? "","已取消"]
                    }
                    else{
                        modeselection?.statusArray = vt.statusArray
                    }
                }
            }
           // let vt = addedMachine[index].customOptions[subIndex]



            
        }
//        modeselection?.isCustomers = true
//        modeselection?.customersList = self.customersList
        
        modeselection?.delegate = self
        self.rightview.addSubview(modeselection!)
        var height = CGFloat(modeselection?.statusArray.count ?? 0) * 50.calcvaluey()
        if height > 300.calcvaluey()
        {
            height = 300.calcvaluey()
        }
        modeselection?.anchor(top: textField.bottomAnchor, leading: textField.leadingAnchor, bottom: nil, trailing: textField.trailingAnchor,size: .init(width: 0, height: height))
    }
    func addNewCustomForm(index:Int) {
        addedMachine[index].customOptions.append(customForm(id: 0, status: -1, is_Cancel: false, statusArray: [], options: []))
        reloadSectionWithIndex(index: 1)
    }
    func setCustomCount(index: Int, subIndex: Int,thirdIndex: Int, amount: Int) {
        
        addedMachine[index].customOptions[subIndex].options[thirdIndex].amount = amount
        
        for i in addedMachine {
            print(i.customOptions)
        }
        reloadSectionWithIndex(index: 1)

    }
    func sendCustomPrice(index: Int, subIndex: Int,thirdIndex:Int, text: String) {
        if let priceText = text.doubleValue {
            addedMachine[index].customOptions[subIndex].options[thirdIndex].price = text
        
        }
        else{
            let alertController = UIAlertController(title: "只能輸入數字", message: nil, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
        self.reloadSectionWithIndex(index: 1)
    }
    func sendCustomName(index: Int, subIndex: Int,thirdIndex:Int, text: String) {
        addedMachine[index].customOptions[subIndex].options[thirdIndex].text = text
        self.reloadSectionWithIndex(index: 1)
    }
    func sendCustomDate(index: Int, subIndex: Int,thirdIndex:Int, text: String) {
        addedMachine[index].customOptions[subIndex].options[thirdIndex].text = text
        self.reloadSectionWithIndex(index: 1)
    }
    func removeDiscount(index: Int) {
        discountArray.remove(at: index)
        reloadSectionWithIndex(index: 2)
    }
    func setNewDiscount() {
        discountArray.append(Discount(text: "", price: 0))
        reloadSectionWithIndex(index: 2)
    }
    func sendCustomChoice(index: Int, subIndex: Int,thirdIndex:Int,textField: UITextField) {
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        modeselection = CustomizeModeSelectionController()
        modeselection?.tag = thirdIndex
        modeselection?.firstIndex = index
        modeselection?.secondIndex = subIndex
        modeselection?.delegate = self
        self.rightview.addSubview(modeselection!)
        modeselection?.anchor(top: textField.bottomAnchor, leading: textField.leadingAnchor, bottom: nil, trailing: textField.trailingAnchor,size: .init(width: 0, height: 150.calcvaluey()))
    }
    func removeCustom(index: Int, subIndex: Int,thirdIndex:Int) {
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        let customOption = self.addedMachine[index].customOptions[subIndex].options.remove(at: thirdIndex)
        //要加入
//        print(customOption)
//        if customOption.id != "" {
//            if self.deleteAuditArray[index]?.count == 0 {
//                self.deleteAuditArray[index] = [customOption.id]
//            }
//            else{
//                self.deleteAuditArray[index]?.append(customOption.id)
//            }
//
//        }
        if self.addedMachine[index].customOptions[subIndex].options.count == 0{
            self.addedMachine[index].customOptions[subIndex].status = -1
        }
        reloadSectionWithIndex(index: 1)
    }
    func setNewOption(index: Int) {
        //self.addedMachine[index].options.append(SelectedOption(option: nil))
        
        let show = SalesEquipController()
//        show.controller.selectLabel.text = "\(addedMachine[index].sub_series?.title ?? "") \(addedMachine[index].product?.title ?? "")"
        show.view.tag = index
        //show.controller.optionsArray = self.savedOptions[index] ?? []
        show.con = self
        show.controller.vd.isUserInteractionEnabled = false
        
        show.controller.selectedProducts = addedMachine[index].product
        show.controller.selectedSeries = addedMachine[index].sub_series
        show.controller.selectedSpec1 = addedMachine[index].spec
        show.controller.reloadingData()
       // show.controller.selectedProduct = addedMachine[index].product
    
        show.modalPresentationStyle = .fullScreen
        
        self.present(show, animated: true, completion: nil)
//        UIView.setAnimationsEnabled(false)
//        self.rightview.reloadSections(IndexSet(arrayLiteral: 1), with: .automatic)
//        UIView.setAnimationsEnabled(true)
    }
    func setNewCustomOption(index: Int,subIndex:Int) {
        // 要加入

        self.addedMachine[index].customOptions[subIndex].options.append(AddedOption(mode: .Date, text: "", amount: 1, price: "", reply: "", id: ""))
//        self.addedMachine[index].customOptions[subIndex].append())
        if self.addedMachine[index].customOptions[subIndex].status == -1 {
            self.addedMachine[index].customOptions[subIndex].status = 0
        }
        reloadSectionWithIndex(index: 1)
    }
    func removeOption(index: Int, subIndex: Int) {
        let obj = self.addedMachine[index].options.remove(at: subIndex)
        
        for (index2,i) in (savedOptions[index] ?? []).enumerated() {
            
            if let _ = i.option?.children?.firstIndex(where: { (oop) -> Bool in
                return oop.id == obj.option?.id
            }) {
                savedOptions[index]?[index2].selectedRow = 0
            }
        }
        reloadSectionWithIndex(index: 1)
        
    }
}
