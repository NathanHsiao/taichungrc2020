//
//  TableviewExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

extension InterviewController: UITableViewDelegate,UITableViewDataSource,ChangingTextViewHeightProtocol,FormTextViewDelegate,RightTopViewHeaderDelegate {
    func seeInfo() {
        let vd = InterviewInfoViewController()
        vd.modalPresentationStyle = .overCurrentContext
        self.present(vd, animated: false, completion: nil)
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        if mode == .DiscountInfo {
            discountInfo.discount_reason = text
        }
        else if mode == .DiscountPrice {
            
            if let pr = text.integerValue {
                
                
                if let discount_percent = self.userattr?.discount_percent {
                    
                    let discountFormat = self.currentTotal * (Double(discount_percent)/100.0)
                    if pr > Int(discountFormat) {
                        discountInfo.discount = Int(discountFormat)
                    }
                    else{
                        discountInfo.discount = pr
                    }
                }
                else{
                    discountInfo.discount = pr
                }
            }
        }
       // self.reloadSectionWithIndex(index: 2)
    }
    func textViewReloadHeight(height:CGFloat) {
        goChangingHeight()
    }

    func goChangingHeight(){
        UIView.performWithoutAnimation {
            self.rightview.beginUpdates()
            self.rightview.endUpdates()
        }
        
    }
    func reloadSectionWithIndex(index:Int,reloadAll:Bool=false) {
        self.calculateTotal()
        self.createEditInterviewCell()
        self.createMachineCell()
        UIView.setAnimationsEnabled(false)
        if reloadAll{
            self.rightview.reloadData()
        }
        else{
        self.rightview.reloadSections(IndexSet(arrayLiteral: index), with: .automatic)
        }
        UIView.setAnimationsEnabled(true)
    }
    
    
    func setDiscountHeight(discounts:[Discount]) -> CGFloat {
        var defaultHeight = 138.calcvaluey()
        defaultHeight += CGFloat(discounts.count) * 70.calcvaluey()
        return defaultHeight
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        
    }
    func setProductHeight(product:AddedProduct) -> CGFloat{
        let defaultProductHeight = 95.calcvaluey()
        
        var defaultOptionHeight = 126.calcvaluey()
        
        defaultOptionHeight += (CGFloat(product.options.count) * 57.calcvaluey())
        //要加入
        var customHeight :CGFloat = 0
        for i in product.customOptions {
            var defaultHeight : CGFloat = 0
            for j in i.options {
                defaultHeight += 73.calcvaluey()
                if j.reply != "" {
                    defaultHeight += 81.calcvaluey()
                }
            }
            var defaultCustomOptionHeight = 138.5.calcvaluey()
            defaultCustomOptionHeight += defaultHeight
            
            customHeight += defaultCustomOptionHeight

        }
        
        if mode == .Editing && (product.customOptions.last?.is_Cancel == true){
            customHeight += 80.calcvaluey()
        }
        
        return defaultProductHeight + defaultOptionHeight  + customHeight
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if fieldsArray[section] == .Info{
            return 34.calcvaluey()
        }
        return 52.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let f_mode = fieldsArray[section]
        if f_mode == .Info{
            if mode == .Normal {
            return RightViewTopText()
            }
            else{
                let sc = SectionOneTopViewHeader()
                sc.memberlabel.text = "負責業務：\(salesName)"
                sc.statusLabel.text = self.interInfo.status.setInterviewStatus()
                sc.statusLabel.textColor = self.interInfo.status.setInterViewStatusColor()
                return sc
            }
        }
        let topRight = RightTopViewHeader()
        topRight.delegate = self
        if f_mode == .Machine{
            topRight.headerlabel.text = "訪談機台"
        }
        else if f_mode == .Sign {
            topRight.headerlabel.text = "確認簽名"
        }
        else{
            topRight.headerlabel.text = "調整價格"
        }
        return topRight
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //if !buttontap {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
           // print("Bottom")
           // delegate.scrolling(indexarray: [IndexPath(item: 0, section: 6)])
        }
        else{
           // print(self.rightview.indexPathsForVisibleRows!)
        //delegate.scrolling(indexarray: self.tableview.indexPathsForVisibleRows!)
        }
        //}
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let f_mode = fieldsArray[indexPath.section]
        if f_mode == .Info {
//            if mode == .Normal {
//            return 600.calcvaluey()
//            }
//            else if interInfo.files.count != 0 {
//                return 638.calcvaluey()
//            }
//            else{
//                return 526.calcvaluey()
//            }
            return UITableView.automaticDimension
        }
        else if f_mode == .Sign{
            return 199.calcvaluey()
        }
        else if f_mode == .Discount{
            return UITableView.automaticDimension
        }
        else{
            if mode == .Normal {
                return 164.calcvaluey()
            }
            else{
                if addedMachine.count == 0 || addedMachine.count == indexPath.row {
                    return 118.calcvaluey()
                }
                else{
                    
                    return UITableView.automaticDimension //self.setProductHeight(product:self.addedMachine[indexPath.row])
                }
                
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if fieldsArray[section] == .Machine{
            
            if mode == .Editing || mode == .New {
            return addedMachine.count + 1
            }
            else{
                return addedMachine.count
            }
        }
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return fieldsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let f_mode = fieldsArray[indexPath.section]
        if f_mode == .Info {
            if mode == .Normal {
            let cell = InterviewFirstSectionCell(style: .default, reuseIdentifier: "cc")
            cell.customername.textfield.text = "久大行銷股份有限公司"
            cell.interviewdate.textfield.text = "12月30日 下午4:26"
            cell.orderdate.textfield.text = "12月30日 下午4:26"
            cell.moreinfo.textfield.text = "此機台單價較高， 因此客戶想多方比較再決定， 如果需要再進一步做報價，客戶 預計於2020年1月5日時會先電聯。"
            return cell
            }

            else {
//                let cell = EditingInterviewCell(style: .default, reuseIdentifier: "cc")
//               // cell.addFile.addTarget(self, action: #selector(addPhoto), for: .touchUpInside)
//
//                cell.mode = mode
//                cell.delegate = self
//                //cell.locationlabel.delegate = self
//                cell.info = interInfo
                return EditInterviewCell
            }
        }
        else if f_mode == .Sign {

                let cell = SignitureCell(style: .default, reuseIdentifier: "sig")
            cell.mode = self.mode
                cell.delegate = self
            cell.customerField.mode = self.mode
                cell.customerField.signi = signitureArray[0]
               // cell.salesField.signi = signitureArray[1]
                return cell
            
        }
        else if f_mode == .Discount {
            let txt = tableView.dequeueReusableCell(withIdentifier: "discountCell", for: indexPath) as! DiscountViewCell
            txt.textView.t_delegate = self
            txt.priceField.t_delegate = self
            
            txt.textView.text = discountInfo.discount_reason
            txt.textView.textViewDidChange(txt.textView)
            if let pr = discountInfo.discount {
                txt.priceField.text = "\(pr)"
                txt.priceField.textViewDidChange(txt.priceField)
            }
            txt.container.isUserInteractionEnabled = (self.mode == .New || self.mode == .Editing) ? true : false
            
           // txt.textField.delegate = self
           // txt.priceField.delegate = self
            return txt
        }
        else if f_mode == .Machine {
            if (addedMachine.count == 0 || indexPath.row == addedMachine.count) && (mode == .Editing || mode == .New){
            let cell = EditingMachineCell(style: .default, reuseIdentifier: "cm")
                cell.delegate = self
//            cell.addMachine.addTarget(self, action: #selector(goAddMachine), for: .touchUpInside)
            return cell
            }
            else{
//                let cell = SelectedMachineCell(style: .default, reuseIdentifier: "selectedMachine")
//                cell.delegate = self
//
//                cell.mode = self.mode
//               // cell.originalProduct = originalMachine[indexPath.row]
//
//                cell.tag = indexPath.row
//                cell.equipView.tag = indexPath.row
//                cell.machineView.tag = indexPath.row
//                cell.addMoreCustomView.addButton.addButton.tag = indexPath.row
//                cell.addMoreCustomView.addButton.label.tag = indexPath.row
//
//                cell.addedProduct = addedMachine[indexPath.row]
                return machincellsArray[indexPath.row]
            }
        }
        else{
            if mode == .Normal {
        let cell = InterviewSecondSectionCell(style: .default, reuseIdentifier: "ct")
        
        return cell
            }
            else{


                let cell = DiscountTableViewCell(style: .default, reuseIdentifier: "ctt")
                cell.delegate = self
                cell.discountv.addedDiscount = self.discountArray
                return cell
            }

        }
    }
    
    func createEditInterviewCell(){
        let cell = EditingInterviewCell(style: .default, reuseIdentifier: "cc")
        cell.textviewWidth = UIScreen.main.bounds.width - 366.calcvaluex()
        
        cell.mode = mode
        cell.info = interInfo
        cell.delegate = self
        
        cell.textviewDelegate = self
        EditInterviewCell = cell
        
    }
    
    func createMachineCell(){
        machincellsArray = []
        for (index,i) in addedMachine.enumerated() {
            let cell = SelectedMachineCell(style: .default, reuseIdentifier: "selectedMachine")
            cell.delegate = self
           
            cell.mode = self.mode
           // cell.originalProduct = originalMachine[indexPath.row]
            
            cell.tag = index
            cell.equipView.tag = index
            cell.machineView.tag = index
            cell.machineView.counter.firstIndex = index
            cell.addMoreCustomView.addButton.addButton.tag = index
            cell.addMoreCustomView.addButton.label.tag = index
            
            cell.addedProduct = i
            machincellsArray.append(cell)
        }
    }
    
}
