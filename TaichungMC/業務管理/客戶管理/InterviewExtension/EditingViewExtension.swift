//
//  EditingViewExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import MobileCoreServices
extension InterviewController: EditingInterviewDelegate{
    func chooseCustomers(textField: UITextField) {
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        let vd = CustomerSelectionView()
        vd.customersList = self.customersList
        vd.con = self
        //vd.customerList = self.customersList
        vd.modalPresentationStyle = .overCurrentContext
        self.present(vd, animated: false, completion: nil)
    }
    
    func showAttachFile(file: interViewFile) {
        
        let vd = ExpandFileController()
        vd.view.backgroundColor = .clear

        print(3312,file)
        vd.imgv.loadRequest(URLRequest(url: URL(string: "about:blank")!))
        if file.path_url != "" {
            
        guard let url = URL(string: file.path_url) else {return}
            if url.pathExtension == "jpg" || url.pathExtension == "png" || url.pathExtension == "jpeg" {
                vd.imagview.isHidden = false
                vd.imagview.sd_setImage(with: url, completed: nil)
            }
            else{
            vd.imgv.loadRequest(URLRequest(url: url))
            }

        }
        else{
            if let fileUrl = file.fileUrl {
                guard let url = URL(string: fileUrl) else {return}
                vd.imgv.loadRequest(URLRequest(url: url))
            }
            else{
            if let imgv = file.image {
                vd.imagview.isHidden = false
                vd.imagview.image = imgv
                
            }
            }
            
        }
        vd.modalPresentationStyle = .overFullScreen
        
        self.present(vd, animated: false, completion: nil)
    }
    func sendPeriodArray(array: [String]) {
        self.interInfo.periods = array
        reloadSectionWithIndex(index: 0)
    }
    
    
    func removeFile(index: Int) {
        let dt = interInfo.files.remove(at: index)
        removeDataFromFolder(name: dt.path)
        if dt.id != "" {
            deleteFileArray.append(dt.id)
        }
        self.createEditInterviewCell()
        rightview.beginUpdates()
        rightview.reloadData()
        rightview.endUpdates()
    }
    func setOrderDate(text: String) {
        print(text)
        interInfo.orderDate = text
        reloadSectionWithIndex(index: 0)
    }
    func setInfo(text: String) {
        
        interInfo.moreInfo = text
        reloadSectionWithIndex(index: 0)
    }
    func goSelectFile(sender:UIButton) {
        if interInfo.files.count == 6 {
                        let alertController = UIAlertController(title: "一次最多只能上傳6個檔案", message: nil, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
            
            return
        }
        let controller = UIAlertController(title: "請選擇上傳方式", message: nil, preferredStyle: .actionSheet)
        let albumAction = UIAlertAction(title: "相簿", style: .default) { (_) in
            self.goCameraAlbumAction(type: 0)
        }
        controller.addAction(albumAction)
        let cameraAction = UIAlertAction(title: "拍照", style: .default) { (_) in
            self.goCameraAlbumAction(type: 1)
        }
        controller.addAction(cameraAction)
        
        let cloudAction = UIAlertAction(title: "雲端", style: .default) { (_) in
                    let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeMovie), String(kUTTypeVideo), String(kUTTypePlainText), String(kUTTypeMP3)], in: .import)
            documentPickerController.delegate = self
                    documentPickerController.modalPresentationStyle = .fullScreen
            self.present(documentPickerController, animated: true, completion: nil)
        }
        controller.addAction(cloudAction)
        
        if let presenter = controller.popoverPresentationController {
                presenter.sourceView = sender
                presenter.sourceRect = sender.bounds
            presenter.permittedArrowDirections = .any
            }
        self.present(controller, animated: true, completion: nil)
    }
    func setVisitDate(text: String) {
        
        self.interInfo.visitDate = text
        self.reloadSectionWithIndex(index: 0)
    }
    
    func sendLocation(text: String) {
        self.interInfo.location = text
        self.reloadSectionWithIndex(index: 0)
    }
    func sendRating(text: String) {
        if let rating = Int(text) {
            
            if rating > 0 && rating < 6{
                self.interInfo.rating = rating
                
            }
            else{
                self.interInfo.rating = nil
                let alert = UIAlertController(title: "成交率請輸入1~5", message: nil, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "確認", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        else{
            self.interInfo.rating = nil
            let alert = UIAlertController(title: "成交率請輸入1~5", message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "確認", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        self.reloadSectionWithIndex(index: 0)
    }
    
    
    func chooseNotifyDate(textField: UITextField) {
        print(123,textField)
        modeselection = CustomizeModeSelectionController()
        modeselection?.isNotify = true
//        modeselection?.tag = subIndex
//        modeselection?.firstIndex = index
        modeselection?.delegate = self
        self.rightview.addSubview(modeselection!)
        var height = 150.calcvaluey()
        modeselection?.anchor(top: textField.bottomAnchor, leading: textField.leadingAnchor, bottom: nil, trailing: textField.trailingAnchor,size: .init(width: 0, height: height))
    }
}
