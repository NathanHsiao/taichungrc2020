//
//  CustomizeModeExtension.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 6/1/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
extension InterviewController:CustomizeModeDelegate {
    func setNotifyDate(notifyMode: NotifyMode) {
        self.interInfo.notifyDate = notifyMode
        self.reloadSectionWithIndex(index: 0)
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
    }
    func setCustomStatus(index: Int, subIndex: Int, status: Int) {
        print(status,index,subIndex,addedMachine[index].customOptions.count)
        addedMachine[index].customOptions[subIndex].status = status
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        self.reloadSectionWithIndex(index: 1)
    }
    func didSelect(mode: AddedMode, index: Int,subIndex: Int,thirdIndex:Int) {
        addedMachine[index].customOptions[subIndex].options[thirdIndex].mode = mode
        reloadSectionWithIndex(index: 1)
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
    }
    func setCustomer(customer: Customer) {
        
        interInfo.customer = customer
        if let vt = modeselection {
            vt.removeFromSuperview()
        }
        //interInfo.name = text
        reloadSectionWithIndex(index: 0)
    }
}
