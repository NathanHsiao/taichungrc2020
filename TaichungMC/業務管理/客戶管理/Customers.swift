//
//  Customer.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/2/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation
import UIKit
import MapKit
struct Customers : Codable{
    var data : [Customer]?
    var meta : Meta?
}

struct Customer : Codable {
    var id : String
    var code : String
    var name : String
    var business_number : String?
    var sales : Sales?
   // var currentSales : Sales?
    
//    mutating func setCurrentSales(){
//        switch self.sales {
//        case .sales(let x) :
//            self.currentSales = x
//        case .bool(_):
//            ()
//        }
//    }
    
}
struct Sales : Codable  {
    var id : String
    var code : String
    var name : String
    var department_code : String
    var department_name : String
}
enum SalesType : Codable {
    case bool(Bool)
    case sales(Sales)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Bool.self) {
            self = .bool(x)
            return
        }
        if let x = try? container.decode(Sales.self) {
            self = .sales(x)
            return
        }
        throw DecodingError.typeMismatch(FileType.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for MyValue"))
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let x):
            try container.encode(x)
        case .sales(let x):
            try container.encode(x)
        }
    }
}


struct DepartmentGroup {
    var name : String?
    var code : String?
    var sales : [SalesCompany]
}

struct SalesCompany {
    var name : String
    var code : String
    var company : [Customer]
    var interviewList : [InterViewList]
}


struct Company : Codable {
    var data : CompanyData?
    var ERR_CODE: String?
}

struct CompanyData : Codable {
    var id : String
    var code : String
    var name : String?
    var tel : String?
    var fax : String?
    var contact : String?
    var address : String?
    var business_number : String?
    var sales : Sales?
    var type : String?
    var staff_num : String?
    var upstream : [String]?
    var downstream : [String]?
    var contacts : [CompanyContact]?
    var interviews : [Interview]
    var machines : [CustomerEquipment]?
    
}
struct CustomerEquipment:Codable {
    var id : Int
    var account_code : String?
    var model : String?
    var brand: String?
    var number : String?
    var manufacturer: String?
}
struct CompanyContact : Codable{
    var id : Int
    var account_code : String?
    var name : String?
    var type : String?
    var mobile : String?
    var interesting:String?
    var phone: String?
    var email : String?
    var birthday : String?
    var other : ContactOther?
}
struct ContactOther : Codable {
    var ext : String?
}
struct Interview : Codable {
    var id : String
    var status : Int
    
    var visited_at : String?
    var estimated_on : String?
    var rating : Int?
    
    var order_date : String
    var didSelect : Bool?
}
struct AddedOptionFile : Codable {
    var id : String?
    var path : String?
    var tags : [String] = ["file"]
    var pathUrl : String?
    var image : Data?
    var fileUrl : String?
}
struct AddedProduct {
    var series : Series?
    var sub_series : Series?
    
    var product : Product?
    var qty: Int = 1
    var spec : Specification?
    var options : [SelectedOption]
    //要加入
    var customOptions : [customForm]
    var fileAssets: [FilePath]?
}
//要加入
struct customForm {
    var id : Int
    var status : Int
    var is_Cancel : Bool
    //var allow_Add : Bool
   
    var statusArray : [String]
    var options : [AddedOption]
}
struct SelectedOption {
    var name : String
    var option : Option?

}

enum AddedMode : Int{
    case Date = 1
    case Custom = 3
    case Extra = 2
}
struct AddedOption {
    var mode : AddedMode
    var text : String
    var amount : Int
    var price : String
    //要加入
    var reply : String
    var id : String
    var files : [AddedOptionFile] = []
    var del_files : [String]?
}


struct Discount {
    var text : String
    var price : Double
}


enum SignitureMode : String{
    case Customer = "CustomerSigniture.jpg"
    case Sales = "SalesSigniture.jpg"
}
struct Signiture {
    var type : SignitureMode?
    var image : UIImage?
}

//要加入
struct interViewInfo {
    var id : String
    var status : Int
    var customer : Customer?
    var location : String
    var visitDate : String
    var orderDate : String
    var notifyDate : NotifyMode?
    var moreInfo : String
    var rating : Int? = nil
    var periods : [String]
    var files : [interViewFile]
}

struct interViewFile {
    var id : String
    var tag : [String]
    var path : String
    var path_url : String
    var image : UIImage?
    var fileUrl : String?
}

extension String {
    func getTimeInterval() -> TimeInterval {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return formatter.date(from: self)?.timeIntervalSince1970 ?? 0
    }
}

extension Date {
    func getCurrentTime() -> String {
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone(identifier: "Asia/Taipei")
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        return formatter.string(from: self)
        
    }
    func convertToDateShortComponent(text:String) -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(identifier: "Asia/Taipei")
        formatter.dateFormat = "yyyy/MM/dd"
        let date = formatter.date(from: text)
        
        if let dt = date {
        let comp = Calendar.current.dateComponents([.year,.month,.day], from: dt)
        
        if let year = comp.year, let month = comp.month, let day = comp.day {
            var timeString = ""
            
            timeString += "\(year)年"
            timeString += "\(month)月"
            timeString += "\(day)日"
            return timeString
        }
        }
        return ""
    }
    func convertToRangeDate(beginDate:Date?,endDate:Date?) -> String{
        
        if let bt = beginDate, let end = endDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd"
            let start = formatter.string(from: bt)
            let endt = formatter.string(from: end)
            
            return "\(start)-\(endt)"
        }
        return ""
    }
    func getStartingInterval() -> TimeInterval {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let str = formatter.string(from: self) + " 00:00:00"
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.date(from: str)?.timeIntervalSince1970 ?? 0
    }
    func getEndInterval() -> TimeInterval {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let str = formatter.string(from: self) + " 23:59:59"
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.date(from: str)?.timeIntervalSince1970 ?? 0
    }
    func convertToDateComponent(text:String,format : String = "yyyy/MM/dd HH:mm:ss",onlyDate:Bool=false) -> String {
        let formatter = DateFormatter()
        //formatter.timeZone = TimeZone(abbreviation: "CTT")
        formatter.dateFormat = format
        let date = formatter.date(from: text)
        if let date = date{
        let comp = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute], from: date)
        
        if let year = comp.year, let month = comp.month, let day = comp.day, var hour = comp.hour, let minute = comp.minute {

            var timeString = ""
            if onlyDate {
                return "\(year)年\(month)月\(day)日"
            }
            hour += 8
           // timeString += "\(year)年"
            
            timeString += "\(month)月"
            timeString += "\(day)日"
            if hour < 12 {
                timeString += String(format: " 上午%02d:%02d",hour,minute)
            }
            else{
                let convertDict = [12:12,13:1,14:2,15:3,16:4,17:5,18:6,19:7,20:8,21:9,22:10,23:11]
                timeString += String(format: " 下午%02d:%02d",convertDict[hour] ?? 0,minute)
            }
            return timeString
        }
        }
        return ""
    }
}



struct InterViewInfos : Codable {
    var data : InterViewInfo?
   
}
struct DetailInterViewInfosArray:Codable {
    var data : [InterViewInfo]
}
enum DifId : Codable {
    case int(Int), string(String)
    init(from decoder: Decoder) throws {
        if let int = try? decoder.singleValueContainer().decode(Int.self) {
            self = .int(int)
            return
        }
        
        if let string = try? decoder.singleValueContainer().decode(String.self) {
            self = .string(string)
            return
        }
        
        throw QuantumError.missingValue
    }
    
    enum QuantumError:Error {
        case missingValue
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .int(let x):
            try container.encode(x)
        }
    }
}
extension DifId {
    var stringValue : String? {
        switch self {
        case .string(let string): return string
        case .int(let int): return String(int)
        }
    }
}
struct InterViewInfo : Codable {
    var id : DifId
    var status : Int
    var date : String?
    var rating : Int?
    var address : String?
    var order_date : String?
    var updated_at : String?
    var account : Customer?
    var description : String?
    var interview_period : [String]?
    var products : [Product]
    var order_reminder_date : String?
    var files : [InterviewFile]?
    var discount_reason : String?
    var discount : String?
    var owner : InterviewOwner
}

struct InterviewOwner: Codable {
    var employee : InterviewEmployee
}
struct InterviewEmployee:Codable {
    var code: String
    var name : String
    var department_code: String
    var department_name: String
}
struct InterviewFile : Codable {
    var id : String?
    var tags : [String]?
    var path : String?
    var path_url : String?
    //var image : UIImage?
}
struct InterViewListData : Codable {
    var data : [InterViewList]
}
struct InterViewList : Codable {
    var id : String
    var status : Int
    var date : String?
    var rating : Int?
    var order_date : String?
    
    var account : Customer?
    var owner: InterviewOwner
    
}

struct InterViewRecordList : Codable {
    var data : [InterViewRecord]
}
struct InterViewRecord : Codable {
    var id : Int
    var call_id : String?
    var source_id : String
    var status : Int
    var date : String?
    var rating : Int?
    var order_date : String?
    var updated_at : String?
    var account : Customer?
    
}
