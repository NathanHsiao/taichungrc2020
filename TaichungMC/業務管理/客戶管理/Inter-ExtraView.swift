//
//  Inter-ExtraView.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/5/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
class sigField : UIView {
    let label : UILabel = {
       let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.3118359745, green: 0.3118857443, blue: 0.3118250668, alpha: 1)
        return label
    }()
    let imageView = UIImageView()
    let addSigButton : UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .black
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        button.setTitle("新增", for: .normal)
        button.layer.cornerRadius = 16.calcvaluey()
        return button
    }()
    let importButton : UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        button.setTitle("匯入", for: .normal)
        button.layer.cornerRadius = 16.calcvaluey()
        return button
    }()
    var mode : InterviewMode?
    var signi : interViewFile? {
        didSet{
            if signi?.image == nil {
                if signi?.id == "" {
                    imageView.isHidden = true
                    if mode == .Editing || mode == .New{
                        addSigButton.isHidden = false
                        importButton.isHidden = false
                    }
                    else{
                        addSigButton.isHidden = true
                        importButton.isHidden = true
                    }
                    
                    
                }
                else{
                    if signi?.path_url != "" , let url = URL(string: signi?.path_url ?? ""){
                        imageView.sd_setImage(with: url, completed: nil)
                    }
                    else{
                        imageView.image = signi?.image
                    }
                    imageView.isHidden = false
                    addSigButton.isHidden = true
                    importButton.isHidden = true
                }
            }
            else{
                imageView.image = signi?.image
                imageView.isHidden = false
                addSigButton.isHidden = true
                importButton.isHidden = true
            }
        }
    }
    let xbutton = UIButton(type: .custom)
    init(text:String) {
        super.init(frame: .zero)
        
        label.text = text
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,size: .init(width: 0, height: 25.calcvaluey()))
        
        addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        imageView.addshadowColor()
        imageView.backgroundColor = .white
        imageView.anchor(top: label.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 14.calcvaluey(), left: 0, bottom: 0, right: 0))
        imageView.isUserInteractionEnabled = true
        xbutton.setImage(#imageLiteral(resourceName: "ic_multiplication"), for: .normal)
        imageView.addSubview(xbutton)
        xbutton.anchor(top: imageView.topAnchor, leading: nil, bottom: nil, trailing: imageView.trailingAnchor,padding: .init(top: 2.calcvaluey(), left: 0, bottom: 0, right: 2.calcvaluex()),size: .init(width: 30.calcvaluex(), height: 30.calcvaluex()))
        imageView.isHidden = true
        addSubview(addSigButton)
        addSigButton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 9.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 32.calcvaluey()))
        addSigButton.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true
        
        addSubview(importButton)
        importButton.anchor(top: addSigButton.topAnchor, leading: addSigButton.trailingAnchor, bottom: addSigButton.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 8.calcvaluex(), bottom: 0, right: 0),size: .init(width: 72.calcvaluex(), height: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class InsertCustomizationView : UIView {
    let nameField = customizeDetailTextField()
    let counter = CounterView(minCount: 1)
    let priceField = customizeDetailTextField()
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(nameField)
        nameField.arrow.isHidden = true
        nameField.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 226.calcvaluex(), height: 52.calcvaluey()))
        nameField.centerYInSuperview()
        nameField.attributedPlaceholder = NSAttributedString(string: "輸入品項",attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
        addSubview(counter)
        counter.anchor(top: nil, leading: nameField.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0),size: .init(width: 114.calcvaluex(), height: 36.calcvaluey()))
        counter.centerYInSuperview()
        addSubview(priceField)
        priceField.arrow.isHidden = true
        priceField.attributedPlaceholder = NSAttributedString(string: "輸入價錢",attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!])
        priceField.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        priceField.textColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        priceField.anchor(top: nameField.topAnchor, leading: counter.trailingAnchor, bottom: nameField.bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0))
        priceField.isUserInteractionEnabled = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class PaddingTextField : UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
}
class customizeDetailTextField : PaddingTextField {
    let arrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 15.calcvaluey()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8869734406, green: 0.8871011138, blue: 0.8869454265, alpha: 1)
        textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
       // text = "交期"
        font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        backgroundColor = #colorLiteral(red: 0.9693912864, green: 0.9695302844, blue: 0.969360888, alpha: 1)
        
        addSubview(arrow)
        arrow.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 10.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        arrow.centerYInSuperview()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class customizingDetailView : UIView,UITextFieldDelegate {
    var secondIndex : Int?
    var mode: InterviewMode?
    var delegate : SelectedMachineCellDelegate?
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == selectionView {
            delegate?.sendCustomChoice(index:self.firstIndex ?? 0,subIndex: self.secondIndex ?? 0,thirdIndex:self.tag,textField:textField)
            return false
        }
        
        if textField == fileAssetView {
            return false
        }
        

        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == dateField {
            delegate?.sendCustomDate(index:self.firstIndex ?? 0,subIndex:self.secondIndex ?? 0,thirdIndex: self.tag,text:textField.text ?? "")
        }
        else if textField == customField.nameField || textField == extraField.nameField{
            delegate?.sendCustomName(index:self.firstIndex ?? 0,subIndex:self.secondIndex ?? 0,thirdIndex: self.tag,text:textField.text ?? "")
        }
        else if textField == customField.priceField || textField == extraField.priceField{
            delegate?.sendCustomPrice(index:self.firstIndex ?? 0,subIndex:self.secondIndex ?? 0,thirdIndex: self.tag,text:textField.text ?? "")
        }
    }
    var addedOptions : AddedOption? {
        didSet{
            if let _ = addedOptions?.files.first {
                fileAssetView.textColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
            }
            else{
                fileAssetView.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
            }
            if addedOptions?.mode == .Custom {
                selectionView.text = "客製"
                customField.isHidden = false
                dateField.isHidden = true
                extraField.isHidden = true
                customField.nameField.delegate = self
                customField.priceField.delegate = self
                
                customField.nameField.text = addedOptions?.text
                if let price = addedOptions?.price, price.doubleValue != 0 {
                    customField.priceField.text = "\(price)"
                }
                customField.counter.currentCount = addedOptions?.amount ?? 0
                customField.counter.delegate = self.delegate
                customField.counter.firstIndex = self.firstIndex
                customField.counter.tag = self.tag
                customField.counter.subIndex = self.secondIndex
                
            }
            else if addedOptions?.mode == .Date
            {
                selectionView.text = "交期"
                customField.isHidden = true
                dateField.isHidden = false
                extraField.isHidden = true
                dateField.delegate = self
                dateField.text = addedOptions?.text
            }
            else{
                selectionView.text = "附件"
                customField.isHidden = true
                dateField.isHidden = true
                extraField.isHidden = false
                
                extraField.nameField.delegate = self
                extraField.priceField.delegate = self
                
                extraField.nameField.text = addedOptions?.text
                if let price = addedOptions?.price, price.doubleValue != 0 {
                    extraField.priceField.text = "\(price)"
                }
                extraField.counter.currentCount = addedOptions?.amount ?? 0
                extraField.counter.firstIndex = self.firstIndex
                extraField.counter.tag = self.tag
                extraField.counter.subIndex = self.secondIndex
            }
        }
    }

    var firstIndex : Int?
    let selectionView = customizeDetailTextField()
    let cancelButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.tintColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return button
    }()
    let fileAssetView = customizeDetailTextField()
    let dateField = customizeDetailTextField()
    let customField = InsertCustomizationView()
    let extraField = InsertCustomizationView()
    var replayField = customizeDetailTextField()
    lazy var assetTap = UITapGestureRecognizer(target: self, action: #selector(addAsset))
    lazy var assetLongTap = UILongPressGestureRecognizer(target: self, action: #selector(removeAsset))
    @objc func addAsset(){
        
        if let firstIndex = firstIndex, let subIndex = secondIndex {
            
            if let st = addedOptions?.files.first {
                delegate?.showCustomAttachFile(file:st)
            }
            else{
                if mode == .New || mode == .Editing{
            delegate?.addCustomFile(index: firstIndex, subIndex: subIndex, thirdIndex: self.tag, textField: fileAssetView)
                }
            }
        }
        
    }
    @objc func removeAsset(sender:UILongPressGestureRecognizer){
        
        if sender.state == .began {
            if let firstIndex = firstIndex, let subIndex = secondIndex , let _ = addedOptions?.files.first,mode == .New || mode == .Editing{
                delegate?.removeCustomFile(index: firstIndex, subIndex: subIndex, thirdIndex: self.tag, textField: fileAssetView,addedFile: self.addedOptions?.files.first ?? nil)
        }
        }
    }
    init(mode: InterviewMode?) {
        super.init(frame: .zero)
        fileAssetView.text = "附檔"
        fileAssetView.arrow.isHidden = true
        fileAssetView.delegate = self
        fileAssetView.addGestureRecognizer(assetTap)
        fileAssetView.addGestureRecognizer(assetLongTap)
        addSubview(fileAssetView)
        fileAssetView.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 90.calcvaluex(), height: 52.calcvaluey()))
        fileAssetView.centerYInSuperview()
        addSubview(selectionView)
        selectionView.anchor(top: nil, leading:  fileAssetView.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 0),size: .init(width: 128.calcvaluex(), height: 52.calcvaluey()))
        selectionView.centerYInSuperview()
        selectionView.delegate = self
        addSubview(cancelButton)
        cancelButton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        cancelButton.centerYInSuperview()
        
        addSubview(dateField)
        dateField.text = ""
        dateField.attributedPlaceholder = NSAttributedString(string: "請告知", attributes: [NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)])
        dateField.arrow.isHidden = true
        if mode == .Review{
            dateField.anchor(top: selectionView.topAnchor, leading: selectionView.trailingAnchor, bottom: selectionView.bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        }
        else{
       dateField.anchor(top: selectionView.topAnchor, leading: selectionView.trailingAnchor, bottom: selectionView.bottomAnchor, trailing: cancelButton.leadingAnchor,padding: .init(top: 0, left: 16.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        }
        
        addSubview(customField)
        customField.isHidden = true
        customField.anchor(top: dateField.topAnchor, leading: dateField.leadingAnchor, bottom: dateField.bottomAnchor, trailing: dateField.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        addSubview(extraField)
        extraField.isHidden = true
        extraField.anchor(top: dateField.topAnchor, leading: dateField.leadingAnchor, bottom: dateField.bottomAnchor, trailing: dateField.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        
        replayField.isUserInteractionEnabled = false
        replayField.arrow.isHidden = true
        addSubview(replayField)
        replayField.anchor(top: selectionView.bottomAnchor, leading: fileAssetView.leadingAnchor, bottom: nil, trailing: dateField.trailingAnchor,padding: .init(top: 8.calcvaluey(), left: 0, bottom: 0, right: 0))
        replayField.heightAnchor.constraint(equalTo: selectionView.heightAnchor).isActive = true
        replayField.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class PaddedLabel : UILabel {
    override var intrinsicContentSize: CGSize {
        let contentsize = super.intrinsicContentSize
        return .init(width: contentsize.width + 64.calcvaluex(), height: contentsize.height)
    }
}



class CounterView : UIView {
    var delegate : SelectedMachineCellDelegate?
    //var editMachinedelegate:EditMachineDelegate?
    let sep1 = UIView()
    
    let sep2 = UIView()
    var firstIndex : Int?
    var subIndex : Int?
    let numberLabel : UITextField = {
       let nL = UITextField()
        //nL.text = "1"
        nL.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        nL.font = UIFont(name: "Roboto-Medium", size: 18.calcvaluex())
        nL.textAlignment = .center
        return nL
    }()
    let addCounterButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("+", for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 22.calcvaluex())
        button.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
        return button
    }()
    let minusCounterButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("-", for: .normal)
        button.titleLabel?.font = UIFont(name: "Roboto-Regular", size: 22.calcvaluex())
        button.setTitleColor(#colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1), for: .normal)
        return button
    }()
    
    var currentCount = 0 {
        didSet{
            if currentCount < minCount {
                currentCount = minCount
            }
            numberLabel.text = "\(currentCount)"
        }
    }
    var minCount = 0
    init(minCount : Int) {
        super.init(frame: .zero)
        self.minCount = minCount
        self.currentCount = minCount
        numberLabel.text = "\(minCount)"
        layer.cornerRadius = 6.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.9008185267, green: 0.9009482265, blue: 0.9007901549, alpha: 1)
        
        sep1.backgroundColor = #colorLiteral(red: 0.9111731052, green: 0.9113041759, blue: 0.9111443758, alpha: 1)
        sep2.backgroundColor = #colorLiteral(red: 0.9111731052, green: 0.9113041759, blue: 0.9111443758, alpha: 1)
        
        addSubview(sep1)
        sep1.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,padding:.init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 1.calcvaluex(), height: 0))
        addSubview(sep2)
        sep2.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,padding:.init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()),size: .init(width: 1.calcvaluex(), height: 0))
        
        addSubview(numberLabel)
        numberLabel.anchor(top: topAnchor, leading: sep1.trailingAnchor, bottom: bottomAnchor, trailing: sep2.leadingAnchor)
        
        addSubview(minusCounterButton)
        minusCounterButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: sep1.leadingAnchor)
        
        addSubview(addCounterButton)
        addCounterButton.anchor(top: topAnchor, leading: sep2.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        
        minusCounterButton.addTarget(self, action: #selector(minusCount), for: .touchUpInside)
        addCounterButton.addTarget(self, action: #selector(addCount), for: .touchUpInside)
    }
    @objc func minusCount(){
        currentCount = currentCount - 1
        
        if currentCount < minCount {
            currentCount = minCount
        }
        
        self.numberLabel.text = "\(currentCount)"
        if let firstIndex = self.firstIndex {
            delegate?.setMachineQty(index: firstIndex,qty:currentCount)
        }
        if let firstIndex = self.firstIndex,let subIndex = self.subIndex {
            delegate?.setCustomCount(index:firstIndex,subIndex:subIndex,thirdIndex: self.tag,amount:currentCount)

        }
        
        
        
    }
    @objc func addCount(){
       
        currentCount = currentCount + 1
        self.numberLabel.text = "\(currentCount)"
       // print(firstIndex,subIndex,delegate)
        if let firstIndex = self.firstIndex {
            delegate?.setMachineQty(index: firstIndex,qty:currentCount)
        }
        if let firstIndex = self.firstIndex,let subIndex = self.subIndex {
            delegate?.setCustomCount(index:firstIndex,subIndex:subIndex,thirdIndex: self.tag,amount:currentCount)
        }
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class addExtraField : UIView {
    let addButton = UIButton(type: .custom)
    let label : UILabel = {
       let lB = UILabel()
        lB.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        lB.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        return lB
    }()
    init(text: String) {
        super.init(frame: .zero)
        label.text = text
        addButton.setImage(#imageLiteral(resourceName: "ic_addition").withRenderingMode(.alwaysTemplate), for: .normal)
        addButton.tintColor = .white
        addButton.backgroundColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        addButton.layer.cornerRadius = 12.calcvaluey()

        addSubview(addButton)
        addButton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 24.calcvaluey(), height: 24.calcvaluey()))
        addButton.centerYInSuperview()
        
        addSubview(label)
        label.anchor(top: nil, leading: addButton.trailingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 6.calcvaluex(), bottom: 0, right: 0))
        label.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
