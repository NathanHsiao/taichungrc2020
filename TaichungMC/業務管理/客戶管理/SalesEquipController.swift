//
//  SalesEquipController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/6/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class SalesEquipController : SampleController {
    let controller = SampleRCTopViewController(bottomView: SelectEquipController(nextvd: SpecRightView(), mode: .Equip))
    let bt = TopViewButton(text: "儲存")
    weak var con : InterviewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "選配"
//        controller.leftviewAnchor?.width?.constant = 361.calcvaluex()
//        self.view.layoutIfNeeded()
      //  controller.arrowimage.isHidden = true
       // controller.selectLabel.isUserInteractionEnabled = false
        view.addSubview(controller.view)
        controller.view.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        addChild(controller)
        
        bt.layer.cornerRadius = 38.calcvaluey()/2
        
       
        topview.addSubview(bt)
        bt.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        
        bt.addTarget(self, action: #selector(saveOptions), for: .touchUpInside)
        
    }
    
    @objc func saveOptions(){
        if let vc = controller.bottomvc as? SelectEquipController {
            con?.setEquipOption(index:self.view.tag,options: vc.optionsArray)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
