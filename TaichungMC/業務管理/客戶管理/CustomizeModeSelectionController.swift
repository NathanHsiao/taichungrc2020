//
//  CustomizeModeSelectionController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/5/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
let statusDict = ["草稿" : 0, "新合審":1,"待回覆":2 ,"待審核":3, "待定價" :4 ,"完成":5, "已取消":6]
let dictStatus = [ 0 : "草稿", 1:"新合審",2:"待回覆", 3:"待審核", 4:"待定價"  ,5:"完成", 6:"已取消"]
let colorStataus = [ "草稿" : #colorLiteral(red: 0.6621296406, green: 0.662226975, blue: 0.6621083617, alpha: 1), "新合審":#colorLiteral(red: 0.03734050319, green: 0.328607738, blue: 0.5746202469, alpha: 1),"待回覆":#colorLiteral(red: 0.1008876637, green: 0.5688499212, blue: 0.5781469941, alpha: 1), "待審核":#colorLiteral(red: 0.9513289332, green: 0.6846997142, blue: 0.1314668059, alpha: 1), "待定價":#colorLiteral(red: 0.2379338741, green: 0.6737315059, blue: 0.9689859748, alpha: 1)  ,"完成":#colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1), "已取消":#colorLiteral(red: 0.4757143259, green: 0.4757863283, blue: 0.4756985307, alpha: 1)]


enum NotifyMode : String {
    case OneDay = "1"
    case ThreeDay = "3"
    case SevenDay = "7"
    
    var info : (code:Int,description:String) {
        switch self{
        case .OneDay:
            return (1,"一天前")
        case .ThreeDay:
            return (3,"三天前")
        case .SevenDay:
            return (7,"七天前")
        }
    }
}
protocol CustomizeModeDelegate {
    func didSelect(mode : AddedMode, index:Int,subIndex:Int,thirdIndex:Int)
    func setCustomer(customer: Customer)
    
    func setCustomStatus(index:Int,subIndex: Int, status : Int)
    func setNotifyDate(notifyMode:NotifyMode)
}
class CustomizeModeSelectionController : UIView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isNotify {
            return notifyarry.count
        }
        if isCustomers {
            return customersList.count
        }
        else if isStatus {
            return statusArray.count
        }
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.selectionStyle = .none
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())

        if isCustomers {
            cell.textLabel?.text = customersList[indexPath.row].name
        }
        else if isNotify {
            cell.textLabel?.text = notifyarry[indexPath.row].info.description
        }
        else if isStatus {
            cell.textLabel?.text = statusArray[indexPath.row]
            cell.textLabel?.textColor = colorStataus[statusArray[indexPath.row]]
        }
        else{
        if array[indexPath.row] == .Date {
            cell.textLabel?.text = "交期"
        }
        else if array[indexPath.row] == .Custom {
            cell.textLabel?.text = "客製"
        }
        else{
            cell.textLabel?.text = "附件"
        }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isCustomers {
            delegate?.setCustomer(customer: customersList[indexPath.row])
        }
        else if isStatus {
            
            delegate?.setCustomStatus(index:self.firstIndex ?? 0,subIndex: self.secondIndex ?? 0, status : statusDict[statusArray[indexPath.row]] ?? 0)
        }
        else if isNotify {
            delegate?.setNotifyDate(notifyMode: self.notifyarry[indexPath.row])
        }
        else{
            delegate?.didSelect(mode: array[indexPath.row], index: self.firstIndex ?? 0, subIndex: self.secondIndex ?? 0,thirdIndex: self.tag)
        }
    }
    var isStatus : Bool = false
    var isNotify : Bool = false

    var statusArray = [String]()
    let tableView = UITableView(frame: .zero, style: .plain)
    var delegate : CustomizeModeDelegate?
    var array : [AddedMode] = [.Date,.Custom,.Extra]
    var notifyarry : [NotifyMode] = [.OneDay,.ThreeDay,.SevenDay]
    var isCustomers : Bool = false
    var customersList = [Customer]()
    var firstIndex : Int?
    var secondIndex : Int?
    override init(frame: CGRect) {
        super.init(frame: frame)
        addshadowColor()
        backgroundColor = .white
        
        addSubview(tableView)
       
        tableView.fillSuperview()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
