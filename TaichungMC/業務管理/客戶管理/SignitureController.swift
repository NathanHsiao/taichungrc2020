//
//  SignitureController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/5/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
extension UIView {

    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
class Canvas : UIView {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard let context = UIGraphicsGetCurrentContext() else {return}
        lines.forEach { (line) in
            for (i,p) in line.enumerated() {
                if i == 0{
                    context.move(to: p)
                }
                else{
                    context.addLine(to: p)
                }
            }
        }
        

        context.setLineWidth(24.calcvaluex())
        context.setLineCap(.round)
        context.strokePath()
    }
    //var line = [CGPoint]()
    
    var lines = [[CGPoint]]()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        lines.append([CGPoint]())
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else {return}
        guard var lastLine = lines.popLast() else {return}
        lastLine.append(point)
        lines.append(lastLine)
        
        setNeedsDisplay()
    }
}
class SignitureController : SampleController {
    let canvas = Canvas()
    var saveIndex : Int?
    weak var con : InterviewController?
    let bt = TopViewButton(text: "儲存")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "簽名"
        
        canvas.backgroundColor = .white
        
        view.addSubview(canvas)
        canvas.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        bt.layer.cornerRadius = 38.calcvaluey()/2
        
       
        topview.addSubview(bt)
        bt.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        
        bt.addTarget(self, action: #selector(saveImage), for: .touchUpInside)
    }
    @objc func saveImage(){
        con?.saveSignitureImage(image: canvas.asImage(), index: saveIndex ?? 0)
        self.dismiss(animated: true, completion: nil)
    }
}
