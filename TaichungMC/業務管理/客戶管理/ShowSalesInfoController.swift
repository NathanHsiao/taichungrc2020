//
//  ShowSalesInfoController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/13.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class ShowSalesInfoController: UIViewController {
    let newVd = UIView()
    let closeButton = UIImageView(image: #imageLiteral(resourceName: "ic_close2"))
    let salesHeader = SalesHeaderView()
    let numberView = SalesFormView(text: "編號")
    let unitView = SalesFormView(text: "單位")
    let mailView = SalesFormView(text: "信箱")
    let accountView = SalesFormView(text: "帳號")
    override func viewDidLoad() {
        super.viewDidLoad()
        newVd.addshadowColor(color: .white)
        newVd.backgroundColor = .white
        view.addSubview(newVd)
        newVd.centerInSuperview(size: .init(width: 573.calcvaluex(), height: 570.calcvaluey()))
        newVd.layer.cornerRadius = 10.calcvaluex()
        
        closeButton.contentMode = .scaleAspectFit
        
        newVd.addSubview(closeButton)
        closeButton.anchor(top: newVd.topAnchor, leading: nil, bottom: nil, trailing: newVd.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluex()))
        closeButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closing)))
        closeButton.isUserInteractionEnabled = true
        
        for (index,i) in [numberView,unitView,mailView,accountView].enumerated() {
            i.constrainWidth(constant: 486.calcvaluex())
            i.constrainHeight(constant: 90.calcvaluey())
            i.textfield.text = ["2488","國內營業","2dasd@gmail.com","23x22"][index]
        }
        salesHeader.constrainHeight(constant: 92.calcvaluey())
        let spacer = UIView()
        let spacer1 = UIView()
        spacer1.constrainHeight(constant: 14.calcvaluey())
        
        let spacer2 = UIView()
        let spacer3 = UIView()
        let spacer4 = UIView()
        for i in [spacer2,spacer3,spacer4] {
            i.constrainHeight(constant: 12.calcvaluey())
        }
        
        
        let stackview = UIStackView(arrangedSubviews: [salesHeader,spacer1,numberView,spacer2,unitView,spacer3,mailView,spacer4,accountView,spacer])
        stackview.axis = .vertical
        stackview.alignment = .center
        stackview.distribution = .fill
        newVd.addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 34.calcvaluey(), left: 0, bottom: 0, right: 0))
        
    }
    @objc func closing(){
        self.dismiss(animated: false, completion: nil)
    }
}
