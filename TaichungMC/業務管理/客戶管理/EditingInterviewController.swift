//
//  EditingInterviewController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
var textViewInfoWidth = UIScreen.main.bounds.width - 366.calcvaluex()
var textViewDiscountInfoWidth = UIScreen.main.bounds.width - 520.calcvaluex()
var textViewCustomerDetailWidth = (UIScreen.main.bounds.width/2) - 91.calcvaluex()
protocol LocationDelegate {
    func gotoLocation(text:String?)
}
class LocationNormalFormView : InterviewNormalFormView,UITextFieldDelegate{
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        delegate.gotoLocation()
//        return true
//    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        delegate.gotoLocation(text:textField.text)
//    }
    //var delegate:LocationDelegate!
    override init(text: String, placeholdertext: String? = nil) {
        super.init(text: text, placeholdertext: placeholdertext)
        //textfield.delegate = self
        textfield.tintColor = .clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
protocol EditingInterviewDelegate {
    //func setCustomerName(text:String)
    func setOrderDate(text:String)
    func setInfo(text:String)
    func goSelectFile(sender:UIButton)
    func removeFile(index:Int)
    
    func chooseCustomers(textField:UITextField)
    func setVisitDate(text:String)
    func chooseNotifyDate(textField:UITextField)
    func sendRating(text:String)
    func sendLocation(text:String)
    func sendPeriodArray(array:[String])
    func showAttachFile(file:interViewFile)
}
class FileCollecitionViewCell : UICollectionViewCell {
    let imageview = UIImageView()
    let xbutton = UIButton(type: .custom)
    var delegate : EditingInterviewDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        

        addSubview(imageview)
        imageview.contentMode = .scaleAspectFill
        imageview.clipsToBounds = true
        imageview.fillSuperview()
        
        addSubview(xbutton)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close_small").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .white
        xbutton.anchor(top: imageview.topAnchor, leading: nil, bottom: nil, trailing: imageview.trailingAnchor,size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        xbutton.addTarget(self, action: #selector(removeFile), for: .touchUpInside)
    }
    @objc func removeFile(){
        delegate?.removeFile(index: self.tag)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol ChangingTextViewHeightProtocol {
    func goChangingHeight()
}
class EditingInterviewCell: InterviewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,FormTextViewDelegate,PeriodViewDelegate{
    func textViewReloadHeight(height: CGFloat) {
        moreInfoHeightContraint?.constant = height + 30.5.calcvaluey()
        textviewDelegate?.goChangingHeight()
    }
    
    func sendTextViewData(mode: FormTextViewMode, text: String) {
        self.delegate?.setInfo(text: text)
    }
    
    func sendPeriodArray(array: [String]) {
        delegate?.sendPeriodArray(array: array)
    }
    
    var textviewWidth : CGFloat = 0
    var textviewDelegate:ChangingTextViewHeightProtocol?
    func textViewDidChange(_ textView: UITextView) {
//        if mode != .Editing {
//            textView.textContainerInset.bottom = 16.calcvaluey()
//        }
//        else{
//            textView.textContainerInset.bottom = 0.calcvaluey()
//        }
        
        if textView.text == "" {
            moreInfoHeightContraint?.constant = 80.calcvaluey()
        }
        else{
            
            //textViewDidEndEditing(textView)
            var width : CGFloat = 0
            if textviewWidth == 0{
                width = textView.frame.width
            }
            else{
                width = textviewWidth
            }
            let size = CGSize(width: width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        print(estimatedSize)
            moreInfoHeightContraint?.constant = estimatedSize.height + 30.5.calcvaluey()
        }
        textviewDelegate?.goChangingHeight()
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
//        if textField == customerlabel.textfield {
//            delegate?.setCustomerName(text: textField.text ?? "")
//        }
        
//        if textField == moreInfo.textfield {
//            delegate?.setInfo(text: textField.text ?? "")
//        }
        
        if textField == ratinglabel.textfield {
            if let ts = textField.text, ts != ""{
                self.delegate?.sendRating(text: ts)
            }
            
        }
        if textField == locationlabel.textfield {
            if let ts = textField.text, ts != ""{
                self.delegate?.sendLocation(text: ts)
            }
            
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == customerlabel.textfield {
            delegate?.chooseCustomers(textField: textField)
            return false
        }
        if textField == notifyDate.textfield {
            delegate?.chooseNotifyDate(textField:textField)
            return false
        }
        
    return true
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return info?.files.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(778,info?.files[indexPath.item])
        if let file = info?.files[indexPath.item] {
            delegate?.showAttachFile(file: file)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cc", for: indexPath) as! FileCollecitionViewCell
        if mode == .Editing || mode == .New{
            cell.xbutton.isHidden = false
        }
        else{
            cell.xbutton.isHidden = true
        }
        if info?.files[indexPath.item].path_url != "",let url = URL(string: info?.files[indexPath.item].path_url ?? "") {
            if url.pathExtension == "mp4" {
                cell.imageview.image = Thumbnail().getThumbnailImage(forUrl: url)
            }
            else if url.pathExtension == "pdf" {
                cell.imageview.image = Thumbnail().pdfThumbnail(url: url)
            }
            else{
            cell.imageview.sd_setImage(with: url, completed: nil)
            }
        }
        else{
            cell.imageview.image = info?.files[indexPath.item].image
        }
        
        cell.tag = indexPath.item
        cell.delegate = self.delegate
        //cell.backgroundColor = .clear
 
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 125.calcvaluex(), height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12.calcvaluex()
    }
    let customerlabel = InterviewNormalFormView(text: "客戶名稱",placeholdertext: "請選擇客戶")
    let locationlabel = LocationNormalFormView(text: "訪談地點")
    let interviewdatelabel = InterviewDateFormView(text: "拜訪日期",placeholdertext: "請選擇拜訪日期",formatter:"yyyy/MM/dd",mode: "visit")
    let periodView = PeriodView()
    let ordatedatelabel = InterviewDateFormView(text: "預計下單日",placeholdertext: "請選擇時間")
    let moreInfo = NormalTextView(text: "備註",placeholdertext: "請輸入備註")
    let notifyDate = InterviewNormalFormView(text: "提醒日期 (由預計下單日推算)",placeholdertext: "請選擇時間")
    let ratinglabel = InterviewNormalFormView(text: "預估成交率",placeholdertext: "請輸入成交率")
    let addFile = AddFileButton2()
    var delegate:EditingInterviewDelegate? {
        didSet{
            self.ordatedatelabel.delegate = self.delegate
            self.interviewdatelabel.delegate = self.delegate
        }
    }
    var moreInfoHeightContraint : NSLayoutConstraint?
    func setEnable(set:Bool)
    {
        
        customerlabel.isUserInteractionEnabled = set
        locationlabel.isUserInteractionEnabled = set
        interviewdatelabel.isUserInteractionEnabled = set
        ordatedatelabel.isUserInteractionEnabled = set
        moreInfo.isUserInteractionEnabled = set
        notifyDate.isUserInteractionEnabled = set
        addFile.isHidden = !set
        ratinglabel.isUserInteractionEnabled = set
        periodView.isUserInteractionEnabled = set

       
    }
    
    var mode : InterviewMode? {
        didSet{
            if mode == .Editing || mode == .New {
                setEnable(set: true)
            }
            else{
                setEnable(set: false)
            }
        }
    }
    var info : interViewInfo? {
        didSet{
            
            locationlabel.textfield.text = info?.location
            interviewdatelabel.textfield.text = Date().convertToDateComponent(text: info?.visitDate ?? "",format: "yyyy/MM/dd",onlyDate: true)
            customerlabel.textfield.text = info?.customer?.name
            ordatedatelabel.textfield.text = Date().convertToDateShortComponent(text: info?.orderDate ?? "")
            self.layoutSubviews()
            moreInfo.textfield.text = info?.moreInfo
            moreInfo.textfield.textViewDidChange(moreInfo.textfield)
            notifyDate.textfield.text = info?.notifyDate?.info.description
            if info?.files.count != 0{
                extracollect.isHidden = false
            }
            
            if let rating = info?.rating {
                ratinglabel.textfield.text = "\(rating)"
            }
            else{
                ratinglabel.textfield.text = nil
            }
            
            periodView.selectedPeriodArray = info?.periods ?? []
           
        }
    }
    var stackview = UIStackView()
    let extracollect = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        customerlabel.makeImportant()
        let hstack = UIStackView(arrangedSubviews: [customerlabel,locationlabel])
        hstack.spacing = 12.calcvaluex()
        hstack.axis = .horizontal
        hstack.distribution = .fillEqually
        hstack.constrainHeight(constant: 80.calcvaluey())
        
        let hstack1 = UIStackView(arrangedSubviews: [interviewdatelabel,ordatedatelabel,notifyDate])
        hstack1.spacing = 12.calcvaluex()
        hstack1.axis = .horizontal
        hstack1.distribution = .fillEqually
        hstack1.constrainHeight(constant: 80.calcvaluey())
        stackview = UIStackView(arrangedSubviews: [hstack,hstack1,periodView,ratinglabel,moreInfo,addFile,extracollect])
        periodView.delegate = self
        periodView.constrainHeight(constant: 80.calcvaluey())
        ratinglabel.constrainHeight(constant: 80.calcvaluey())
        moreInfoHeightContraint = moreInfo.heightAnchor.constraint(equalToConstant: 80.calcvaluey())
        moreInfoHeightContraint?.isActive = true
        //moreInfo.constrainHeight(constant: 80.calcvaluey())
//        moreInfo.setContentHuggingPriority(.defaultLow, for: .vertical)
        addFile.constrainHeight(constant: 48.calcvaluey())
        extracollect.constrainHeight(constant: 86.calcvaluey())
        stackview.axis = .vertical
        stackview.distribution = .fillProportionally
        stackview.spacing = 20.calcvaluey()
        container.addSubview(stackview)
        stackview.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 26.calcvaluey(), right: 36.calcvaluex()))

        //container.addSubview(addFile)
        addFile.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addFile.image.image = #imageLiteral(resourceName: "ic_attach_file_message").withRenderingMode(.alwaysTemplate)
        addFile.titles.text = "附加檔案(最多上傳六個檔案)"
        addFile.titles.textColor = .white
        addFile.tintColor = .white
        addFile.layer.cornerRadius = 48.calcvaluey()/2
        
//        addFile.anchor(top: stackview.bottomAnchor, leading: stackview.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 21.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 694.calcvaluex(), height: 48.calcvaluey()))
        
       // container.addSubview(extracollect)
        extracollect.backgroundColor = .clear
        (extracollect.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        extracollect.isHidden = true
//        extracollect.anchor(top: addFile.bottomAnchor, leading: addFile.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 86.calcvaluey()))
        extracollect.delegate = self
        extracollect.dataSource = self
        extracollect.alwaysBounceHorizontal = true
        extracollect.register(FileCollecitionViewCell.self, forCellWithReuseIdentifier: "cc")
        extracollect.showsHorizontalScrollIndicator = false
        customerlabel.textfield.delegate = self
        moreInfo.textfield.t_delegate = self
        notifyDate.textfield.delegate = self
        locationlabel.textfield.delegate = self
        addFile.addTarget(self, action: #selector(addingFile), for: .touchUpInside)
        ratinglabel.textfield.delegate = self
        
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        //textViewDidChange(self.moreInfo.textfield)
        //textViewDidChange(self.moreInfo.textfield)
    }
    @objc func addingFile(sender:UIButton){
        delegate?.goSelectFile(sender:sender)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol PeriodViewDelegate {
    func sendPeriodArray(array:[String])
}
class PeriodView : UIView {
    let tlabel = UILabel()
    let stackview = UIStackView()
    var periodsarray = ["8:00 ~ 10:00","10:00 ~ 12:00","13:00 ~ 15:00","15:00 ~ 17:00"]
    var selectedPeriodArray = [String]() {
        didSet{
            for i in stackview.arrangedSubviews {
                if let ast = i as? periodStackCell {
                    if selectedPeriodArray.contains(ast.label.text ?? "") {
                        ast.checkbox.isSelected = true
                    }
                }
            }
        }
    }
    var delegate : PeriodViewDelegate?
    @objc func goCheck(sender:UIButton){
        if let st = stackview.arrangedSubviews[sender.tag] as? periodStackCell {
            st.checkbox.isSelected = !st.checkbox.isSelected
            if st.checkbox.isSelected {
                selectedPeriodArray.append(st.label.text ?? "")
            }
            else{
                selectedPeriodArray = selectedPeriodArray.filter({ (sta) -> Bool in
                    return sta != st.label.text
                })
            }
            delegate?.sendPeriodArray(array: selectedPeriodArray)
            
        }
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        addSubview(tlabel)
        tlabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        tlabel.text = "拜訪時間"
        tlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        tlabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,size: .init(width: 0, height: 16.calcvaluey()))
        
        addSubview(stackview)
        

        stackview.distribution = .fillEqually
        
        stackview.spacing = 24.calcvaluex()
        stackview.axis = .horizontal
        stackview.anchor(top: tlabel.bottomAnchor, leading: tlabel.leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        
        for (index,i) in periodsarray.enumerated() {
            let vt = periodStackCell()
            
            vt.label.text = i
            vt.checkbox.tag = index
            //vt.checkbox.isUserInteractionEnabled = false
            //vt.checkbox.isSelected = true
            
                vt.checkbox.addTarget(self, action: #selector(goCheck), for: .touchUpInside)
            
            vt.checkbox.isUserInteractionEnabled = self.isUserInteractionEnabled
            //vt.checkbox.addTarget(self, action: #selector(goCheck), for: .touchUpInside)
            vt.constrainHeight(constant: 50.calcvaluey())
            stackview.addArrangedSubview(vt)
        }

    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class periodStackCell : UIView {
    var label : UILabel = {
       let label = UILabel()
        label.text = "8:00 ~ 10:00"
        label.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.6026002765, green: 0.602689445, blue: 0.6025807261, alpha: 1)
        return label
    }()
    var checkbox : UIButton = {
        let check = UIButton(type: .custom)
        check.setImage(#imageLiteral(resourceName: "btn_check_box_normal"), for: .normal)
        check.setImage(#imageLiteral(resourceName: "btn_check_box_pressed"), for: .selected)
        return check
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        //backgroundColor = .green
        addSubview(label)
        label.anchor(top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor)
        
        addSubview(checkbox)
        checkbox.anchor(top: nil, leading: nil, bottom: nil, trailing: label.leadingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 0),size: .init(width: 40.calcvaluex(), height: 40.calcvaluex()))
        checkbox.centerYInSuperview()
        //checkbox.addTarget(self, action: #selector(goCheck), for: .touchUpInside)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class EditingMoreTextView : MoreInfoView {
    let editingtextview = MoreInfoTextField()
    override init(text: String) {
        super.init(text: text)
        
        textfield.removeFromSuperview()
        
        addSubview(editingtextview)
        editingtextview.anchor(top: tlabel.bottomAnchor, leading: tlabel.leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 52.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol EditMachineDelegate {
    func goAddMachine()
}
class EditingMachineCell:InterviewCell {
    let addMachine = AddButton4()
    var delegate: EditMachineDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(addMachine)
        addMachine.titles.text = "加入機台"
        addMachine.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0),size: .init(width: 180.calcvaluex(), height: 38.calcvaluey()))
        addMachine.layer.cornerRadius = 38.calcvaluey()/2
        addMachine.centerYInSuperview()
        addMachine.addTarget(self, action: #selector(goAdd), for: .touchUpInside)
    }
    @objc func goAdd(){
        delegate?.goAddMachine()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol SelectedMachineCellDelegate{
    func showFile(filePaths:[FilePath])
    func setNewOption(index:Int)
    func setNewCustomOption(index:Int,subIndex:Int)
    func removeOption(index:Int,subIndex:Int)
    
    func removeCustom(index:Int,subIndex:Int,thirdIndex:Int)
    
    func removeMachine(index:Int)
    func sendCustomChoice(index:Int,subIndex:Int,thirdIndex:Int,textField:UITextField)
    func addCustomFile(index:Int,subIndex:Int,thirdIndex:Int,textField:UITextField)
    func removeCustomFile(index:Int,subIndex:Int,thirdIndex:Int,textField:UITextField,addedFile:AddedOptionFile?)
    func setNewDiscount()
    func removeDiscount(index:Int)
    
    func sendCustomDate(index:Int ,subIndex:Int,thirdIndex:Int,text:String)
    
    func sendCustomName(index:Int,subIndex:Int,thirdIndex:Int,text:String)
    func sendCustomPrice(index:Int,subIndex:Int,thirdIndex:Int,text:String)
    func setCustomCount(index:Int,subIndex:Int,thirdIndex:Int,amount:Int)
    //要加入
    func addNewCustomForm(index:Int)
    
    func modifyCustomStatus(index:Int,subIndex:Int,textField:UITextField)
    func setMachineQty(index:Int,qty:Int)
    func showCustomAttachFile(file:AddedOptionFile)
}
//要加入
class MoreCustomView : UIView {
    let addButton = addExtraField(text: "增加合審單")
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(addButton)
        addButton.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()),size: .init(width: 0, height: 25.calcvaluey()))
        addButton.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class SelectedMachineCell : InterviewCell {
    var delegate : SelectedMachineCellDelegate? {
        didSet{
            machineView.counter.delegate = self.delegate
            equipView.delegate = self.delegate
            customView.delegate = self.delegate
            
        }
    }
    func setEnable(set:Bool) {
        equipView.set = set
        machineView.cancelButton.isHidden = set
        equipView.addField.isHidden = set
        machineView.isUserInteractionEnabled = !set
        if set{
            
            machineView.cancelButtonAnchor?.width?.constant = 0.calcvaluex()
            machineView.cancelButtonAnchor?.trailing?.constant = 5.calcvaluex()
        }
        else{
            machineView.cancelButtonAnchor?.width?.constant = 36.calcvaluex()
            machineView.cancelButtonAnchor?.trailing?.constant = -36.calcvaluex()
        }
       
        //customView.addField.isHidden = set
       // addMoreCustomView.isHidden = set
    }
    var mode : InterviewMode? {
        didSet{
            if mode == .Editing || mode == .New{
                setEnable(set: false)
            }
            else{
                setEnable(set: true)
            }
        }
    }
    let machineView = selectedMachineView()
    let equipView = selectedEquipView()
    let customView = addedCustomView()
    //要加入
    let addMoreCustomView = MoreCustomView()
    
    var addedProduct: AddedProduct? {
        didSet{
            
            equipView.mode = self.mode
            
            //customView.mode = self.mode
            machineView.counter.currentCount = addedProduct?.qty ?? 1
            machineView.machineLabel.text = self.addedProduct?.product?.full_name
            let total_price = (self.addedProduct?.product?.getProductsPrices()?.price ?? 0.0) * Double(addedProduct?.qty ?? 1)
            machineView.priceLabel.text = "$\(total_price)"
            equipView.selectedOptions = addedProduct?.options ?? []
            
          //  customView.addedCustomOptions = addedProduct?.customOptions ?? []
            
            //要加入
            if mode == .Editing {
                if (addedProduct?.customOptions.last?.status == 0 || addedProduct?.customOptions.last?.status == 6 || addedProduct?.customOptions.last?.status == -1) {
                    machineView.cancelButton.isHidden = false
                    machineView.counter.isUserInteractionEnabled = true
                    equipView.showing = true
                }
                else{
                    machineView.cancelButton.isHidden = true
                    machineView.counter.isUserInteractionEnabled = false
                    equipView.showing = false
                }
            }
            
            if mode == .Editing && (addedProduct?.customOptions.last?.is_Cancel == true){
                
                addMoreCustomView.isHidden = false
            }
            else{
                addMoreCustomView.isHidden = true
            }
            
            if addedProduct?.customOptions.count == 0{
                equipView.seperator.isHidden = true
            }
            else{
                equipView.seperator.isHidden = false
            }
            
            if stackview.arrangedSubviews.count == addedProduct?.customOptions.count {
                print(1117)
                return
            }
            stackview.safelyRemoveArrangedSubviews()
            
            for (index,i) in (addedProduct?.customOptions ?? []).enumerated() {
               
                let vd = addedCustomView()
                if i.status == -1{
                    vd.modeView.isHidden = true
                }
                else{
                    vd.modeView.isHidden = false
                }
                if addedProduct?.fileAssets?.count == 0 {
                    vd.fileButton.isHidden = true
                }
                else{
                    vd.fileButton.isHidden = false
                }
                    //                if i.replay != "" {
//                    vd.replayField.text = "回覆 : \(i.replay)"
//                    vd.replayFieldHeightAnchor?.constant = 52.calcvaluey()
//                    vd.replayFieldAnchor?.bottom?.constant = -26.calcvaluey()
//                    vd.replayField.isHidden = false
//                }
//                else{
//                    vd.replayFieldHeightAnchor?.constant = 0
//                    vd.replayFieldAnchor?.bottom?.constant = 0
//                    vd.replayField.isHidden = true
//                }
                vd.status = i.status
                vd.isCancel = i.is_Cancel
                if index == ((addedProduct?.customOptions.count ?? 0) - 1){
                    vd.seperator.isHidden = true
                }
                else{
                    vd.seperator.isHidden = false
                }
                if mode == .New {
                    vd.addField.isHidden = false
                }
                else{
                    if (i.status == 0 || i.status == -1) && mode == .Editing{
                        vd.addField.isHidden = false
                        
                    }
                    else{
                        vd.addField.isHidden = true
                    }
                }

                vd.firstIndex = self.tag
                vd.tag = index
                vd.delegate = self.delegate
                vd.mode = self.mode
                vd.modeView.text = dictStatus[i.status]
                vd.modeView.textColor = colorStataus[dictStatus[i.status] ?? "草稿"]
                vd.addedCustomOptions = i.options
                vd.addField.addButton.tag = index
                vd.addField.addButton.addTarget(self, action: #selector(addNewCustomField), for: .touchUpInside)
                vd.fileButton.addTarget(self, action: #selector(showFile), for: .touchUpInside)
                vd.addField.label.tag = index
                vd.addField.label.isUserInteractionEnabled = true
                vd.addField.label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addCustomField2)))
                stackview.addArrangedSubview(vd)
            }
        }
    }
    @objc func showFile(){
        delegate?.showFile(filePaths: addedProduct?.fileAssets ?? [])
    }
    @objc func addCustomField2(gesture:UITapGestureRecognizer) {
        if let view = gesture.view {
            delegate?.setNewCustomOption(index: self.tag,subIndex:view.tag)
        }
    }
    let stackview = UIStackView()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let mm_stackview = UIStackView()
        mm_stackview.distribution = .fill
        mm_stackview.axis = .vertical
        contentView.addSubview(mm_stackview)
        machineView.constrainHeight(constant: 95.calcvaluey())
        addMoreCustomView.constrainHeight(constant: 80.calcvaluey())
        mm_stackview.addArrangedSubview(machineView)
        mm_stackview.addArrangedSubview(equipView)
        mm_stackview.addArrangedSubview(stackview)
        mm_stackview.addArrangedSubview(addMoreCustomView)
        
        mm_stackview.fillSuperview()
        //customView.backgroundColor = .green
//        contentView.addSubview(machineView)
//        machineView.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor,size: .init(width: 0, height: 95.calcvaluey()))
//        contentView.addSubview(equipView)
//
//        equipView.anchor(top: machineView.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor)
//        // 要加入
//        contentView.addSubview(stackview)
//        stackview.anchor(top: equipView.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor)
        stackview.axis = .vertical
       
      //  stackview.addArrangedSubview(customView)
//        contentView.addSubview(customView)
//        customView.anchor(top: equipView.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: contentView.trailingAnchor)
        //要加入
        //customView.seperator.isHidden = true
        machineView.cancelButton.addTarget(self, action: #selector(removeMachine), for: .touchUpInside)
        equipView.addField.label.isUserInteractionEnabled = true
        equipView.addField.label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addNewOptionField)))
        equipView.addField.addButton.addTarget(self, action: #selector(addNewOptionField), for: .touchUpInside)
        
        customView.addField.addButton.addTarget(self, action: #selector(addNewCustomField), for: .touchUpInside)
        
        //要加入
        //addMoreCustomView.backgroundColor = .red
//        contentView.addSubview(addMoreCustomView)
//        addMoreCustomView.anchor(top: stackview.bottomAnchor, leading: stackview.leadingAnchor, bottom: contentView.bottomAnchor, trailing: stackview.trailingAnchor,size: .init(width: 0, height: 80.calcvaluey()))
        //addMoreCustomView.backgroundColor = .red
        addMoreCustomView.addButton.label.isUserInteractionEnabled = true
        addMoreCustomView.addButton.label.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addingNewCustom2)))
        addMoreCustomView.addButton.addButton.addTarget(self, action: #selector(addingNewCustom), for: .touchUpInside)
        
    }
    //要加入
    @objc func addingNewCustom2(gesture : UITapGestureRecognizer) {
        delegate?.addNewCustomForm(index:self.tag)
    }
    @objc func addingNewCustom(){
        delegate?.addNewCustomForm(index:self.tag)
        //stackview.addArrangedSubview(addedCustomView())
    }
    @objc func removeMachine(){
        delegate?.removeMachine(index: self.tag)
    }
    @objc func addNewOptionField(){
        delegate?.setNewOption(index: self.tag)
    }
    @objc func addNewCustomField(sender:UIButton){
        delegate?.setNewCustomOption(index: self.tag,subIndex:sender.tag)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class DiscountTableViewCell : InterviewCell {
    let discountv = discountCustomView()
    var delegate : SelectedMachineCellDelegate? {
        didSet{
            discountv.delegate = self.delegate
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        container.addSubview(discountv)
        discountv.fillSuperview()
        discountv.seperator.isHidden = true
        discountv.addField.addButton.addTarget(self, action: #selector(addDiscount), for: .touchUpInside)
    }
    @objc func addDiscount(){
        delegate?.setNewDiscount()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol SignitureProtocol {
    func goSigniture(index:Int)
    func removeSigniture(index:Int)
}
class SignitureCell : InterviewCell {
    let goToSign = UIButton(type: .custom)
    var delegate : SignitureProtocol?
    var mode : InterviewMode? {
        didSet{
            if mode == .Editing || mode == .New{
                customerField.xbutton.isHidden = false
                //salesField.xbutton.isHidden = false
            }
            else{
                customerField.xbutton.isHidden = true
               // salesField.xbutton.isHidden = true
            }
        }
    }
    var customerField = sigField(text: "客戶簽名")
    //var salesField = sigField(text: "業務承辦簽名")
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        container.addSubview(customerField)
        customerField.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 34.calcvaluey(), left: 26.calcvaluex(), bottom: 34.calcvaluey(), right: 0),size: .init(width: 170.calcvaluex(), height: 0))
        customerField.xbutton.tag = 0
        customerField.xbutton.addTarget(self, action: #selector(removeSig), for: .touchUpInside)
        customerField.addSigButton.tag = 0
        customerField.addSigButton.addTarget(self, action: #selector(goSign), for: .touchUpInside)
        
//        container.addSubview(salesField)
//        salesField.anchor(top: container.topAnchor, leading: customerField.trailingAnchor, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 34.calcvaluey(), left: 10.calcvaluex(), bottom: 34.calcvaluey(), right: 0),size: .init(width: 170.calcvaluex(), height: 0))
//        salesField.xbutton.tag = 1
//        salesField.xbutton.addTarget(self, action: #selector(removeSig), for: .touchUpInside)
//        salesField.addSigButton.tag = 1
//        salesField.addSigButton.addTarget(self, action: #selector(goSign), for: .touchUpInside)
    }
    @objc func removeSig(sender:UIButton)
    {
        
        delegate?.removeSigniture(index:sender.tag)
    }
    @objc func goSign(sender:UIButton){
        delegate?.goSigniture(index:sender.tag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
