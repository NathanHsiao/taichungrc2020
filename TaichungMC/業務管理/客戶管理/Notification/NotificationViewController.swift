//
//  NotificationViewController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/30/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
class RoundShadowView: UIView {
  
    let containerView = UIView()
    let cornerRadius: CGFloat = 15.calcvaluex()
  
    override init(frame: CGRect) {
        super.init(frame: frame)

        layoutView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func layoutView() {
      
      // set the shadow of the view's layer
      addshadowColor()
        
      // set the cornerRadius of the containerView's layer
      containerView.layer.cornerRadius = cornerRadius
      containerView.layer.masksToBounds = true
        containerView.backgroundColor = .white
      addSubview(containerView)
      
      //
      // add additional views to the containerView here
      //
      
      // add constraints
      containerView.translatesAutoresizingMaskIntoConstraints = false
      
      // pin the containerView to the edges to the view
      containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
      containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
      containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
      containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
protocol underLabelDelegate {
    func didTapTab(index:Int)
}
class underLineLabel : UILabel {
    var delegate:underLabelDelegate?

    let underline = UIView()
    var isSelected = false {
        didSet{
            if isSelected {
                isSelectedChange()
            }
            else{
                unSelectedChange()
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
       
        underline.backgroundColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        underline.layer.cornerRadius = 3.calcvaluey()
        addSubview(underline)
        underline.anchor(top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 15.calcvaluex(), bottom: -3.calcvaluey(), right: 15.calcvaluex()),size: .init(width: 0, height: 6.calcvaluey()))
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTap)))
    }
    @objc func didTap(){
        delegate?.didTapTab(index: self.tag)
    }
    func isSelectedChange(){
        font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        textColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        underline.isHidden = false
    }
    func unSelectedChange(){
        font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        underline.isHidden = true
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
enum NotificationMode : Int{
    case Employee = 0
    case History = 1
}
class NotificationViewController:SampleController,underLabelDelegate {
    var interview_id : String?
    func didTapTab(index: Int) {
        if let c_mode = NotificationMode(rawValue: index) {
        
        if mode != c_mode {
            searchTextField.text = nil
            if c_mode == .Employee{
                employeeButton.isSelected = true
                historyButton.isSelected = false
                createData(original_data: original_data)
            }
            else{
                employeeButton.isSelected = false
                historyButton.isSelected = true
                
                let sr = original_data.filter { (emp) -> Bool in
                    return history_array.contains { (ee) -> Bool in
                        return emp.employee_code == ee.code
                    }
                }
                
                createData(original_data: sr)
            }
            //let myRange: NSRange = .init()
//            textField(searchTextField, shouldChangeCharactersIn: myRange, replacementString: "")
            mode = c_mode
        }
        }
    }
    var mode : NotificationMode = .Employee
    let topSelectionView = UIView()
    let employeeButton = underLineLabel()
    let historyButton = underLineLabel()
    var departmentTitle : UILabel = {
        let label = UILabel()
        label.text = "部門"
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        return label
    }()
    
    var employeeTitle : UILabel = {
        let label = UILabel()
        label.text = "人員"
        label.font = UIFont(name: "Roboto-Light", size: 18.calcvaluex())
        label.textColor = #colorLiteral(red: 0.1323070228, green: 0.0926380828, blue: 0.08122352511, alpha: 1)
        return label
    }()
    
    var container = UIView()
    let searchTextField = SearchTextField()
    let departmentTableView = UITableView(frame: .zero, style: .grouped)
    let employeeTableView = UITableView(frame: .zero, style: .grouped)
    var departmentIndex = 0
    var notifyButton : UIButton = {
        let notify = UIButton(type: .custom)
        notify.backgroundColor = .black
        notify.setTitleColor(.white, for: .normal)
        
        notify.setTitle("通知", for: .normal)
        notify.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        return notify
    }()
    let bottomNotificationView = UIView()
    let employeeContentView = RoundShadowView()
    var selectedNotifyUser = [NotificationEmployer]()
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "選擇通知對象"
        topSelectionView.backgroundColor = .white
        topSelectionView.addshadowColor()
        view.addSubview(topSelectionView)
        topSelectionView.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 65.calcvaluey()))
        employeeButton.delegate = self
        historyButton.delegate = self
        employeeButton.tag = 0
        employeeButton.text = "選擇人員"
        employeeButton.isSelected = true
        historyButton.tag = 1
        historyButton.text = "歷史人員"
        historyButton.isSelected = false
        view.addSubview(employeeButton)
        employeeButton.anchor(top: topSelectionView.topAnchor, leading: view.leadingAnchor, bottom: topSelectionView.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 82.calcvaluex(), bottom: 0, right: 0))
        
        view.addSubview(historyButton)
        historyButton.anchor(top: employeeButton.topAnchor, leading: employeeButton.trailingAnchor, bottom: employeeButton.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 76.calcvaluex(), bottom: 0, right: 0))
        
        view.addSubview(container)
        container.anchor(top: topSelectionView.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 22.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        container.addSubview(departmentTitle)
        departmentTitle.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 27.calcvaluex(), bottom: 0, right: 0))
        
        container.addSubview(employeeTitle)
        employeeTitle.anchor(top: departmentTitle.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 403.calcvaluex(), bottom: 0, right: 0))
        
        container.addSubview(searchTextField)
        searchTextField.anchor(top: departmentTitle.bottomAnchor, leading: departmentTitle.leadingAnchor, bottom: nil, trailing: employeeTitle.leadingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 18.calcvaluex()),size: .init(width: 0, height: 46.calcvaluey()))
        searchTextField.layer.cornerRadius = 46.calcvaluey()/2
        searchTextField.addshadowColor()
        searchTextField.backgroundColor = .white
        searchTextField.delegate = self
        searchTextField.attributedPlaceholder = NSAttributedString(string: "搜尋部門或姓名", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)])
        
        container.addSubview(departmentTableView)
        departmentTableView.anchor(top: searchTextField.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: employeeTitle.leadingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        departmentTableView.showsVerticalScrollIndicator = false
        departmentTableView.delegate = self
        departmentTableView.dataSource = self
        departmentTableView.separatorStyle = .none
        departmentTableView.backgroundColor = .clear
        departmentTableView.contentInset.bottom = 80.calcvaluey()
        
        container.addSubview(employeeContentView)
        employeeContentView.anchor(top: employeeTitle.bottomAnchor, leading: employeeTitle.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()))
        //employeeContentView.backgroundColor = .white
        
        employeeContentView.addSubview(employeeTableView)
        
        employeeTableView.fillSuperview(padding: .init(top: 4.calcvaluex(), left: 4.calcvaluex(), bottom: 4.calcvaluex(), right: 4.calcvaluex()))
        employeeTableView.contentInset.top = 20.calcvaluey()
        employeeTableView.contentOffset = .init(x: 0, y: -20.calcvaluey())
        employeeTableView.separatorStyle = .none
        employeeTableView.contentInset.bottom = 80.calcvaluey()
        employeeTableView.delegate = self
        employeeTableView.dataSource = self
        employeeTableView.backgroundColor = .white
        employeeTableView.showsVerticalScrollIndicator = false
        employeeTableView.addshadowColor()
        bottomNotificationView.backgroundColor = .white
        bottomNotificationView.addshadowColor()
        view.addSubview(bottomNotificationView)
        bottomNotificationView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 78.calcvaluey()))
        bottomNotificationView.addSubview(notifyButton)
        notifyButton.anchor(top: bottomNotificationView.topAnchor, leading: nil, bottom: bottomNotificationView.bottomAnchor, trailing: bottomNotificationView.trailingAnchor,size: .init(width: 200.calcvaluex(), height: 0))
        notifyButton.addTarget(self, action: #selector(goNotify), for: .touchUpInside)
        grabUser()
        fetchApi()
    }

    var userattr : UserDAttribute?
    func grabUser(){
        if let data = UserDefaults.standard.getUserData(id: UserDefaults.standard.getUserId()) {
            do {
                let json = try JSONDecoder().decode(UserData.self, from: data)

                self.userattr = json.data?.attributes
                
                
                //要加入
                //self.reloadSectionWithIndex(index: 0)
            }
            catch{
                
            }
        }
    }
    @objc func goNotify(){
      let hud = JGProgressHUD()
        hud.indicatorView = JGProgressHUDIndicatorView()
        hud.show(in: container)
        hud.textLabel.text = "通知中.."
        let parameter = create_notify_json()
        print(112,parameter)
        NetworkCall.shared.postCall(parameter: "api-or/v1/notification", param: parameter, decoderType: UpdateClass.self) { (json) in
            DispatchQueue.main.async {
                if let json = json{
                    print(889,json)
                    if json.data {
                        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        
                        hud.textLabel.text = "通知成功"
                        hud.dismiss(afterDelay: 1.5, animated: true) {
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    else{
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        
                        hud.textLabel.text = "通知失敗"
                        hud.dismiss(afterDelay: 1.5, animated: true)
                    }
                }
                else{
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    
                    hud.textLabel.text = "通知失敗"
                    hud.dismiss(afterDelay: 1.5, animated: true)
                }
            }

        }
        
        if let encode = try?
            JSONEncoder().encode(selectedNotifyUser) {
            UserDefaults.standard.saveHistory(data: encode)
       }
    }
    func create_notify_json() -> [String:Any] {
        var parameter = [String:Any]()
        var users_id = [Int]()
        for i in selectedNotifyUser {
            if let id = Int(i.id) {
            users_id.append(id)
            }
        }
        var notification = [String:Any]()
        
        notification["title"] = "訪談通知提醒"
        notification["body"] = "\(userattr?.name ?? "") 通知您查看訪談紀錄"
        
        var data = [String:Any]()
        data["type"] = "interview"
        data["id"] = self.interview_id ?? ""
        
        
        var apns = [String:Any]()
        apns["headers"] = ["apns-priority":"10"]
        apns["payload"] = ["aps":["badge":1,"content-available":1,"mutable-content":1]]
        parameter["message"] = ["user_id":users_id,"notification":notification,"data":data,"apns":apns]
        parameter["to_boss"] = 0
        return parameter
    }
    var original_data = [NotificationUser]()

    var department_array = [NotificationDepartment]()
    var history_array = [NotificationEmployer]()
    let hud = JGProgressHUD()
    func fetchApi(){
        hud.show(in: self.view)
        
        container.isHidden = true
        NetworkCall.shared.getCall(parameter: "api-or/v1/users/employees", decoderType: NotificationGroup.self) { (json) in
            DispatchQueue.main.async {
                if let json = json {
                    self.original_data = json.data
                    self.hud.dismiss()
                    self.getHistory()
                    self.createData(original_data:self.original_data)
                    
                }
            }
 
        }
    }
    func getHistory(){
        if let his_data = UserDefaults.standard.loadHistory(), let array = try? JSONDecoder().decode([NotificationEmployer].self, from: his_data) {
            for i in array {
                if original_data.contains(where: { (user) -> Bool in
                    return user.employee_code == i.code
                }) {
                    history_array.append(i)
                }
            }
           
        }
    }
    func createData(original_data: [NotificationUser]){
        department_array = []
        for i in original_data {
            if i.department_code == nil || i.employee_code == nil{
                continue
            }
            var o_emp = NotificationEmployer(id: i.id, name: i.employee_name ?? "", code: i.employee_code ?? "", selected: false)
            if selectedNotifyUser.contains(where: { (emp) -> Bool in
                return emp.code == o_emp.code
            }) {
                o_emp.selected = true
            }
            if let index = department_array.firstIndex(where: { (dep) -> Bool in
                return dep.code == i.department_code
            }) {
                if !department_array[index].employee.contains(where: { (emp) -> Bool in
                    return emp.code == i.employee_code
                }) {
                    department_array[index].employee.append(o_emp)
                }
            }
            else{
                 
                department_array.append(NotificationDepartment(name: i.department_name ?? "", code: i.department_code ?? "", employee: [o_emp]))
            }
        }
        self.departmentTableView.reloadData()
        self.employeeTableView.reloadData()
        container.isHidden = false

    }

}

extension NotificationViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == departmentTableView {
        let cell = tableView.cellForRow(at: indexPath) as? ProductTabCell

        //Briefly fade the cell on selection
        UIView.animate(withDuration: 0.1,
                       animations: {
                        //Fade-out
                        cell?.transform = .init(scaleX: 1.2, y: 1.2)
        }) { (completed) in
            UIView.animate(withDuration: 0.1,
                           animations: {
                            //Fade-out
                            cell?.transform = .identity
                           }) {
                (completed) in
                    self.departmentIndex = indexPath.row

                    self.departmentTableView.reloadData()
                self.employeeTableView.reloadData()
                
            }
        }
        }
        else{
            
            
            self.department_array[self.departmentIndex].employee[indexPath.row].selected = !self.department_array[self.departmentIndex].employee[indexPath.row].selected
            if self.department_array[self.departmentIndex].employee[indexPath.row].selected {
                self.selectedNotifyUser.append(self.department_array[self.departmentIndex].employee[indexPath.row])
            }
            else{
                if let index = self.selectedNotifyUser.firstIndex(where: { (emp) -> Bool in
                    emp.code == self.department_array[self.departmentIndex].employee[indexPath.row].code
                }) {
                    self.selectedNotifyUser.remove(at: index)
                }
            }
            
            print(self.selectedNotifyUser)
            self.employeeTableView.reloadData()
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == departmentTableView {
            return department_array.count
        }
        return department_array.count == 0 ? 0 : department_array[departmentIndex].employee.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == employeeTableView {
            let cell = EmployeeNotificationCell(style: .default, reuseIdentifier: "cell2")
            let employee = department_array[departmentIndex].employee[indexPath.row]
            cell.label.text = employee.name
            if employee.selected{
                cell.didSelect()
            }
            else{
                cell.unSelect()
            }
            
            return cell
        }
        let cell = ProductTabCell(style: .default, reuseIdentifier: "cell")
        cell.label.text = department_array[indexPath.row].name
        if indexPath.row == self.departmentIndex {
            cell.setColor()
        }
        else{
        cell.unsetColor()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == employeeTableView {
            return 80.calcvaluey()
        }
        return 92.calcvaluey()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
}

class EmployeeNotificationCell:UITableViewCell{
    let seperator = UIView()
    let selectedView = UIView()
    var checkBox : UIButton = {
        let check = UIButton(type: .custom)
        check.setImage(#imageLiteral(resourceName: "btn_check_box_normal").withRenderingMode(.alwaysTemplate), for: .normal)
        check.setImage(#imageLiteral(resourceName: "btn_check_box_pressed"), for: .selected)
        check.tintColor = .black
        check.contentVerticalAlignment = .fill
        check.contentHorizontalAlignment = .fill
        return check
    }()
    var label : UILabel = {
       let label = UILabel()
        label.textColor = .black
        label.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        label.text = "郭繼仁"
        return label
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        seperator.backgroundColor = #colorLiteral(red: 0.8591447473, green: 0.8592688441, blue: 0.8591176271, alpha: 1)
        contentView.addSubview(seperator)
        seperator.anchor(top: nil, leading: contentView.leadingAnchor, bottom: contentView.bottomAnchor, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()),size: .init(width: 0, height: 1.calcvaluex()))
        contentView.addSubview(selectedView)
        selectedView.anchor(top: contentView.topAnchor, leading: seperator.leadingAnchor, bottom: contentView.bottomAnchor, trailing: seperator.trailingAnchor,padding: .init(top: 6.calcvaluey(), left: 0, bottom: 6.calcvaluey(), right: 0))
        selectedView.layer.cornerRadius = 15.calcvaluex()
        selectedView.backgroundColor = #colorLiteral(red: 0.9693912864, green: 0.9695302844, blue: 0.969360888, alpha: 1)
        
        contentView.addSubview(checkBox)
        checkBox.isUserInteractionEnabled = false
        checkBox.centerYInSuperview()
        checkBox.anchor(top: nil, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 60.calcvaluex(), bottom: 0, right: 0),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        
        contentView.addSubview(label)
        label.anchor(top: nil, leading: checkBox.trailingAnchor, bottom: nil, trailing: selectedView.trailingAnchor,padding: .init(top: 0, left: 39.calcvaluex(), bottom: 0, right: 12.calcvaluex()))
        label.centerYInSuperview()
    }
    func didSelect(){
        seperator.isHidden = true
        selectedView.isHidden = false
        checkBox.isSelected = true
    }
    func unSelect(){
        seperator.isHidden = false
        selectedView.isHidden = true
        checkBox.isSelected = false
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension NotificationViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if let text = textField.text,
                       let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                        with: string)
            var full_data = [NotificationUser]()
            if mode == .History {
                full_data = original_data.filter({ (ut) -> Bool in
                    return history_array.contains { (emp) -> Bool in
                        return emp.code == ut.employee_code
                    }
                })
            }
            else{
                full_data = original_data
            }
            let filter_data = full_data.filter { (user) -> Bool in
                if let emp = user.employee_name , let dep = user.department_name {
                    return emp.contains(updatedText) || dep.contains(updatedText)
                }
                return false
            }
            
            if updatedText == ""{
                
                createData(original_data: full_data)
            }
            else{
                
                createData(original_data: filter_data)
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
