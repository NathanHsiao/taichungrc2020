//
//  NotificationGroup.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/30/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation


struct NotificationGroup : Codable{
    var data : [NotificationUser]
}

struct NotificationUser: Codable{
    var id : String
    var employee_code : String?
    var employee_name : String?
    var department_code : String?
    var department_name : String?
}


struct NotificationDepartment:Codable {
    var name: String
    var code : String
    var employee: [NotificationEmployer]
}

struct NotificationEmployer:Codable{
    var id : String
    var name : String
    var code : String
    var selected : Bool
}
