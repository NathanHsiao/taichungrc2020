//
//  DiscountViewCell.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/24/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class DiscountViewCell : InterviewCell{
    let stackview = UIStackView()
    let label = UILabel()
    let textView = FormTextView(placeText: "請輸入折扣項目", mode: .DiscountInfo)
    let priceField = FormTextView(placeText: "", mode: .DiscountPrice,setMinus: true)
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        container.addSubview(stackview)
        stackview.axis = .horizontal
        stackview.distribution = .fill
        stackview.spacing = 18.calcvaluex()
        stackview.alignment = .top
        container.addSubview(label)
        label.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        label.text = "折扣項目"
       
        label.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        label.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 18.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 36.calcvaluex()))
        
        
       
        textView.setContentHuggingPriority(.defaultLow, for: .horizontal)
        priceField.setContentHuggingPriority(.defaultHigh, for: .horizontal)

//
//        textField.attributedPlaceholder = NSAttributedString(string: "輸入折扣項目", attributes: [NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 18.calcvaluex())!,NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)])

       // priceField.constrainHeight(constant: 52.calcvaluey())
       //textField.frame.size.height = 52.calcvaluey()
    //priceField.frame.size.height = 52.calcvaluey()
    // textField.constrainHeight(constant: 52.calcvaluey())
        priceField.constrainWidth(constant: 180.calcvaluex())
        //textField.constrainWidth(constant: 50.calcvaluex())
        container.addSubview(stackview)
        stackview.anchor(top: label.bottomAnchor, leading: label.leadingAnchor, bottom: container.bottomAnchor, trailing: label.trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 24.calcvaluey(), right: 0))
        stackview.addArrangedSubview(textView)
        stackview.addArrangedSubview(priceField)
//        textField.setContentHuggingPriority(.init(1000), for: .horizontal)

        
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
