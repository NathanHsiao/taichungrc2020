//
//  AddMachineController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/26.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension UITextField{
    func addToolBar(){
//        let toolBar = UIToolbar()
//        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
//        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donePressed))
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelPressed))
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//        toolBar.sizeToFit()
//
//        
//        self.inputAccessoryView = toolBar
    }
    @objc func donePressed(){
        self.resignFirstResponder()
    }
    @objc func cancelPressed(){
        self.resignFirstResponder() // or do something
    }
}
class AddMachinePickerView: AddMachineFormView,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,FormToolBarDelegate {
    func toolbarDonePressed() {
        self.textfield.text = currentdata
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentdata = data[row]
    }
    
    let dropdownarrow = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
    let pickerview = UIPickerView()
    var data = ["立式加工中心機"]
    lazy var currentdata = self.data[0]
    init(text:String,pickertext:String) {
        super.init(text: text)
        
        
        self.textfield.text = pickertext
        self.textfield.tintColor = .clear
//        self.textfield.delegate = self
        textfield.inputView = pickerview
        self.textfield.addSubview(dropdownarrow)
        dropdownarrow.contentMode = .scaleAspectFit
        dropdownarrow.anchor(top: nil, leading: nil, bottom: nil, trailing: textfield.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 10.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        dropdownarrow.centerYInSuperview()
        textfield.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        textfield.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        textfield.addToolBar()
        textfield.toolBarDelegate = self
        pickerview.dataSource = self
        pickerview.delegate = self
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddMachineFormView:InterviewNormalFormView {
    
    override init(text: String, placeholdertext: String? = nil) {
        super.init(text: text, placeholdertext: placeholdertext)
        self.tlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        textfieldanchor.height?.constant = 61.calcvaluey()
        textfieldanchor.top?.constant = 4.calcvaluey()
        
        textfield.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class PreviewImageView : UIImageView {
    let centerlabel = UILabel()
    override var image: UIImage? {
        didSet{
            centerlabel.isHidden = true
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        layer.cornerRadius = 15.calcvaluex()
        layer.borderWidth = 1.calcvaluex()
        layer.borderColor = #colorLiteral(red: 0.8861749768, green: 0.8863304257, blue: 0.886176765, alpha: 1)
        
        addSubview(centerlabel)
        centerlabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        centerlabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        centerlabel.text = "可預覽機台"
        
        centerlabel.centerInSuperview()
        
        contentMode = .scaleAspectFit
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AddMachineController: UIViewController {
    let container = UIView()
    var containeranchor:AnchoredConstraints!
    var previewImageView = PreviewImageView(frame: .zero)
    
    let donebutton = UIButton()
    
    let kindview = AddMachinePickerView(text: "類別", pickertext: "請選擇類別")
    let seriesview = AddMachinePickerView(text: "系列", pickertext: "請選擇系列")
    let modelview = AddMachinePickerView(text: "型號", pickertext: "請選擇型號")

    override func viewDidLoad() {
        super.viewDidLoad()
        container.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        
        if #available(iOS 11.0, *) {
            container.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
             container.layer.cornerRadius = 15.calcvaluex()
        } else {
            // Fallback on earlier versions
        }
        
        view.addSubview(container)
        containeranchor = container.anchor(top: view.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 548.calcvaluey()))
        
        let xbutton = UIButton(type: .custom)
        xbutton.imageView?.contentMode = .scaleAspectFill
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        container.addSubview(xbutton)
        xbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.addTarget(self, action: #selector(popview), for: .touchUpInside)
        let header = UILabel()
        header.text = "加入機台"
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        header.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        container.addSubview(header)
        header.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0))
        header.centerXInSuperview()
        
        
        previewImageView.image = #imageLiteral(resourceName: "machine_LV850")
        
        container.addSubview(previewImageView)
        previewImageView.anchor(top: header.bottomAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 55.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 456.calcvaluex(), height: 326.calcvaluey()))
        
        donebutton.setTitle("完成", for: .normal)
        donebutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        donebutton.setTitleColor(.white, for: .normal)
        donebutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        container.addSubview(donebutton)
        donebutton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluey(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        donebutton.centerXInSuperview()
        donebutton.layer.cornerRadius = 48.calcvaluey()/2
        donebutton.addshadowColor(color: #colorLiteral(red: 0.869818449, green: 0.8720664382, blue: 0.8773229718, alpha: 1))
        
        let hstack = interviewHstack(views: [kindview,seriesview], spacing: 18.calcvaluex(), distribution: .fillEqually)
        let stackview = UIStackView(arrangedSubviews: [hstack,modelview,UIView()])
        for (index,vt) in stackview.arrangedSubviews.enumerated() {
            if index == stackview.arrangedSubviews.count-1 {
                vt.constrainHeight(constant: 61.calcvaluey())
            }
            else{
            vt.constrainHeight(constant: 90.calcvaluey())
            }
        }
        stackview.axis = .vertical
        //stackview.distribution = .fillEqually
        stackview.spacing = 35.calcvaluey()
        if #available(iOS 11.0, *) {
            stackview.setCustomSpacing(50.calcvaluey(), after: modelview)
        } else {
            // Fallback on earlier versions
        }
        container.addSubview(stackview)
        stackview.anchor(top: header.bottomAnchor, leading: container.leadingAnchor, bottom: donebutton.topAnchor, trailing: previewImageView.leadingAnchor,padding: .init(top: 49.calcvaluey(), left: 26.calcvaluex(), bottom: 42.calcvaluey(), right: 18.calcvaluex()))
       
    }
    @objc func popview(){
        containeranchor.top?.isActive = false
        container.topAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
        containeranchor.top?.constant = -548.calcvaluey()
        
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
}
