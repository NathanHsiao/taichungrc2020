//
//  SalesBottomView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
extension Int {
    func setInterviewStatus() -> String{
        switch self {
        case 0 :
            return ""
        case 1 :
            return "拓展中"
        case 2 :
            return "洽談中"
        case 3 :
            return "轉訂單"
        case 4 :
            return "結案"
        default :
            ()
        }
        return ""
    }
    
    func setInterViewStatusColor() -> UIColor{
        switch self {
        case 0 :
            return #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        case 1 :
            return #colorLiteral(red: 0.2379338741, green: 0.6737315059, blue: 0.9689859748, alpha: 1)
        case 2 :
            return #colorLiteral(red: 0.1008876637, green: 0.5688499212, blue: 0.5781469941, alpha: 1)
        case 3 :
            return #colorLiteral(red: 0.03734050319, green: 0.328607738, blue: 0.5746202469, alpha: 1)
        case 4 :
            return #colorLiteral(red: 0.4757143259, green: 0.4757863283, blue: 0.4756985307, alpha: 1)
        default :
            ()
        }
        return #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
    }
}
protocol SalesBottomViewDelegate {
    func showSaleRecord(id:String)
    func showPriceRecord(id: String)
}
class SalesBottomView: UIView {
    let searchcontroller = SearchTextField()
    let addbutton = AddButton4()
    let tableview = UITableView(frame: .zero, style: .plain)
    var delegate:SalesBottomViewDelegate!
    var searchcontrolleranchor:AnchoredConstraints!
    var currentCustomers = [Customer]() {
        didSet{
            let offset = tableview.contentOffset;
            tableview.reloadData()
            //tableview.layoutIfNeeded() // Force layout so things are updated before resetting the contentOffset.
            tableview.setContentOffset(offset, animated: false)
           // self.tableview.reloadData()
        }
    }
    var isSale = true{
        didSet{
            //self.tableview.reloadData()
        }
    }
    
    var list = [InterViewList]() {
        didSet{
           // list.reverse()
            let offset = tableview.contentOffset;
            tableview.reloadData()
            //tableview.layoutIfNeeded() // Force layout so things are updated before resetting the contentOffset.
            tableview.setContentOffset(offset, animated: false)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        addshadowColor(color: #colorLiteral(red: 0.9166875482, green: 0.9190561175, blue: 0.9245964885, alpha: 1))
        
//        searchcontroller.attributedPlaceholder = "搜尋客戶公司、統編".convertoSearchAttributedString()
//        searchcontroller.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
//
//        addSubview(searchcontroller)
//        searchcontrolleranchor = searchcontroller.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 262.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
//        searchcontroller.layer.cornerRadius = 38.calcvaluey()/2
//        searchcontroller.isHidden = true
//        addSubview(addbutton)
//        addbutton.titles.text = "建立客戶公司"
//        addbutton.backgroundColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
//        addbutton.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 206.calcvaluex(), height: 38.calcvaluey()))
//        addbutton.layer.cornerRadius = 38.calcvaluey()/2
//
        tableview.backgroundColor = .clear
        addSubview(tableview)
        tableview.contentInset = .init(top: 26.calcvaluey(), left: 0, bottom: 0, right: 0)
        tableview.delegate = self
        tableview.dataSource = self
        tableview.separatorStyle = .none
        tableview.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor)
        tableview.showsVerticalScrollIndicator = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension SalesBottomView:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if !isSale {
            return list.count
        }
        return currentCustomers.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSale {
        let cell = SalesBottomViewCell(style: .default, reuseIdentifier: "ce")
            cell.namelabel.text = currentCustomers[indexPath.section].name
            cell.numberlabel.text = "統編：\(currentCustomers[indexPath.section].business_number ?? "")"
        return cell
        }
        else{
            let cell = PriceManagementCell(style: .default, reuseIdentifier: "ct")
            cell.toplabel.text = list[indexPath.section].account?.name
            cell.timelabel.text = list[indexPath.section].date
            cell.statuslabel.text = list[indexPath.section].status.setInterviewStatus()
            cell.statuslabel.textColor = list[indexPath.section].status.setInterViewStatusColor()
            cell.statuslabel.layer.borderColor = list[indexPath.section].status.setInterViewStatusColor().cgColor
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104.calcvaluey()
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: false)
        if isSale {
            delegate.showSaleRecord(id:currentCustomers[indexPath.section].id)
        }
        else{
            delegate.showPriceRecord(id:list[indexPath.section].id)
        }
        
    }
}

class SalesBottomViewCell:UITableViewCell {
    let container = UIView()
    let namelabel = UILabel()
    let numberlabel = UILabel()
    let rightarrow = UIImageView(image: #imageLiteral(resourceName: "ic_faq_next"))
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        container.backgroundColor = .white
        container.addshadowColor(color: #colorLiteral(red: 0.8710518479, green: 0.8733028769, blue: 0.8785669208, alpha: 1))
        container.layer.cornerRadius = 15.calcvaluex()
        addSubview(container)
        container.fillSuperview(padding: .init(top: 2, left: 26.calcvaluex(), bottom: 2, right: 26.calcvaluex()))
        
        namelabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        //namelabel.text = "久大行銷股份有限公司"
        namelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        container.addSubview(namelabel)
        namelabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 46.calcvaluex(), bottom: 0, right: 0))
        //namelabel.centerYInSuperview()
        container.addSubview(numberlabel)
        numberlabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        numberlabel.textColor = #colorLiteral(red: 0.5685836673, green: 0.5686682463, blue: 0.5685651302, alpha: 1)
        
        numberlabel.anchor(top: namelabel.bottomAnchor, leading: namelabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 4.calcvaluey(), left: 0, bottom: 0, right: 0))
        rightarrow.tintColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(rightarrow)
        rightarrow.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        rightarrow.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
