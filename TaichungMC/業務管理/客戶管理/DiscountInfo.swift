//
//  DiscountInfo.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/25/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

struct DiscountInfo {
    var discount_reason : String?
    var discount : Int?
}
