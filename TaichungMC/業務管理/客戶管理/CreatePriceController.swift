//
//  CreatePriceController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/25.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class interviewHstack:UIStackView {
    init(views:[UIView],spacing:CGFloat,distribution:UIStackView.Distribution) {
        super.init(frame: .zero)
        self.distribution = distribution
        self.spacing = spacing
        self.axis = .horizontal
        
        for i in views {
            self.addArrangedSubview(i)
        }
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CreatePriceFirstSectionCell:InterviewCell {
    let customername = InterviewNormalFormView(text: "客戶名稱",placeholdertext: "請輸入客戶名稱")
    let pricedate = InterviewNormalFormView(text: "報價日期")
    let priceid = InterviewNormalFormView(text: "報價編號",placeholdertext: "請輸入報價編號")
    let deliveryCountry = InterviewNormalFormView(text: "出貨地")
    let deliveredCountry = InterviewNormalFormView(text: "收貨地")
    let paymentType = InterviewNormalFormView(text: "付款方式",placeholdertext: "請輸入付款方式")
    let priceReason = InterviewNormalFormView(text: "價格依據",placeholdertext: "請輸入價格依據")
    let deliveryDate = InterviewNormalFormView(text: "交貨時間")
    let insuranceDate = InterviewNormalFormView(text: "保固時間")
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        customername.makeImportant()
        let hstack1 = interviewHstack(views:[pricedate,priceid],spacing: 12.calcvaluex(), distribution: .fillEqually)
        
        let hstack2 = interviewHstack(views: [deliveryCountry,deliveredCountry], spacing: 12.calcvaluex(), distribution: .fillEqually)
        
        let hstack3 = interviewHstack(views: [priceReason,deliveryDate], spacing: 12.calcvaluex(), distribution: .fillEqually)
        let stackview = UIStackView(arrangedSubviews: [customername,hstack1,hstack2,paymentType,hstack3,insuranceDate])
        stackview.axis = .vertical
        stackview.spacing = 20.calcvaluey()
        
        stackview.distribution = .fillEqually
        container.addSubview(stackview)
        stackview.fillSuperview(padding: .init(top: 26.calcvaluey(), left: 36.calcvaluex(), bottom: 26.calcvaluex(), right: 36.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class BottomPriceView:UIView {
    let blabel = UILabel()
    let priceLabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        blabel.text = "總報價金額："
        blabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        blabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        addSubview(blabel)
        blabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        blabel.centerYInSuperview()
        
        priceLabel.text = "$90,399 USD"
        priceLabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        priceLabel.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        addSubview(priceLabel)
        priceLabel.anchor(top: nil, leading: blabel.trailingAnchor, bottom: nil, trailing: nil)
        priceLabel.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CreatePriceSecondSectionCell : InterviewSecondSectionCell {
    let priceLabel = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        priceLabel.text = "$88,999"
        priceLabel.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        priceLabel.font = UIFont(name: "Roboto-Bold", size: 22.calcvaluex())
        container.addSubview(priceLabel)
        priceLabel.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 39.calcvaluey(), right: 36.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class EquipmentPriceCell : InterviewCell {
    let equipmentname = UILabel()
    let equipmentprice = UILabel()
    let totalprice = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        container.addSubview(equipmentname)
        equipmentname.font = UIFont(name: "Roboto-Medium", size: 22.calcvaluex())
        equipmentname.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        equipmentname.text = "自動門"
        equipmentname.anchor(top: nil, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 36.calcvaluex(), bottom: 0, right: 0))
        equipmentname.centerYInSuperview()
        
        equipmentprice.text = "$800X3"
        equipmentprice.font = UIFont(name: "Roboto-Regular", size: 22.calcvaluex())
        equipmentprice.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        
        container.addSubview(equipmentprice)
        equipmentprice.anchor(top: nil, leading: equipmentname.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 88.calcvaluex(), bottom: 0, right: 0))
        equipmentprice.centerYInSuperview()
        
        totalprice.text = "$88,999"
        totalprice.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        totalprice.font = UIFont(name: "Roboto-Bold", size: 22.calcvaluex())
        container.addSubview(totalprice)
        totalprice.anchor(top: nil, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 36.calcvaluex()))
        totalprice.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CreatePriceController: InterviewController {
    let bottomPriceview = BottomPriceView()
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "報價單"
        
        stackviewanchor.height?.constant = 65.calcvaluey() * 3
        topstackview.arrangedSubviews.forEach { (vt) in
            vt.removeFromSuperview()
        }
        for (index,val) in ["編輯","分享","預覽","歷史紀錄","刪除"].enumerated() {
            let bt = TopViewButton(text: val)
//            if index == 2{
//                bt.addTarget(self, action: #selector(goPrice), for: .touchUpInside)
//            }
            bt.layer.cornerRadius = 38.calcvaluey()/2
            topstackview.addArrangedSubview(bt)
        }
        
        if mode == .Editing {
            topstackview.isHidden = true
        }
        view.addSubview(bottomPriceview)
        
        rightview.contentInset.bottom += 26.calcvaluey()
        bottomPriceview.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 92.calcvaluey()))
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let rt = RightTopViewHeader()
        if section == 0{
        if mode == .Normal {
            return RightViewTopText()
        }
        else{
                return SectionOneTopViewHeader()
            }
        }
        else if section == 1{
            
                rt.headerlabel.text = "報價機台"
            
        }
        else if section == 2{
            rt.headerlabel.text = "選配"
            
        }
        else if section == 3{

            rt.headerlabel.text = "調整價格"
            
        }
        else if section == 4{
            rt.headerlabel.text = "確認簽名"
        }

        return rt
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if mode == .Normal {
            let cell = CreatePriceFirstSectionCell(style: .default, reuseIdentifier: "cc")
//            cell.customername.textfield.text = "久大行銷股份有限公司"
//            cell.interviewdate.textfield.text = "12月30日 下午4:26"
//            cell.orderdate.textfield.text = "12月30日 下午4:26"
//            cell.moreinfo.textfield.text = "此機台單價較高， 因此客戶想多方比較再決定， 如果需要再進一步做報價，客戶 預計於2020年1月5日時會先電聯。"
            return cell
            }
            else {
                let cell = CreatePriceFirstSectionCell(style: .default, reuseIdentifier: "cc")
//                cell.addFile.addTarget(self, action: #selector(addPhoto), for: .touchUpInside)
//                cell.mode = mode
//                cell.locationlabel.delegate = self
                return cell
            }
        }
        else if indexPath.section == 1 {
                        if mode == .Normal {
            let cell = CreatePriceSecondSectionCell(style: .default, reuseIdentifier: "ct")
            
            return cell
                }
                else{
                    let cell = EditingMachineCell(style: .default, reuseIdentifier: "cm")
                    //cell.addMachine.addTarget(self, action: #selector(goAddMachine), for: .touchUpInside)
                    return cell
                }
        }
        else if indexPath.section == 4{
            let cell = SixthSectionCell(style: .default, reuseIdentifier: "yu")
            return cell
        }
        else {
            if mode == .Normal {
            let cell = EquipmentPriceCell(style: .default, reuseIdentifier: "xt")
                
            
            return cell
            }
            else {
                
                let cell = EditingMachineCell(style: .default, reuseIdentifier: "xt")
                cell.clipsToBounds = true
                //cell.addMachine.addTarget(self, action: #selector(goAddMachine), for: .touchUpInside)
//                if indexPath.section == 2{
//                    let cell = UITableViewCell(style: .default, reuseIdentifier: "tt")
//                    cell.isHidden = true
//                    return cell
//                }
//                else{
//                    cell.isHidden = false
//                }
                
                return cell
            }
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if mode == .Editing {
            if section == 2 {
                return 0
            }
            
        }
        
        if section == 0{
            return 34.calcvaluey()
        }
        return 52.calcvaluey()
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 632.calcvaluey()
        case 1:
            if mode == .Editing {
                return 106.calcvaluey()
            }
            return 164.calcvaluey()
        case 2:
            if mode == .Editing {
                return 0
            }
            return 98.calcvaluey()
        case 3:
            if mode == .Editing {
                return 106.calcvaluey()
            }
            return 98.calcvaluey()
        case 4:
            return 199.calcvaluey()
        default:
            return 0
        }
    }
}
