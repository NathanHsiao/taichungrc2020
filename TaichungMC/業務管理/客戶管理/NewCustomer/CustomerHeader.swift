//
//  CustomerHeader.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/31/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit

class CustomerHeader : UIView{
    let lT = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        lT.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        lT.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        addSubview(lT)
        lT.fillSuperview(padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
protocol RemoveContactDelegate {
    func removeContact(tag:Int)
}
class CustomerTitleDoubleHeader:UIView{
    let lT = UILabel()
    let lT2 = UILabel()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        return xbutton
    }()
    var contactLabel : UILabel = {
        let contact = UILabel()
        contact.font = UIFont(name: "Roboto-Bold", size: 20.calcvaluex())
        contact.textAlignment = .center
        contact.text = "聯絡人 1"
        contact.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        return contact
    }()
    var delegate:RemoveContactDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        lT.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        lT.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        lT2.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        lT2.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        addSubview(contactLabel)
        contactLabel.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        addSubview(lT)
        lT.anchor(top: contactLabel.bottomAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: centerXAnchor,padding: .init(top: 12.calcvaluey(), left: 26.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        addSubview(lT2)
        lT2.anchor(top: contactLabel.bottomAnchor, leading: lT.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 12.calcvaluey(), left: 12.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        
        addSubview(xbutton)
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        //xbutton.centerYInSuperview()
        xbutton.centerYAnchor.constraint(equalTo: contactLabel.centerYAnchor).isActive = true
        xbutton.isHidden = true
        xbutton.addTarget(self, action: #selector(removeContact), for: .touchUpInside)

    }
    @objc func removeContact(){
        delegate?.removeContact(tag: self.tag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class CustomerDoubleHeader : UIView{
    let lT = UILabel()
    let lT2 = UILabel()
    var xbutton : UIButton = {
        let xbutton = UIButton(type: .custom)
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2").withRenderingMode(.alwaysTemplate), for: .normal)
        xbutton.tintColor = .black
        return xbutton
    }()

    var delegate:RemoveContactDelegate?
    override init(frame: CGRect) {
        super.init(frame: frame)
        lT.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        lT.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        lT2.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        lT2.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        
        
        
        addSubview(lT)
        lT.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: centerXAnchor,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 0, right: 6.calcvaluex()))
        addSubview(lT2)
        lT2.anchor(top: topAnchor, leading: lT.trailingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,padding: .init(top: 0, left: 12.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        
        addSubview(xbutton)
        xbutton.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 12.calcvaluex()),size: .init(width: 24.calcvaluex(), height: 24.calcvaluex()))
        xbutton.centerYInSuperview()
        xbutton.isHidden = true
        xbutton.addTarget(self, action: #selector(removeContact), for: .touchUpInside)

    }
    @objc func removeContact(){
        delegate?.removeContact(tag: self.tag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
