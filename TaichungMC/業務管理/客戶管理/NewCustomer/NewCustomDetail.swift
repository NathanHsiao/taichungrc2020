//
//  NewCustomDetail.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 5/31/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation
struct Industries : Codable{
    var data : [Industry]
}
struct Industry : Codable {
    var id : String
    var code : String
    var company : String
    var title : String
    
}
class CustomerCreateInfo {
    var name:String
    var address:String
    var business_number: String
    var company_phone: String
    var main_contact:String
    var num_worker:String
    var industry_type:Industry?
    var equipment_array : [NewCustomerEquipment]
    var upper_array : [FactoryUser]
    var down_array : [FactoryUser]

    init(name:String,address:String,business_number:String,company_phone:String,main_contact:String,num_worker:String,industry_type:Industry?,equipment_array:[NewCustomerEquipment],upper_array:[FactoryUser],down_array:[FactoryUser]) {
        self.name = name
        self.address = address
        self.business_number = business_number
        self.company_phone = company_phone
        self.main_contact = main_contact
        self.num_worker = num_worker
        self.industry_type = industry_type
        self.equipment_array = equipment_array
        self.upper_array = upper_array
        self.down_array = down_array

    }
}
class CustomerContact {
    var id:Int?
    var name : String
    var phone_number:String
    var email:String
    var birthDay:String
    var interest :String
    var position:String
    var exTel:String
    var other:String
    
    init(id:Int?,name:String,phone_number:String,email:String,birthDay:String,interest:String,position:String,exTel:String,other:String) {
        self.name = name
        self.phone_number = phone_number
        self.email = email
        self.birthDay = birthDay
        self.interest = interest
        self.position = position
        self.exTel = exTel
        self.other = other
    }
}
class NewCustomerEquipment {
    var id : Int?
    var model:String
    var qty : Int
    var brand: String
    var year : String
    
    init(id:Int?,model:String,qty:Int,brand:String,year:String) {
        self.id = id
        self.model = model
        self.qty = qty
        self.brand = brand
        self.year = year
    }
}

class FactoryUser {
    var text:String
    init(text:String){
        self.text = text
    }
}
