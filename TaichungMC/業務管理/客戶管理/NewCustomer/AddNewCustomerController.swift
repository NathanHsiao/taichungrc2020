//
//  AddNewCustomerController.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 3/28/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD

class AddNewCustomerController: SampleController {
    
    let detailView = CreateCustomerDetailView(mode: .CustomerDetail, text: "客戶公司資訊")
    let contactView = CreateCustomerDetailView(mode: .ContactDetail , text: "客戶聯絡人資訊")
    var container = UIView()
    var addContact = AddButton4()
    var sendOutView = UIView()
    var company:Company? {
        didSet{

        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if detailView.canEdit {
            if let _ = company {
                titleview.text = "編輯客戶公司"
            }
            else {
                titleview.text = "建立客戶公司"
            }
        }
        else{
            titleview.text = "查看客戶公司"
        }
        
        view.addSubview(container)
        
        container.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        container.addSubview(detailView)
        detailView.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.centerXAnchor,padding: .init(top: 18.calcvaluey(), left: 26.calcvaluex(), bottom: 90.calcvaluey(), right: 13.calcvaluex()))
        
        container.addSubview(contactView)
        contactView.anchor(top: detailView.topAnchor, leading: view.centerXAnchor, bottom: detailView.bottomAnchor, trailing: container.trailingAnchor,padding: .init(top: 0, left: 13.calcvaluex(), bottom: 0, right: 26.calcvaluex()))
        contactView.anchorConstraint?.bottom?.constant = -74.calcvaluey()
        contactView.addSubview(addContact)
        addContact.centerXInSuperview()
        addContact.anchor(top: contactView.tableview.bottomAnchor, leading: nil, bottom: contactView.bottomAnchor, trailing: nil,padding: .init(top: 12.calcvaluey(), left: 0, bottom: 12.calcvaluey(), right: 0))
        addContact.layer.cornerRadius = 50.calcvaluey()/2
        addContact.titles.text = "加入聯絡人"
        addContact.addTarget(self, action: #selector(goAddNewContact), for: .touchUpInside)
        detailView.fields = [.CustomerName,.Address,.NO_Phone,.MainContact_Worker,.Industry,.Equipment,.UpperFactory,.DownwardFactory]
        
        contactView.fields = []
        fetchApi()
        
        view.addSubview(sendOutView)
        sendOutView.backgroundColor = .white
        sendOutView.addshadowColor()
        
        sendOutView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,size: .init(width: 0, height: 78.calcvaluey()))
        
        let saveButton = UIButton(type: .custom)
        saveButton.setTitle("儲存", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        saveButton.backgroundColor = #colorLiteral(red: 0.9411263466, green: 0.5122044683, blue: 0.008263183758, alpha: 1)
        
        sendOutView.addSubview(saveButton)
        saveButton.anchor(top: sendOutView.topAnchor, leading: nil, bottom: sendOutView.bottomAnchor, trailing: sendOutView.trailingAnchor,size: .init(width: 200.calcvaluex(), height: 0))
        
        saveButton.addTarget(self, action: #selector(saveCustomer), for: .touchUpInside)
        
        
    }
    @objc func saveCustomer(){
        let info = detailView.customerInfo
        let contacts = contactView.contacts
        var param = [String:Any]()
        if let com = company?.data{
            param["code"] = com.code
        }
        param["name"] = info.name
        param["tel"] = info.company_phone
        param["address"] = info.address
        print(221,info.business_number)
        param["business_number"] = info.business_number
        param["contact"] = info.main_contact
        param["type"] = info.industry_type?.code ?? ""
        param["staff_num"] = info.num_worker
        var up_array = [String]()
        for i in info.upper_array {
            up_array.append(i.text)
        }
        param["upstream"] = up_array
        var down_array = [String]()
        for i in info.down_array {
            down_array.append(i.text)
        }
        param["downstream"] = down_array
        
        var machine_array = [[String:Any]]()
        for i in info.equipment_array {
            var machine = [String:Any]()
            if let id = i.id {
                machine["id"] = id
            }
            machine["model"] = i.model
            machine["number"] = String(i.qty)
            machine["manufacturer"] = i.year
            machine["brand"] = i.brand
            machine["detail"] = ""
            machine_array.append(machine)
        }
        param["machines"] = machine_array
        
        var contact_array = [[String:Any]]()
        for i in contacts {
            var con = [String:Any]()
            if let id = i.id {
                con["id"] = id
            }
            con["type"] = i.position
            con["name"] = i.name
            con["interesting"] = i.interest
            con["mobile"] = i.phone_number
            con["email"] = i.email
            con["birthday"] = i.birthDay
            con["other"] = ["ext":i.other]
            contact_array.append(con)
        }
        param["contacts"] = contact_array
        if detailView.remove_machine.count != 0{
            param["del_machines"] = detailView.remove_machine
        }
        
        if contactView.remove_contact.count != 0{
            param["del_contact"] = contactView.remove_contact
        }
        let hud = JGProgressHUD()
        hud.indicatorView = JGProgressHUDIndicatorView()
        hud.textLabel.text = "建立中.."
        hud.show(in: container)
        
        NetworkCall.shared.postCall(parameter: "api-or/v1/account_detail/store", param: param, decoderType: Company.self) { (json) in
            DispatchQueue.main.async {
                if let json = json{
                    if let error = json.ERR_CODE{
                        let message = error == "H505" ? "統一編號已存在請重新輸入" : "建立失敗"
                        
                        hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        
                        hud.textLabel.text = message
                        hud.dismiss(afterDelay: 1.5)
                        
                        return
                    }
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    hud.textLabel.text = "建立成功"
                    hud.dismiss(afterDelay: 1.5, animated: true) {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else{
                    hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    
                    hud.textLabel.text = "建立失敗"
                    hud.dismiss(afterDelay: 1.5)
                }
            }

        }
    }
    @objc func goAddNewContact(){
        let tt : [CustomerField] = [.ContactName_PhoneNumber,.ContactMail,.ContactBirth_Interest,.ContactPosition_ExPhone,.ContactOther]
        for i in tt{
            contactView.fields.append(i)
        }
        contactView.contacts.append(CustomerContact(id: nil, name: "", phone_number: "", email: "", birthDay: "", interest: "", position: "", exTel: "", other: ""))
        contactView.tableview.reloadData()
    }
    func fetchApi(){
        container.isHidden = true
        NetworkCall.shared.getCall(parameter: "api-or/v1/industries", decoderType: Industries.self) { (json) in
            DispatchQueue.main.async {
                if let json = json {
                    self.container.isHidden = false
                    self.detailView.industries = json.data
                    self.setData()
                }
            }

        }
    }
    func setData() {
        if let com = self.company?.data {
            var upperStream = [FactoryUser]()
            var downStream = [FactoryUser]()
            var equipments = [NewCustomerEquipment]()
            for i in com.upstream ?? []{
                upperStream.append(FactoryUser(text: i))
            }
            for i in com.downstream ?? []{
                downStream.append(FactoryUser(text: i))
            }
            for i in com.machines ?? [] {
                equipments.append(NewCustomerEquipment(id: i.id, model: i.model ?? "", qty: Int(i.number ?? "1") ?? 1, brand: i.brand ?? "", year: i.manufacturer ?? ""))
            }
            let indust = detailView.industries.first { (inn) -> Bool in
                return com.type == inn.code.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
            let customInfo = CustomerCreateInfo(name: com.name ?? "", address: com.address ?? "", business_number: com.business_number ?? "", company_phone: com.tel ?? "", main_contact: com.contact ?? "", num_worker: com.staff_num ?? "", industry_type: indust, equipment_array: equipments, upper_array: upperStream, down_array: downStream)
            detailView.customerInfo = customInfo
            detailView.tableview.reloadData()
            
            var contacts = [CustomerContact]()
            var fields = [CustomerField]()
            for i in com.contacts ?? [] {
                contacts.append(CustomerContact(id: i.id, name: i.name ?? "", phone_number: i.mobile ?? "", email: i.email ?? "", birthDay: i.birthday ?? "", interest: i.interesting ?? "", position: i.type ?? "", exTel: i.phone ?? "", other: i.other?.ext ?? ""))
                let cust : [CustomerField] = [.ContactName_PhoneNumber,.ContactMail,.ContactBirth_Interest,.ContactPosition_ExPhone,.ContactOther]
                for j in cust{
                    fields.append(j)
                }
            }
//            if com.contacts?.count == 0{
//                contacts.append(CustomerContact(id: nil, name: "", phone_number: "", email: "", birthDay: "", interest: "", position: "", exTel: "", other: ""))
//                let tt : [CustomerField] = [.ContactName_PhoneNumber,.ContactMail,.ContactBirth_Interest,.ContactPosition_ExPhone,.ContactOther]
//                for i in tt{
//                    fields.append(i)
//                }
//            }
            contactView.contacts = contacts
            contactView.fields = fields
        }
    }

}
