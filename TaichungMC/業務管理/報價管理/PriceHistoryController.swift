//
//  PriceHistoryController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/30.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
class HistoryTotalView : UIView {
    let pricelabel = UILabel()
    let pricetext = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        addshadowColor(color: #colorLiteral(red: 0.8508846164, green: 0.8510343432, blue: 0.8508862853, alpha: 1))
        pricelabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        pricelabel.text = "總報價金額："
        pricelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(pricelabel)
        pricelabel.anchor(top: nil, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 46.calcvaluex(), bottom: 0, right: 0))
        pricelabel.centerYInSuperview()
        
        pricetext.text = "$22,869 USD"
        pricetext.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        pricetext.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        addSubview(pricetext)
        pricetext.anchor(top: nil, leading: pricelabel.trailingAnchor, bottom: nil, trailing: nil)
        pricetext.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class HistoryCell: UITableViewCell {
    let container = UIView()
    let selectionview = UIView()
    let titlelabel = UILabel()
    let datelabel = UILabel()
    func setColor(){
        selectionview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
    }
    func unsetColor(){
        selectionview.backgroundColor = .white
    }
   
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionview.backgroundColor = .white
        addSubview(selectionview)
        selectionview.layer.cornerRadius = 15.calcvaluex()
        selectionview.addshadowColor(color: #colorLiteral(red: 0.8698186278, green: 0.8720664382, blue: 0.877323091, alpha: 1))
        selectionview.fillSuperview(padding: .init(top: 2, left: 2, bottom: 2, right: 2))
        //selectionview.addshadowColor(color: #colorLiteral(red: 0.8631278276, green: 0.8653584719, blue: 0.8705746531, alpha: 1))
        
        container.backgroundColor = .white
        container.layer.cornerRadius = 15.calcvaluex()
        if #available(iOS 11.0, *) {
            container.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        selectionview.addSubview(container)
        container.fillSuperview(padding: .init(top: 0, left: 11.calcvaluex(), bottom: 0, right: 0))
        
        titlelabel.font = UIFont(name: "Roboto-Regular", size: 16.calcvaluex())
        titlelabel.text = "編輯時間"
        titlelabel.textColor = #colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1)
        container.addSubview(titlelabel)
        titlelabel.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 13.calcvaluey(), left: 36.calcvaluex(), bottom: 0, right: 0))
        
        datelabel.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        //datelabel.text = "10月十日下午4:47"
        datelabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        container.addSubview(datelabel)
        datelabel.anchor(top: titlelabel.bottomAnchor, leading: titlelabel.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 3.calcvaluey(), left: 0, bottom: 0, right: 0))
        
        
    }
//    override func layoutSubviews() {
//        super.layoutSubviews()
//         layoutIfNeeded()
//        container.roundCorners(corners: [.topRight,.bottomRight], radius: 15.calcvaluex())
//
////        selectionview.addShadow(corners: [.topLeft,.bottomLeft], radius: 15.calcvaluex())
//
//    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class PriceHistoryController: SampleController,UITableViewDelegate,UITableViewDataSource {
    
    var id : String?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HistoryCell(style: .default, reuseIdentifier: "cx")
        
        cell.datelabel.text = Date().convertToDateComponent(text: data[indexPath.section].updated_at ?? "",format: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        cell.selectionStyle = .none
        if indexPath.section == self.current {
            cell.setColor()
        }
        else{
            cell.unsetColor()
        }
        return cell
    }
    let bt = TopViewButton(text: "修改")
    let bs = TopViewButton(text: "送出訂單")
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20.calcvaluey()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72.calcvaluey()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != current {
//            if let curr = self.current {
//        let prevcell = tableView.cellForRow(at: curr) as! HistoryCell
//        prevcell.selectionview.backgroundColor = .white
//            }
//        let cell = tableView.cellForRow(at: indexPath) as! HistoryCell
//
//        cell.selectionview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
//        self.current = indexPath
  

            self.current = indexPath.section
            print(100,data[indexPath.section].call_id ?? "")
            self.tableview.reloadData()
            if indexPath.section == 0{
                fetchWithId(id: data[indexPath.section].call_id ?? "",callfirst:true)
            }
            else{
                fetchWithId(id: data[indexPath.section].call_id ?? "",callfirst:false)
            }
            
        }
    }
     let tableview = UITableView(frame: .zero, style: .plain)
    var current = 0
     let rightview = InterviewController()
    let historybottomview = HistoryTotalView()
    let topstackview = UIStackView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleview.text = "歷史紀錄"
       
        
        view.addSubview(tableview)
        tableview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 21.calcvaluex(), bottom: 0, right: 0),size: .init(width: 250.calcvaluex(), height: 0))
        tableview.delegate = self
        tableview.dataSource = self
        tableview.backgroundColor = .clear
        tableview.separatorStyle = .none
        tableview.showsVerticalScrollIndicator = false
        addChild(rightview)
        rightview.mode = .Review
        view.addSubview(rightview.view)
        rightview.backbutton.isHidden = true
        rightview.titleview.isHidden = true
        rightview.contentView.backgroundColor = #colorLiteral(red: 0.9795874953, green: 0.9797278047, blue: 0.9795567393, alpha: 1)
        rightview.topviewAnchor?.height?.constant = 0
        rightview.stackviewanchor.width?.constant = 0
        rightview.stackview.isHidden = true
//        rightview.backgroundColor = .clear
//        rightview.separatorStyle = .none
//        rightview.contentInset.bottom += 80.calcvaluey()
        rightview.view.anchor(top: topview.bottomAnchor, leading: nil, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: -10.calcvaluey(), left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 709.calcvaluex(), height: 0))
        view.sendSubviewToBack(rightview.view)
        
//        view.addSubview(historybottomview)
//        historybottomview.anchor(top: nil, leading: rightview.leadingAnchor, bottom: view.bottomAnchor, trailing: rightview.trailingAnchor,size: .init(width: 0, height: 80.calcvaluey()))
        bt.layer.cornerRadius = 38.calcvaluey()/2
        topview.addSubview(topstackview)
        topstackview.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 10.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        topstackview.axis = .horizontal
        topstackview.spacing = 10.calcvaluex()
        topstackview.addArrangedSubview(bt)
       
//        topview.addSubview(bt)
//        bt.anchor(top: nil, leading: nil, bottom: topview.bottomAnchor, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 12.calcvaluey(), right: 26.calcvaluex()),size: .init(width: 0, height: 38.calcvaluey()))
        
        bt.addTarget(self, action: #selector(goEdit), for: .touchUpInside)
        bt.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchApi()
    }
    @objc func goEdit(){
        let interviewController = InterviewController()
        interviewController.editMode = true
        interviewController.mode = .Editing
       
        interviewController.id = self.id ?? ""
        interviewController.modalPresentationStyle = .fullScreen
        
        self.present(interviewController, animated: true, completion: nil)
    }
    var data = [InterViewRecord]()
    let hud = JGProgressHUD()
    func fetchApi(){
       
        hud.show(in: self.view)
        self.tableview.isHidden = true
        self.rightview.view.isHidden = true
        self.rightview.topstackview
        NetworkCall.shared.getCall(parameter: "api-or/v1/archive_interviews/\(id ?? "")", decoderType: InterViewRecordList.self) { (json) in
           // DispatchQueue.main.async {
            
                if let json = json {
                    self.data = json.data
                    for (index,i) in self.data.enumerated() {
                        self.data[index].call_id = "\(i.id)"
                    }
                    self.fetchSecondApi()
                }
            //}

        }
//        NetworkCall.shared.getCall(parameter: "api-or/v1/interview/\(id ?? "")", decoderType: InterViewInfos.self) { (json) in
//            DispatchQueue.main.async {
//                hud.dismiss()
//                if let json = json?.data {
//                    self.data = [json]
//                    if let first = self.data.first {
//                        self.setData(json:first)
//                    }
//
//                    self.tableview.reloadData()
//                }
//            }
//
//        }
    }
    var interviewInfo : InterViewInfo? {
        didSet{
            rightview.id = interviewInfo?.id.stringValue ?? ""
        }
    }
    @objc func sendOutData(){
        self.rightview.removeDirectory()
        self.rightview.createDirectory()
        let controller = UIAlertController(title: "送出訂單後不可在編輯，確定是否送出？", message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "確定", style: .default) { (_) in
            
            
            self.rightview.interInfo.status = 3
            self.rightview.saveData()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                self.fetchApi()
//            }
        }
        let action2 = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        
        controller.addAction(action)
        controller.addAction(action2)
        self.present(controller, animated: true, completion: nil)

        
    }
    func addSendButton(){
        if topstackview.arrangedSubviews.count != 2{
            bs.layer.cornerRadius = 38.calcvaluey()/2
        topstackview.addArrangedSubview(bs)
            bs.addTarget(self, action: #selector(sendOutData), for: .touchUpInside)
        }
        bs.isHidden = false

    }
    func removeSendButton(){
        bs.isHidden = true
    }
    func checkIsValidSend(info:InterViewInfo) -> Bool{
        if info.products.count == 0 || info.status == 3{
            return false
        }
        
        var count = 0
        var com_count = 1
        for i in info.products {
            
            if i.audit?.count != 0 && i.audit != nil{
                count = 1
            }
            
            for j in i.audit ?? [] {
                print(3315,j.status)
                if j.status != 5 && j.status != 6{
                    com_count = 0
                }
            }
        }
        
        if count == 0 {
            return true
        }
        
        if com_count == 0{
            return false
        }
        
        return true
    }
    func fetchSecondApi(){
        NetworkCall.shared.getCall(parameter: "api-or/v1/interview/\(id ?? "")", decoderType: InterViewInfos.self) { (json) in
            DispatchQueue.main.async {
                self.hud.dismiss()
                self.tableview.isHidden = false
                
                if let json = json?.data {
//                    self.data = json.data
//                    if json.status == 0 || json.status == 1 || json.status == 2{
//                        self.addSendButton()
//                    }
//                    else{
//                        self.removeSendButton()
//                    }
                    if self.checkIsValidSend(info: json) {
                        self.addSendButton()
                    }
                    else{
                        self.removeSendButton()
                    }
                    if json.status == 3{
                        self.bt.isHidden = true
                    }
                    else{
                        self.bt.isHidden = false
                    }
//                    self.fetchSecondApi()
                    self.interviewInfo = json
                    if let info = self.interviewInfo {
                        self.setData(json: info)
                    }
                    
                    self.rightview.view.isHidden = false
                    self.data.insert(InterViewRecord(id: 0, call_id: json.id.stringValue, source_id: "", status: 0, date: json.date, rating: 0, order_date: "",updated_at: json.updated_at ?? "", account: nil), at: 0)
                    
                    self.tableview.reloadData()
                }
            }

        }
    }
    func fetchWithId(id:String,callfirst:Bool) {
        //hud.show(in: self.view)
        //self.rightview.view.isHidden = true

        rightview.isHistory = !callfirst
        rightview.id = id
//        NetworkCall.shared.getCall(parameter: str, decoderType: InterViewInfos.self) { (json) in
//            DispatchQueue.main.async {
//                self.hud.dismiss()
//
//
//                if let json = json?.data {
//                    self.rightview.view.isHidden = false
//                    self.interviewInfo = json
//
//                }
//            }
//
//        }
    }
    func setData(json: InterViewInfo) {
        
//        rightview.interInfo.customer = json.account
//        rightview.interInfo.location = json.address ?? ""
//        rightview.interInfo.visitDate = json.date ?? ""
//        rightview.interInfo.orderDate = json.order_date ?? ""
        var count = 1
        rightview.addedMachine = self.creatWithProduct(products:json.products, count: &count)
        if count == 0{
            self.removeSendButton()
        }
//        rightview.signitureArray[0].image = UIImage()
//        rightview.signitureArray[1].image = UIImage()
//        self.rightview.calculateTotal()
//        self.rightview.rightview.reloadData()
//        self.rightview.view.isHidden = false
    }
    
    func creatWithProduct(products:[Product],count: inout Int) -> [AddedProduct] {
        var finalData : [AddedProduct] = []
        for i in products {
            var options = [SelectedOption]()
            for i in i.options ?? [] {
                for j in i.children ?? [] {
                    for m in j.children ?? [] {
                        if m.selected == true {
                            options.append(SelectedOption(name: "\(j.title) / \(m.title)", option: m))
                        }
                    }
                }
            }
            //要加入
            var auditArray : [AddedOption] = []
            if let current_audit = i.audit?.first {
                for i in current_audit.content {
                    auditArray.append(AddedOption(mode: AddedMode(rawValue: i.type) ?? .Date, text: i.content ?? "", amount: i.qty ?? 0, price: i.price ?? "", reply: i.reply ?? "", id: i.id ?? ""))
                }
                if current_audit.status != 6 && current_audit.status != 5{
                    count = 0
                }
            }
            
            
            //finalData.append(AddedProduct(series: nil, sub_series: nil, product: i, options: options, customOptions: auditArray))
            
        }
        
        return finalData
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//                tableview.selectRow(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .none)
//                tableView(tableview, didSelectRowAt: IndexPath(item: 0, section: 0))
    }
}
