//
//  FilterDateController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/30.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class FilterDateController: UIViewController {
    let container = UIView()
    let header = UILabel()
    let startdateField = PriceManagementDateForm(text: "起始時間", placeholdertext: "請選擇起始時間")
    let enddateField = PriceManagementDateForm(text: "結束時間", placeholdertext: "請選擇結束時間")
    let registbutton = UIButton()
    weak var con : PriceManagementController?
    override func viewDidLoad() {
        super.viewDidLoad()
        container.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        container.layer.cornerRadius = 15.calcvaluex()
        
        view.addSubview(container)
        container.anchor(top: view.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 183.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 573.calcvaluex(), height: 403.calcvaluey()))
        container.centerXInSuperview()
        
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        header.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        header.text = "時間篩選"
        container.addSubview(header)
        header.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluex(), left: 0, bottom: 0, right: 0))
        header.centerXInSuperview()
        
        let xbutton = UIButton(type: .custom)
        xbutton.imageView?.contentMode = .scaleAspectFill
        xbutton.setImage(#imageLiteral(resourceName: "ic_close2"), for: .normal)
        container.addSubview(xbutton)
        xbutton.anchor(top: container.topAnchor, leading: nil, bottom: nil, trailing: container.trailingAnchor,padding: .init(top: 20.calcvaluey(), left: 0, bottom: 0, right: 20.calcvaluex()),size: .init(width: 36.calcvaluex(), height: 36.calcvaluey()))
        xbutton.addTarget(self, action: #selector(popview), for: .touchUpInside)
        
        container.addSubview(startdateField)
        startdateField.anchor(top: header.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 44.calcvaluex(), bottom: 0, right: 0),size: .init(width: 486.calcvaluex(), height: 90.calcvaluey()))
        
        container.addSubview(enddateField)
        enddateField.anchor(top: startdateField.bottomAnchor, leading: container.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 44.calcvaluex(), bottom: 0, right: 0),size: .init(width: 486.calcvaluex(), height: 90.calcvaluey()))
        
        registbutton.setTitle("確認", for: .normal)
        registbutton.setTitleColor(.white, for: .normal)
        registbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        registbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        registbutton.addshadowColor(color: #colorLiteral(red: 0.8979385495, green: 0.8980958462, blue: 0.8979402781, alpha: 1))
        container.addSubview(registbutton)
        registbutton.anchor(top: nil, leading: nil, bottom: container.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 0, bottom: 26.calcvaluex(), right: 0),size: .init(width: 124.calcvaluex(), height: 48.calcvaluey()))
        registbutton.centerXInSuperview()
        
        registbutton.layer.cornerRadius = 48.calcvaluey()/2
        registbutton.addTarget(self, action: #selector(confirmTime), for: .touchUpInside)
        
        startdateField.tag = 0
        enddateField.tag = 1
        startdateField.delegate2 = self
        enddateField.delegate2 = self

    }
    @objc func confirmTime(){
        if beginDate == nil{
          showAlert(text: "起始時間")
        }
        
        if endDate == nil{
            showAlert(text: "結束時間")
        }
        con?.startDate = self.beginDate
        con?.endDate = self.endDate
        con?.createDateFilter()
        popview()
    }
    
    func showAlert(text:String) {
        let alertcon = UIAlertController(title: "請選擇\(text)", message: nil, preferredStyle: .alert)
        alertcon.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
        self.present(alertcon, animated: true, completion: nil)
    }
    var beginDate : Date?
    var endDate : Date?
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
}


extension FilterDateController: InterViewDateDelegate {
    func sendBeginDate(date: Date) {
        beginDate = date
        
        enddateField.datepicker.minimumDate = date
    }
    
    func sendEndDate(date: Date) {
        endDate = date
        startdateField.datepicker.maximumDate = date
    }
    

}
