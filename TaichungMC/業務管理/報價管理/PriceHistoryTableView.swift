//
//  PriceHistoryTableView.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/31.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class PriceHistoryTableView: UITableView,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
        func numberOfSections(in tableView: UITableView) -> Int {
            return 5
        }
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let rt = RightTopViewHeader()
            if section == 0{

                return RightViewTopText()
            

            }
            else if section == 1{
                
                    rt.headerlabel.text = "報價機台"
                
            }
            else if section == 2{
                rt.headerlabel.text = "選配"
                
            }
            else if section == 3{

                rt.headerlabel.text = "調整價格"
                
            }
            else if section == 4{
                rt.headerlabel.text = "確認簽名"
            }

            return rt
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if indexPath.section == 0 {

                let cell = CreatePriceFirstSectionCell(style: .default, reuseIdentifier: "cc")

                return cell
                

            }
            else if indexPath.section == 1 {
                            
                let cell = CreatePriceSecondSectionCell(style: .default, reuseIdentifier: "ct")
                
                return cell
                    

            }
            else if indexPath.section == 4{
                let cell = SixthSectionCell(style: .default, reuseIdentifier: "yu")
                return cell
            }
            else {
                
                let cell = EquipmentPriceCell(style: .default, reuseIdentifier: "xt")
                    
                
                return cell
                

            }
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

            
            if section == 0{
                return 34.calcvaluey()
            }
            return 52.calcvaluey()
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            switch indexPath.section {
            case 0:
                return 632.calcvaluey()
            case 1:

                return 164.calcvaluey()
            case 2:

                return 98.calcvaluey()
            case 3:

                return 98.calcvaluey()
            case 4:
                return 199.calcvaluey()
            default:
                return 0
            }
        }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
