//
//  SampleController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class SampleController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    let topview = UIView()
    let backbutton = UIImageView(image: #imageLiteral(resourceName: "ic_back"))
    let titleview = UILabel()
    var originCon:ViewController?
    var titleviewanchor:AnchoredConstraints!
    var topviewAnchor : AnchoredConstraints?
        let settingButton = UIButton(type: .custom)
    override func viewDidLoad() {
        super.viewDidLoad()
        
         topview.isUserInteractionEnabled = true
         topview.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        view.addSubview(topview)
        topviewAnchor = topview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: 86.calcvaluey()))
        let slideinview = UIView()
        view.addSubview(slideinview)
        slideinview.backgroundColor = #colorLiteral(red: 0.8205059171, green: 0.4463779926, blue: 0.01516667381, alpha: 1)
        
        slideinview.anchor(top: view.topAnchor, leading: topview.leadingAnchor, bottom: nil, trailing: topview.trailingAnchor,size: .init(width: 0, height: UIApplication.shared.statusBarFrame.size.height))
        view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
        
        
        topview.addSubview(backbutton)
        backbutton.anchor(top: nil, leading: topview.leadingAnchor, bottom: topview.bottomAnchor, trailing: nil,padding: .init(top: 0, left: 26.calcvaluex(), bottom: 17.calcvaluey(), right: 0),size: .init(width: 28.calcvaluex(), height: 28.calcvaluey()))
        backbutton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(popview)))
        backbutton.isUserInteractionEnabled = true
        
        topview.addSubview(titleview)
        titleview.text = "常見問題"
        titleview.textColor = .white
        titleview.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        titleviewanchor = titleview.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 80.calcvaluex(), bottom: 17.calcvaluey(), right: 0))
        titleview.centerYAnchor.constraint(equalTo: backbutton.centerYAnchor).isActive = true
        //titleview
        
        settingButton.setImage(#imageLiteral(resourceName: "ic_set"), for: .normal)
        settingButton.contentHorizontalAlignment = .fill
        settingButton.contentVerticalAlignment = .fill
        settingButton.isHidden = true
        topview.addSubview(settingButton)
        
        settingButton.anchor(top: nil, leading: nil, bottom: nil, trailing: topview.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()),size: .init(width: 28.calcvaluex(), height: 28.calcvaluex()))
        settingButton.centerYAnchor.constraint(equalTo: titleview.centerYAnchor).isActive = true
        
        settingButton.addTarget(self, action: #selector(goSetting), for: .touchUpInside)
    }
    @objc func goSetting(){
        let con = SettingController()
        con.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        con.modalPresentationStyle = .overFullScreen
        
        self.present(con, animated: false, completion: nil)
    }
    @objc func popview(){
        self.dismiss(animated: true, completion: nil)
    }
}
