//
//  RegisterController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class RegisterController: SampleController {
    
    
    let namearray = ["帳號","密碼","確認密碼","姓名"]
    let placeholder = ["example@mail.com","0-8位數密碼, 請區分大小寫","請再次輸入密碼","請輸入您的姓名"]
    
    let sendbutton = UIButton(type: .system)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        let stackview = UIStackView()
        stackview.axis = .vertical
        stackview.alignment = .center
        stackview.spacing = 24.yscalevalue()
        for (index,value) in namearray.enumerated() {
            let ed = AccountField()
            ed.header.text = value
            ed.textfield.attributedPlaceholder = NSAttributedString(string: placeholder[index], attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
            
            ed.constrainWidth(constant: 560.calcvaluex())
            ed.constrainHeight(constant: 97.calcvaluey())
            ed.tag = index
            stackview.addArrangedSubview(ed)
        
        }
        
        sendbutton.setTitle("送出", for: .normal)
        sendbutton.setTitleColor(.white, for: .normal)
        if #available(iOS 11.0, *) {
            stackview.setCustomSpacing(66.calcvaluey(), after: stackview.arrangedSubviews[3])
        } else {
            // Fallback on earlier versions
        }
        sendbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        sendbutton.constrainHeight(constant: 48.calcvaluey())
        sendbutton.constrainWidth(constant: 124.calcvaluex())
        sendbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        sendbutton.layer.cornerRadius = 48.calcvaluey()/2
        stackview.addArrangedSubview(sendbutton)
        stackview.addArrangedSubview(UIView())
        
        view.addSubview(stackview)
        stackview.anchor(top: topview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 46.yscalevalue(), left: 0, bottom: 0, right: 0))
        
        
    }
}
