//
//  LoginController.swift
//  TaichungMC
//
//  Created by Wilson on 2020/2/24.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
import JGProgressHUD
import FirebaseMessaging
extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
}
class LoginController: UIViewController,UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == accountfield.textfield {
            accountDict["username"] = textField.text ?? ""
        }
        if textField == passwordfield.textfield{
            accountDict["password"] = textField.text ?? ""
        }
        print(accountDict)
        stackviewAnchor?.top?.constant = 60.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        stackviewAnchor?.top?.constant = -180.calcvaluey()
        UIView.animate(withDuration: 0.4) {
            self.view.layoutIfNeeded()
        }
    }
    let statusview = UIView()
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    let headerlabel = UILabel()
    let imageview = UIImageView()
    
    let accountfield = AccountField()
    let passwordfield = AccountField()
    
    let loginbutton = UIButton(type: .system)
    let language = LanguageSelection()
    let registerlabel = UILabel()
    var accountDict = ["username":"","password":"","firebaseToken":""]
    let forgotpassword = UIButton(type: .custom)
    var stackviewAnchor:AnchoredConstraints?

    func setText(){
        
            headerlabel.text = "會員登入".localized
            accountfield.header.text = "帳號".localized
            forgotpassword.setTitle("忘記密碼?".localized, for: .normal)
        
            passwordfield.header.text = "密碼".localized
            passwordfield.textfield.attributedPlaceholder = NSAttributedString(string: "0-8位數密碼, 請區分大小寫".localized, attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
            loginbutton.setTitle("登入".localized, for: .normal)
            
//            let attr = NSMutableAttributedString(string: "還沒有帳號?".localized,attributes: [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
//            attr.append(NSAttributedString(string: "  註冊  ".localized, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.9368859529, green: 0.5106889009, blue: 0, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!]))
//            
//            registerlabel.attributedText = attr
       

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Bundle.main.bundleIdentifier)
        if UserDefaults.standard.getToken() != "" {
            let vd = UINavigationController(rootViewController: ViewController())
            vd.modalPresentationStyle = .fullScreen
            vd.view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
            self.present(vd, animated: false, completion: nil)
        }
        setText()
        view.backgroundColor = #colorLiteral(red: 0.9998885989, green: 1, blue: 0.9998806119, alpha: 1)
        navigationController?.navigationBar.isHidden = true
        view.addSubview(statusview)
        statusview.backgroundColor = #colorLiteral(red: 0.8205059171, green: 0.4463779926, blue: 0.01516667381, alpha: 1)
        statusview.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor,size: .init(width: 0, height: UIApplication.shared.statusBarFrame.height))
        
        
        headerlabel.font = UIFont(name: "Roboto-Medium", size: 30.calcvaluex())
        headerlabel.textAlignment = .center
//        headerlabel.constrainWidth(constant: UIScreen.main.bounds.width)
//        headerlabel.constrainHeight(constant: (headerlabel.text?.height(withConstrainedWidth: .infinity, font: headerlabel.font))!)
        
        //imageview.backgroundColor = .red
        imageview.contentMode = .scaleAspectFit
        imageview.image = #imageLiteral(resourceName: "ic_sign_in_logo")
        imageview.constrainWidth(constant: 106.calcvaluex())
        imageview.constrainHeight(constant: 74.calcvaluey())
        
        
        accountfield.textfield.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        accountfield.textfield.autocorrectionType = .no
        accountfield.textfield.autocapitalizationType = .none
        accountfield.textfield.attributedPlaceholder = NSAttributedString(string: "example@gmail.com", attributes: [NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1),NSAttributedString.Key.font:UIFont(name: "Roboto-Regular", size: 20.calcvaluex())!])
        accountfield.constrainHeight(constant: 97.calcvaluey())
        accountfield.constrainWidth(constant: 560.calcvaluex())
        accountfield.textfield.delegate = self
        forgotpassword.setTitleColor(#colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1), for: .normal)
        
        forgotpassword.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        
        
        passwordfield.textfield.addSubview(forgotpassword)
        
        passwordfield.textfield.isSecureTextEntry = true
        passwordfield.textfield.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        passwordfield.textfield.delegate = self
        forgotpassword.anchor(top: nil, leading: nil, bottom: nil, trailing: passwordfield.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 20.calcvaluex()))
        forgotpassword.centerYInSuperview()
        forgotpassword.addTarget(self, action: #selector(gorecover), for: .touchUpInside)
        passwordfield.constrainHeight(constant: 97.calcvaluex())
        passwordfield.constrainWidth(constant: 560.calcvaluex())
        
        
        loginbutton.backgroundColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        
        loginbutton.setTitleColor(.white, for: .normal)
        loginbutton.constrainWidth(constant: 124.calcvaluex())
        loginbutton.constrainHeight(constant: 48.calcvaluey())
        loginbutton.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        loginbutton.layer.cornerRadius = 48.calcvaluey()/2
        loginbutton.addTarget(self, action: #selector(gologin), for: .touchUpInside)
        //language.backgroundColor = .green
        language.constrainHeight(constant: 28.calcvaluey())
        language.constrainWidth(constant: UIScreen.main.bounds.width)
        
        language.english.addTarget(self, action: #selector(switchLang), for: .touchUpInside)
        language.mandarin.addTarget(self, action: #selector(switchLang), for: .touchUpInside)
        registerlabel.textAlignment = .center
        

        registerlabel.isUserInteractionEnabled = true
        registerlabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goregister)))
        
        let stackview = UIStackView(arrangedSubviews: [headerlabel,imageview,accountfield,passwordfield,loginbutton,language,registerlabel,UIView()])
        stackview.axis = .vertical
        stackview.spacing = 34.yscalevalue()
        if #available(iOS 11.0, *) {
            stackview.setCustomSpacing(34.calcvaluey(), after: stackview.arrangedSubviews[0])
            stackview.setCustomSpacing(40.calcvaluey(), after: stackview.arrangedSubviews[1])
            stackview.setCustomSpacing(24.calcvaluey(), after: stackview.arrangedSubviews[2])
            stackview.setCustomSpacing(46.calcvaluey(), after: stackview.arrangedSubviews[3])
            stackview.setCustomSpacing(18.calcvaluey(), after: stackview.arrangedSubviews[4])
            stackview.setCustomSpacing(62.calcvaluey(), after: stackview.arrangedSubviews[5])
        } else {
            // Fallback on earlier versions
        }
        
        stackview.distribution = .fill
        stackview.alignment = .center
        view.addSubview(stackview)
        stackviewAnchor = stackview.anchor(top: statusview.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,padding: .init(top: 60.calcvaluey(), left: 0, bottom: 0, right: 0))
        view.bringSubviewToFront(statusview)
        let tap = UITapGestureRecognizer(target: self, action: #selector(endEdit))
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)
        registerLanguageChange()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Messaging.messaging().token { (str, err) in
            self.accountDict["firebaseToken"] = str

        }
    }
    @objc func switchLang(sender:UIButton){
        if sender.tag == 0{
            UserDefaults.standard.saveAppLanguage(lang: "zh-Hant")
        }
        else{
        UserDefaults.standard.saveAppLanguage(lang: "en")
        }
        
        self.postLangeChange()
    }
    override func changeLanguage() {
        setText()
    }
    @objc func endEdit(){
        view.endEditing(true)
    }

    @objc func gologin(){
        view.endEditing(true)
        if accountDict["username"] == "" || accountDict["password"] == "" {
            let alert = UIAlertController(title: "請確認帳號密碼是否輸入正確", message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: "請確認有連接到網路", message: nil, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        let hud = JGProgressHUD()
        hud.textLabel.text = "登入中...".localized
        hud.show(in: self.view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            
            NetworkCall.shared.postCall(parameter: "api-victor/v1/auth/login", dict: self.accountDict, decoderType: Account.self) { (json) in
                DispatchQueue.main.async {
                    
                if let json = json{
                    UserDefaults.standard.saveUserId(id: json.user.id)
                    UserDefaults.standard.saveToken(token: json.access_token)
                    hud.textLabel.text = "成功登入".localized
                    hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                    
                    hud.dismiss(afterDelay: 0.3, animated: true) {
                                let vd = UINavigationController(rootViewController: ViewController())
                                vd.modalPresentationStyle = .fullScreen
                                vd.view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
                                self.present(vd, animated: true, completion: nil)
                    }
                }
                else{
                    hud.dismiss()
                    let alert = UIAlertController(title: "請確認帳號密碼是否輸入正確", message: nil, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "確定", style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
                    
                }
            }
        }


    }
//    @objc func gologin(){
//        let vd = UINavigationController(rootViewController: ViewController())
//        vd.modalPresentationStyle = .fullScreen
//        vd.view.backgroundColor = #colorLiteral(red: 0.9802826047, green: 0.9804535508, blue: 0.980284512, alpha: 1)
//        self.present(vd, animated: true, completion: nil)
//    }
    @objc func gorecover(){
        let vd = ForgotPasswordController()
        vd.modalPresentationStyle = .fullScreen
        self.present(vd, animated: true, completion: nil)
    }
    @objc func goregister(gesture:UITapGestureRecognizer){
        let text = "還沒有帳號?  註冊  ".localized
        let termsRange = (text as NSString).range(of: "  註冊  ".localized)
        // comment for now
        //let privacyRange = (text as NSString).range(of: "Privacy Policy")

        if gesture.didTapAttributedTextInLabel(label: registerlabel, inRange: termsRange) {
            
            let vd = RegisterController()
            vd.modalPresentationStyle = .fullScreen
            self.present(vd, animated: true, completion: nil)
//
//            //self.dismiss(animated: true, completion: nil)
//           // self.view.endEditing(true)
        }else {
            print("Tapped none")
        }
    }
}
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)

        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)

        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize

        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        //let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              //(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

        //let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                        // locationOfTouchInLabel.y - textContainerOffset.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }

}
class AccountField: UIView {
    let header = UILabel()
    let textfield = EditTextField()
    override init(frame: CGRect) {
        super.init(frame: frame)
        textfield.padding = .init(top: 0, left: 30.calcvaluex(), bottom: 0, right: 0)
        addSubview(header)
        header.font = UIFont(name: "Roboto-Regular", size: 18.calcvaluex())
        header.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        
        textfield.backgroundColor = #colorLiteral(red: 0.9685191512, green: 0.9686883092, blue: 0.9685210586, alpha: 1)
        textfield.layer.cornerRadius = 15.calcvaluex()
        textfield.layer.borderColor = #colorLiteral(red: 0.8940173984, green: 0.8941739202, blue: 0.8940190673, alpha: 1)
        textfield.layer.borderWidth = 2.calcvaluex()
        
        addSubview(textfield)
        textfield.anchor(top: header.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,padding: .init(top: 10.calcvaluey(), left: 0, bottom: 0, right: 0),size: .init(width: 0, height: 62.calcvaluey()))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class LanguageSelection:UIView {
    let mandarin = UIButton(type: .system)
    
    let english = UIButton(type: .system)
    override init(frame: CGRect) {
        super.init(frame: frame)
        mandarin.tag = 0
        english.tag = 1
        mandarin.setTitle("中文", for: .normal)
        mandarin.setTitleColor(#colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1), for: .normal)
        mandarin.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        english.setTitleColor(#colorLiteral(red: 0.568561554, green: 0.5686645508, blue: 0.568562746, alpha: 1), for: .normal)
        english.setTitle("English", for: .normal)
        english.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        let seperator = UIView()
        seperator.backgroundColor = #colorLiteral(red: 0.8940173984, green: 0.8941739202, blue: 0.8940190673, alpha: 1)
        seperator.constrainWidth(constant: 0.7)
        
        let stackview = UIStackView(arrangedSubviews: [mandarin,seperator,english])
        stackview.axis = .horizontal
        stackview.spacing = 10.calcvaluex()
        
        addSubview(stackview)
        stackview.centerInSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
