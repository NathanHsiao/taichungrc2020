//
//  ForgotCl.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 2/21/21.
//  Copyright © 2021 TaichungMC. All rights reserved.
//

import Foundation


struct ForgotCl : Codable{
    var data : Bool
}
