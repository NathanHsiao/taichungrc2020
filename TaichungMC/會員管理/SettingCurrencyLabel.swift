//
//  SettingCurrencyLabel.swift
//  TaichungMC
//
//  Created by Wilson on 2020/7/30.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
class CurrencyTextField:UITextField {
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 24.calcvaluex(), dy: 0)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //text = "台幣 ( NT$)"
        textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        backgroundColor = #colorLiteral(red: 0.9449923038, green: 0.9451575875, blue: 0.9449942708, alpha: 1)
        font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        
        layer.cornerRadius = 18.calcvaluey()
        
        let imgv = UIImageView(image: #imageLiteral(resourceName: "ic_arrow_drop_down"))
        imgv.contentMode = .scaleAspectFit
        
        addSubview(imgv)
        imgv.anchor(top: nil, leading: nil, bottom: nil, trailing: trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 14.calcvaluex()),size: .init(width: 24.calcvaluey(), height: 24.calcvaluey()))
        imgv.centerYInSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol CurrencyFieldsDelegate {
    func showCurrencies(textField:UITextField)
}
class SettingCurrencyLabel: UITableViewCell,UITextFieldDelegate {
    let topLabel = UILabel()
    let pushswitch = UISwitch()
    let currency = CurrencyTextField()
    var delegate : CurrencyFieldsDelegate?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        topLabel.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        topLabel.textColor = #colorLiteral(red: 0.1319426596, green: 0.08858136088, blue: 0.08063519746, alpha: 1)
        topLabel.text = "產品價格顯示"
        
        contentView.addSubview(topLabel)
        topLabel.anchor(top: contentView.topAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 38.calcvaluex(), bottom: 0, right: 0),size: .init(width: 0, height: 28.calcvaluey()))
        
        contentView.addSubview(pushswitch)
        pushswitch.anchor(top: topLabel.bottomAnchor, leading: nil, bottom: nil, trailing: contentView.trailingAnchor,padding: .init(top: 0, left: 0, bottom: 0, right: 26.calcvaluex()))
        //pushswitch.centerYInSuperview()
        pushswitch.onTintColor = #colorLiteral(red: 0.9116738439, green: 0.499684751, blue: 0.002810306381, alpha: 1)
        pushswitch.backgroundColor = #colorLiteral(red: 0.4666113853, green: 0.4666975737, blue: 0.4666124582, alpha: 1)
        layoutIfNeeded()
        pushswitch.layer.cornerRadius = pushswitch.frame.height/2
        //pushswitch.isHidden = true
        
        contentView.addSubview(currency)
        currency.anchor(top: topLabel.bottomAnchor, leading: contentView.leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 6.calcvaluey(), left: 58.calcvaluex(), bottom: 0, right: 0),size: .init(width: 174.calcvaluex(), height: 36.calcvaluey()))
        currency.delegate = self
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        delegate?.showCurrencies(textField:textField)
        return false
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
