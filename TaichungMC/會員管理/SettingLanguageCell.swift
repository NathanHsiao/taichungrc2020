//
//  SettingLanguageCell.swift
//  TaichungMC
//
//  Created by Wilson on 2020/3/4.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit

class SettingLanguageCell: UITableViewCell {
    let header = UILabel()
    let chineseButton = UILabel()
    let englishButton = UILabel()
    let seperator = UILabel()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        header.text = "語系切換"
        header.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        addSubview(header)
        header.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 26.calcvaluey(), left: 38.calcvaluex(), bottom: 0, right: 0))
        
        addSubview(chineseButton)
        chineseButton.text = "繁體中文"
        chineseButton.textColor = #colorLiteral(red: 0.9408788085, green: 0.514672935, blue: 0.006906570401, alpha: 1)
        chineseButton.font = UIFont(name: "Roboto-Medium", size: 20.calcvaluex())
        chineseButton.anchor(top: header.bottomAnchor, leading: leadingAnchor, bottom: nil, trailing: nil,padding: .init(top: 16.calcvaluey(), left: 58.calcvaluex(), bottom: 0, right: 0))
        
        addSubview(seperator)
        seperator.text = "/"
        seperator.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        seperator.anchor(top: nil, leading: chineseButton.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 14.calcvaluex(), bottom: 0, right: 0))
        seperator.centerYAnchor.constraint(equalTo: chineseButton.centerYAnchor).isActive = true
        
        addSubview(englishButton)
        englishButton.text = "English"
        englishButton.font = UIFont(name: "Roboto-Regular", size: 20.calcvaluex())
        englishButton.anchor(top: nil, leading: seperator.trailingAnchor, bottom: nil, trailing: nil,padding: .init(top: 0, left: 14.calcvaluex(), bottom: 0, right:0))
        englishButton.centerYAnchor.constraint(equalTo: chineseButton.centerYAnchor).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
