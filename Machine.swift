//
//  Machine.swift
//  TaichungMC
//
//  Created by Huang Ming Hsiao on 10/8/20.
//  Copyright © 2020 TaichungMC. All rights reserved.
//

import UIKit
struct titleArray {
    func getTitle() -> [NewSpecField] {
        return [NewSpecField(title: "射座形式", decodeId: "injection-model"),NewSpecField(title: "螺桿直徑", decodeId: "screw-diameter"),NewSpecField(title: "螺桿細長比(L/D)", decodeId: "screw-l-d-ratio"),NewSpecField(title: "理論射出容積", decodeId: "calculated-injection-capacity"),NewSpecField(title: "實際射出重量(PS)", decodeId: "actual-shot-weight-ps2"),NewSpecField(title: "理論塑化能力(PS)", decodeId: "plasticizing-capacity-ps")]
    }
}
struct Machine: Codable {
    var id: Int
    var name : Lang
    var desc : Lang
    var subdesc : [Lang]
    var erp_company_code : String
    var erp_model_code : String
    var erp_machine_code:[MachineCode]
    var machine_img:String
    var view360:[String]
    var partsxy : [xyLocation]
    var slider : SlidingPic
    var specification : [Spec]
    var content : [Con]
    var standard_equipment : Equipment
    var optional : [OpEquip]
    private enum CodingKeys: String,CodingKey {
        case id,name,desc,subdesc = "sub-desc", erp_company_code, erp_model_code,erp_machine_code, machine_img, view360, partsxy = "parts 點位座標", slider, specification = "specification 機械規格", content = "content 資訊類資料", standard_equipment = "standard_equipment 標配", optional = "optional 選配"
    }
    
}
struct Equipment : Codable {
    var lable : [Con]
    
}
struct OpEquip: Codable {
    var name : Lang
    var items: [Item]
}
struct Item : Codable {
    var name : Lang
    var file_url: Lang
    var erp_option_code : [MachineCode]
}
struct Con : Codable {
    var type : Lang
    var name : Lang
    var file_url : Lang
}
struct Spec : Codable {
    var lable: [Lab]
    var unit : [String]
    var Y16: [String]
    var Y19: [String]
    var X22: [String]
    var X25: [String]
    var K28: [String]
    var K32: [String]
    var K36: [String]
    private enum CodingKeys: String,CodingKey {
        case lable,unit,Y16 = "Y-16", Y19 = "Y-19", X22 = "X-22",X25 = "X-25", K28 = "K-28",K32 = "K-32",K36 = "K-36"
    }
}
struct Lab : Codable {
    var type : Lang
    var name : Lang
}
struct SlidingPic : Codable{
    var en : [String]
    var zhTW : [String]
    private enum CodingKeys: String,CodingKey {
        case en, zhTW = "zh-TW"
    }
}
struct xyLocation : Codable {
    var file_url : Lang
    var x : String
    var y : String
}
struct Lang:Codable {
    var en:String?
    var zhTW:String?
    
    private enum CodingKeys: String,CodingKey {
        case en, zhTW = "zh-TW"
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(en, forKey: .en)
        try container.encode(zhTW, forKey: .zhTW)
    }
}
struct MachineCode:Codable {
    var id:Int
    var name:String
    var code:String
    var price : Price
}
struct Price:Codable {
    var USD : String
    var TWD : String
}
